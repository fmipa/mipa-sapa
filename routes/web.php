<?php

use App\Http\Controllers\AddFormController;
use App\Http\Controllers\UserController;
use App\Models\File;
use App\Models\KodeNaskah;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('pages.landing');
});

Route::namespace('Auth')->controller(AuthController::class)->group(function() {
    Route::group(['middleware' => ['guest']], function() {
        Route::get('/login', 'login')->name('login');
        // Route::get('/login', function () {
        //     return redirect('/');
        // })->name('login');
        Route::get('/register', 'register')->name('register');
        Route::post('/store', 'store')->name('store');
        Route::post('/authenticate', 'doLogin')->name('authenticate');
    });
    Route::group(['middleware' => ['auth']], function() {
        // Route::get('/beranda', 'beranda')->name('beranda');
        Route::get('/profile', 'profile')->name('profile');
        Route::post('/logout', 'logout')->name('logout');
    });
});

Route::middleware(['auth'])->group(function () {
    Route::get('/beranda', 'BerandaController@cekLogin')->name('beranda');
    Route::resource('/berkas-pegawai', BerkasPegawaiController::class);
    Route::resource('/box', BoxController::class);
    Route::resource('/dokumen', DocumentController::class);
    Route::resource('/ekstensi', ExtensionController::class);
    Route::resource('/file', FileController::class);
    Route::resource('/folder', FolderController::class);
    Route::resource('/folder-storage', FolderStorageController::class);
    Route::resource('/kategori-berkas', KategoriBerkasController::class);
    Route::resource('/kode-naskah', KodeNaskahController::class);
    Route::resource('/jurusan', JurusanController::class);
    Route::resource('/kode-unit', KodeUnitController::class);
    Route::resource('/master-agama', MasterAgamaController::class);
    Route::resource('/pegawai', PegawaiController::class);
    Route::resource('/prodi', ProdiController::class);
    Route::resource('/sk', SKController::class);
    Route::resource('/surat-keluar', SuratKeluarController::class);
    Route::resource('/surat-masuk', SuratMasukController::class);
    // ! SUPERADMIN
    Route::resource('/pengguna', UserController::class)->withTrashed();
    Route::put('/update-profile', [UserController::class, 'updateProfile'])->name('update-profile');

    Route::resource('/izin', PermissionController::class);
    Route::resource('/hak-akses', RoleController::class);

    // ! Route Tambahan
    Route::controller(AddFormController::class)->group(function() {
        Route::get('/add-form/select/{model}/{value}/{multiple}', 'select')->name('add-form.select');
    });
    // ! Akun Untuk E-Rapat
    Route::get('/e-rapat', 'PegawaiController@eRapat')->name('export.erapat');
    Route::get('/file-folder', 'FileController@folderView')->name('file.folderView');


    Route::get('/box/label/{id}/{tipeLabel?}', 'BoxController@printLabel')->name('box.label');
    Route::get('/cari-file/{kataKunci?}/{idUnitKerja?}/{idFolder?}', 'FileController@cari')->name('file.cari');
    Route::get('/cari-file-pegawai/{kataKunci?}/{idFolder?}/{idPegawai?}', 'FileController@cariByPegawai')->name('file.cariByPegawai');
    // ! Folder
    Route::get('/file/detail/{idSecret}/{tipeDokumen}', 'FileController@detail')->name('file.detail');
    Route::get('/kembali/{idSecret}/{idUnitKerja?}', 'FileController@kembali')->name('file.kembali');
    Route::get('/unit-folder', 'FileController@unitfolder')->name('file.unitfolder');
    Route::get('/folders/{idSecret}/{idUnitKerja?}', 'FileController@folder')->name('file.folder');
    Route::get('/folder-pegawais/{parentFolderPegawai}/{idPegawai}/{idUnitKerja?}', 'FileController@folderPegawai')->name('file.folderPegawai');
    Route::get('/data-pegawai/{idSecret}/{idPegawai}', 'FileController@dataPegawai')->name('file.dataPegawai');
});

Route::controller(PencarianController::class)->group(function() {
    Route::get('/cari-berkas/{boxId}', 'byBox')->name('cari-berkas.qrcode');
    Route::get('/lihat-file-direct/{id}/{tipeFile}', 'viewFile')->name('lihat-file');
    Route::get('/download-file-direct/{id}/{tipeFile}', 'downloadFile')->name('unduh-file');
    Route::get('/lihat-file/{unique_id}/{id}/{tipeFile}/{password}', 'viewFileBox')->name('lihat-file-box');
    Route::get('/download-file/{unique_id}/{id}/{tipeFile}/{password}', 'downloadFileBox')->name('unduh-file-box');

});

Route::get('test-function', function () {
    
});

Route::get('/cari', function () {
    return view('pages.cari');
});

