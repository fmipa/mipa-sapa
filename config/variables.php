<?php
  // Variables
  return [
    #Aplikasi
    "appName" => "Sistem Kearsipan SAPA",
    "creatorName" => "TIM TIK FMIPA UNTAN",
    "CP" => "6285386150568",
    "creatorInstitute" => "FMIPA UNTAN",
    "creatorUrl" => "https://mipa.untan.ac.id",
    "quotes" => "--- Quality is Our Concern ---",
    "appDescription" => "Sistem Kearsipan Satu Data FMIPA (SAPA) Fakultas Matematika dan Ilmu Pengetahuan Alam Universitas Tanjungpura",
    "appKeyword" => "arsip, arsip digital, sistem kearsipan, satu data MIPA, SAPA, FMIPA Untan",
    #Logo
    "logoSAPA" => env('APP_LOGO_SAPA', ''),
    "logoUntan" => env('APP_LOGO_UNTAN', ''),
    #EnkripsiUniqueId
    "secretKey"=>"mipa",
    "secretUnique"=>"mipauntan",
    #Template
    "templateUtama"=>"sneat" ?? env('APP_TEMPLATE'),
    //scoxe,sneat
    "templateName" => "sneat" ?? env('APP_TEMPLATE'), 
    //container-fluid,container-xxl,container
    "templateContainer" => 'container-fluid', 
    //vertikal,horizontal,float
    "templateForm" => 'vertikal', 
    //vertical,horizontal
    "templateLayout" => 'vertical', 
    "licenseUrl" => "https://themeselection.com/license/",
    "livePreview" => "https://demos.themeselection.com/sneat-bootstrap-html-laravel-admin-template-free/demo/",
    "productPage" => "https://themeselection.com/item/sneat-bootstrap-html-laravel-admin-template/",
    "support" => "https://github.com/themeselection/sneat-html-laravel-admin-template-free/issues",
    "moreThemes" => "https://themeselection.com/",
    "documentation" => "https://themeselection.com/demo/sneat-bootstrap-html-admin-template/documentation/laravel-introduction.html",
    "repository" => "https://github.com/themeselection/sneat-html-laravel-admin-template-free",
    "facebookUrl" => "https://www.facebook.com/ThemeSelections/",
    "twitterUrl" => "https://twitter.com/Theme_Selection",
    "githubUrl" => "https://github.com/themeselection",
    "dribbbleUrl" => "https://dribbble.com/themeselection",
    "instagramUrl" => "https://www.instagram.com/themeselection/",
  ];
