<?php

namespace App\Observers;

use App\Models\Folder;
use App\Traits\Accessors\DecryptId;

class FolderObserver
{
    use DecryptId;
    /**
     * Function for decrypt unix Id to ID.
     */
    public function decryptId($decrypt): array
    {
        foreach ($decrypt as $key => $value) {
            $data[] = $this->decrypt((string)$value);
        }
        return $data;
    }

    /**
     * Handle the Folder "created" event.
     */
    public function created(Folder $folder): void
    {
        switch (request()->akses_fil) {
            case 'tim':
                $pegawais = $this->decryptId(request()->role_id);
                break;
            
            case 'tag':
                $pegawais = $this->decryptId(request()->pegawai_id);
                break;
            
            default:
                $pegawais = auth()->id();
                break;

        }
        $folder->sync($pegawais);
    }

    /**
     * Handle the Folder "updated" event.
     */
    public function updated(Folder $folder): void
    {
        //
    }

    /**
     * Handle the Folder "deleted" event.
     */
    public function deleted(Folder $folder): void
    {
        //
    }

    /**
     * Handle the Folder "restored" event.
     */
    public function restored(Folder $folder): void
    {
        //
    }

    /**
     * Handle the Folder "force deleted" event.
     */
    public function forceDeleted(Folder $folder): void
    {
        //
    }
}
