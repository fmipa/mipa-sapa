<?php

namespace App\Observers;

use App\Models\User;
use App\Traits\Accessors\DecryptId;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserObserver
{
    use DecryptId;
    
    /**
     * Handle the User "created" event.
     */
    public function created(User $user): void
    {
        // 
    }

    /**
     * Function for decrypt unix Id to ID.
     */
    public function roles($roles): array
    {
        foreach ($roles as $key => $value) {
            $role_id[] = $this->decrypt((string)$value);
        }
        return $role_id;
    }

    /**
     * Handle the User "updated" event.
     */
    public function updated(User $user): void
    {
        // 
    }

    /**
     * Handle the User "deleted" event.
     */
    public function deleted(User $user): void
    {
        // 
    }

    /**
     * Handle the User "restored" event.
     */
    public function restored(User $user): void
    {
        //
    }

    /**
     * Handle the User "force deleted" event.
     */
    public function forceDeleted(User $user): void
    {
        //
    }
}
