<?php

namespace App\Models\Scopes;

use App\Enums\RoleEnum;
use App\Traits\JurusanFolder;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Spatie\Permission\Models\Role;

class RoleFolderJurusanScope implements Scope
{
    use JurusanFolder;
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (auth()->user()) {
            $builder->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::ADJUR->value
                    );
            }) == TRUE), function($query) {
                $pegawai = auth()->user()->pegawai;
                $unitKerjaId = $pegawai->unit_kerja_id;
                $prodiId = $pegawai->prodi_id;
                $prodiName = strtoupper(str_replace(' ', '', auth()->user()->pegawai->prodi->nama));
                $query->where('prodi_id', $prodiId)
                // ->where('parent_id', $this->adminJurusan($prodiName))
                // ->where('parent_id', '!=', 0)
                // ->whereId($this->adminJurusan($prodiName))
                ->where($query->qualifyColumn('unit_kerja_id'), $unitKerjaId);
            });
        }
    }
}
