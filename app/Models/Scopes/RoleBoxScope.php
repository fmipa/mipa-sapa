<?php

namespace App\Models\Scopes;

use App\Enums\RoleEnum;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class RoleBoxScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (auth()->user()) {
            // if (!auth()->user()->operator) {
                $builder->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                    return (
                            $value == RoleEnum::SUPERADMIN->value ||
                            $value == RoleEnum::ADMIN->value ||
                            $value == RoleEnum::DEKAN->value ||
                            $value == RoleEnum::WD->value ||
                            $value == RoleEnum::KTU->value ||
                            $value == RoleEnum::SUBKOOR->value
                        );
                }) == FALSE), function($query) {
                        // $roles = auth()->user()->getRoleNames()->toArray();
                        // $pegawaiRoles = User::role($roles)->pluck('id')->toArray();
                        $query->where($query->qualifyColumn('unit_kerja_id'), auth()->user()->pegawai->unit_kerja_id);
                    });
            // }
        }
    }
}
