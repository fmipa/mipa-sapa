<?php

namespace App\Models\Scopes;

use App\Enums\RoleEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Spatie\Permission\Models\Role;

class RoleFolderStorageScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (auth()->user()) {
            $builder->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::SUPERADMIN->value ||
                        $value == RoleEnum::ADMIN->value ||
                        $value == RoleEnum::DEKAN->value ||
                        $value == RoleEnum::WD->value ||
                        $value == RoleEnum::KTU->value ||
                        $value == RoleEnum::SUBKOOR->value
                    );
            }) == FALSE), function($query) {
                $rolesName = auth()->user()->getRoleNames()->toArray();
                $roles = Role::whereIn('name', $rolesName)->pluck('id');
                $query->whereIn($query->qualifyColumn('role_id'), $roles);
            });
        }
    }
}
