<?php

namespace App\Models\Scopes;

use App\Enums\RoleEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class RoleSuperAdminScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (auth()->user()) {
            $builder->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::SUPERADMIN->value
                    );
            }) == FALSE), function($query) {
                $query->whereNotIn($query->qualifyColumn('id'), [1, 2]);
            });
        }
    }
}
