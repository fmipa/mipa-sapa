<?php

namespace App\Models\Scopes;

use App\Enums\RoleEnum;
use App\Models\UnitKerja;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;
use Spatie\Permission\Models\Role;

class RoleFolderScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (auth()->user()) {
            $builder->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::SUPERADMIN->value ||
                        $value == RoleEnum::ADMIN->value ||
                        $value == RoleEnum::DEKAN->value ||
                        $value == RoleEnum::WD->value ||
                        $value == RoleEnum::KTU->value ||
                        $value == RoleEnum::SUBKOOR->value
                    );
            }) == FALSE), function($query) {
                $unitKerjaId = auth()->user()->pegawai->unit_kerja_id == 1 ? UnitKerja::whereIn('nama', ['AKADEMIK', 'KEMAHASISWAAN'])->pluck('id') : [auth()->user()->pegawai->unit_kerja_id];
                $query->whereIn($query->qualifyColumn('unit_kerja_id'), $unitKerjaId);
            });
        }
    }
}
