<?php

namespace App\Models\Scopes;

use App\Enums\RoleEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class RoleAdminScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (auth()->user()) {
            $builder->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::SUPERADMIN->value ||
                        $value == RoleEnum::ADMIN->value
                    );
            }) == FALSE), function($query) {
                $query->where($query->qualifyColumn('created_by'), auth()->id());
            });
        }
    }
}
