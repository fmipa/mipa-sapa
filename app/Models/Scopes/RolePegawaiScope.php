<?php

namespace App\Models\Scopes;

use App\Enums\RoleEnum;
use App\Models\Pegawai;
use App\Models\Prodi;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class RolePegawaiScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     */
    public function apply(Builder $builder, Model $model): void
    {
        if (auth()->user()) {
                // $prodis = auth()->user()->pegawai->prodi_id;
                $builder->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::SUPERADMIN->value ||
                        $value == RoleEnum::ADMIN->value ||
                        $value == RoleEnum::DEKAN->value ||
                        $value == RoleEnum::WD->value ||
                        $value == RoleEnum::KTU->value ||
                        $value == RoleEnum::SUBKOOR->value ||
                        $value == RoleEnum::KEPEGAWAIAN->value
                    );
            }) == FALSE), function($query) use($prodis) {
                // $jurusanId = Pegawai::find(auth()->user()->pegawai_id)->prodi_id;
                // $prodis = Prodi::where('jurusan_id', $jurusanId)->pluck('id');
                // $query->whereIn($query->qualifyColumn('prodi_id'), $prodis);
            });
        }
    }
}
