<?php

namespace App\Models;

use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class MasterAgama extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'master_agamas';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        // Order by agama ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('agama', 'asc');
        });
    }

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    // public function creator(): BelongsTo
    // {
    //     return $this->belongsTo(User::class, 'created_by', 'id');
    // }

    /**
     * Get all of the pegawais for the MasterAgama
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pegawais(): HasMany
    {
        return $this->hasMany(Pegawai::class, 'agama_id', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function agama(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::upper($value),
            get: fn (string $value) => ucwords(Str::lower($value)),
        );
    }

    protected function kode(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::upper($value),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'agama'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->agama;

            return $data;
        });
    }
    // ! END SCOPE
}
