<?php

namespace App\Models;

use App\Models\Scopes\RolePimpinanScope;
use App\Models\Scopes\RoleKodeNaskahScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class SK extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sk';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RolePimpinanScope);
        // Order by nomor ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('nomor', 'asc');
        });
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_file', 'tingkatan', 'nomor_sk', 'kode_unit_id_encrypt', 'kode_naskah_id_encrypt'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the file that owns the SK
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class, 'file_id', 'id')->withoutGlobalScopes([RolePimpinanScope::class]);
    }

    /**
     * Get the kodeUnit that owns the SK
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kodeUnit(): BelongsTo
    {
        return $this->belongsTo(KodeUnit::class, 'kode_unit_id', 'id')->withoutGlobalScopes([RolePimpinanScope::class]);
    }

    /**
     * Get the Kode Naskah that owns the SK
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kodeNaskah(): BelongsTo
    {
        return $this->belongsTo(KodeNaskah::class, 'kode_naskah_id', 'id')->withoutGlobalScopes([RoleKodeNaskahScope::class]);
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function judul(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::upper($value),
            get: fn (string $value) => Str::ucfirst($value),
        );
    }

    protected function urlFile(): Attribute
    {
        $url = NULL;
        $file = $this->file;
        if ($file->folderStorage) {
            $url = 'app/'.strtolower($file->folderStorage->jenisAkses->nama).'/'.$file->folderStorage->path.'/'.$file->nama_file;
        }
        return new Attribute(
            get: fn () => (string) $url,
        );
    }

    protected function tingkatan(): Attribute
    {
        return new Attribute(
            get: fn () => (string) $this->kode_unit == 1 ? 'Universitas' : 'Fakultas',
        );
    }

    protected function nomorSk(): Attribute
    {
        return new Attribute(
            get: fn () => (string) $this->nomor.'/'.($this->kodeUnit ? $this->kodeUnit->kode : '').'/'.($this->kodeNaskah ? $this->kodeNaskah->kode : '').'/'.$this->tahun,
        );
    }

    protected function kodeUnitIdEncrypt(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->kode_unit_id),
        );
    }


    protected function kodeNaskahIdEncrypt(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->kode_naskah_id),
        );
    }

    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'nomor'])->map(function($data) {
            $data['key'] = $data->idSecret;
            $data['text'] = $data->nomor;

            return $data;
        });
    }
    // ! END SCOPE
}
