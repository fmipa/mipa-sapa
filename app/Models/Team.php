<?php

namespace App\Models;

use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Team extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'teams';

    protected static function boot()
    {
        parent::boot();
        // Order by nama ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('nama', 'asc');
        });
    }
}
