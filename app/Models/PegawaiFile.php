<?php

namespace App\Models;

use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\Pivot;

class PegawaiFile extends Pivot
{
    use HasFactory, EncryptId;

    protected $table = 'pegawai_file';

    protected $guarded = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['file_id', 'unit_kerja_id', 'pegawai_id', 'created_by'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['id_secret', 'pegawai_id_secret', 'unit_kerja_id_secret'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the file that owns the PegawaiFile
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class, 'file_id', 'id');
    }
    
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }
    protected function pegawaiIdSecret(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->pegawai_id ? $this->encrypt($this->pegawai_id) : NULL),
        );
    }
    protected function unitKerjaIdSecret(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->unit_kerja_id ? $this->encrypt($this->unit_kerja_id) : NULL),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    public static function scopeWhereInMultiple(Builder $query, array $columns, array $values)
    {
        collect($values)
            ->transform(function ($v) use ($columns) {
                $clause = [];
                foreach ($columns as $index => $column) {
                    $clause[] = [$column, '=', $v[$index]];
                }
                return $clause;
            })
            ->each(function($clause, $index) use ($query) {
                $query->where($clause, null, null, $index === 0 ? 'and' : 'or');
            });

        return $query;
    }
    // ! END SCOPE
}
