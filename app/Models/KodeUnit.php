<?php

namespace App\Models;

use App\Models\Scopes\RolePimpinanScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class KodeUnit extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kode_units';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        // static::addGlobalScope(new RolePimpinanScope);
        // Order by kode ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('kode', 'asc');
        });
    }

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get all of the sks for the KodeUnit
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sks(): HasMany
    {
        return $this->hasMany(SK::class, 'kode_unit_id', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function kode(): Attribute
    {
        return new Attribute(
            set: fn ($value) => Str::upper($value),
        );
    }

    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'kode', 'deskripsi'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->kode . ' - '. $data->deskripsi;

            return $data;
        });
    }
    // ! END SCOPE
}
