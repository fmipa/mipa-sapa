<?php

namespace App\Models;

use App\Models\Scopes\RolePegawaiScope;
use App\Models\Scopes\RoleSuperAdminScope;
use App\Traits\Mutators\EncryptId;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Pegawai extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pegawais';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RoleSuperAdminScope);
        // static::addGlobalScope(new RolePegawaiScope);
        // Order by nama depan ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('firstname', 'asc')->orderBy('middlename');
        });
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    protected $appends = ['id_secret', 'full_name', 'full_name_with_title', 'ttl'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    // public function creator(): BelongsTo
    // {
    //     return $this->belongsTo(User::class, 'created_by', 'id');
    // }

    /**
     * Get the master Agama that owns the Pegawai
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function masterAgama(): BelongsTo
    {
        return $this->belongsTo(MasterAgama::class, 'agama_id', 'id');
    }

    /**
     * Get the user associated with the Pegawai
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function user(): HasOne
    {
        return $this->hasOne(User::class, 'pegawai_id', 'id');
    }

    /**
     * The files that belong to the Pegawai
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files(): BelongsToMany
    {
        return $this->belongsToMany(File::class, 'pegawai_file', 'pegawai_id', 'file_id');
    }

    /**
     * The folder storages that belong to the Pegawai
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function folderStorages(): BelongsToMany
    {
        return $this->belongsToMany(FolderStorage::class, 'pegawai_folder_storage', 'pegawai_id', 'folder_storage_id');
    }

    /**
     * Get the prodi that owns the Pegawai
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function prodi(): BelongsTo
    {
        return $this->belongsTo(Prodi::class, 'prodi_id', 'id')->withDefault([
            'nama' => 'Fakultas',
        ]);
    }

    /**
     * Get the unitKerja that owns the Pegawai
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitKerja(): BelongsTo
    {
        return $this->belongsTo(UnitKerja::class, 'unit_kerja_id', 'id')->withDefault([
            'nama' => 'Lainnya',
        ]);
    }
    
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function tanggalLahir(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value ? Carbon::parse($value)->format('d-m-Y') : NULL,
        );
    }

    protected function fullName(): Attribute
    {
        return new Attribute(
            get: fn () => $this->firstname . ($this->middlename ? (' '. $this->middlename) : NULL) . ($this->lastname ? (' '. $this->lastname) : NULL),
        );
    }
    
    protected function fullNameWithTitle(): Attribute
    {
        return new Attribute(
            get: fn () => ($this->gelar_depan ? ($this->gelar_depan .' ') : NULL) . $this->firstname . ($this->middlename ? (' '. $this->middlename) : NULL) . ($this->lastname ? (' '. $this->lastname) : NULL) . ($this->gelar_belakang ? (', '. $this->gelar_belakang) : NULL),
        );
    }

    protected function ttl(): Attribute
    {
        return new Attribute(
            get: fn () => (($this->tempat_lahir ?? '-') .', '. ($this->tanggal_lahir == NULL ? '-' : date('d-M-Y', strtotime($this->tanggal_lahir)))) ?? '-',
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'firstname', 'middlename', 'lastname', 'gelar_depan', 'gelar_belakang'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->fullNameWithTitle;

            return $data;
        });
    }
    // ! END SCOPE
}
