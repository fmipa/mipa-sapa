<?php

namespace App\Models;

use App\Models\Scopes\RoleKBerkasScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;


class KategoriBerkas extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kategori_berkas';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RoleKBerkasScope);
        // Order by kategori ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('kategori', 'asc');
        });
    }

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get all of the berkas pegawais for the KategoriBerkas
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function berkasPegawais(): HasMany
    {
        return $this->hasMany(BerkasPegawai::class, 'kategori_id', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function kode(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::upper($value),
        );
    }

    protected function kategori(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::upper($value),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'kategori'])->map(function($data) {
            $data['key'] = $data->idSecret;
            $data['text'] = $data->kategori;

            return $data;
        });
    }
    // ! END SCOPE
}
