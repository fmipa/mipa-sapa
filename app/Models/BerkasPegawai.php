<?php

namespace App\Models;

use App\Models\Scopes\RolePimpinanScope;
use App\Models\Scopes\RoleKBerkasScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class BerkasPegawai extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'berkas_pegawai';
    
    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RoleKBerkasScope);
        // Order by nomor ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('nama', 'asc');
        });
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_file', 'kategori_id_encrypt'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the file that owns the SuratKeluar
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class, 'file_id', 'id')->withoutGlobalScopes([RolePimpinanScope::class]);
    }
    
    /**
     * Get the user that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function kategoriBerkas(): BelongsTo
    {
        return $this->belongsTo(KategoriBerkas::class, 'kategori_id', 'id')->withoutGlobalScopes([RoleKBerkasScope::class]);
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function nama(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::upper($value),
        );
    }
    
    protected function urlFile(): Attribute
    {
        $url = NULL;
        $file = $this->file;
        if ($file->folderStorage) {
            $url = 'app/'.strtolower($file->folderStorage->jenisAkses->nama).'/'.$file->folderStorage->path.'/'.$file->nama_file;
        }
        return new Attribute(
            get: fn () => (string) $url,
        );
    }
    
    protected function kategoriIdEncrypt(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->kategori_id),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'nama'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->nama;

            return $data;
        });
    }
    // ! END SCOPE
}
