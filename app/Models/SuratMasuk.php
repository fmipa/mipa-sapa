<?php

namespace App\Models;

use App\Models\Scopes\RolePimpinanScope;
use App\Traits\Mutators\EncryptId;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SuratMasuk extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'surat_masuk';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RolePimpinanScope);
        // Order by nomor ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('nomor', 'asc');
        });
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['url_file'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the file that owns the SuratMasuk
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class, 'file_id', 'id')->withoutGlobalScopes([RolePimpinanScope::class]);
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    // protected function tanggalDiterbitkan(): Attribute
    // {
    //     return new Attribute(
    //         get: fn (string $value) => Carbon::parse($value)->format('d-M-Y'),
    //     );
    // }

    // protected function tanggalDiterima(): Attribute
    // {
    //     return new Attribute(
    //         get: fn (string $value) => Carbon::parse($value)->format('d-M-Y'),
    //     );
    // }

    protected function urlFile(): Attribute
    {
        $url = NULL;
        $file = $this->file;
        if ($file->folderStorage) {
            $url = 'app/'.strtolower($file->folderStorage->jenisAkses->nama).'/'.$file->folderStorage->path.'/'.$file->nama_file;
        }
        return new Attribute(
            get: fn () => (string) $url,
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'nomor'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->nomor;

            return $data;
        });
    }
    // ! END SCOPE
}
