<?php

namespace App\Models;

use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class Activation extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'activations';
    
    protected $guarded = [];

    // ! RELATIONS
    /**
     * Get the user that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function kode(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::random(30),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'name'])->map(function($data) {
            $data['key'] = $data->idSecret;
            $data['text'] = $data->name;

            return $data;
        });
    }
    // ! END SCOPE
}
