<?php

namespace App\Models;

use App\Models\Scopes\RolePimpinanScope;
use App\Traits\Mutators\EncryptId;
use App\Traits\UnitKerjaRole;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Spatie\Permission\Models\Role;

class UnitKerja extends Model
{
    use HasFactory, EncryptId, UnitKerjaRole;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'unit_kerjas';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        // static::addGlobalScope(new RolePimpinanScope());
        // Order by master folder ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('is_folder', 'desc');
            $builder->orderBy('urutan', 'asc');
        });
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['id_secret'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    // public function creator(): BelongsTo
    // {
    //     return $this->belongsTo(User::class, 'created_by', 'id');
    // }

    /**
     * Get the box associated with the UnitKerja
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function box(): HasOne
    {
        return $this->hasOne(Box::class, 'unit_kerja_id', 'id');
    }

    /**
     * The unit kerja that belong to the folder
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function folders(): BelongsToMany
    {
        return $this->belongsToMany(Folder::class, 'unit_folder', 'unit_kerja_id', 'folder_id');
    }

    /**
     * The unit kerja that belong to the role
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->belongsToMany(Role::class, 'unit_role', 'unit_kerja_id', 'role_id');
    }

    /**
     * The files that belong to the Pegawai
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files(): BelongsToMany
    {
        return $this->belongsToMany(File::class, 'pegawai_file', 'unit_kerja_id', 'file_id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function nama(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value ? strtoupper($value) : '-',
            set: fn ($value) => strtoupper($value),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeIsFolder($query)
    {
        return $query->whereIsFolder(1);
    }

    protected function scopeView($query, $changeName=false, $idIsText = false)
    {
        return $query->get(['id', 'nama'])->map(function($data) use ($changeName, $idIsText) {
            $text = $changeName ? $this->changeNameUnitKerja($data->nama) : $data->nama;
            $data['key']    = $idIsText ? $text : $data->idSecret;
            $data['text']   = $text;

            return $data;
        });
    }
    // ! END SCOPE
}
