<?php

namespace App\Models;

use App\Models\Scopes\RoleBoxScope;
use App\Models\Scopes\RolePimpinanScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class File extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'files';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RolePimpinanScope);
        // Order by Nama File ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('id', 'asc');
        });
    }
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_path'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the older storage that owns the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function folderStorage(): BelongsTo
    {
        return $this->belongsTo(FolderStorage::class, 'folder_storage_id', 'id')->withoutGlobalScopes([RolePimpinanScope::class]);
    }

    /**
     * Get the berkas pegawai associated with the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function berkasPegawai(): HasOne
    {
        return $this->hasOne(BerkasPegawai::class, 'file_id', 'id');
    }

    /**
     * Get the document associated with the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function document(): HasOne
    {
        return $this->hasOne(Document::class, 'file_id', 'id');
    }

    /**
     * Get the sk associated with the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function sk(): HasOne
    {
        return $this->hasOne(SK::class, 'file_id', 'id');
    }

    /**
     * Get the surat Keluar associated with the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function suratKeluar(): HasOne
    {
        return $this->hasOne(SuratKeluar::class, 'file_id', 'id');
    }

    /**
     * Get the surat Masuk associated with the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function suratMasuk(): HasOne
    {
        return $this->hasOne(suratMasuk::class, 'file_id', 'id');
    }

    /**
     * The pegawais that belong to the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pegawais(): BelongsToMany
    {
    return $this->belongsToMany(Pegawai::class, 'pegawai_file', 'file_id', 'pegawai_id')->using(PegawaiFile::class);
    }

    /**
     * The unit kerja that belong to the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function unitKerjas(): BelongsToMany
    {
    return $this->belongsToMany(UnitKerja::class, 'pegawai_file', 'file_id', 'unit_kerja_id')->using(PegawaiFile::class);
    }

    /**
     * The box that belong to the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function boxes(): BelongsToMany
    {
        return $this->belongsToMany(Box::class, 'file_box', 'file_id', 'box_id')->withoutGlobalScopes([RoleBoxScope::class]);
    }

    /**
     * Get the folder that owns the File
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function folder(): BelongsTo
    {
        return $this->belongsTo(Folder::class, 'folder_id', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    public function fullPath(): Attribute
    {
        return new Attribute(
            get: fn () => $this->folderStorage->path ?? 'Not Found',
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'nama_file'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->nama_file;

            return $data;
        });
    }
    // ! END SCOPE
}
