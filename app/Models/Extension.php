<?php

namespace App\Models;

use App\Models\Scopes\RoleAdminScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Extension extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'extensions';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        // static::addGlobalScope(new RoleAdminScope);
        // Order by extension ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('extension', 'asc');
        });
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['extension_file'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function extensionFile(): Attribute
    {
        return new Attribute(
            get: fn () => ('.'.$this->extension),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'extension'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->extension;

            return $data;
        });
    }
    // ! END SCOPE
}
