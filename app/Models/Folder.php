<?php

namespace App\Models;

use App\Models\Scopes\RoleFolderAkademikScope;
use App\Models\Scopes\RoleFolderJurusanScope;
use App\Models\Scopes\RoleFolderScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Folder extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'folders';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RoleFolderScope);
        static::addGlobalScope(new RoleFolderJurusanScope);
        // Order by nomor ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('nama', 'asc')->orderBy('nama');
        });
    }
    
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    protected $appends = ['id_secret', 'parent_id_secret', 'nama_parent_utama'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * The unitKerja that belong to the Folder
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitKerja(): BelongsTo
    {
        return $this->belongsTo(UnitKerja::class, 'unit_kerja_id', 'id');
    }

    /**
     * The unitKerja that belong to the Folder
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function unitKerjas(): BelongsToMany
    {
        return $this->belongsToMany(UnitKerja::class, 'unit_folder', 'folder_id', 'unit_kerja_id');
    }

    /**
     * Get the Parent associated with the Folder
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function parents(): BelongsTo
    {
        return $this->belongsTo(static::class, 'parent_id')->orderBy('nama', 'asc');
    }

    public function parent()
    {
        return $this->parents()->with('parent');
    }

    /**
     * Get all of the Children for the Folder
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childs(): HasMany
    {
        return $this->hasMany(static::class, 'parent_id', 'id')->select('id','nama','parent_id', 'unit_kerja_id')->orderBy('nama', 'asc');
    }

    // public static function folder()
    // {
    //     return static::with(implode('.', array_fill(0, 100, 'children')))->where('parent_id', '=', '0')->orderBy('nama', 'asc')->get();
    // }

    public function child()
    {
        return $this->childs()->with('child');
    }

    public function allChildren()
    {
        $children = [];
        $childs = [$this];
        while(count($childs) > 0){
            $nextChilds = [];
            foreach ($childs as $child) {
                $child->childs->all();
                $children = array_merge($children, $child->childs->all());
                $nextChilds = array_merge($nextChilds, $child->childs->all());
            }
            $childs = $nextChilds;
        }
        return new Collection($children); //Illuminate\Database\Eloquent\Collection
    }

    function user_all_childs_ids($child=false)
    {
        $all_ids = [];
        if ($this->childs->count() > 0) {
            foreach ($this->childs as $child) {
                $all_ids[] = $child->id;
                $all_ids=array_merge($all_ids, is_array($this->user_all_childs_ids($child)) ? $this->user_all_childs_ids($child):[] );
            }
        }
        return $all_ids;
    }

    protected function viewSub($text, $data)
    {
        $text .= '-> '.$data->nama;
        if ($data->child) {
            $nama = $this->viewSub($text, $data);
        }
        return $text;
    }

    // ! END RELATIONS
    
    // ! ACCESSORS & MUcTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    // protected function parentId(): Attribute
    // {
    //     return new Attribute(
    //         get: fn ($value) => $value == 0 ? 'Utama' : $value,
    //     );
    // }

    protected function parentIdSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->parent_id ? $this->encrypt($this->parent_id) : NULL,
        );
    }

    protected function namaParentUtama(): Attribute
    {
        return new Attribute(
            get: fn () => $this->parent_utama ? (new static)::find($this->parent_utama)->nama : 'NULL',
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query, $parent = false)
    {
        return $query->when($parent, function($parentQuery) {
            $parentQuery->whereParentId(0);
        })
        ->get(['id', 'nama', 'parent_id'])->map(function($data) {
            $data['key']    = $data->idSecret;

            $text = $data->nama;
            $nama = $text;
            // $nama = $this->viewSub($text, $data);
            if ($data->parent) {
                $nama = $data->parent->nama .' -> '. $nama;
                if ($data->parent->parent) {
                    $nama = $data->parent->parent->nama .' -> '. $nama;
                    if ($data->parent->parent->parent) {
                        $nama = $data->parent->parent->parent->nama .' -> '. $nama;
                        if ($data->parent->parent->parent->parent) {
                            $nama = $data->parent->parent->parent->parent->nama .' -> '. $nama;
                            if ($data->parent->parent->parent->parent->parent) {
                                $nama = $data->parent->parent->parent->parent->parent->nama .' -> '. $nama;
                                if ($data->parent->parent->parent->parent->parent->parent) {
                                    $nama = $data->parent->parent->parent->parent->parent->parent->nama .' -> '. $nama;
                                }
                            }
                        }
                    }
                }
                
            }
            $data['text']   = $nama;

            return $data;
        });
    }

    // ! END SCOPE
}
