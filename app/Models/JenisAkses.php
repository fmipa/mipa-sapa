<?php

namespace App\Models;

use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Str;

class JenisAkses extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jenis_akses';

    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    protected $appends = ['id_secret'];

    // ! RELATIONS
    /**
     * Get the folder storage associated with the AksesFile
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function folderStorage(): HasOne
    {
        return $this->hasOne(FolderStorage::class, 'jenis_akses_id', 'id');
    }
    // ! END RELATIONS

    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function nama(): Attribute
    {
        return new Attribute(
            get: fn ($value) => Str::upper($value),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->where('nama', '!=', 'publik')->get(['id', 'nama'])->map(function($data) {
            $data['key']    = $data->id;
            $data['text']   = $data->nama;

            return $data;
        });
    }
    // ! END SCOPE
}
