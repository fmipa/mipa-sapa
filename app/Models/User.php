<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;
use Laravel\Sanctum\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Support\Str;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, HasRoles, EncryptId;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'username',
        'email',
        'password',
        'pegawai_id',
        'operator',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];
    
    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['is_active', 'is_admin', 'id_secret', 'my_role', 'is_operator'];

    protected static function booted()
    {
        static::addGlobalScope(function($query) 
        {
            // $query->whereHas('roles', fn ($roles) => $roles->where('name', '!=', 'super-admin'));
            $query->orderBy('name', 'asc');
        });
    }

    // ! RELATIONS
    /**
     * Get the activation associated with the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function activation(): HasOne
    {
        return $this->hasOne(Activation::class, 'user_id', 'id');
    }

    /**
     * Get all of the extensions for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function extensions(): HasMany
    {
        return $this->hasMany(Extension::class, 'created_by', 'id');
    }

    /**
     * Get the pegawai that owns the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function pegawai(): BelongsTo
    {
        return $this->belongsTo(Pegawai::class, 'pegawai_id', 'id');
    }

    /**
     * Get all of the fileShare for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fileShares(): HasMany
    {
        return $this->hasMany(FileShare::class, 'created_by', 'id');
    }

    /**
     * Get all of the kategori Berkas for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kategoriberkasPegawais(): HasMany
    {
        return $this->hasMany(KategoriBerkasPegawai::class, 'created_by', 'id');
    }

    /**
     * Get all of the Kode Naskah for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kodeNaskahs(): HasMany
    {
        return $this->hasMany(KodeNaskah::class, 'created_by', 'id');
    }

    /**
     * Get all of the kode Unit for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function kodeUnits(): HasMany
    {
        return $this->hasMany(KodeUnit::class, 'created_by', 'id');
    }

    /**
     * Get all of the master Agama for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // public function masterAgamas(): HasMany
    // {
    //     return $this->hasMany(File::class, 'created_by', 'id');
    // }

    /**
     * Get all of the pegawais for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    // public function pegawaiss(): HasMany
    // {
    //     return $this->hasMany(Pegawai::class, 'created_by', 'id');
    // }

    /**
     * Get all of the sk for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sks(): HasMany
    {
        return $this->hasMany(SK::class, 'created_by', 'id');
    }

    /**
     * Get all of the surat Keluar for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suratKeluars(): HasMany
    {
        return $this->hasMany(SuratKeluar::class, 'created_by', 'id');
    }

    /**
     * Get all of the surat Masuk for the User
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suratMasuks(): HasMany
    {
        return $this->hasMany(SuratMasuk::class, 'created_by', 'id');
    }

    // ! END RELATIONS

    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function isActive(): Attribute
    {
        return new Attribute(
            get: fn () => $this->activation ? true : false,
        );
    }

    protected function isAdmin(): Attribute
    {
        return new Attribute(
            get: fn () => $this->hasRole('super-admin'),
        );
    }

    protected function isOperator(): Attribute
    {
        return new Attribute(
            get: fn () => $this->operator == 1 ? true : false,
        );
    }

    protected function myRole(): Attribute
    {
        $roles = implode(',', $this->getRoleNames()->toArray());
        return new Attribute(
            get: fn () => $roles ?? false ,
        );
    }

    protected function name(): Attribute
    {
        return new Attribute(
            get: fn (string $value) => ucwords(Str::lower($value)),
            set: fn (string $value) => Str::upper($value),
        );
    }

    protected function email(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::lower($value),
        );
    }

    protected function username(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::lower($value),
        );
    }

    protected function password(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => bcrypt($value),
        );
    }
    // ! END ACCESORS & MUTATORS
    
    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'name'])->map(function($data) {
            $data['key'] = $data->idSecret;
            $data['text'] = $data->name;

            return $data;
        });
    }
    // ! END SCOPE


}
