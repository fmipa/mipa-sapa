<?php

namespace App\Models;

use App\Models\Scopes\RoleAdminScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class FileShare extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'file_shares';

    protected $guarded = [];
    
    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RoleAdminScope);
        // Order by created_at ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('created_at', 'asc');
        });
    }

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the file that owns the FileShare
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function file(): BelongsTo
    {
        return $this->belongsTo(File::class, 'file_id', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function kode(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::random(40),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->with('files')->get(['id', 'name'])->map(function($data) {
            $data['key'] = $data->idSecret;
            $data['text'] = $data->file->nama_file;

            return $data;
        });
    }
    // ! END SCOPE
}
