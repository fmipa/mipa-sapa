<?php

namespace App\Models;

use App\Models\Scopes\RoleKodeNaskahScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Str;

class KodeNaskah extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'kode_naskahs';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RoleKodeNaskahScope);
        // Order by kode ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('kode', 'asc');
        });
    }
    
    // ! RELATIONS
    /**
     * Get the Creator that owns the Kode Naskah
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get all of the Surat Keluar for the Kode Naskah
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suratKeluars(): HasMany
    {
        return $this->hasMany(SuratKeluar::class, 'kode_naskah_id', 'id');
    }
    
    /**
     * Get all of the SK for the Kode Naskah
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sks(): HasMany
    {
        return $this->hasMany(SK::class, 'kode_naskah_id', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function kode(): Attribute
    {
        return new Attribute(
            set: fn (string $value) => Str::upper($value),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'kode', 'substansi'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->kode . ' - ' . $data->substansi;

            return $data;
        });
    }
    // ! END SCOPE
}
