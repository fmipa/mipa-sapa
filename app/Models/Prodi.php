<?php

namespace App\Models;

use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Str;

class Prodi extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'prodis';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        // Order by extension ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('nama', 'asc');
        });
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['nama_jurusan', 'jurusan_secret', 'id_secret'];

    // ! RELATIONS
    /**
     * Get the jurusans that owns the Prodi
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jurusan(): BelongsTo
    {
        return $this->belongsTo(Jurusan::class, 'jurusan_id', 'id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function namaJurusan(): Attribute
    {
        return new Attribute(
            get: fn () => $this->jurusan ? $this->jurusan->nama : 'Fakultas',
        );
    }

    protected function jurusanSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->jurusan_id),
        );
    }

    protected function strata(): Attribute
    {
        return new Attribute(
            set: fn ($value) => Str::upper($value),
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'jurusan_id', 'nama'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->nama;

            return $data;
        });
    }
    // ! END SCOPE
}
