<?php

namespace App\Models;

use App\Enums\RoleEnum;
use App\Models\Scopes\RolePimpinanScope;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class FolderStorage extends Model
{
    use HasFactory, EncryptId;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'folder_storages';

    protected $guarded = [];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */

    protected $appends = ['id_secret'];

    protected static function boot()
    {
        parent::boot();
        static::addGlobalScope(new RolePimpinanScope);
        // Order by nama ASC
        static::addGlobalScope('order', function (Builder $builder) {
        $builder->orderBy('nama', 'asc');
        });
    }

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the jenis akses that owns the Folder Storage
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function jenisAkses(): BelongsTo
    {
        return $this->belongsTo(JenisAkses::class, 'jenis_akses_id', 'id');
    }
    
    /**
     * Get all of the files for the Folder Storage
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function files(): HasMany
    {
        return $this->hasMany(File::class, 'folder_storage_id', 'id');
    }

    /**
     * The pegawais that belong to the Folder Storage
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function pegawais(): BelongsToMany
    {
        return $this->belongsToMany(Pegawai::class, 'pegawai_folder_storage', 'folder_storage_id', 'pegawai_id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->when(auth()->user(), function($queryAuth) {
            return $queryAuth->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::SUPERADMIN ||
                        $value == RoleEnum::ADMIN
                    );
                }) == FALSE), function($queryPegawai) {
                    $queryPegawai->whereHas('pegawais', function ($subQuery) {
                    return $subQuery->where('pegawai_id', '=', auth()->user()->pegawai_id);
                });
            });
        })->get(['id', 'nama'])->map(function($data) {
            $data['key'] = $data->idSecret;
            $data['text'] = $data->nama;

            return $data;
        });
    }
    // ! END SCOPE
}
