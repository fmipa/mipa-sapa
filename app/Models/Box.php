<?php

namespace App\Models;

use App\Models\Scopes\RoleBoxScope;
use App\Traits\Mutators\EncryptId;
use App\Traits\QR;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Auth;

class Box extends Model
{
    use HasFactory, EncryptId, QR;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'boxes';

    protected $guarded = [];

    protected static function boot()
    {
        parent::boot();
        // static::addGlobalScope(new RoleBoxScope());
        // Order by extension ASC
        static::addGlobalScope('order', function (Builder $builder) {
            $builder->orderBy('nomor', 'asc');
        });
    }

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['id_secret', 'nama_unit_kerja', 'qrcode', 'nama_tahun', 'nama_view'];

    // ! RELATIONS
    /**
     * Get the creator that owns the Activation
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }

    /**
     * Get the unitKerja that owns the Box
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function unitKerja(): BelongsTo
    {
        return $this->belongsTo(UnitKerja::class, 'unit_kerja_id', 'id');
    }

    /**
     * The files that belong to the Box
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function files(): BelongsToMany
    {
        return $this->belongsToMany(File::class, 'file_box', 'box_id', 'file_id');
    }
    // ! END RELATIONS
    
    // ! ACCESSORS & MUTATORS
    protected function idSecret(): Attribute
    {
        return new Attribute(
            get: fn () => $this->encrypt($this->id),
        );
    }

    protected function nama(): Attribute
    {
        return new Attribute(
            get: fn ($value) => $value ? strtoupper($value) : '-',
            set: fn ($value) => strtoupper($value),
        );
    }

    protected function namaUnitKerja(): Attribute
    {
        return new Attribute(
            get: fn () => $this->unit_kerja_id ? $this->unitKerja->nama : 'Fakultas',
        );
    }

    protected function bagian(): Attribute
    {
        return new Attribute(
            get: fn () => $this->bagian ?? 'Fakultas',
        );
    }

    protected function qrcode(): Attribute
    {
        return new Attribute(
            get: fn () => $this->generate($this->unique_id, 150, 'round', '#00008B') ?? 'QRCODE ERROR',
        );
    }

    protected function namaTahun(): Attribute
    {
        return new Attribute(
            get: fn () => $this->nama .' '. $this->tahun,
        );
    }

    protected function namaView(): Attribute
    {
        $nama = 'Nomor : '.$this->nomor.', Nama : '.$this->nama .' '. $this->tahun;        
        if (auth()->user()->hasRole('super-admin')) {
            $nama = $nama.' | '.$this->namaUnitKerja;
        }return new Attribute(
            get: fn () => $nama,
        );
    }
    // ! END ACCESSORS & MUTATORS

    // ! SCOPE
    protected function scopeView($query)
    {
        return $query->get(['id', 'nama', 'nomor', 'tahun', 'unit_kerja_id', 'unique_id'])->map(function($data) {
            $data['key']    = $data->idSecret;
            $data['text']   = $data->namaView;

            return $data;
        });
    }

    protected function scopeMyBox($query, $id = false)
    {
        return $query->where('created_by', $id ?? auth()->id());
    }
    // ! END SCOPE
}
