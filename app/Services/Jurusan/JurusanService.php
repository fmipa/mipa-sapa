<?php

namespace App\Services\Jurusan;

use App\Repositories\Jurusan\JurusanRepositoryInterface;
use Exception;

class JurusanService implements JurusanServiceInterface
{
    private $jurusanRepository;
    public function __construct(JurusanRepositoryInterface $jurusanRepository)
    {
        $this->jurusanRepository = $jurusanRepository;
    }

    public function getAll()
    {
        return $this->jurusanRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->jurusanRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->jurusanRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($jurusanId)
    {
        return $this->jurusanRepository->findById($jurusanId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->jurusanRepository->whereBy($key, $value);
    }
    
    public function store(array $jurusanData)
    {
        try {
            $saving = $this->jurusanRepository->store($jurusanData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($jurusanId, array $newJurusan)
    {
        try {
            $saving = $this->jurusanRepository->update($jurusanId, $newJurusan);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($jurusanId)
    {
        try {
            $saving =  $this->jurusanRepository->delete($jurusanId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
}