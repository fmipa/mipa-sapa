<?php

namespace App\Services\Jurusan;

interface JurusanServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($jurusanId);

    public function whereBy($key, $value);

    public function store(array $jurusanData);

    public function update($jurusanId, array $newJurusan);

    public function delete($jurusanId);
}