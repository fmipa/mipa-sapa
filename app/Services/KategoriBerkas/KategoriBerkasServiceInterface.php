<?php

namespace App\Services\KategoriBerkas;

interface KategoriBerkasServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($kategoriBerkasId);

    public function whereBy($key, $value);

    public function store(array $kategoriBerkasData);

    public function update($kategoriBerkasId, array $newKategoriBerkas);

    public function delete($kategoriBerkasId);
}