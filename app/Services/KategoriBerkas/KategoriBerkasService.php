<?php

namespace App\Services\KategoriBerkas;

use App\Repositories\KategoriBerkas\KategoriBerkasRepositoryInterface;
use Exception;

class KategoriBerkasService implements KategoriBerkasServiceInterface
{
    private $kategoriBerkasRepository;
    public function __construct(KategoriBerkasRepositoryInterface $kategoriBerkasRepository)
    {
        $this->kategoriBerkasRepository = $kategoriBerkasRepository;
    }

    public function getAll()
    {
        return $this->kategoriBerkasRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->kategoriBerkasRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->kategoriBerkasRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($kategoriBerkasId)
    {
        return $this->kategoriBerkasRepository->findById($kategoriBerkasId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->kategoriBerkasRepository->whereBy($key, $value);
    }
    
    public function store(array $kategoriBerkasData)
    {
        try {
            $saving = $this->kategoriBerkasRepository->store($kategoriBerkasData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($kategoriBerkasId, array $newKategoriBerkas)
    {
        try {
            $saving = $this->kategoriBerkasRepository->update($kategoriBerkasId, $newKategoriBerkas);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($kategoriBerkasId)
    {
        try {
            $saving =  $this->kategoriBerkasRepository->delete($kategoriBerkasId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}