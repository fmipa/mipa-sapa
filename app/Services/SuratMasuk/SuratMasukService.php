<?php

namespace App\Services\SuratMasuk;

use App\Repositories\SuratMasuk\SuratMasukRepositoryInterface;
use App\Services\File\FileServiceInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SuratMasukService implements SuratMasukServiceInterface
{
    private $suratMasukRepository, $fileService;
    public function __construct(
        SuratMasukRepositoryInterface $suratMasukRepository,
        FileServiceInterface $fileService,
        )
    {
        $this->suratMasukRepository = $suratMasukRepository;
        $this->fileService          = $fileService;
    }

    public function getAll()
    {
        return $this->suratMasukRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->suratMasukRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->suratMasukRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($suratMasukId)
    {
        return $this->suratMasukRepository->findById($suratMasukId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->suratMasukRepository->whereBy($key, $value);
    }
    
    public function store(array $suratMasukData)
    {
        DB::beginTransaction();
        try {
            $suratMasukData['nama_file'] = $suratMasukData['nomor'];
            $fileSave = $this->fileService->store(Arr::only($suratMasukData, ['attachment', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'created_by', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $suratMasukData['file_id'] = $fileSave->id;
            $saving = $this->suratMasukRepository->store(Arr::only($suratMasukData, ['file_id', 'nomor', 'perihal', 'tahun', 'instansi', 'tanggal_diterbitkan', 'tanggal_diterima', 'created_by']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($suratMasukId, array $newSuratMasuk)
    {
        DB::beginTransaction();
        try {
            $data = $this->suratMasukRepository->findById($suratMasukId);
            // ! Cek Perubahan Nomor Surat untuk pengecekan Nama File
            if ($data->nomor == $newSuratMasuk['nomor'])
            {
                $newSuratMasuk['nama_file'] = $data->file->nama_file;
                $newSuratMasuk['cek_perubahan_nama'] = false;
            } else {
                $newSuratMasuk['nama_file'] = $newSuratMasuk['nomor'];
                $newSuratMasuk['cek_perubahan_nama'] = true;
            }
            $fileSave = $this->fileService->update($data->file_id, Arr::only($newSuratMasuk, ['attachment', 'cek_perubahan_nama', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $newSuratMasuk['file_id'] = $fileSave->id;
            $saving = $this->suratMasukRepository->update($suratMasukId, Arr::only($newSuratMasuk, ['file_id', 'nomor', 'perihal', 'tahun', 'instansi', 'tanggal_diterbitkan', 'tanggal_diterima', 'created_by']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($suratMasukId)
    {
        DB::beginTransaction();
        try {
            $data       = $this->suratMasukRepository->findById($suratMasukId);
            $deleteFile =  $this->fileService->delete($data->file_id);
            if (!$deleteFile) {
                throw new Exception($deleteFile);
            }
            $saving =  $this->suratMasukRepository->delete($suratMasukId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    public function detail($suratMasukId)
    {
        return view('pages.'.config('variables.templateName').'.surat-masuk.detail', [
            'data' => $this->findById($suratMasukId),
        ]);
    }
}