<?php

namespace App\Services\Permission;

interface PermissionServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($permissionId);

    public function whereBy($key, $value);

    public function store(array $permissionData);

    public function update($permissionId, array $newPermission);

    public function delete($permissionId);
}