<?php

namespace App\Services\Permission;

use App\Repositories\Permission\PermissionRepositoryInterface;
use App\Services\Permission\PermissionServiceInterface;
use Exception;

class PermissionService implements PermissionServiceInterface
{
    private $permissionRepository;
    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function getAll()
    {
        return $this->permissionRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->permissionRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->permissionRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($permissionId)
    {
        return $this->permissionRepository->findById($permissionId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->permissionRepository->whereBy($key, $value);
    }
    
    public function store(array $permissionData)
    {
        try {
            $saving = $this->permissionRepository->store($permissionData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($permissionId, array $newPermission)
    {
        try {
            $saving = $this->permissionRepository->update($permissionId, $newPermission);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($permissionId)
    {
        try {
            $saving =  $this->permissionRepository->delete($permissionId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
}