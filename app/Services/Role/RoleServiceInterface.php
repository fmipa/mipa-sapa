<?php

namespace App\Services\Role;

interface RoleServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($roleId);

    public function whereBy($key, $value);

    public function store(array $roleData);

    public function update($roleId, array $newRole);

    public function delete($roleId);
}