<?php

namespace App\Services\Role;

use App\Repositories\Role\RoleRepositoryInterface;
use App\Services\Role\RoleServiceInterface;
use Exception;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class RoleService implements RoleServiceInterface
{
    private $roleRepository;
    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
    }

    public function getAll()
    {
        return $this->roleRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->roleRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->roleRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($roleId)
    {
        return $this->roleRepository->findById($roleId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->roleRepository->whereBy($key, $value);
    }
    
    public function store(array $roleData)
    {
        try {
            DB::beginTransaction();
            $saving = $this->roleRepository->store($roleData);
            if (!$saving) {
                throw new Exception($saving);
            }
            if ($roleData['permission_id']) {
                $permissions = Permission::whereIn('id', $roleData['permission_id'])->pluck('name');
                $savingPermission = $saving->syncPermissions($permissions);

                if (!$savingPermission) {
                    throw new Exception($savingPermission);
                }
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($roleId, array $newRole)
    {
        try {
            DB::beginTransaction();
            $data   = $this->roleRepository->findById($roleId);
            $saving = $this->roleRepository->update($roleId, $newRole);
            if (!$saving) {
                throw new Exception($saving);
            }
            if ($newRole['permission_id']) {
                $permissions = Permission::whereIn('id', $newRole['permission_id'])->pluck('name');
                $savingPermission = $data->syncPermissions($permissions);

                if (!$savingPermission) {
                    throw new Exception($savingPermission);
                }
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($roleId)
    {
        try {
            DB::beginTransaction();
            $saving =  $this->roleRepository->delete($roleId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
}