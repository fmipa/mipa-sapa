<?php

namespace App\Services\Box;

use App\Repositories\Box\BoxRepositoryInterface;
use Exception;

class BoxService implements BoxServiceInterface
{
    private $boxRepository;
    public function __construct(BoxRepositoryInterface $boxRepository)
    {
        $this->boxRepository = $boxRepository;
    }

    public function getAll()
    {
        return $this->boxRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->boxRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->boxRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($boxId)
    {
        return $this->boxRepository->findById($boxId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->boxRepository->whereBy($key, $value);
    }
    
    public function store(array $boxData)
    {
        try {
            $saving = $this->boxRepository->store($boxData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($boxId, array $newBox)
    {
        try {
            $saving = $this->boxRepository->update($boxId, $newBox);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($boxId)
    {
        try {
            $saving =  $this->boxRepository->delete($boxId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
}