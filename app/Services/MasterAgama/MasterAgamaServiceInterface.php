<?php

namespace App\Services\MasterAgama;

interface MasterAgamaServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($masterAgamaId);

    public function whereBy($key, $value);

    public function store(array $masterAgamaData);

    public function update($masterAgamaId, array $newMasterAgama);

    public function delete($masterAgamaId);
}