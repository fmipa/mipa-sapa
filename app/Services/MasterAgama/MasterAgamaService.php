<?php

namespace App\Services\MasterAgama;

use App\Repositories\MasterAgama\MasterAgamaRepositoryInterface;
use Exception;

class MasterAgamaService implements MasterAgamaServiceInterface
{
    private $masterAgamaRepository;
    public function __construct(MasterAgamaRepositoryInterface $masterAgamaRepository)
    {
        $this->masterAgamaRepository = $masterAgamaRepository;
    }

    public function getAll()
    {
        return $this->masterAgamaRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order  = $orderby ?? 'asc';
        return $this->masterAgamaRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->masterAgamaRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($masterAgamaId)
    {
        return $this->masterAgamaRepository->findById($masterAgamaId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->masterAgamaRepository->whereBy($key, $value);
    }
    
    public function store(array $masterAgamaData)
    {
        try {
            $saving = $this->masterAgamaRepository->store($masterAgamaData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($masterAgamaId, array $newMasterAgama)
    {
        try {
            $saving = $this->masterAgamaRepository->update($masterAgamaId, $newMasterAgama);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($masterAgamaId)
    {
        try {
            $saving =  $this->masterAgamaRepository->delete($masterAgamaId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
}