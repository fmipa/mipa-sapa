<?php

namespace App\Services\SuratKeluar;

use App\Repositories\SuratKeluar\SuratKeluarRepositoryInterface;
use App\Services\File\FileServiceInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SuratKeluarService implements SuratKeluarServiceInterface
{
    private $suratKeluarRepository, $fileService;

    public function __construct(
        SuratKeluarRepositoryInterface $suratKeluarRepository,
        FileServiceInterface $fileService,
    )
    {
        $this->suratKeluarRepository    = $suratKeluarRepository;
        $this->fileService              = $fileService;
    }

    public function getAll()
    {
        return $this->suratKeluarRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->suratKeluarRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->suratKeluarRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($suratKeluarId)
    {
        return $this->suratKeluarRepository->findById($suratKeluarId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->suratKeluarRepository->whereBy($key, $value);
    }
    
    public function store(array $suratKeluarData)
    {
        DB::beginTransaction();
        try {
            $suratKeluarData['nama_file'] = $suratKeluarData['nomor'];
            $fileSave = $this->fileService->store(Arr::only($suratKeluarData, ['attachment', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'created_by', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $suratKeluarData['file_id'] = $fileSave->id;
            $saving = $this->suratKeluarRepository->store(Arr::only($suratKeluarData, ['file_id', 'nomor', 'perihal', 'tahun', 'tujuan', 'lampiran', 'perihal', 'tanggal_diterbitkan', 'kode_naskah_id', 'created_by']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($suratKeluarId, array $newSuratKeluar)
    {
        DB::beginTransaction();
        try {
            $data = $this->suratKeluarRepository->findById($suratKeluarId);
            // ! Cek Perubahan Nomor Surat untuk pengecekan Nama File
            if ($data->nomor == $newSuratKeluar['nomor'])
            {
                $newSuratKeluar['nama_file'] = $data->file->nama_file;
                $newSuratKeluar['cek_perubahan_nama'] = false;
            } else {
                $newSuratKeluar['nama_file'] = $newSuratKeluar['nomor'];
                $newSuratKeluar['cek_perubahan_nama'] = true;
            }
            $fileSave = $this->fileService->update($data->file_id, Arr::only($newSuratKeluar, ['attachment', 'cek_perubahan_nama', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $newSuratKeluar['file_id'] = $fileSave->id;
            $saving = $this->suratKeluarRepository->update($suratKeluarId, Arr::only($newSuratKeluar, ['file_id', 'nomor', 'perihal', 'tahun', 'tujuan', 'tanggal_diterbitkan', 'kode_naskah_id', 'created_by']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($suratKeluarId)
    {
        DB::beginTransaction();
        try {
            $data       = $this->suratKeluarRepository->findById($suratKeluarId);
            $deleteFile =  $this->fileService->delete($data->file_id);
            if (!$deleteFile) {
                throw new Exception($deleteFile);
            }
            $saving =  $this->suratKeluarRepository->delete($suratKeluarId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    public function detail($suratKeluarId)
    {
        return view('pages.'.config('variables.templateName').'.surat-keluar.detail', [
            'data' => $this->findById($suratKeluarId),
        ]);
    }
}