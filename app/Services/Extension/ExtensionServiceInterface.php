<?php

namespace App\Services\Extension;

interface ExtensionServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($extensionId);

    public function whereBy($key, $value);

    public function store(array $extensionData);

    public function update($extensionId, array $newExtension);

    public function delete($extensionId);
}