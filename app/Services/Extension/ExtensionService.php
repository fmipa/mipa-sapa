<?php

namespace App\Services\Extension;

use App\Repositories\Extension\ExtensionRepositoryInterface;
use App\Services\Extension\ExtensionServiceInterface;
use Exception;

class ExtensionService implements ExtensionServiceInterface
{
    private $extensionRepository;
    public function __construct(ExtensionRepositoryInterface $extensionRepository)
    {
        $this->extensionRepository = $extensionRepository;
    }

    public function getAll()
    {
        return $this->extensionRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->extensionRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->extensionRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($extensionId)
    {
        return $this->extensionRepository->findById($extensionId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->extensionRepository->whereBy($key, $value);
    }
    
    public function store(array $extensionData)
    {
        try {
            $saving = $this->extensionRepository->store($extensionData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($extensionId, array $newExtension)
    {
        try {
            $saving = $this->extensionRepository->update($extensionId, $newExtension);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($extensionId)
    {
        try {
            $saving =  $this->extensionRepository->delete($extensionId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
}