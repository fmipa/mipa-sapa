<?php

namespace App\Services\JenisAkses;

interface JenisAksesServiceInterface 
{
    public function getPegawais(array $datas);

    public function storeRelation($collect, $pegawais, $creator);

    public function updateRelation($collect, $pegawais, $creator);
}