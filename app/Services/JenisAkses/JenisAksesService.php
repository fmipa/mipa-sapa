<?php

namespace App\Services\JenisAkses;

use App\Enums\JenisAksesEnum;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\DB;

class JenisAksesService implements JenisAksesServiceInterface
{
    public function __construct()
    {
    }

    public function getPegawais(array $datas)
    {
        switch ($datas['jenis_akses_id']) {
            case (JenisAksesEnum::UNITKERJA->value):
                $pegawaisId = $this->tim($datas['role_id']);
                break;
            
            case (JenisAksesEnum::TAG->value):
                $pegawaisId = $datas['pegawai_id'];
                break;
            
            default:
                $pegawaisId = [auth()->user()->pegawai_id];
                break;
        }

        return $pegawaisId;
    }

    public function tim($roles)
    {
        return User::role($roles)->pluck('pegawai_id');
    }

    public function storeRelation($collect, $ids, $jenis_akases_id, $creator = false)
    {
        switch ($jenis_akases_id) {
            case '2':
                foreach ($ids as $value) {
                    $collect->pegawais()->attach($value, ['created_by' => $creator ?? auth()->id()]);
                }
                break;
            
            case '3':
                foreach ($ids as $value) {
                    $collect->pegawais()->attach($value, ['created_by' => $creator ?? auth()->id()]);
                }
                break;
            
            default:
                foreach ($ids as $value) {
                    $collect->unitKerjas()->attach($value, ['created_by' => $creator ?? auth()->id()]);
                }
                break;
        }
    }

    public function updateRelation($collect, $ids, $jenis_akases_id, $creator = false)
    {
        switch ($jenis_akases_id) {
            case '2':
                $collect->pegawais()->syncWithPivotValues($ids, ['created_by' => $creator ?? auth()->id()]);

                break;
            
            case '3':
                $collect->pegawais()->syncWithPivotValues($ids, ['created_by' => $creator ?? auth()->id()]);

                break;
        
            default:
                $collect->unitKerjas()->syncWithPivotValues($ids, ['created_by' => $creator ?? auth()->id()]);

                break;
        }
        // $collect->pegawais()->syncWithPivotValues($pegawais, ['created_by' => $creator ?? auth()->id()]);
    }
    
}