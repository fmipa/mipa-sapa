<?php

namespace App\Services\Dokumen;

interface DocumentServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($documentId);

    public function whereBy($key, $value);

    public function store(array $documentData);

    public function update($documentId, array $newDocument);

    public function delete($documentId);

    public function detail($documentId);
}