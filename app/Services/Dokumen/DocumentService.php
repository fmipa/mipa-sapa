<?php

namespace App\Services\Dokumen;

use App\Repositories\Dokumen\DocumentRepositoryInterface;
use App\Services\File\FileServiceInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class DocumentService implements DocumentServiceInterface
{
    private $documentRepository, $fileService;

    public function __construct(
        DocumentRepositoryInterface $documentRepository,
        FileServiceInterface $fileService,
    )
    {
        $this->documentRepository = $documentRepository;
        $this->fileService      = $fileService;
    }

    public function getAll()
    {
        return $this->documentRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->documentRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->documentRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($documentId)
    {
        return $this->documentRepository->findById($documentId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->documentRepository->whereBy($key, $value);
    }
    
    public function store(array $documentData)
    {
        DB::beginTransaction();
        try {
            $documentData['nama_file'] = $documentData['nama'];
            $fileSave = $this->fileService->store(Arr::only($documentData, ['attachment', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'created_by', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $documentData['file_id'] = $fileSave->id;
            $saving = $this->documentRepository->store(Arr::only($documentData, ['file_id', 'nama', 'kategori_id', 'created_by']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($documentId, array $newDocument)
    {
        DB::beginTransaction();
        try {
            $data = $this->documentRepository->findById($documentId);
            // ! Cek Perubahan Nomor Surat untuk pengecekan Nama File
            if ($data->nama == $newDocument['nama'])
            {
                $newDocument['nama_file'] = $data->file->nama_file;
                $newDocument['cek_perubahan_nama'] = false;
            } else {
                $newDocument['nama_file'] = $newDocument['nama'];
                $newDocument['cek_perubahan_nama'] = true;
            }
            $fileSave = $this->fileService->update($data->file_id, Arr::only($newDocument, ['attachment', 'cek_perubahan_nama', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $newDocument['file_id'] = $fileSave->id;
            $saving = $this->documentRepository->update($documentId, Arr::only($newDocument, ['file_id', 'nama', 'kategori_id']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($documentId)
    {
        DB::beginTransaction();
        try {
            $data       = $this->documentRepository->findById($documentId);
            $deleteFile =  $this->fileService->delete($data->file_id);
            if (!$deleteFile) {
                throw new Exception($deleteFile);
            }
            $saving =  $this->documentRepository->delete($documentId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function detail($documentId)
    {
        return view('pages.'.config('variables.templateName').'.dokumen.detail', [
            'data' => $this->findById($documentId),
        ]);
    }
}