<?php

namespace App\Services\File;

use App\Enums\JenisAksesEnum;
use App\Enums\RoleEnum;
use App\Enums\UnitKerjaEnum;
use App\Enums\UnitKerjaRoleEnum;
use App\Models\BerkasPegawai;
use App\Models\Document;
use App\Models\File;
use App\Models\Folder;
use App\Models\Pegawai;
use App\Models\PegawaiFile;
use App\Models\SK;
use App\Models\SuratKeluar;
use App\Models\SuratMasuk;
use App\Models\UnitKerja;
use App\Models\User;
use App\Repositories\File\FileRepositoryInterface;
use App\Services\FolderStorage\FolderStorageServiceInterface;
use App\Services\JenisAkses\JenisAksesServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\FileFilter;
use App\Traits\Mutators\EncryptId;
use App\Traits\Mutators\JenisAkses;
use App\Traits\ProcessFile;
use App\Traits\UnitKerjaRole;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File as FileFacades;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Role;

use function PHPUnit\Framework\isNull;

class FileService implements FileServiceInterface
{
    use ProcessFile, EncryptId, DecryptId, UnitKerjaRole, FileFilter;
    private $fileRepository, $jenisAksesService, $folderStorageService;

    public function __construct(
        FileRepositoryInterface $fileRepository,
        JenisAksesServiceInterface $jenisAksesService,
        FolderStorageServiceInterface $folderStorageService,
    )
    {
        $this->fileRepository       = $fileRepository;
        $this->jenisAksesService    = $jenisAksesService;
        $this->folderStorageService = $folderStorageService;
    }

    public function allTypeFiles()
    {
        $berkas = DB::table('berkas_pegawai')
        ->select('berkas_pegawai.id', 'berkas_pegawai.file_id')
        ->selectRaw('berkas_pegawai.nama as nama')
        ->selectRaw('berkas_pegawai.nama as perihal')
        ->selectRaw("CONCAT('Berkas Pegawai') as kategori")
        ->selectRaw("CONCAT('berkas-pegawai') as tipeFile")
        ->selectRaw('users.name as creator')
        ->join('users', 'berkas_pegawai.created_by', '=', 'users.id')
        ->join('files', 'berkas_pegawai.file_id', '=', 'files.id');

        $documents = DB::table('documents')
        ->select('documents.id', 'documents.file_id')
        ->selectRaw('documents.nama as nama')
        ->selectRaw('documents.nama as perihal')
        ->selectRaw("CONCAT('Dokumen') as kategori")
        ->selectRaw("CONCAT('dokumen') as tipeFile")
        ->selectRaw('users.name as creator')
        ->join('users', 'documents.created_by', '=', 'users.id')
        ->join('files', 'documents.file_id', '=', 'files.id');
        
        $suratMasuk = DB::table('surat_masuk')
        ->select('surat_masuk.id', 'surat_masuk.file_id')
        ->selectRaw('surat_masuk.nomor as nama')
        ->selectRaw('surat_masuk.perihal as perihal')
        ->selectRaw("CONCAT('Surat Masuk') as kategori")
        ->selectRaw("CONCAT('surat-masuk') as tipeFile")
        ->selectRaw('users.name as creator')
        ->join('users', 'surat_masuk.created_by', '=', 'users.id')
        ->join('files', 'surat_masuk.file_id', '=', 'files.id');

        $suratKeluar = DB::table('surat_keluar')
        ->select('surat_keluar.id', 'surat_keluar.file_id')
        ->selectRaw("CONCAT(surat_keluar.nomor,'/UN22.8/',kode_naskahs.kode,'/',surat_keluar.tahun) as nama")
        ->selectRaw('surat_keluar.perihal as perihal')
        ->selectRaw("CONCAT('Surat Keluar') as kategori")
        ->selectRaw("CONCAT('surat-keluar') as tipeFile")
        ->selectRaw('users.name as creator')
        ->join('users', 'surat_keluar.created_by', '=', 'users.id')
        ->join('files', 'surat_keluar.file_id', '=', 'files.id')
        ->join('kode_naskahs', 'surat_keluar.kode_naskah_id', '=', 'kode_naskahs.id');

        $sk = DB::table('sk')
        ->select('sk.id', 'sk.file_id')
        ->selectRaw("CONCAT(sk.nomor,'/',kode_units.kode,'/',kode_naskahs.kode,'/',sk.tahun) as nama")
        ->selectRaw('sk.judul as perihal')
        ->selectRaw("CONCAT('SK') as kategori")
        ->selectRaw("CONCAT('sk') as tipeFile")
        ->selectRaw('users.name as creator')
        ->join('users', 'sk.created_by', '=', 'users.id')
        ->join('files', 'sk.file_id', '=', 'files.id')
        ->join('kode_units', 'sk.kode_unit_id', '=', 'kode_units.id')
        ->join('kode_naskahs', 'sk.kode_naskah_id', '=', 'kode_naskahs.id')
        ->union($berkas)
        ->union($documents)
        ->union($suratMasuk)
        ->union($suratKeluar)
        ->orderByRaw('file_id DESC')
        ->get();

        $datas = PegawaiFile::when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                $value == RoleEnum::SUPERADMIN->value ||
                $value == RoleEnum::ADMIN->value 
                // $value == RoleEnum::DEKAN->value ||
                // $value == RoleEnum::WD->value ||
                // $value == RoleEnum::KTU->value ||
                // $value == RoleEnum::SUBKOOR->value
            );
        }) == FALSE), function($query) {
            $query->whereIn('pegawai_id', $this->pegawais());
            $query->orWhereIn('unit_kerja_id', $this->unitKerjas());
            $query->orWhereIn('created_by', $this->creators());
        })
        ->orderByRaw(3)
        ->get()
        ->pluck('file_id')->toArray();

        // ! Sementara menggunakan Role
        // $unitKerjaFile = PegawaiFile::when((auth()->user()->getRoleNames()->contains(function($value, $key) {
        //     return (
        //         $value == RoleEnum::SUPERADMIN->value ||
        //         $value == RoleEnum::ADMIN->value ||
        //         $value == RoleEnum::DEKAN->value ||
        //         $value == RoleEnum::WD->value ||
        //         $value == RoleEnum::KTU->value ||
        //         $value == RoleEnum::SUBKOOR->value
        //     );
        // }) == FALSE), function($query) {
        //     // $unitKerjas = DB::table('unit_role')->whereIn('role_id', auth()->user()->roles()->pluck('id'))->pluck('unit_kerja_id');
        //     $query->whereIn('unit_kerja_id', auth()->user()->roles()->pluck('id'));
        // })
        // ->orderByRaw(3)
        // ->get();

        // $datas = new \Illuminate\Database\Eloquent\Collection; //Create empty collection which we know has the concat() method
        // $datas = $datas->concat($pegawaiFile);
        // $datas = $datas->concat($unitKerjaFile);
        // $datas = $datas->pluck('file_id')->toArray();
        $data   = [];
        $no     = 0;
        foreach ($sk as $key => $item) {
            if(in_array($item->file_id, $datas))
            {
                $data[$no]['no'] = 1+($no++);
                $data[$no]['idSecret']  = $this->encrypt($item->id);
                $data[$no]['id']        = $item->id;
                $data[$no]['perihal']   = $item->perihal;
                $data[$no]['nama']      = $item->nama;
                $data[$no]['kategori']  = $item->kategori;
                $data[$no]['tipeFile']  = $item->tipeFile;
                $data[$no]['creator']   = $item->creator;
            }
            
        }
        return $data;
    }

    public function getAll()
    {
        return $this->fileRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->fileRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->fileRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($fileId)
    {
        return $this->fileRepository->findById($fileId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->fileRepository->whereBy($key, $value);
    }
    
    public function store(array $fileData)
    {
        DB::beginTransaction();
        try {
            // ! Proses Save File
            if ($fileData['attachment']) {
                $file = $this->detailFile($fileData['attachment']);
                $fileData['mime_type']  = $file['mime_type'];
                $fileData['size']       = $file['size'];
                $fileData['extension']  = $file['extension'];
            }
            $fileData['nama_file'] = $this->generateName($fileData['nama_file']).'.'.$fileData['extension'];
            $saving = $this->fileRepository->store(Arr::only($fileData, ['nama_file', 'extension', 'mime_type', 'size', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'created_by']));
            if (!$saving) {
                throw new Exception($saving);
            }
            // ! Proses Simpan Relasi Pegawai Id 
            // $getPegawais = $this->jenisAksesService->getPegawais(Arr::only($fileData, ['jenis_akses_id', 'role_id', 'pegawai_id']));
            $this->jenisAksesService->storeRelation($saving, $fileData['ids'], $fileData['jenis_akses_id'],  $fileData['created_by']);
            // ! Proses Simpan Relasi Box Id 
            if ($fileData['box_id']) {
                foreach ($fileData['box_id'] as $value) {
                    $saving->boxes()->attach($value, ['created_by' => auth()->id()]);
                }
            }
            // ! Proses Upload File
            if ($fileData['attachment']) {
                $folderStorage = $saving->folderStorage ?? $this->folderStorageService->findById($saving->folder_storage_id);
                Storage::disk($folderStorage->disk)
                    ->put(strtolower($folderStorage->jenisAkses->nama).'/'.$folderStorage->path.'/'.$fileData['nama_file'], 
                    FileFacades::get($fileData['attachment']) 
                );            
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($fileId, array $newFile)
    {
        DB::beginTransaction();
        try {
            $data = $this->fileRepository->findById($fileId);
            // ! Proses Save File
            if (isset($newFile['attachment'])) {
                $file = $this->detailFile($newFile['attachment']);
                $newFile['mime_type']  = $file['mime_type'];
                $newFile['size']       = $file['size'];
                $newFile['extension']  = $file['extension'];
            } else {
                $newFile['extension']  = $data->extension;
            }
            // ! Cek Perubahan Nama File
            if($newFile['cek_perubahan_nama'])
            {
                $newFile['nama_file'] = $this->generateName($newFile['nama_file']).'.'.$newFile['extension'];
            } else {
                if ($newFile['extension'] != $data->extension) {
                    $pecahNama = explode('.', $data->nama_file);
                    $ambilNama = $pecahNama[0];
                    $newFile['nama_file'] = $ambilNama.'.'.$newFile['extension'];
                } else {
                    $newFile['nama_file'] = $data->nama_file;
                }
            }
            $saving = $this->fileRepository->update($fileId, Arr::only($newFile, ['nama_file', 'extension', 'mime_type', 'size', 'folder_storage_id', 'folder_id', 'jenis_akses_id']));
            if (!$saving) {
                throw new Exception($saving);
            }
           // ! Proses Sync Pegawai Id 
            // $getPegawais = $this->jenisAksesService->getPegawais(Arr::only($newFile, ['jenis_akses_id', 'role_id', 'pegawai_id']));
            // $this->jenisAksesService->updateRelation($data, $getPegawais, auth()->id());
            $this->jenisAksesService->updateRelation($saving, $newFile['ids'], $newFile['jenis_akses_id'], $saving->created_by ?? auth()->id());
            // ! Proses Simpan Relasi Box Id 
            if (isset($newFile['box_id'])) {
                $data->boxes()->syncWithPivotValues($newFile['box_id'], ['created_by' => $saving->created_by ?? auth()->id()]);
            }
            // ! Proses Update File
            $upload             = true;
            $folderStorage      = $saving->folderStorage ?? $this->folderStorageService->findById($saving->folder_storage_id);
            $fileBaru           = strtolower($folderStorage->jenisAkses->nama).'/'.$folderStorage->path.'/'.$newFile['nama_file'];

            $folderStorageLama  = $data->folderStorage ?? $this->folderStorageService->findById($data->folder_storage_id);
            $fileLama           = strtolower($folderStorageLama->jenisAkses->nama).'/'.$folderStorageLama->path.'/'.$data->nama_file;
            if (isset($newFile['attachment'])) {
                if(Storage::disk($folderStorageLama->disk)->exists($fileLama))
                {
                    Storage::disk($folderStorageLama->disk)->delete($fileLama);          
                }
                $upload = Storage::disk($folderStorage->disk)->put(
                    $fileBaru, 
                    FileFacades::get($newFile['attachment'])
                );  
            } else {
                if($newFile['cek_perubahan_nama'])
                {
                    if(Storage::disk($folderStorageLama->disk)->exists($fileLama))
                    {
                        $upload = Storage::disk($folderStorageLama->disk)->move($fileLama, $fileBaru);          
                    }
                }
            }
            if (!$upload) {
                throw new Exception($upload);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($fileId)
    {
        DB::beginTransaction();
        try {
            $data           = $this->fileRepository->findById($fileId);
            $folderStorage  = $data->folderStorage ?? $this->folderStorageService->findById($data->folder_storage_id);
            $file           = strtolower($folderStorage->jenisAkses->nama).'/'.$folderStorage->path.'/'.$data->nama_file;
            if(Storage::disk($folderStorage->disk)->exists($file))
            {
                Storage::disk($folderStorage->disk)->delete($file);          
            }
            $saving = $this->fileRepository->delete($fileId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function userByTim($roles)
    {
        return User::role($roles)->pluck('id');
    }

    public function unitFolder()
    {
        return view('pages.'.config('variables.templateName').'.file.folder.unit-folder', [
            'folders' => UnitKerja::whereIsFolder(1)->get(['id','nama'])->map(function($item) {
                $datas['name']        = $item->nama;
                if (in_array($item->nama, ['DOSEN', 'TENAGA KEPENDIDIKAN', 'MITRA'])) {
                    $datas['total']       = $this->getPegawaiFile($item->roles()->pluck('name'))->count();
                } else if (in_array($item->nama, ['LABORATORIUM', 'RUANG BACA'])) {
                    $datas['total']       = $this->getPegawaiFile($item->roles()->pluck('name'), true)->count();
                } else {
                    $datas['total']       = $this->getData($item->roles()->pluck('name'))->count();
                }
                $datas['idSecret']    = $item->idSecret;
                return $datas;
            }),
        ]);
    }

    // ! Folder
    public function folder($idSecret, $idUnitKerja = NULL)
    {
        $idParent       = $idSecret ? $this->decrypt($idSecret) : 0;
        $idUnitKerja    = $this->decrypt($idUnitKerja);
        $cekFolder      = Folder::find($idParent);
        if($cekFolder && ($idUnitKerja == 2 || $idUnitKerja == 10))
        { 
            $folder = Pegawai::whereStatus($cekFolder->nama)->when($idUnitKerja, function($query) use ($idUnitKerja) {
                switch ($idUnitKerja) {
                    case 2:
                        $query->where('team_id', '!=', 4);
                        break;

                    case 10:
                        $query->where('team_id', '=', 4);
                        break;
                }
            })
            ->get(['id', 'firstname', 'middlename', 'lastname'])->map(function($item) use($cekFolder, $idParent) {
                $folder['name']         = $item->fullName;
                $isFolderUtama          = $cekFolder->parent_utama == 'NULL' ? true : false;
                $folderId               = $isFolderUtama ? $idParent : false;
                $folder['total']        = $this->getDataByPegawai($folderId, $item->id, $isFolderUtama)->count();
                $folder['idPegawai']    = $item->idSecret;
                return $folder;
            });
            $namaFile = 'pegawai-folder';
        } else {
            $folder = Folder::whereUnitKerjaId($idUnitKerja)->whereParentId($idParent)->get(['id','nama','unit_kerja_id','created_by'])->map(function($item) {
                $folder['idFolder'] = $item->id;
                $folder['name']     = $item->nama;
                $isFolderUtama      = $item->parent_utama == 'NULL' ? true : false;
                if (in_array($item->nama, ['PNS', 'PPPK', 'KONTRAK', 'PHL', 'OUTSOURCING', 'ASISTEN'])) {
                    switch ($item->unitKerja->nama) {
                        case 'TENAGA KEPENDIDIKAN':
                            $rolesName = [
                                RoleEnum::KTU->value,
                                RoleEnum::SUBKOOR->value,
                                RoleEnum::AKADEMIK->value,
                                RoleEnum::KEPEGAWAIAN->value,
                                RoleEnum::KEUANGAN->value,
                                RoleEnum::UMPER->value,
                                RoleEnum::ADJUR->value,
                                RoleEnum::LABORAN->value,
                                RoleEnum::RUANGBACA->value,
                                RoleEnum::TIK->value,
                            ];
                            break;
                        
                        default:
                            $rolesName = [RoleEnum::DOSEN->value];
                            break;
                    }
                    $folder['total']    = $this->getPegawaiFile($rolesName, false, $item->nama)->count();
                } else {
                    $folder['total']    = $this->getData($item->id, true, $isFolderUtama)->count();
                }
                $folder['idSecret'] = $item->idSecret;
                return $folder;
            });
            $namaFile = 'folder';
        }
        $kembaliId = 0;
        if ($cekFolder) {
            $kembaliId  = $cekFolder->parent_id ? $cekFolder->parent_id_secret : 0;
        }
        return view('pages.'.config('variables.templateName').'.file.folder.'.$namaFile, [
            'kembaliId' => $kembaliId ?? 'testes',
            'folderId'  => $idSecret ?? 0,
            'folders'   => $folder,
            'namaFolder'=> $idParent ? ($cekFolder ? $cekFolder->nama : DB::table('unit_kerjas')->find($idUnitKerja)->nama) : DB::table('unit_kerjas')->find($idUnitKerja)->nama,
        ]);
    }
    // ! Folder Semua Pegawai
    public function pegawaiFolder($idSecret, $idUnitKerja = NULL)
    {
        return 'pegawai folder';
    }
    // ! Folder dari Pegawai
    public function folderPegawai($parentFolderPegawai, $idPegawai, $idUnitKerja = NULL)
    {
        // return $this->decrypt($parentFolderPegawai); 1637
        $pegawaiId      = $idPegawai ? $this->decrypt($idPegawai) : 0;
        $idUnitKerja    = $this->decrypt($idUnitKerja);
        $folders = DB::table('unit_folder')->where('unit_kerja_id', $idUnitKerja)->get()->map(function($item) use ($pegawaiId) {
            $folder = Folder::find($item->folder_id);
            $data['name']   = $folder->nama;
            $data['total']  = $this->getDataByPegawai($folder->id, $pegawaiId, true)->count();
            $data['idFolder']  = $folder->idSecret;
            return $data;
        });
        
        return view('pages.'.config('variables.templateName').'.file.folder.folder-pegawai', [
            'kembaliId'     => $parentFolderPegawai ?? 0,
            'folders'       => $folders,
            'pegawaiId'     => $idPegawai,
            'namaPegawai'    => strtoupper(Pegawai::find($pegawaiId)->full_name_with_title ?? ''),
        ]);
    }

    public function dataPegawai($idSecret, $idPegawai)
    {
        $folderId = $this->decrypt($idSecret);
        $idPegawai = $this->decrypt($idPegawai);
        return view('pages.'.config('variables.templateName').'.file.folder.pegawai', [
            'kembaliId'     => $idSecret,
            'namaFolder'    => strtoupper(DB::table('folders')->find($folderId)->nama),
        ]);
    }

    public function getPegawaiFile($rolesName, $isOperator = false, $status = false)
    {
        $userIds = User::select('id', 'pegawai_id')
        ->when($isOperator, function($query) {
            $query->whereOperator(1);
        })
        ->when($status, function($query) use($status) {
            $query->whereHas('pegawai', function($pegawai) use($status) {
                $pegawai->whereStatus($status);
            });
        })
        ->role($rolesName)->pluck('pegawai_id');
        return DB::table('pegawai_file')->whereIn('pegawai_id', $userIds)->get()->groupBy('file_id');
    }

    public function kembali($idSecret, $idUnitKerja = NULL)
    {
        $folderId   = $this->decrypt($idSecret);
        $cekFolder  = Folder::find($folderId);
        $idUnitKerja = $idUnitKerja ? $this->decrypt($idUnitKerja) : NULL;
        // ! Jika Subfolder
        $folder     = Folder::when($idUnitKerja != NULL, function($query) use ($idUnitKerja) {
            $query->whereUnitKerjaId($idUnitKerja);
        })
        ->when($cekFolder, function($query) use ($folderId) {
            $query->whereParentId($folderId);
        })
        ->when(!$cekFolder, function($query) {
            $query->whereParentId(0);
        })
        ->get(['id','nama','unit_kerja_id','created_by'])->map(function($item) {
            $folder['name']     = $item->nama;
            if (in_array($item->nama, ['PNS', 'PPPK', 'KONTRAK', 'PHL', 'OUTSOURCING', 'ASISTEN'])) {
                switch ($item->unitKerja->nama) {
                    case 'TENAGA KEPENDIDIKAN':
                        $rolesName = [
                            RoleEnum::KTU->value,
                            RoleEnum::SUBKOOR->value,
                            RoleEnum::AKADEMIK->value,
                            RoleEnum::KEPEGAWAIAN->value,
                            RoleEnum::KEUANGAN->value,
                            RoleEnum::UMPER->value,
                            RoleEnum::ADJUR->value,
                            RoleEnum::LABORAN->value,
                            RoleEnum::RUANGBACA->value,
                            RoleEnum::TIK->value,
                        ];
                        break;
                    
                    default:
                        $rolesName = [RoleEnum::DOSEN->value];
                        break;
                }
                $folder['total']    = $this->getPegawaiFile($rolesName, false, $item->nama)->count();
            } else {
                $folder['total']    = $this->getData($item->id, true)->count();
            }
            $folder['idSecret'] = $item->idSecret;
            return $folder;
        });
        $kembaliId = 0;
        if ($cekFolder) {
            $kembaliId  = $cekFolder->parent_id ? $cekFolder->parent_id_secret : 0;
        }
        return view('pages.'.config('variables.templateName').'.file.folder.folder', [
            'kembaliId' => $kembaliId,
            'folderId'  => $idSecret,
            'folders'   => $folder,
            'namaFolder'=> $cekFolder ? $cekFolder->nama : (DB::table('unit_kerjas')->find($idUnitKerja)->nama ?? '') ,

        ]);

    }

    // ! Get Data Total 
    // ? $value = Folder Id / Role Id
    // ? FolderId untuk get data berdasarkan folder
    public function getData($value, $isFolderId=false, $isFolderUtama=false)
    {
        $columnNULL = false;
        switch ($isFolderId) {
            case true:
                $columnName = 'folder_id';
                if ($value == NULL) {
                    $columnNULL = true;
                }
                $columnValue = Folder::find($value)->allChildren()->pluck('id');
                $columnValue[] = $value;
                break;
            
            default:
                $userId = User::select('id','operator')->whereOperator(1)->role($value)->pluck('id');
                $columnName = 'created_by';
                $columnValue = $userId;
                break;
        }
        $berkasPegawai = BerkasPegawai::with('file.folder')->whereHas('file', function($q) use ($columnName, $columnValue, $columnNULL, $isFolderUtama) {
            if ($columnNULL) {
                $q->whereNuLL($columnName);
            } else {
                $q->whereIn($columnName, $columnValue);
            }
            if ($isFolderUtama) {
                $q->orWhereHas('folder', function($notFolderUtama) use($columnValue) {
                    $notFolderUtama->whereIn('parent_utama', $columnValue);
                });
            }
        })
        ->get(['id', 'nama', 'file_id', 'kategori_id'])->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama;
            $map['kategori'] = 'Berkas Pegawai';
            $map['tipeFile'] = 'berkas-pegawai';
            return $map;
        });

        $document = Document::whereHas('file', function($q) use ($columnName, $columnValue, $columnNULL, $isFolderUtama) {
            if ($columnNULL) {
                $q->whereNuLL($columnName);
            } else {
                $q->whereIn($columnName, $columnValue);
            }
            if ($isFolderUtama) {
                $q->orWhereHas('folder', function($notFolderUtama) use($columnValue) {
                    $notFolderUtama->whereIn('parent_utama', $columnValue);
                });
            }
            $q->whereHas('creator', function($operator) {
                $operator->whereOperator(1);
            });
        })
        ->get(['id', 'nama', 'file_id'])->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama ;
            $map['kategori'] = 'Dokumen';
            $map['tipeFile'] = 'dokumen';
            return $map;
        });

        $suratMasuk = SuratMasuk::whereHas('file', function($q) use ($columnName, $columnValue, $columnNULL, $isFolderUtama) {
            if ($columnNULL) {
                $q->whereNuLL($columnName);
            } else {
                $q->whereIn($columnName, $columnValue);
            }
            if ($isFolderUtama) {
                $q->orWhereHas('folder', function($notFolderUtama) use($columnValue) {
                    $notFolderUtama->whereIn('parent_utama', $columnValue);
                });
            }
        })
        ->get(['id', 'nomor', 'perihal', 'file_id'])->map(function($map){
            $map['nomor'] = $map->nomor;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Masuk';
            $map['tipeFile'] = 'surat-masuk';
            return $map;
        });

        $suratKeluar = SuratKeluar::whereHas('file', function($q) use ($columnName, $columnValue, $columnNULL, $isFolderUtama) {
            if ($columnNULL) {
                $q->whereNuLL($columnName);
            } else {
                $q->whereIn($columnName, $columnValue);
            }
            if ($isFolderUtama) {
                $q->orWhereHas('folder', function($notFolderUtama) use($columnValue) {
                    $notFolderUtama->whereIn('parent_utama', $columnValue);
                });
            }
        })
        ->get(['id', 'nomor', 'perihal', 'tahun', 'kode_naskah_id', 'file_id'])->map(function($map){
            $map['nomor'] = $map->nomorSurat;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Keluar';
            $map['tipeFile'] = 'surat-keluar';
            return $map;
        });

        $SK = SK::whereHas('file', function($q) use ($columnName, $columnValue, $columnNULL, $isFolderUtama) {
            if ($columnNULL) {
                $q->whereNuLL($columnName);
            } else {
                $q->whereIn($columnName, $columnValue);
            }
            if ($isFolderUtama) {
                $q->orWhereHas('folder', function($notFolderUtama) use($columnValue) {
                    $notFolderUtama->whereIn('parent_utama', $columnValue);
                });
            }
        })
        ->get(['id', 'nomor', 'judul', 'tahun', 'kode_unit_id', 'kode_naskah_id', 'file_id'])->map(function($map){
            $map['nomor'] = $map->nomorSk;
            $map['judul'] = $map->judul;
            $map['kategori'] = 'SK';
            $map['tipeFile'] = 'sk';
            return $map;
        });
        $datas = new \Illuminate\Database\Eloquent\Collection; //Create empty collection which we know has the merge() method
        $datas = $datas->concat($berkasPegawai);
        $datas = $datas->concat($suratMasuk);
        $datas = $datas->concat($suratKeluar);
        $datas = $datas->concat($SK);
        $datas = $datas->concat($document);
        
        return $datas;
    }

    public function getDataByPegawai($folderId, $pegawaiId=false, $isFolderUtama=false)
    {
        $berkasPegawai = BerkasPegawai::with('file.folder')->whereHas('file', function($files) use($pegawaiId, $folderId, $isFolderUtama) {
            $files->when($folderId, function($folder) use($folderId, $isFolderUtama) {
                $folder->whereHas('folder', function($query) use ($folderId, $isFolderUtama){
                    $query->where('id', $folderId);
                    if ($isFolderUtama) {
                        $query->orWhere('parent_utama', $folderId);
                    }
                });
            })->whereHas('pegawais', function($query) use ($pegawaiId){
                    $query->where('pegawais.id', $pegawaiId);
                });
            })
        ->get(['id', 'nama', 'file_id', 'kategori_id'])->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama;
            $map['kategori'] = 'Berkas Pegawai';
            $map['tipeFile'] = 'berkas-pegawai';
            return $map;
        });

        $document = Document::with('file.folder')->whereHas('file', function($files) use($pegawaiId, $folderId, $isFolderUtama) {
            $files->when($folderId, function($folder) use($folderId, $isFolderUtama) {
                $folder->whereHas('folder', function($query) use ($folderId, $isFolderUtama){
                    $query->where('id', $folderId);
                    if ($isFolderUtama) {
                        $query->orWhere('parent_utama', $folderId);
                    }
                });
            })->whereHas('pegawais', function($query) use ($pegawaiId){
                    $query->where('pegawais.id', $pegawaiId);
                });
            })
        ->get(['id', 'nama', 'file_id'])->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama ;
            $map['kategori'] = 'Dokumen';
            $map['tipeFile'] = 'dokumen';
            return $map;
        });

        $suratMasuk = SuratMasuk::with('file.folder')->whereHas('file', function($files) use($pegawaiId, $folderId, $isFolderUtama) {
            $files->when($folderId, function($folder) use($folderId, $isFolderUtama) {
                $folder->whereHas('folder', function($query) use ($folderId, $isFolderUtama){
                    $query->where('id', $folderId);
                    if ($isFolderUtama) {
                        $query->orWhere('parent_utama', $folderId);
                    }
                });
            })->whereHas('pegawais', function($query) use ($pegawaiId){
                    $query->where('pegawais.id', $pegawaiId);
                });
            })
        ->get(['id', 'nomor', 'perihal', 'file_id'])->map(function($map){
            $map['nomor'] = $map->nomor;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Masuk';
            $map['tipeFile'] = 'surat-masuk';
            return $map;
        });

        $suratKeluar = SuratKeluar::with('file.folder')->whereHas('file', function($files) use($pegawaiId, $folderId, $isFolderUtama) {
            $files->when($folderId, function($folder) use($folderId, $isFolderUtama) {
                $folder->whereHas('folder', function($query) use ($folderId, $isFolderUtama){
                    $query->where('id', $folderId);
                    if ($isFolderUtama) {
                        $query->orWhere('parent_utama', $folderId);
                    }
                });
            })->whereHas('pegawais', function($query) use ($pegawaiId){
                    $query->where('pegawais.id', $pegawaiId);
                });
            })
        ->get(['id', 'nomor', 'perihal', 'tahun', 'kode_naskah_id', 'file_id'])->map(function($map){
            $map['nomor'] = $map->nomorSurat;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Keluar';
            $map['tipeFile'] = 'surat-keluar';
            return $map;
        });

        $SK = SK::with('file.folder')->whereHas('file', function($files) use($pegawaiId, $folderId, $isFolderUtama) {
            $files->when($folderId, function($folder) use($folderId, $isFolderUtama) {
                $folder->whereHas('folder', function($query) use ($folderId, $isFolderUtama){
                    $query->where('id', $folderId);
                    if ($isFolderUtama) {
                        $query->orWhere('parent_utama', $folderId);
                    }
                });
            })->whereHas('pegawais', function($query) use ($pegawaiId){
                    $query->where('pegawais.id', $pegawaiId);
                });
            })
        ->get(['id', 'nomor', 'judul', 'tahun', 'kode_unit_id', 'kode_naskah_id', 'file_id'])->map(function($map){
            $map['nomor'] = $map->nomorSk;
            $map['judul'] = $map->judul;
            $map['kategori'] = 'SK';
            $map['tipeFile'] = 'sk';
            return $map;
        });
        $datas = new \Illuminate\Database\Eloquent\Collection; //Create empty collection which we know has the merge() method
        $datas = $datas->concat($berkasPegawai);
        $datas = $datas->concat($suratMasuk);
        $datas = $datas->concat($suratKeluar);
        $datas = $datas->concat($SK);
        $datas = $datas->concat($document);
        
        return $datas;
    }

    // ! Pencarian
    public function cari($kataKunci = false, $idUnitKerja = NULL, $idFolder = NULL, $idPegawai = NULL)
    {
        $kataKunci = $kataKunci == 'false' ? false : $kataKunci;
        $datas = $this->prosesCari($kataKunci, $idUnitKerja, $idFolder);
        return view('components.'.config('variables.templateName').'.pages.file.cari.dataTables', [
            'datas'     => $datas,
            'kataKunci' => $kataKunci,
            'total'     => count($datas),
        ]);
    }

    public function prosesCari($kataKunci = false, $idUnitKerja = false, $idFolder = false)
    {
        if ($idFolder) {
            $idFolder = $this->decrypt($idFolder);
        }
        if ($idUnitKerja) {
            $idUnitKerja = $this->decrypt($idUnitKerja);
        }
        $berkasPegawai = BerkasPegawai::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nama', 'like', '%'.$kataKunci.'%');
        })
        ->when($idUnitKerja, function($query) use($idUnitKerja, $idFolder) {
            $roles = UnitKerja::find($idUnitKerja)->roles;
            $folder = Folder::find($idFolder);
            $users = User::when($folder && $folder->prodi_id != NULL, function($query) use ($folder) {
                $query->whereHas('pegawai', function($prodi) use ($folder) {
                    $prodi->whereProdiId($folder->prodi_id);
                });
            })->role($roles)->get()->pluck(['id']);
            $query->whereIn('created_by', $users);
        })
        ->when($idFolder, function($query) use($idFolder) {
            $query->whereHas('file', function($que) use($idFolder) {
                $que->whereFolderId($idFolder);
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama;
            $map['kategori'] = 'Berkas Pegawai';
            $map['tipeFile'] = 'berkas-pegawai';
            return $map;
        });

        $document = Document::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nama', 'like', '%'.$kataKunci.'%');
        })
        ->when($idUnitKerja, function($query) use($idUnitKerja, $idFolder) {
            $roles = UnitKerja::find($idUnitKerja)->roles;
            $folder = Folder::find($idFolder);
            $users = User::whereOperator(1)->when($folder && $folder->prodi_id != NULL, function($query) use ($folder) {
                $query->whereHas('pegawai', function($prodi) use ($folder) {
                    $prodi->whereProdiId($folder->prodi_id);
                });
            })->role($roles)->get()->pluck(['id']);
            $query->whereIn('created_by', $users);
        })
        ->when($idFolder, function($query) use($idFolder) {
            $query->whereHas('file', function($que) use($idFolder) {
                $que->whereFolderId($idFolder);
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama;
            $map['kategori'] = 'Dokumen';
            $map['tipeFile'] = 'dokumen';
            return $map;
        });

        $suratMasuk = SuratMasuk::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nomor', 'like', '%'.$kataKunci.'%')->orWhere('perihal', 'like', '%'.$kataKunci.'%');
        })
        ->when($idUnitKerja, function($query) use($idUnitKerja, $idFolder) {
            $roles = UnitKerja::find($idUnitKerja)->roles;
            $folder = Folder::find($idFolder);
            $users = User::when($folder && $folder->prodi_id != NULL, function($query) use ($folder) {
                $query->whereHas('pegawai', function($prodi) use ($folder) {
                    $prodi->whereProdiId($folder->prodi_id);
                });
            })->role($roles)->get()->pluck(['id']);
            $query->whereIn('created_by', $users);
        })
        ->when($idFolder, function($query) use($idFolder) {
            $query->whereHas('file', function($que) use($idFolder) {
                $que->whereFolderId($idFolder);
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomor;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Masuk';
            $map['tipeFile'] = 'surat-masuk';
            return $map;
        });

        $suratKeluar = SuratKeluar::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nomor', 'like', '%'.$kataKunci.'%')->orWhere('perihal', 'like', '%'.$kataKunci.'%');
        })
        ->when($idUnitKerja, function($query) use($idUnitKerja, $idFolder) {
            $roles = UnitKerja::find($idUnitKerja)->roles;
            $folder = Folder::find($idFolder);
            $users = User::when($folder && $folder->prodi_id != NULL, function($query) use ($folder) {
                $query->whereHas('pegawai', function($prodi) use ($folder) {
                    $prodi->whereProdiId($folder->prodi_id);
                });
            })->role($roles)->get()->pluck(['id']);
            $query->whereIn('created_by', $users);
        })
        ->when($idFolder, function($query) use($idFolder) {
            $query->whereHas('file', function($que) use($idFolder) {
                $que->whereFolderId($idFolder);
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomorSurat;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Keluar';
            $map['tipeFile'] = 'surat-keluar';
            return $map;
        });

        $SK = SK::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nomor', 'like', '%'.$kataKunci.'%')->orWhere('judul', 'like', '%'.$kataKunci.'%');
        })
        ->when($idUnitKerja, function($query) use($idUnitKerja, $idFolder) {
            $roles = UnitKerja::find($idUnitKerja)->roles;
            $folder = Folder::find($idFolder);
            $users = User::when($folder && $folder->prodi_id != NULL, function($query) use ($folder) {
                $query->whereHas('pegawai', function($prodi) use ($folder) {
                    $prodi->whereProdiId($folder->prodi_id);
                });
            })->role($roles)->get()->pluck(['id']);
            $query->whereIn('created_by', $users);
        })
        ->when($idFolder, function($query) use($idFolder) {
            $query->whereHas('file', function($que) use($idFolder) {
                $que->whereFolderId($idFolder);
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomorSk;
            $map['judul'] = $map->judul;
            $map['kategori'] = 'SK';
            $map['tipeFile'] = 'sk';
            return $map;
        });
        
        $datas = new \Illuminate\Database\Eloquent\Collection; 
        //Create empty collection which we know has the merge() method
        $datas = $datas->concat($suratMasuk);
        $datas = $datas->concat($suratKeluar);
        $datas = $datas->concat($SK);
        $datas = $datas->concat($berkasPegawai);
        $datas = $datas->concat($document);
        return $datas;
    }

    // ! Cari Berdasarkan Pegawai
    public function cariByPegawai($kataKunci = false, $idFolder = NULL, $idPegawai = NULL)
    {
        $kataKunci = $kataKunci == 'false' ? false : $kataKunci;
        $datas = $this->prosesCariByPegawai($kataKunci, $idFolder, $idPegawai);
        return view('components.'.config('variables.templateName').'.pages.file.cari.dataTables', [
            'datas'     => $datas,
            'kataKunci' => $kataKunci,
            'total'     => count($datas),
        ]);
    }

    public function prosesCariByPegawai($kataKunci = false, $idFolder = false, $idPegawai = false)
    {
        if ($idFolder) {
            $idFolder = $this->decrypt($idFolder);
        }
        if ($idPegawai) {
            $idPegawai = $this->decrypt($idPegawai);
        }
        // return $this->unitKerjas($idPegawai);
        $berkasPegawai = BerkasPegawai::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nama', 'like', '%'.$kataKunci.'%');
        })
        ->whereHas('file', function($query) use($idPegawai, $idFolder) {
            $query->when($idFolder, function($folders) use($idFolder, $idPegawai) {
                $folders->whereHas('folder', function($folder) use ($idFolder, $idPegawai){
                    $folder->where('folders.id', $idFolder);
                    $folder->when($idPegawai, function($folderParent) use($idFolder) {
                        $folderParent->orWhere('parent_utama', $idFolder);
                    });
                });
            })
            ->when($idPegawai, function($pegawais) use($idPegawai) {
                $pegawais->whereHas('pegawais', function($pegawai) use ($idPegawai){
                    $pegawai->where('pegawais.id', $idPegawai);
                    // $pegawai->orWhere('created_by', $idPegawai);
                });
                // $pegawais->orWhereHas('unitKerjas', function($pegawai) use ($idPegawai){
                //     $pegawai->whereIn('unit_kerjas.id', $this->unitKerjas($idPegawai));
                // });
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama;
            $map['kategori'] = 'Berkas Pegawai';
            $map['tipeFile'] = 'berkas-pegawai';
            return $map;
        });

        $document = Document::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nama', 'like', '%'.$kataKunci.'%');
        })
        ->whereHas('file', function($query) use($idPegawai, $idFolder) {
            $query->when($idFolder, function($folders) use($idFolder, $idPegawai) {
                $folders->whereHas('folder', function($folder) use ($idFolder, $idPegawai){
                    $folder->where('folders.id', $idFolder);
                    $folder->when($idPegawai, function($folderParent) use($idFolder) {
                        $folderParent->orWhere('parent_utama', $idFolder);
                    });
                });
            })
            ->when($idPegawai, function($pegawais) use($idPegawai) {
                $pegawais->whereHas('pegawais', function($pegawai) use ($idPegawai){
                    $pegawai->where('pegawais.id', $idPegawai);
                    // $pegawai->orWhere('created_by', $idPegawai);
                });
                // $pegawais->orWhereHas('unitKerjas', function($pegawai) use ($idPegawai){
                //     $pegawai->whereIn('unit_kerjas.id', $this->unitKerjas($idPegawai));
                // });
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama;
            $map['kategori'] = 'Dokumen';
            $map['tipeFile'] = 'dokumen';
            return $map;
        });

        $suratMasuk = SuratMasuk::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nomor', 'like', '%'.$kataKunci.'%')->orWhere('perihal', 'like', '%'.$kataKunci.'%');
        })
        ->whereHas('file', function($query) use($idPegawai, $idFolder) {
            $query->when($idFolder, function($folders) use($idFolder, $idPegawai) {
                $folders->whereHas('folder', function($folder) use ($idFolder, $idPegawai){
                    $folder->where('folders.id', $idFolder);
                    $folder->when($idPegawai, function($folderParent) use($idFolder) {
                        $folderParent->orWhere('parent_utama', $idFolder);
                    });
                });
            })
            ->when($idPegawai, function($pegawais) use($idPegawai) {
                $pegawais->whereHas('pegawais', function($pegawai) use ($idPegawai){
                    $pegawai->where('pegawais.id', $idPegawai);
                    // $pegawai->orWhere('created_by', $idPegawai);
                });
                // $pegawais->orWhereHas('unitKerjas', function($pegawai) use ($idPegawai){
                //     $pegawai->whereIn('unit_kerjas.id', $this->unitKerjas($idPegawai));
                // });
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomor;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Masuk';
            $map['tipeFile'] = 'surat-masuk';
            return $map;
        });

        $suratKeluar = SuratKeluar::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nomor', 'like', '%'.$kataKunci.'%')->orWhere('perihal', 'like', '%'.$kataKunci.'%');
        })
        ->whereHas('file', function($query) use($idPegawai, $idFolder) {
            $query->when($idFolder, function($folders) use($idFolder, $idPegawai) {
                $folders->whereHas('folder', function($folder) use ($idFolder, $idPegawai){
                    $folder->where('folders.id', $idFolder);
                    $folder->when($idPegawai, function($folderParent) use($idFolder) {
                        $folderParent->orWhere('parent_utama', $idFolder);
                    });
                });
            })
            ->when($idPegawai, function($pegawais) use($idPegawai) {
                $pegawais->whereHas('pegawais', function($pegawai) use ($idPegawai){
                    $pegawai->where('pegawais.id', $idPegawai);
                    // $pegawai->orWhere('created_by', $idPegawai);
                });
                // $pegawais->orWhereHas('unitKerjas', function($pegawai) use ($idPegawai){
                //     $pegawai->whereIn('unit_kerjas.id', $this->unitKerjas($idPegawai));
                // });
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomorSurat;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Keluar';
            $map['tipeFile'] = 'surat-keluar';
            return $map;
        });

        $SK = SK::when($kataKunci, function($query) use($kataKunci) {
            $query->where('nomor', 'like', '%'.$kataKunci.'%')->orWhere('judul', 'like', '%'.$kataKunci.'%');
        })
        ->whereHas('file', function($query) use($idPegawai, $idFolder) {
            $query->when($idFolder, function($folders) use($idFolder, $idPegawai) {
                $folders->whereHas('folder', function($folder) use ($idFolder, $idPegawai){
                    $folder->where('folders.id', $idFolder);
                    $folder->when($idPegawai, function($folderParent) use($idFolder) {
                        $folderParent->orWhere('parent_utama', $idFolder);
                    });
                });
            })
            ->when($idPegawai, function($pegawais) use($idPegawai) {
                $pegawais->whereHas('pegawais', function($pegawai) use ($idPegawai){
                    $pegawai->where('pegawais.id', $idPegawai);
                    // $pegawai->orWhere('created_by', $idPegawai);
                });
                // $pegawais->orWhereHas('unitKerjas', function($pegawai) use ($idPegawai){
                //     $pegawai->whereIn('unit_kerjas.id', $this->unitKerjas($idPegawai));
                // });
            });
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomorSk;
            $map['judul'] = $map->judul;
            $map['kategori'] = 'SK';
            $map['tipeFile'] = 'sk';
            return $map;
        });
        
        $datas = new \Illuminate\Database\Eloquent\Collection; 
        //Create empty collection which we know has the merge() method
        $datas = $datas->concat($suratMasuk);
        $datas = $datas->concat($suratKeluar);
        $datas = $datas->concat($SK);
        $datas = $datas->concat($berkasPegawai);
        $datas = $datas->concat($document);
        return $datas;
    }
}