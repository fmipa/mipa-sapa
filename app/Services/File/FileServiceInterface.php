<?php

namespace App\Services\File;

interface FileServiceInterface 
{
    public function allTypeFiles();

    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($fileId);

    public function whereBy($key, $value);

    public function store(array $fileData);

    public function update($fileId, array $newFile);

    public function delete($fileId);

    public function unitFolder();

    public function folder($idSecret, $idUnitKerja);
    
    public function pegawaiFolder($idSecret, $idUnitKerja);

    public function folderPegawai($idPegawai, $idUnitKerja);

    public function dataPegawai($idSecret, $idPegawai);
    
    public function kembali($idSecret, $idUnitKerja);

    public function cari($kataKunci, $idUnitKerja, $idFolder);

    public function cariByPegawai($kataKunci, $idFolder, $idPegawai);
}