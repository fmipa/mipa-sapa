<?php

namespace App\Services\Folder;

interface FolderServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($folderId);

    public function whereBy($key, $value);

    public function whereMultiple($where);

    public function store(array $folderData);

    public function update($folderId, array $newFolder);

    public function delete($folderId);

    public function allChildren($folderId = false);

}