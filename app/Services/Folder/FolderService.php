<?php

namespace App\Services\Folder;

use App\Repositories\Folder\FolderRepositoryInterface;
use App\Services\Folder\FolderServiceInterface;
use Exception;

class FolderService implements FolderServiceInterface
{
    private $folderRepository;
    public function __construct(FolderRepositoryInterface $folderRepository)
    {
        $this->folderRepository = $folderRepository;
    }

    public function getAll()
    {
        return $this->folderRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->folderRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->folderRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($folderId)
    {
        return $this->folderRepository->findById($folderId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->folderRepository->whereBy($key, $value);
    }
    
    public function whereMultiple($where)
    {
        return $this->folderRepository->whereMultiple($where);
    }

    public function store(array $folderData)
    {
        try {
            $folderData['parent_id'] = $folderData['parent_id'] == NULL ? 0 : $folderData['parent_id'];
            $saving = $this->folderRepository->store($folderData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($folderId, array $newFolder)
    {
        try {
            $saving = $this->folderRepository->update($folderId, $newFolder);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($folderId)
    {
        try {
            $saving =  $this->folderRepository->delete($folderId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }

    public function allChildren($folderId = false)
    {
        return $this->folderRepository->allChildren($folderId);
    }
    
}