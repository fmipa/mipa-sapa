<?php

namespace App\Services\Pegawai;

use App\Repositories\Pegawai\PegawaiRepositoryInterface;
use App\Services\User\UserServiceInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class PegawaiService implements PegawaiServiceInterface
{
    public function __construct(
        protected PegawaiRepositoryInterface $pegawaiRepository,
        protected UserServiceInterface $userService,
    )
    {
    }

    public function getAll()
    {
        return $this->pegawaiRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        
        $dataPegawais = $this->pegawaiRepository->allWithOrder($column, $order);
        $sorted = $dataPegawais->sortBy('full_name_with_title');
        return $pegawais = $sorted->values()->all();
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->pegawaiRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($pegawaiId)
    {
        return $this->pegawaiRepository->findById($pegawaiId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->pegawaiRepository->whereBy($key, $value);
    }
    
    public function store(array $pegawaiData)
    {
        try {
            DB::beginTransaction();
            $saving = $this->pegawaiRepository->store(Arr::only($pegawaiData, [
                'firstname',
                'middlename',
                'lastname',
                'gelar_depan',
                'gelar_belakang',
                'nomor_unik',
                'tempat_lahir',
                'tanggal_lahir',
                'agama_id',
                'prodi_id',
                'jenis_kelamin',
                'status',
            ]));
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            $pegawaiData['pegawai_id'] = $saving->id;
            $pegawaiData['remember_token'] = Str::random(20);
            $savingUser = $this->userService->store(Arr::only($pegawaiData, ['pegawai_id', 'username', 'name', 'password', 'email', 'role_id', 'active', 'remember_token']));
            if (!$savingUser) {
                $this->pegawaiRepository->delete($saving->id);
                throw new Exception($savingUser);
            }
            $data = collect($saving)->merge($savingUser);
            return $data->all();
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($pegawaiId, array $newPegawai)
    {
        try {
            DB::beginTransaction();
            $saving = $this->pegawaiRepository->update($pegawaiId, Arr::only($newPegawai, [
                'firstname',
                'middlename',
                'lastname',
                'gelar_depan',
                'gelar_belakang',
                'nomor_unik',
                'tempat_lahir',
                'tanggal_lahir',
                'agama_id',
                'prodi_id',
                'jenis_kelamin',
                'status',
                'created_by'
            ]));
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            $newPegawai['pegawai_id'] = $pegawaiId;
            $pegawaiData['remember_token'] = Str::random(20);
            $savingUser = $this->userService->update($newPegawai['user_id'], Arr::only($newPegawai, ['pegawai_id', 'username', 'name', 'password', 'email', 'role_id', 'active']));
            if (!$savingUser) {
                $this->pegawaiRepository->delete($saving->id);
                throw new Exception($savingUser);
            }
            $data = collect($saving)->merge($savingUser);
            return $data->all();
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($pegawaiId)
    {
        try {
            DB::beginTransaction();
            $saving =  $this->pegawaiRepository->delete($pegawaiId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    // ! PROSES E-RAPAT
    public function exportERapat()
    {        
        $dataPegawais = $this->pegawaiRepository->getAll(true)->map(function($dataPegawai) {
            $pegawai['user_id'] = $dataPegawai->user->id;
            $pegawai['pegawai_id'] = $dataPegawai->id;
            $pegawai['nip'] = $dataPegawai->nomor_unik;
            $pegawai['nidk'] = NULL;
            $pegawai['nama'] = $dataPegawai->full_name_with_title;
            $pegawai['username'] = $dataPegawai->user->username;
            $pegawai['password'] = $dataPegawai->user->password;
            $pegawai['tanggal_lahir'] = $dataPegawai->tanggal_lahir;
            $pegawai['jabatan'] = $this->masterJabatan($dataPegawai->user->roles()->pluck('name')->toarray(), $dataPegawai->prodi_id, $dataPegawai->user->id);
            $pegawai['bidang_studi'] = $this->masterProdi(strtoupper($dataPegawai->prodi->nama));
            $pegawai['jenis_kelamin'] = $dataPegawai->jenis_kelamin;
            return $pegawai;
        });
        return $dataPegawais;
    }

    public function masterJabatan($jabatans, $prodiId = false, $id = false){
        if (in_array('dekan', $jabatans)) {
            $jabatan = "Dekan Fakultas";
        } else if(in_array('wakil-dekan', $jabatans)) {
            $jabatan = match ($id) {
                70 => 'Wakil Dekan Bidang Akademik',
                49 => 'Wakil Dekan Bidang Umum dan Keuangan',
                136 => 'Wakil Dekan Kemahasiswaan dan Alumni'
            };
        } else if(in_array('dosen', $jabatans)) {
            if(in_array('ketua-jurusan', $jabatans)) {
                $jabatan = match ($prodiId) {
                    1,4,9,10 => 'Ketua Jurusan/Prodi '. $this->nameProdi($prodiId),
                    default => 'Ketua Jurusan '. $this->nameJurusan($prodiId),
                };
            } else if(in_array('ketua-prodi ', $jabatans)) {
                $jabatan = 'Ketua Prodi '. $this->nameProdi($prodiId);
            } else {
                $jabatan = 'Dosen '. $this->nameProdi($prodiId);
            }
            $jabatan = $jabatan;
        } else if(in_array('ktu', $jabatans)) {
            $jabatan = "Koordinator Tata Usaha";
        } else if(in_array('sub-koordinator', $jabatans)) {
            $jabatan = "Sub Koordinator Akademik dan Kemahasiswaan";
        } else if(in_array('admin-jurusan', $jabatans)) {
            $jabatan = 'Operator Jurusan';
        } else if(in_array('kepegawaian', $jabatans)) {
            $jabatan = match ($id) {
                7 => 'Operator Fakultas',
                default => 'Tenaga Kependidikan',
            };
        } else if(in_array('akademik', $jabatans)) {
            $jabatan = match ($id) {
                24,47 => 'Operator Fakultas',
                default => 'Tenaga Kependidikan',
            };
        } else if(in_array('umper', $jabatans)) {
            $jabatan = match ($id) {
                5,20 => 'Operator Fakultas',
                default => 'Tenaga Kependidikan',
            };
        } else {
            $jabatan = 'Tenaga Kependidikan';
        }

        return $jabatan;
    }

    public function nameProdi($prodiId) {
        return match ($prodiId) {
            1 => 'Biologi',
            2 => 'Fisika',
            3 => 'Geofisika',
            4 => 'Ilmu Kelautan',
            5 => 'S1 Kimia',
            6 => 'S2 Kimia',
            7 => 'Matematika',
            8 => 'Statistika',
            9 => 'Rekayasa Sistem Komputer',
            10 => 'Sistem Informasi',
            default => 'Fakultas',
        };
    }

    public function nameJurusan($prodiId) {
        return match ($prodiId) {
            1 => 'Biologi',
            2 => 'Fisika',
            3 => 'Fisika',
            4 => 'Ilmu Kelautan',
            5 => 'Kimia',
            6 => 'Kimia',
            7 => 'Matematika',
            8 => 'Matematika',
            9 => 'Rekayasa Sistem Komputer',
            10 => 'Sistem Informasi',
            default => 'Fakultas',
        };
    }

    public function masterProdi($prodiName) {
        return match ($prodiName) {
            "BIOLOGI" => "Jurusan/Prodi Biologi",
            "FISIKA" => "Program Studi Fisika",
            "GEOFISIKA" => "Program Studi Geofisika",
            "ILMU KELAUTAN" => "Jurusan/Prodi Ilmu Kelautan",
            "MATEMATIKA" => "Program Studi Matematika",
            "KIMIA" => "Program Studi S1 Kimia",
            "KIMIA S2" => "Program Studi S2 Kimia",
            "REKAYASA SISTEM KOMPUTER" => "Jurusan/Prodi Rekayasa Sistem Komputer",
            "SISTEM INFORMASI" => "Jurusan/Prodi Sistem Informasi",
            "STATISTIKA" => "Program Studi Statistika",
            default => "Fakultas MIPA",
        };
    }

    // ! END PROSES E-RAPAT

}