<?php

namespace App\Services\KodeNaskah;

use App\Repositories\KodeNaskah\KodeNaskahRepositoryInterface;
use Exception;

class KodeNaskahService implements KodeNaskahServiceInterface
{
    private $kodeNaskahRepository;
    public function __construct(KodeNaskahRepositoryInterface $kodeNaskahRepository)
    {
        $this->kodeNaskahRepository = $kodeNaskahRepository;
    }

    public function getAll()
    {
        return $this->kodeNaskahRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->kodeNaskahRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->kodeNaskahRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($kodeNaskahId)
    {
        return $this->kodeNaskahRepository->findById($kodeNaskahId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->kodeNaskahRepository->whereBy($key, $value);
    }
    
    public function store(array $kodeNaskahData)
    {
        try {
            $saving = $this->kodeNaskahRepository->store($kodeNaskahData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($kodeNaskahId, array $newKodeNaskah)
    {
        try {
            $saving = $this->kodeNaskahRepository->update($kodeNaskahId, $newKodeNaskah);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($kodeNaskahId)
    {
        try {
            $saving =  $this->kodeNaskahRepository->delete($kodeNaskahId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}