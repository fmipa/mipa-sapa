<?php

namespace App\Services\User;

use App\Repositories\User\UserRepositoryInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class UserService implements UserServiceInterface
{
    private $userRepository;
    public function __construct(UserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAll()
    {
        return $this->userRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order  = $orderby ?? 'asc';
        return $this->userRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->userRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($userId)
    {
        return $this->userRepository->findById($userId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->userRepository->whereBy($key, $value);
    }
    
    public function store(array $userData)
    {
        try {
            DB::beginTransaction();
            $user = $this->userRepository->store($userData);
            if (!$user) {
                throw new Exception($user);
            }
            if ($userData['active']) {
                $user->activation()->create(['user_id' => $user->id, 'kode' => Str::random(20)]);
            }
            if ($userData['role_id']) {
                $user->assignRole($userData['role_id']);
            }
            DB::commit();
            return $user;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($userId, array $newUser)
    {
        try {
            $newData = [
                'username'  => $newUser['username'],
                'email'     => $newUser['email'],
            ];
            if (Arr::exists($newUser, 'name') && $newUser['name']) {
                $newData['name'] = $newUser['name'];
            }
            if (Arr::exists($newUser, 'password') && $newUser['password']) {
                $newData['password'] = $newUser['password'];
            }
            DB::beginTransaction();
            $user = $this->userRepository->update($userId, $newData);
            if (!$user) {
                throw new Exception($user);
            }
            if (Arr::exists($newUser, 'active') && $newUser['active']) {
                if ($user->is_active == false) {
                    $user->activation()->create(['user_id' => $user->id, 'kode' => Str::random(40)]);
                }
            } else {
                $user->activation()->delete();
            }
            if (Arr::exists($newUser, 'role_id') && $newUser['role_id']) {
                $roles  = Role::whereIn('id', $newUser['role_id'])->pluck('name');
                $user->syncRoles($roles);
            }
            DB::commit();
            return $user;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($userId)
    {
        try {
            DB::beginTransaction();
            $user = $this->userRepository->findById($userId);
            $user->activation()->delete();
            $saving = $this->userRepository->delete($userId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
}