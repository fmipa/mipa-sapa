<?php

namespace App\Services\KodeUnit;

interface KodeUnitServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($kodeUnitId);

    public function whereBy($key, $value);

    public function store(array $kodeUnitData);

    public function update($kodeUnitId, array $newKodeUnit);

    public function delete($kodeUnitId);
}