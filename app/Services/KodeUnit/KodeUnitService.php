<?php

namespace App\Services\KodeUnit;

use App\Repositories\KodeUnit\KodeUnitRepositoryInterface;
use Exception;

class KodeUnitService implements KodeUnitServiceInterface
{
    private $kodeUnitRepository;
    public function __construct(KodeUnitRepositoryInterface $kodeUnitRepository)
    {
        $this->kodeUnitRepository = $kodeUnitRepository;
    }

    public function getAll()
    {
        return $this->kodeUnitRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->kodeUnitRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->kodeUnitRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($kodeUnitId)
    {
        return $this->kodeUnitRepository->findById($kodeUnitId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->kodeUnitRepository->whereBy($key, $value);
    }
    
    public function store(array $kodeUnitData)
    {
        try {
            $saving = $this->kodeUnitRepository->store($kodeUnitData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($kodeUnitId, array $newKodeUnit)
    {
        try {
            $saving = $this->kodeUnitRepository->update($kodeUnitId, $newKodeUnit);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($kodeUnitId)
    {
        try {
            $saving =  $this->kodeUnitRepository->delete($kodeUnitId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
}