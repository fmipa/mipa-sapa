<?php

namespace App\Services\SK;

use App\Repositories\SK\SKRepositoryInterface;
use App\Services\File\FileServiceInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SKService implements SKServiceInterface
{
    private $SKRepository, $fileService;

    public function __construct(
        SKRepositoryInterface $SKRepository,
        FileServiceInterface $fileService,
    )
    {
        $this->SKRepository = $SKRepository;
        $this->fileService  = $fileService;
    }

    public function getAll()
    {
        return $this->SKRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order  = $orderby ?? 'asc';
        return $this->SKRepository->allWithOrder($column, $order)->map(function($item) {
            return $item;
        });
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->SKRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($SKId)
    {
        return $this->SKRepository->findById($SKId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->SKRepository->whereBy($key, $value);
    }
    
    public function store(array $SKData)
    {
        DB::beginTransaction();
        try {
            $SKData['nama_file'] = $SKData['nomor_sk'];
            $fileSave = $this->fileService->store(Arr::only($SKData, ['attachment', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'created_by', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $SKData['file_id'] = $fileSave->id;
            $saving = $this->SKRepository->store(Arr::only($SKData, ['file_id', 'nomor', 'judul', 'tahun', 'kode_unit_id', 'kode_naskah_id', 'created_by']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($SKId, array $newSK)
    {
        DB::beginTransaction();
        try {
            $data = $this->SKRepository->findById($SKId);
            // ! Cek Perubahan Nomor Surat untuk pengecekan Nama File
            if ($data->nomor_sk == $newSK['nomor_sk'])
            {
                $newSK['nama_file'] = $data->file->nama_file;
                $newSK['cek_perubahan_nama'] = false;
            } else {
                $newSK['nama_file'] = $newSK['nomor_sk'];
                $newSK['cek_perubahan_nama'] = true;
            }
            $fileSave = $this->fileService->update($data->file_id, Arr::only($newSK, ['attachment', 'cek_perubahan_nama', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $newSK['file_id'] = $fileSave->id;
            $saving = $this->SKRepository->update($SKId, Arr::only($newSK, ['file_id', 'nomor', 'judul', 'tahun', 'kode_unit_id', 'kode_naskah_id', 'created_by']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($SKId)
    {
        DB::beginTransaction();
        try {
            $data       = $this->SKRepository->findById($SKId);
            $deleteFile =  $this->fileService->delete($data->file_id);
            if (!$deleteFile) {
                throw new Exception($deleteFile);
            }
            $saving =  $this->SKRepository->delete($SKId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    public function detail($SKId)
    {
        return view('pages.'.config('variables.templateName').'.sk.detail', [
            'data' => $this->findById($SKId),
        ]);
    }
    
}