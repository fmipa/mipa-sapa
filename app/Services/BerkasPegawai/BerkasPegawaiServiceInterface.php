<?php

namespace App\Services\BerkasPegawai;

interface BerkasPegawaiServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($berkasPegawaiId);

    public function whereBy($key, $value);

    public function store(array $berkasPegawaiData);

    public function update($berkasPegawaiId, array $newBerkasPegawai);

    public function delete($berkasPegawaiId);

    public function detail($berkasPegawaiId);
}