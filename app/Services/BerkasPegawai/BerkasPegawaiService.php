<?php

namespace App\Services\BerkasPegawai;

use App\Repositories\BerkasPegawai\BerkasPegawaiRepositoryInterface;
use App\Services\File\FileServiceInterface;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class BerkasPegawaiService implements BerkasPegawaiServiceInterface
{
    private $berkasPegawaiRepository, $fileService;

    public function __construct(
        BerkasPegawaiRepositoryInterface $berkasPegawaiRepository,
        FileServiceInterface $fileService,
    )
    {
        $this->berkasPegawaiRepository = $berkasPegawaiRepository;
        $this->fileService      = $fileService;
    }

    public function getAll()
    {
        return $this->berkasPegawaiRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->berkasPegawaiRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->berkasPegawaiRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($berkasPegawaiId)
    {
        return $this->berkasPegawaiRepository->findById($berkasPegawaiId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->berkasPegawaiRepository->whereBy($key, $value);
    }
    
    public function store(array $berkasPegawaiData)
    {
        DB::beginTransaction();
        try {
            $berkasPegawaiData['nama_file'] = $berkasPegawaiData['nama'];
            $fileSave = $this->fileService->store(Arr::only($berkasPegawaiData, ['attachment', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'created_by', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $berkasPegawaiData['file_id'] = $fileSave->id;
            $saving = $this->berkasPegawaiRepository->store(Arr::only($berkasPegawaiData, ['file_id', 'nama', 'kategori_id', 'created_by']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($berkasPegawaiId, array $newBerkasPegawai)
    {
        DB::beginTransaction();
        try {
            $data = $this->berkasPegawaiRepository->findById($berkasPegawaiId);
            // ! Cek Perubahan Nomor Surat untuk pengecekan Nama File
            if ($data->nama == $newBerkasPegawai['nama'])
            {
                $newBerkasPegawai['nama_file'] = $data->file->nama_file;
                $newBerkasPegawai['cek_perubahan_nama'] = false;
            } else {
                $newBerkasPegawai['nama_file'] = $newBerkasPegawai['nama'];
                $newBerkasPegawai['cek_perubahan_nama'] = true;
            }
            $fileSave = $this->fileService->update($data->file_id, Arr::only($newBerkasPegawai, ['attachment', 'cek_perubahan_nama', 'nama_file', 'folder_storage_id', 'folder_id', 'jenis_akses_id', 'unit_kerja_id', 'pegawai_id', 'box_id', 'ids']));
            if (!$fileSave) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($fileSave);
            }
            $newBerkasPegawai['file_id'] = $fileSave->id;
            $saving = $this->berkasPegawaiRepository->update($berkasPegawaiId, Arr::only($newBerkasPegawai, ['file_id', 'nama', 'kategori_id']));
            if (!$saving) {
                $this->fileService->delete($fileSave->id);
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($berkasPegawaiId)
    {
        DB::beginTransaction();
        try {
            $data       = $this->berkasPegawaiRepository->findById($berkasPegawaiId);
            $deleteFile =  $this->fileService->delete($data->file_id);
            if (!$deleteFile) {
                throw new Exception($deleteFile);
            }
            $saving =  $this->berkasPegawaiRepository->delete($berkasPegawaiId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    public function detail($berkasPegawaiId)
    {
        return view('pages.'.config('variables.templateName').'.berkas-pegawai.detail', [
            'data' => $this->findById($berkasPegawaiId),
        ]);
    }
    
}