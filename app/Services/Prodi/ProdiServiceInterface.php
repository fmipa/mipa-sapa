<?php

namespace App\Services\Prodi;

interface ProdiServiceInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($prodiId);

    public function whereBy($key, $value);

    public function store(array $prodiData);

    public function update($prodiId, array $newProdi);

    public function delete($prodiId);
}