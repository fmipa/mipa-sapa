<?php

namespace App\Services\Prodi;

use App\Repositories\Prodi\ProdiRepositoryInterface;
use Exception;

class ProdiService implements ProdiServiceInterface
{
    private $prodiRepository;
    public function __construct(ProdiRepositoryInterface $prodiRepository)
    {
        $this->prodiRepository = $prodiRepository;
    }

    public function getAll()
    {
        return $this->prodiRepository->getAll();
    }

    public function allWithOrder($key, $orderby)
    {
        $column = $key ?? 'id';
        $order = $orderby ?? 'asc';
        return $this->prodiRepository->allWithOrder($column, $order);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->prodiRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($prodiId)
    {
        return $this->prodiRepository->findById($prodiId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->prodiRepository->whereBy($key, $value);
    }
    
    public function store(array $prodiData)
    {
        try {
            $saving = $this->prodiRepository->store($prodiData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($prodiId, array $newProdi)
    {
        try {
            $saving = $this->prodiRepository->update($prodiId, $newProdi);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($prodiId)
    {
        try {
            $saving =  $this->prodiRepository->delete($prodiId);
            if (!$saving) {
                throw new Exception($saving);
            }
            return $saving;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
}