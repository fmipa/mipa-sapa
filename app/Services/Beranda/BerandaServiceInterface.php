<?php

namespace App\Services\Beranda;

interface BerandaServiceInterface 
{
    public function indexPimpinan();

    public function indexOperator();
}