<?php

namespace App\Services\Beranda;

use App\Enums\RoleEnum;
use App\Models\File;
use App\Models\Pegawai;
use App\Models\PegawaiFile;
use App\Models\User;
use App\Repositories\Beranda\BerandaRepositoryInterface;
use App\Services\Beranda\BerandaServiceInterface;
use App\Traits\FileFilter;
use DateInterval;
use DatePeriod;
use DateTime;
use Exception;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class BerandaService implements BerandaServiceInterface
{
    use FileFilter;
    
    public function __construct()
    {
    }

    // ! Pimpinan
    public function indexPimpinan()
    {
        // ! TOTAL
        $data['totalFile']          = DB::table('files')->select('id')->count();
        $data['totalSK']            = DB::table('sk')->select('id')->count();
        $data['totalSuratMasuk']    = DB::table('surat_masuk')->select('id')->count();
        $data['totalSuratKeluar']   = DB::table('surat_keluar')->select('id')->count();
        $data['totalBerkasPegawai'] = DB::table('berkas_pegawai')->select('id')->count();
        $data['totalDokumen']       = DB::table('documents')->select('id')->count();
        // ! TOTAL HARI INI
        $data['totalSKToday']               = DB::table('sk')->select('id')->whereDate('created_at', date('Y-m-d', strtotime(' -1 day')))->count();
        $data['totalSuratMasukToday']       = DB::table('surat_masuk')->select('id')->whereDate('created_at', date('Y-m-d', strtotime(' -1 day')))->count();
        $data['totalSuratKeluarToday']      = DB::table('surat_keluar')->select('id')->whereDate('created_at', date('Y-m-d', strtotime(' -1 day')))->count();
        $data['totalBerkasPegawaiToday']    = DB::table('berkas_pegawai')->select('id')->whereDate('created_at', date('Y-m-d', strtotime(' -1 day')))->count();
        $data['totalDokumenToday']          = DB::table('documents')->select('id')->whereDate('created_at', date('Y-m-d', strtotime(' -1 day')))->count();
        // ! GRAFIK UPLOAD PERHARI
        $today      = date('Y-m-d');
        $datePrev   = date('Y-m-d', strtotime(' -10 day'));
        if (config('variables.templateName') == 'scoxe') {
            $data['uploadPerHari'] = $this->formatGrafikHarianScoxe( $this->datePeriod('files', $datePrev, $today));
        } else {
            $data['uploadPerHari'] = $this->formatGrafikHarianSneat( $this->datePeriod('files', $datePrev, $today));
        }
        
        // ! TOTAL PERUNIT
        $roles = DB::table('roles')
                ->select('name')
                ->whereNotIn('name', ['super-admin', 'admin', 'mitra', 'dekan', 'wakil-dekan', 'ketua-jurusan', 'ketua-prodi', 'sekretaris-jurusan', 'ktu', 'sub-koordinator', 'tik'])
                ->orderBy('name')->get();
        $user = User::query();
        $userRoles = [];
        foreach ($roles as $key => $value) {
            $userRoles[$value->name] = $user->select('id')->clone()->role($value->name)->pluck('id')->toArray();
        }
        $fileRoles = [];
        $file = DB::table('files');
        foreach ($userRoles as $key => $value) {
            $nameKey = ucwords(str_replace('-', ' ', $key));
            $fileRoles[$key]['nama'] = $nameKey;
            $fileRoles[$key]['total'] = $file->select('id')->clone()->whereIn('created_by', $value)->count();
            $fileRoles[$key]['hari_ini'] = $file->select('id')->clone()->whereIn('created_by', $value)->whereDate('created_at', date('Y-m-d'))->count();
        }
        usort($fileRoles, function ($a, $b) {
            return strnatcmp($b['total'], $a['total']);
        });
        $data['dataRoles'] = $this->formatGrafikUnit(array_slice($fileRoles, 0, 4));
        // ! RANKING OPERTATOR
        // $data['ranking-operator'] = DB::table('files')
        //     ->select(DB::raw('created_by as operator'), DB::raw('count(*) as count'))
        //     ->groupBy('operator ')->get();

        return $data;
    }
    
    public function datePeriod($table, $from, $to)
    {
        $period = new DatePeriod( new DateTime($from), new DateInterval('P1D'), new DateTime($to));
        $dbData = [];

        foreach($period as $date){
            $range[$date->format("Y-m-d")] = 0;
        }

        $data = DB::table($table)
                    ->select(DB::raw('DATE(created_at) as time'), DB::raw('count(*) as count'))
                    ->whereDate('created_at', '>=', date($from).' 00:00:00')
                    ->whereDate('created_at', '<=', date($to).' 00:00:00')
                    ->groupBy('time')
                    ->get();

        foreach($data as $val){
            $dbData[$val->time] = $val->count;
        }

        return $data = array_replace($range, $dbData);

        // return json_encode($data);
    }

    public function formatGrafikHarianScoxe($datas)
    {
        $no = 0;
        $data = [];
        foreach ($datas as $key => $value) {
            $data[$no]['tanggal'] = $key;  
            $data[$no]['total'] = $value;  
            $no++;
        }

        return json_encode($data);
    }

    public function formatGrafikHarianSneat($datas)
    {
        $no = 0;
        $data = [];
        foreach ($datas as $key => $value) {
            $data['tanggal'][$no] = $key;  
            $data['total'][$no] = $value;  
            $no++;
        }
        return $data;
        return json_encode($data);
    }

    public function formatGrafikUnit($datas)
    {
        $no = 0;
        $data = [];
        $warna = ['#31cb72', '#ff5b5b', '#f1c31c', '#19c0ea'];
        foreach ($datas as $value) {
            $data[$no]['ranking']   = ($no + 1); 
            $data[$no]['unit']      = $value['nama'];  
            $data[$no]['warna']     = $warna[$no];  
            $data[$no]['total']     = $value['total'];  
            $data[$no]['hari_ini']  = $value['hari_ini'];  
            $no++;
        }

        return $data;
    }

    // ! Operator
    public function indexOperator()
    {
        // ! Total Berkas
        $pegawaiFile = PegawaiFile::query();
        $totalTag = $pegawaiFile->clone()->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                $value == RoleEnum::SUPERADMIN->value ||
                $value == RoleEnum::ADMIN->value ||
                $value == RoleEnum::DEKAN->value ||
                $value == RoleEnum::WD->value ||
                $value == RoleEnum::KTU->value ||
                $value == RoleEnum::SUBKOOR->value
            );
        }) == FALSE), function($query) {
            $query->where('pegawai_id', auth()->user()->pegawai_id);
            $query->orWhereIn('unit_kerja_id', $this->unitKerjas());
        });
        
        $data['totalTag'] = $totalTag->clone()->count();
        
        $totalUpload = $pegawaiFile->clone()->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                $value == RoleEnum::SUPERADMIN->value ||
                $value == RoleEnum::ADMIN->value ||
                $value == RoleEnum::DEKAN->value ||
                $value == RoleEnum::WD->value ||
                $value == RoleEnum::KTU->value ||
                $value == RoleEnum::SUBKOOR->value
            );
        }) == FALSE), function($query) {
            $query->where('created_by', auth()->id());
        })
        ->distinct('file_id');
        $data['totalUpload'] = $totalUpload->clone()->count();

        // ! Berkas Terbaru
        $today      = date('Y-m-d');
        $datePrev   = date('Y-m-d', strtotime(' -150 day'));
        $data['fileTerbaru'] = File::whereBetween('created_at', [$datePrev." 00:00:00", $today." 23:59:59"])->get(['id', 'jenis_akses_id', 'folder_id', 'folder_storage_id', 'created_by', 'created_at']);

        // ! Grafik
        $bulan = [ 'januari','februari','maret','april','mei','juni','juli','agustus','september','oktober','november','desember'];
        foreach ($bulan as $key => $value) {
            $data['grafikTag'][$value] = $totalTag->clone()->whereHas('file', function($query) use($key) { 
                $query->whereYear('created_at', '2023')->whereMonth('created_at', ($key+1));
            })
            ->get();
            $data['totalUpload'][$value] = $totalUpload->clone()->whereHas('file', function($query) use($key) { 
                $query->whereYear('created_at', '2023')->whereMonth('created_at', ($key+1));
            })
            ->get();
        }
        return $data['totalUpload'];
    }
}