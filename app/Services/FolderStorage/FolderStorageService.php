<?php

namespace App\Services\FolderStorage;

use App\Models\User;
use App\Repositories\FolderStorage\FolderStorageRepositoryInterface;
use App\Services\FolderStorage\FolderStorageServiceInterface;
use App\Services\JenisAkses\JenisAksesServiceInterface;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class FolderStorageService implements FolderStorageServiceInterface
{
    private $folderStorageRepository, $jenisAksesService;
    public function __construct(
        FolderStorageRepositoryInterface $folderStorageRepository,
        JenisAksesServiceInterface $jenisAksesService,
    )
    {
        $this->folderStorageRepository  = $folderStorageRepository;
        $this->jenisAksesService        = $jenisAksesService;
    }

    public function getAll()
    {
        return $this->folderStorageRepository->getAll();
    }

    public function allWithOrder($key, $orderby, $columns)
    {
        $key    = $key ?? 'id';
        $order  = $orderby ?? 'asc';
        $column = $columns ?? ['*'];
        return $this->folderStorageRepository->allWithOrder($key, $order, $column);
    }

    public function allWithFomat(array $format)
    {
        try {
            return $this->folderStorageRepository->allWithFomat($format);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function findById($folderStorageId)
    {
        return $this->folderStorageRepository->findById($folderStorageId);
    }
    
    public function whereBy($key, $value)
    {
        return $this->folderStorageRepository->whereBy($key, $value);
    }
    
    public function store(array $folderStorageData)
    {
        DB::beginTransaction();
        try {
            $saving = $this->folderStorageRepository->store(Arr::only($folderStorageData, ['nama', 'jenis_akses_id', 'path', 'disk', 'created_by']));
            if (!$saving) {
                throw new Exception($saving);
            }
            // ! Proses Simpan Relasi Pegawai Id 
            $getPegawais = $this->jenisAksesService->getPegawais(Arr::only($folderStorageData, ['jenis_akses_id', 'role_id', 'pegawai_id']));
            $this->jenisAksesService->storeRelation($saving, $getPegawais, auth()->id());
            // ! Proses Buat FolderStorage
            $jenisAkses = DB::table('jenis_akses')->select('id', 'nama')->where('id', $folderStorageData['jenis_akses_id'])->first()->nama;
            $dir        = $jenisAkses . '/' . $folderStorageData['path'];
            Storage::disk($folderStorageData['disk'])->makeDirectory($dir);
            
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($folderStorageId, array $newFolderStorage)
    {
        DB::beginTransaction();
        try {
            $data = $this->folderStorageRepository->findById($folderStorageId);
            
            // ! Directory Lama
            $jenisAksesId   = $data->jenis_akses_id;
            $path           = $data->path;
            $directory      = Str::lower($data->jenisAkses->nama) . '/' . $data->path;
            $newDir         = $directory;
            // ! Jika Jenis Akses Berubah
            if($newFolderStorage['jenis_akses_id'] != $data->jenis_akses_id)
            {
                $jenisAksesId   = $newFolderStorage['jenis_akses_id'];
                $path           = $newFolderStorage['path'];
                $jenisAkses = DB::table('jenis_akses')->select('id', 'nama')->where('id', $jenisAksesId)->first()->nama;
                $newDir     = $jenisAkses . '/' . $path ;
                if ($data->path != $path) {
                    Storage::disk($newFolderStorage['disk'])->move($directory, $newDir);
                }
                // ! Remove Directory
                Storage::disk($newFolderStorage['disk'])->deleteDirectory($directory);
            } else {
                $newFolderStorage['path'] = $data->path;
            }
            $saving = $this->folderStorageRepository->update($folderStorageId, Arr::only($newFolderStorage, ['nama', 'jenis_akses_id', 'path', 'disk']));
            if (!$saving) {
                throw new Exception($saving);
            }
            // ! Proses Sync Pegawai Id 
            $getPegawais = $this->jenisAksesService->getPegawais(Arr::only($newFolderStorage, ['jenis_akses_id', 'role_id', 'pegawai_id']));
            $this->jenisAksesService->updateRelation($data, $getPegawais, auth()->id());
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($folderStorageId)
    {
        DB::beginTransaction();
        try {
            $data       = $this->folderStorageRepository->findById($folderStorageId);
            $directory  = Str::lower($data->jenisAkses->nama) . '/' . $data->path;
            // ! Remove Directory
            Storage::deleteDirectory($directory);
            // ! Remove Data
            $saving = $this->folderStorageRepository->delete($folderStorageId);
            if (!$saving) {
                throw new Exception($saving);
            }
            DB::commit();
            return $saving;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }

    public function tim($roles)
    {
        return User::role($roles)->pluck('pegawai_id');
    }
    
}