<?php

namespace App\Traits;

use App\Enums\FolderAdminJurusanEnum;
use App\Enums\RoleEnum;

trait JurusanFolder
{
    // ! Get PRODI ID jika role admin jurusan, laboran dan dosen
    public function cekProdi($prodi_id)
    {
        if(auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::ADJUR->value ||
                    $value == RoleEnum::LABORAN->value ||
                    $value == RoleEnum::DOSEN->value
                );
        }) == TRUE)
        {
            return $prodi_id != NULL ? $this->decrypt((string) $prodi_id) : auth()->user()->pegawai->prodi_id;
        } else {
            return $prodi_id != NULL ? $this->decrypt((string) $prodi_id) : NULL; 
        }
    }

    // ! Get PARENT FOLDER ID jika role admin jurusan, laboran dan dosen
    public function cekDefaultFolder($folder_id)
    {
        if(auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::ADJUR->value ||
                    $value == RoleEnum::LABORAN->value ||
                    $value == RoleEnum::DOSEN->value
                );
        }) == TRUE)
        {
            return $folder_id != NULL ? $this->decrypt((string) $folder_id) : $this->adminJurusan($folder_id);
        } else {
            return $folder_id != NULL ? $this->decrypt((string) $folder_id) : NULL; 
        }
    }

    // ! Get default FOLDER ID Jurusan berdasarkan PRODI
    public function folderByProdi($nama = false)
    {
        $nama = $nama ?? auth()->user()->pegawai->prodi->nama;
        return match (strtoupper($nama)) {
            'BIOLOGI'        => 155,
            'FISIKA'         => 156,
            'GEOFISIKA'      => 157,
            'MATEMATIKA'     => 158,
            'STATISTIKA'     => 159,
            'ILMUKELAUTAN'   => 160,
            'KIMIA'          => 161,
            'KIMIAS2'        => 162,
            'REKAYASASISTEMKOMPUTER' => 163,
            'SISTEMINFORMASI'        => 165,
            'DEFAULT'        => NULL,
        };
    }

    // ! Get PARENT FOLDER ID Jurusan berdasarkan PRODI
    public function adminJurusan($nama = false)
    {
        $nama = $nama ?? auth()->user()->pegawai->prodi->nama;
        return match (str_replace(' ', '', strtoupper($nama))) {
            'BIOLOGI'        => 148,
            'FISIKA'         => 149,
            'GEOFISIKA'      => 149,
            'MATEMATIKA'     => 150,
            'STATISTIKA'     => 150,
            'ILMUKELAUTAN'   => 151,
            'KIMIA'          => 152,
            'KIMIAS2'        => 152,
            'REKAYASASISTEMKOMPUTER' => 153,
            'SISTEMINFORMASI'        => 154,
            'DEFAULT'        => NULL,
        };
    }

    // ! Get PARENT FOLDER ID Jurusan berdasarkan PRODI
    public function laboran($nama = false)
    {
        $nama = $nama ?? auth()->user()->pegawai->prodi->nama;
        return match (str_replace(' ', '', strtoupper($nama))) {
            'BIOLOGI'        => 165,
            'FISIKA'         => 166,
            'MATEMATIKA'     => 167,
            'ILMUKELAUTAN'   => 168,
            'KIMIA'          => 169,
            'REKAYASASISTEMKOMPUTER' => 170,
            'SISTEMINFORMASI'        => 171,
            'DEFAULT'        => NULL,
        };
    }

    // ! Get Parent ID Jurusan berdasarkan PRODI
    public function dosen($nama = false)
    {
        $nama = $nama ?? auth()->user()->pegawai->prodi->nama;
        return match (str_replace(' ', '', strtoupper($nama))) {
            'BIOLOGI'        => 172,
            'FISIKA'         => 173,
            'GEOFISIKA'      => 174,
            'MATEMATIKA'     => 175,
            'STATISTIKA'     => 176,
            'ILMUKELAUTAN'   => 177,
            'KIMIA'          => 178,
            'KIMIAS2'        => 179,
            'REKAYASASISTEMKOMPUTER' => 180,
            'SISTEMINFORMASI'        => 181,
            'DEFAULT'        => NULL,
        };
    }
}
