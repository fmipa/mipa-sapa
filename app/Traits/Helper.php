<?php

namespace App\Traits;

use App\Enums\AksesFileEnum;
use App\Traits\Mutators\EncryptId;
use Exception;
use Illuminate\Support\Arr;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

trait Helper
{   
    use EncryptId;

    public function acronym(string $string, $duplicate = false): String
    {
        $words = explode(" ", $string);
        $acronym = "";
        foreach ($words as $w) {
            $acronym .= mb_substr($w, 0, 1);
        }
        if ($duplicate) {
            $acronym .='-2';
        }
        return $acronym;
    }

    // ! Mengolah Data menjadi Enkripsi
    public function enumView($enum, $defaultKey = true , $encrypt = false, $removeValue = false): array
    {
        $data = [];
        $dataEnum = $enum;
        if($removeValue){
            $dataEnum = array_splice($enum, $removeValue);
        }
        foreach($dataEnum as $key => $result){
                $data[$key]['key']    = $encrypt ? $this->encrypt(($defaultKey ? $key+1 : $result)) : ($defaultKey ? $key+1 : $result);
                $data[$key]['text']   = $result;
            }
        return $data;
    }

    // ! Generate List Tahun
    public function generateTahun(int $tahunAwal, int $tahunAkhir, bool $sort = false, bool $blade = false): array
    {
        $generate = range($tahunAwal, $tahunAkhir ?? date('Y'));
        if ($sort) {
            $listTahun = rsort($generate);
        } else {
            $listTahun = $generate;
        }
        foreach ($generate as $key => $number) {
            if ($blade) {
                $tahuns[$key]['key']  = $number;
                $tahuns[$key]['text'] = $number;
            } else {
                $tahuns[] = $number;
            }
        }

        return $tahuns ?? [];
    }

}
