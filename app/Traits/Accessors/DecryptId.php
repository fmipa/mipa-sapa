<?php

namespace App\Traits\Accessors;

trait DecryptId
{
    public function decrypt(string $string, $secretKey = false): String
    {
        $output = false;
    
        $encrypt_method = "AES-256-CBC";
        // pls set your unique hashing key
        $secret_key = $secretKey ?? env('SECRET_KEY', 'mipa');
        $secret_iv = env('SECRET_UNIQUE', 'mipauntan');
    
        // hash
        $key = hash('sha256', $secret_key);
    
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
    
        //decrypt the given text/string/number
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
    
        return $output;
    }
}
