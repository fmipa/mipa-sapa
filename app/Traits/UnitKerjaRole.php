<?php

namespace App\Traits;

trait UnitKerjaRole
{   
    public function getRole($unitKerjaNama = '*'): array
    {
        return match ($unitKerjaNama) {
            'AKADEMIK'              => ['akademik'],
            'DOSEN'                 => ['dosen', 'dekan', 'wakil-dekan', 'ketua-jurusan', 'sekretaris-jurusan', 'ketua-prodi'],
            'JURUSAN'               => ['admin-jurusan'],
            'KEPEGAWAIAN'           => ['kepegawaian'],
            'KEUANGAN'              => ['keuangan'],
            'LABORATORIUM'          => ['laboran'],
            'MITRA'                 => ['mitra'],
            'RUANG BACA'            => ['ruang-baca'],
            'UMUM DAN PERLENGKAPAN' => ['umper'],
            'TENAGA KEPENDIDIKAN'   => ['ktu', 'sub-koordinator', 'admin-jurusan', 'kepegawaian', 'keuangan', 'laboran', 'ruang-baca', 'umper'],
            'SEMUA PEGAWAI'         => ['dosen', 'dekan', 'wakil-dekan', 'ketua-jurusan', 'sekretaris-jurusan', 'ketua-prodi', 'kepala-lab', 'ktu', 'sub-koordinator', 'admin-jurusan', 'kepegawaian', 'keuangan', 'laboran', 'ruang-baca', 'umper', 'tik'],
            'PIMPINAN FAKULTAS'     => ['dekan', 'wakil-dekan', 'ktu', 'sub-koordinator'],
            'KETUA JURUSAN'         => ['ketua-jurusan'],
            'SEKRETARIS JURUSAN'    => ['sekretaris-jurusan'],
            'KETUA PRODI'           => ['ketua-prodi'],
            'KEPALA LAB'            => ['kepala-lab'],
            'DOSEN BIOLOGI'         => ['dosen'],
            'DOSEN FISIKA'          => ['dosen'],
            'DOSEN GEOFISIKA'       => ['dosen'],
            'DOSEN ILMU KELAUTAN'   => ['dosen'],
            'DOSEN KIMIA'           => ['dosen'],
            'DOSEN S2 KIMIA'        => ['dosen'],
            'DOSEN MATEMATIKA'      => ['dosen'],
            'DOSEN REKAYASA SISTEM KOMPUTER'    => ['dosen'],
            'DOSEN SISTEM INFORMASI'            => ['dosen'],
            'DOSEN STATISTIK'       => ['dosen'],
            'TIM TIK'               => ['tik'],

            default                 => [NULL],
        };
    }

    public function changeNameUnitKerja($unitKerjaNama)
    {
        return match ($unitKerjaNama) {
            'DOSEN'                 => 'SEMUA DOSEN',
            'JURUSAN'               => 'SEMUA ADMIN JURUSAN',
            'LABORATORIUM'          => "SEMUA LABORAN",
            'MITRA'                 => "SEMUA MITRA",
            'TENAGA KEPENDIDIKAN'   => "SEMUA TENAGA KEPENDIDIKAN",
            'PIMPINAN FAKULTAS'     => "PIMPINAN FAKULTAS (DEKAN, WAKIL DEKAN, KTU DAN SUB KOORDINATOR)",
            'KETUA JURUSAN'         => "SEMUA KETUA JURUSAN",
            'SEKRETARIS JURUSAN'    => "SEMUA SEKRETARIS JURUSAN",
            'KETUA PRODI'           => "SEMUA KETUA PRODI",
            'KEPALA LAB'            => "SEMUA KEPALA LABORATORIUM",
            'DOSEN BIOLOGI'         => "DOSEN BIOLOGI",
            'DOSEN FISIKA'          => "DOSEN FISIKA",
            'DOSEN GEOFISIKA'       => "DOSEN GEOFISIKA",
            'DOSEN ILMU KELAUTAN'   => "DOSEN ILMU KELAUTAN",
            'DOSEN KIMIA'           => "DOSEN KIMIA",
            'DOSEN S2 KIMIA'        => "DOSEN S2 KIMIA",
            'DOSEN MATEMATIKA'      => "DOSEN MATEMATIKA",
            'DOSEN REKAYASA SISTEM KOMPUTER'    => "DOSEN REKAYASA SISTEM KOMPUTER",
            'DOSEN SISTEM INFORMASI'            => "DOSEN SISTEM INFORMASI",
            'DOSEN STATISTIK'       => "DOSEN STATISTIK",
            default                 => $unitKerjaNama,
        };
    }

}
