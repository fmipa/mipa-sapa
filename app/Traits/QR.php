<?php

namespace App\Traits;

use Illuminate\Http\Request;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

trait QR
{
    public function generate(string $code, $size = false, $style = 'round', $color = '#000000', $colorBG = '#FFFFFF', $gradient = false)
    {
        $colorQR = $this->hexToRgb($color);
        $colorBGQR = $this->hexToRgb($colorBG);
        return $qrcode = QrCode::size($size)
                ->style($style)
                // ->color($colorQR['r'],$colorQR['g'],$colorQR['b'])
                ->backgroundColor($colorBGQR['r'],$colorBGQR['g'],$colorBGQR['b'])
                ->gradient(0, 0, $colorQR['r'], $colorQR['r'], $colorQR['g'], $colorQR['b'], 'diagonal')
            ->generate(
                $this->generateLink($code),
            );

        // return response($qrcode)
        //     ->header('Content-type', 'image/png');
    }

    public function generateSVG(string $code, $size = false, $style = 'round', $color = '#000000', $colorBG = '#FFFFFF', $gradient = false)
    {
        $colorQR        = $this->hexToRgb($color);
        $colorBGQR      = $this->hexToRgb($colorBG);
        return QrCode::format('svg')->size($size)
                ->style($style)
                ->backgroundColor($colorBGQR['r'],$colorBGQR['g'],$colorBGQR['b'])
                // ->gradient(0, 0, $colorQR['r'], $colorQR['r'], $colorQR['g'], $colorQR['b'], 'diagonal')
                ->errorCorrection('H')
            ->generate(
                $this->generateLink($code),
            );
    }

    public function generateLink(string $code): string
    {
        return route('cari-berkas.qrcode', $code);
    }

    function hexToRgb($hex, $alpha = false)
    {
        $hex = str_replace('#', '', $hex);
        $length = strlen($hex);
        $rgb['r'] = hexdec($length == 6 ? substr($hex, 0, 2) : ($length == 3 ? str_repeat(substr($hex, 0, 1), 2) : 0));
        $rgb['g'] = hexdec($length == 6 ? substr($hex, 2, 2) : ($length == 3 ? str_repeat(substr($hex, 1, 1), 2) : 0));
        $rgb['b'] = hexdec($length == 6 ? substr($hex, 4, 2) : ($length == 3 ? str_repeat(substr($hex, 2, 1), 2) : 0));
        if ($alpha) {
            $rgb['a'] = $alpha;
        }
        return $rgb;
    }

    public function download(string $code)
    {
        return response()->streamDownload(
            function () {
                echo QrCode::size(200)
                    ->format('png')
                    ->generate($code);
            },
            'qr-code.png',
            [
                'Content-Type' => 'image/png',
            ]
        );
    }
}
