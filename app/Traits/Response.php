<?php

namespace App\Traits;

use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Response as FacadesResponse;

trait Response
{   
    public static function sendError($message, $errors=[], $code=401)
    {
        $response = [
            'success'   => false,
            'meesage'   => $message,
        ];
        if(!empty($errors))
        {
            $response['data'] = $errors;
        }
        throw new HttpResponseException(FacadesResponse::json($response, $code));
    }

    public static function pesanFlash($code=500)
    {
        return match ($code) {
            200 => 'Berhasil disimpan.',
            201 => 'Berhasil diubah.',
            202 => 'Berhasil dihapus.',
            default => 'Terjadi Kesalahan, silahkan coba lagi.',
        };
    }

}
