<?php

namespace App\Traits;

use App\Enums\RoleEnum;
use App\Models\Box;
use App\Models\Extension;
use App\Models\Folder;
use App\Models\JenisAkses;
use App\Models\KategoriBerkas;
use App\Models\KodeNaskah;
use App\Models\KodeUnit;
use App\Models\MasterAgama;
use App\Models\Pegawai;
use App\Models\Prodi;
use App\Models\Scopes\RoleKodeNaskahScope;
use App\Models\UnitKerja;
use App\Models\User;
use App\Traits\Mutators\EncryptId;
use Spatie\Permission\Models\Role;

trait Variables
{
    use EncryptId;

    public function jenisAkses()
    {
        return JenisAkses::View()->toArray();
    }

    public function folders()
    {
        $folders = Folder::when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value
                );
        }) == FALSE), function($query) {
            $unitKerjaId = auth()->user()->pegawai->unit_kerja_id == 1 ? UnitKerja::whereIn('nama', ['AKADEMIK', 'KEMAHASISWAAN'])->pluck('id') : [auth()->user()->pegawai->unit_kerja_id];
            $query->whereIn('unit_kerja_id',$unitKerjaId);
        })->View()->sortBy('text');

        return $folders->values()->all();
    }

    public function myFolder($justParent = false)
    {
        return Folder::when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value
                );
        }) == FALSE), function($query) {
            $unitKerjaId = auth()->user()->pegawai->unit_kerja_id == 1 ? UnitKerja::whereIn('nama', ['AKADEMIK', 'KEMAHASISWAAN'])->pluck('id') : [auth()->user()->pegawai->unit_kerja_id];
            $query->whereIn('unit_kerja_id', $unitKerjaId);
        })->when($justParent, function($query) {
            $query->whereParentId(0);
        })->get();
    }

    public function boxes()
    {
        return Box::when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value ||
                    $value == RoleEnum::KEPEGAWAIAN->value
                );
        }) == FALSE), function($query) {
            $query->where('unit_kerja_id', auth()->user()->pegawai->unit_kerja_id);
        })->View()->toArray();
    }

    public function myBox($myId = false)
    {
        $user = auth()->user();
        $id = $myId ?? $user->id;
        if ($user->operator) {
            return $this->boxes();
        } else {
            return Box::when((auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::SUPERADMIN->value ||
                        $value == RoleEnum::ADMIN->value ||
                        $value == RoleEnum::DEKAN->value ||
                        $value == RoleEnum::WD->value ||
                        $value == RoleEnum::KTU->value ||
                        $value == RoleEnum::SUBKOOR->value ||
                        $value == RoleEnum::KEPEGAWAIAN->value
                    );
            }) == FALSE), function($query) {
                $query->where('unit_kerja_id', auth()->user()->pegawai->unit_kerja_id);
            })->MyBox($id)->View()->toArray();
        }
    }

    public function roles()
    {
        return Role::View()->toArray();
    }

    // ! IsFolder = Unit Kerja yang sebagai master data.
    public function unitKerjas($isFolder = false, $changeName = false, $idIsText = false)
    {
        return UnitKerja::when($isFolder, function($que) {
            $que->isFolder();
        })->View($changeName, $idIsText)->toArray();
    }

    public function myUnitKerja($role_id = false, $view = false)
    {
        $unitKerjas = UnitKerja::whereHas('roles', function($query) use ($role_id) {
            $role_id = $role_id ? [$role_id] : auth()->user()->roles()->pluck('id');
            $query->whereIn('role_id', $role_id);
        });
        if ($view) {
            return $unitKerjas->View(true)->toArray();
        } else {
            return $unitKerjas->get();
        }
    }

    public function pegawais()
    {
        return Pegawai::View()->toArray();
    }

    public function accepts()
    {
        return Extension::View()->implode('extension', ',');
    }

    public function kategoriBerkas()
    {
        return KategoriBerkas::View()->toArray();
    }

    public function kodeNaskahs()
    {
        return KodeNaskah::withoutGlobalScopes([RoleKodeNaskahScope::class])->View()->toArray();
    }

    public function kodeUnits()
    {
        return KodeUnit::View()->toArray();
    }

    public function masterAgamas()
    {
        return MasterAgama::View()->toArray();
    }

    public function prodis()
    {
        return Prodi::View()->toArray();
    }

    public function users()
    {
        return User::View()->toArray();
    }
}
