<?php

namespace App\Traits;

use App\Enums\AksesFileEnum;
use App\Traits\Mutators\EncryptId;
use Illuminate\Http\Request;

trait Folder
{
    use EncryptId;

    public function penetapan(string $kodeUnik): string
    {
        return $this->encrypt($kodeUnik.date('YmdHis'));
    }
}
