<?php

namespace App\Traits;

use App\Enums\RoleEnum;
use App\Models\User;
use Illuminate\Support\Facades\DB;

trait FileFilter
{
    // ! Get PRODI ID jika role admin jurusan, laboran dan dosen
    public function creators()
    {
        if(auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::AKADEMIK->value ||
                    $value == RoleEnum::KEPEGAWAIAN->value ||
                    $value == RoleEnum::KEUANGAN->value ||
                    $value == RoleEnum::UMPER->value ||
                    $value == RoleEnum::ADJUR->value ||
                    $value == RoleEnum::LABORAN->value ||
                    $value == RoleEnum::RUANGBACA->value ||
                    $value == RoleEnum::TIK->value
                );
        }) == TRUE)
        {
            return User::whereOperator(true)
            ->whereHas('roles', function($query) {
                $query->whereIn('id', auth()->user()->roles()->pluck('id'));
            })
            ->when(auth()->user()->getRoleNames()->contains(function($value, $key) {
                return (
                        $value == RoleEnum::ADJUR->value ||
                        $value == RoleEnum::LABORAN->value ||
                        $value == RoleEnum::DOSEN->value
                    );
            }) == TRUE, function($query) {
                $query->whereHas('pegawai', function ($user) {
                    $user->whereProdiId(auth()->user()->pegawai->prodi_id);
                });
            })
            ->pluck('users.id');
        } else {
            return [auth()->id()];
        }
    }

    public function unitKerjas($pegawaiId = false)
    {
        $user = $pegawaiId ? User::wherePegawaiId($pegawaiId)->first() : auth()->user();
        return DB::table('unit_role')->whereIn('role_id', $user->roles()->pluck('id'))->pluck('unit_kerja_id')->toArray();
        // ->map(function($unit) {
        //     return DB::table('unit_kerjas')->find($unit->unit_kerja_id)->nama;
        // });
    }

    public function pegawais()
    {
        $operator = $this->creators();
        // if (auth()->user()->operator == 0) {
        //     $operator[] = auth()->id();
        // }
        return $operator;
    }

    // ! Get default FOLDER ID Jurusan berdasarkan PRODI
    public function folderByProdi($nama = false)
    {
        $nama = $nama ?? auth()->user()->pegawai->prodi->nama;
        return match (strtoupper($nama)) {
            'BIOLOGI'        => 155,
            'FISIKA'         => 156,
            'GEOFISIKA'      => 157,
            'MATEMATIKA'     => 158,
            'STATISTIKA'     => 159,
            'ILMUKELAUTAN'   => 160,
            'KIMIA'          => 161,
            'KIMIAS2'        => 162,
            'REKAYASASISTEMKOMPUTER' => 163,
            'SISTEMINFORMASI'        => 165,
            'DEFAULT'        => NULL,
        };
    }
}
