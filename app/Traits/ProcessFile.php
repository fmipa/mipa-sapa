<?php

namespace App\Traits;

use App\Traits\Mutators\EncryptId;
use Illuminate\Http\Request;

trait ProcessFile
{
    use EncryptId;

    public function generateName(string $name): string
    {
        return date('YmdHis').$this->generateRandomString().'-'.str_replace('/', '', $name);
    }

    protected function generateRandomString($length = 5): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function detailFile($file): array
    {
        $detail['original_name']    = $file->getClientOriginalName();
        $detail['extension']        = $file->getClientOriginalExtension();
        $detail['size']             = $file->getSize();
        $detail['mime_type']        = $file->getMimeType();
        $detail['path']             = $file->getRealPath();

        return $detail;
    }
}
