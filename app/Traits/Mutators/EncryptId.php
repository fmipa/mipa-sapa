<?php

namespace App\Traits\Mutators;

trait EncryptId
{
    public function encrypt(string $string, $secretKey = false): String
    {
        $output = false;
    
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = $secretKey ?? env('SECRET_KEY', 'mipa');
        $secret_iv = env('SECRET_UNIQUE', 'mipauntan');
    
        // hash
        $key = hash('sha256', $secret_key);
    
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
    
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
    
        return $output;
    }

    
}
