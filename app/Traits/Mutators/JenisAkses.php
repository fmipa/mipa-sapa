<?php

namespace App\Traits\Mutators;

use App\Enums\JenisAksesEnum;
use Spatie\Permission\Models\Role;

trait JenisAkses
{
    // $ids == Id dari role atau pegawai atau lainnya
    // $idExtra == id khusus pegawai yg dipilih atau id tambahan
    public function prosesAkses($jenisAksesId, $ids, $idExtra = false)
    {
        switch ($jenisAksesId) {
            case JenisAksesEnum::TIM->value:
                $idResult = [auth()->user()->pegawai->unit_kerja_id];
                break;

            case JenisAksesEnum::TAG->value:
                $idResult   = $this->arraysId($ids);
                $idResult[] = $idExtra ? $idExtra : auth()->user()->pegawai_id;
                break;
            
            case JenisAksesEnum::UNITKERJA->value:
                $idResult = $ids ? $this->arraysId($ids) : [];
                break;
            
            default:
                $idResult = [$idExtra ? $idExtra : auth()->user()->pegawai_id];
                break;
        }

        return $idResult ?? NULL;
    }

    public function getRole($jenisAksesId)
    {
        switch ($jenisAksesId) {
            case 3:
                $roles = Role::whereIn('name', ['dekan'])->pluck('id');
                break;
            
            case 4:
                $roles = Role::whereIn('name', ['wakil-dekan'])->pluck('id');
                break;
            
            case 5:
                $roles = Role::whereIn('name', ['ktu'])->pluck('id');
                break;
            
            case 6:
                $roles = Role::whereIn('name', ['sub-koordinator'])->pluck('id');
                break;
            
            case 7:
                $roles = Role::whereIn('name', ['ketua-jurusan'])->pluck('id');
                break;
            
            case 8:
                $roles = Role::whereIn('name', ['sekretaris-jurusan'])->pluck('id');
                break;
            
            case 9:
                $roles = Role::whereIn('name', ['ketua-prodi'])->pluck('id');
                break;
            
            case 10:
                $roles = Role::whereIn('name', ['dosen'])->pluck('id');
                break;
            
            case 11:
                $roles = Role::whereIn('name', ['akademik'])->pluck('id');
                break;
            
            case 12:
                $roles = Role::whereIn('name', ['kepegawaian'])->pluck('id');
                break;
            
            case 13:
                $roles = Role::whereIn('name', ['keuangan'])->pluck('id');
                break;
            
            case 14:
                $roles = Role::whereIn('name', ['umper'])->pluck('id');
                break;
            
            case 15:
                $roles = Role::whereIn('name', ['admin-jurusan'])->pluck('id');
                break;
            
            case 16:
                $roles = Role::whereIn('name', ['laboran'])->pluck('id');
                break;
            
            case 17:
                $roles = Role::whereIn('name', ['tik'])->pluck('id');
                break;
            
            case 18:
                $roles = Role::whereIn('name', ['ruang-baca'])->pluck('id');
                break;

            case 19:
                $roles = Role::whereIn('name', ['mitra'])->pluck('id');
                break;
            //! TENDIK
            case 20:
                $roles = Role::whereIn('name', ['akademik', 'kepegawaian'. 'keuangan', 'umper', 'admin-jurusan', 'laboran', 'tik', 'ruang-baca'])->pluck('id');
                break;
            //! Jurusan
            case 21:
                $roles = Role::whereIn('name', ['akademik', 'kepegawaian'. 'keuangan', 'umper', 'admin-jurusan', 'laboran', 'tik', 'ruang-baca'])->pluck('id');
                break;
            
            default:
                # code...
                break;
        }

        return $roles;
    }

    public function arraysId($arrays)
    {
        $data = [];
        foreach ($arrays as $value) {
            $data[] = $this->decrypt((string)$value);
        }
        return $data ?? NULL;
    }
    
}
