<?php

namespace App\Traits;


trait ValidateSizeFile
{
    public function rulesHelper($size = 1): int
    {
        if (auth()->user()->operator == 1 || (auth()->user()->hasRole('super-admin') || auth()->user()->hasRole('kepegawaian'))) {
            return $size==100 ?  102400 : 10240;
        } else {
            return $size==1 ? 1024 : 2048;
        }
    }

    public function messagesHelper($size = 1): string
    {
        if (auth()->user()->operator == 1 || (auth()->user()->hasRole('super-admin') || auth()->user()->hasRole('kepegawaian'))) {
            return $size==100 ? '100MB' : '10MB';
        } else {
            return $size==1 ? '1MB' : '2MB';;
        }
    }
}
