<?php

namespace App\Repositories\KodeNaskah;

interface KodeNaskahRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($kodeNaskahKId);

    public function whereBy($key, $value);

    public function store(array $kodeNaskahKData);

    public function update($kodeNaskahKId, array $newKodeNaskah);
    
    public function delete($kodeNaskahKId);
}