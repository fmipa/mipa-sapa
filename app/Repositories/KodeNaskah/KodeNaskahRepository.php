<?php

namespace App\Repositories\KodeNaskah;

use App\Repositories\KodeNaskah\KodeNaskahRepositoryInterface;
use App\Models\KodeNaskah;

class KodeNaskahRepository implements KodeNaskahRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new KodeNaskah();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($kodeNaskahId)
    {
        return $this->model->findOrFail($kodeNaskahId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $kodeNaskahData)
    {
        try {
            return $this->model->create($kodeNaskahData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($kodeNaskahId, array $newKodeNaskah)
    {
        try {
            $kodeNaskah = $this->model->findOrFail($kodeNaskahId);
            $kodeNaskah->update($newKodeNaskah);
            return $kodeNaskah;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($kodeNaskahId)
    {
        try {
            return $this->model->findOrFail($kodeNaskahId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}