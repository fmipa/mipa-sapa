<?php

namespace App\Repositories\Extension;

use App\Repositories\Extension\ExtensionRepositoryInterface;
use App\Models\Extension;

class ExtensionRepository implements ExtensionRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Extension();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($extensionId)
    {
        return $this->model->findOrFail($extensionId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $extensionData)
    {
        try {
            return $this->model->create($extensionData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($extensionId, array $newExtension)
    {
        try {
            $extension = $this->model->findOrFail($extensionId);
            $extension->update($newExtension);
            return $extension;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($extensionId)
    {
        try {
            return $this->model->findOrFail($extensionId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}