<?php

namespace App\Repositories\Extension;

interface ExtensionRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($extensionId);

    public function whereBy($key, $value);

    public function store(array $extensionData);

    public function update($extensionId, array $newExtension);
    
    public function delete($extensionId);
}