<?php

namespace App\Repositories\Dokumen;

use App\Repositories\Dokumen\DocumentRepositoryInterface;
use App\Models\Document;

class DocumentRepository implements DocumentRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Document();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($documentId)
    {
        return $this->model->withoutGlobalScopes()->findOrFail($documentId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $documentData)
    {
        try {
            return $this->model->create($documentData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($documentId, array $newDocument)
    {
        try {
            $document = $this->model->findOrFail($documentId);
            $document->update($newDocument);
            return $document;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($documentId)
    {
        try {
            return $this->model->findOrFail($documentId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}