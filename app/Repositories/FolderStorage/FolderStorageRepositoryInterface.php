<?php

namespace App\Repositories\FolderStorage;

interface FolderStorageRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby, $columns);

    public function allWithFomat(array $format);

    public function findById($folderStorageId);

    public function whereBy($key, $value);

    public function store(array $folderStorageData);

    public function update($folderStorageId, array $newFolderStorage);
    
    public function delete($folderStorageId);
}