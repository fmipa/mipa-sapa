<?php

namespace App\Repositories\FolderStorage;

use App\Repositories\FolderStorage\FolderStorageRepositoryInterface;
use App\Models\FolderStorage;
use Illuminate\Support\Facades\DB;

class FolderStorageRepository implements FolderStorageRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new FolderStorage();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby, $columns)
    {
        return $this->model->orderBy($key, $orderby)->get($columns);
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($folderStorageId)
    {
        return $this->model->findOrFail($folderStorageId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $folderStorageData)
    {
        try {
            DB::beginTransaction();
            $folderStorage = $this->model->create($folderStorageData);
            DB::commit();
            return $folderStorage;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($folderStorageId, array $newFolder)
    {
        try {
            DB::beginTransaction();
            $folderStorage = $this->model->findOrFail($folderStorageId);
            $folderStorage->update($newFolder);
            DB::commit();
            return $folderStorage;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($folderStorageId)
    {
        try {
            DB::beginTransaction();
            $data = $this->model->findOrFail($folderStorageId);
            $data->pegawais() ? $data->pegawais()->detach() : NULL;
            DB::commit();
            return $data->delete();
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    

}