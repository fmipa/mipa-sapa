<?php

namespace App\Repositories\User;

use App\Repositories\User\UserRepositoryInterface;
use App\Models\User;

class UserRepository implements UserRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new User();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model
        // ->whereHas('roles', function($item) {
        //     $item->whereIn('name', ['dosen']);
        // })
        ->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($userId)
    {
        return $this->model->findOrFail($userId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $userData)
    {
        try {
            return $this->model->create($userData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($userId, array $newUser)
    {
        try {
            $user = $this->model->findOrFail($userId);
            $user->update($newUser);
            return $user;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($userId)
    {
        try {
            return $this->model->findOrFail($userId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}