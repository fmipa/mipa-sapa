<?php

namespace App\Repositories\User;

interface UserRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($userId);

    public function whereBy($key, $value);

    public function store(array $userData);

    public function update($userId, array $newUser);
    
    public function delete($userId);
}