<?php

namespace App\Repositories\SuratMasuk;

use App\Repositories\SuratMasuk\SuratMasukRepositoryInterface;
use App\Models\SuratMasuk;

class SuratMasukRepository implements SuratMasukRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new SuratMasuk();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($suratMasukId)
    {
        return $this->model->withoutGlobalScopes()->findOrFail($suratMasukId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $suratMasukData)
    {
        try {
            return $this->model->create($suratMasukData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($suratMasukId, array $newSuratMasuk)
    {
        try {
            $suratMasuk = $this->model->findOrFail($suratMasukId);
            $suratMasuk->update($newSuratMasuk);
            return $suratMasuk;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($suratMasukId)
    {
        try {
            return $this->model->findOrFail($suratMasukId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}