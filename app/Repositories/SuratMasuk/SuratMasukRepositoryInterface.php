<?php

namespace App\Repositories\SuratMasuk;

interface SuratMasukRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($suratMasukId);

    public function whereBy($key, $value);

    public function store(array $suratMasukData);

    public function update($suratMasukId, array $newSuratMasuk);
    
    public function delete($suratMasukId);
}