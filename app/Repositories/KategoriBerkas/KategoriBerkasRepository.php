<?php

namespace App\Repositories\KategoriBerkas;

use App\Repositories\KategoriBerkas\KategoriBerkasRepositoryInterface;
use App\Models\KategoriBerkas;

class KategoriBerkasRepository implements KategoriBerkasRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new KategoriBerkas();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($kategoriBerkasId)
    {
        return $this->model->findOrFail($kategoriBerkasId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $kategoriBerkasData)
    {
        try {
            return $this->model->create($kategoriBerkasData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($kategoriBerkasId, array $newKategoriBerkas)
    {
        try {
            $kategoriBerkas = $this->model->findOrFail($kategoriBerkasId);
            $kategoriBerkas->update($newKategoriBerkas);
            return $kategoriBerkas;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($kategoriBerkasId)
    {
        try {
            return $this->model->findOrFail($kategoriBerkasId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}