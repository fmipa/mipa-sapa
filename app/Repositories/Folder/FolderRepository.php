<?php

namespace App\Repositories\Folder;

use App\Enums\RoleEnum;
use App\Repositories\Folder\FolderRepositoryInterface;
use App\Models\Folder;
use App\Models\UnitKerja;

class FolderRepository implements FolderRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Folder();
    }

    public function getAll() 
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value
                );
        }) == FALSE), function($query) {
            $unitKerjaId = auth()->user()->pegawai->unit_kerja_id == 1 ? UnitKerja::whereIn('nama', ['AKADEMIK', 'KEMAHASISWAAN'])->pluck('id') : [auth()->user()->pegawai->unit_kerja_id];
            $query->whereIn('unit_kerja_id', $unitKerjaId);
        })->get();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value
                );
        }) == FALSE), function($query) {
            $unitKerjaId = auth()->user()->pegawai->unit_kerja_id == 1 ? UnitKerja::whereIn('nama', ['AKADEMIK', 'KEMAHASISWAAN'])->pluck('id') : [auth()->user()->pegawai->unit_kerja_id];
            $query->whereIn('unit_kerja_id', $unitKerjaId);
        })->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value
                );
        }) == FALSE), function($query) {
            $unitKerjaId = auth()->user()->pegawai->unit_kerja_id == 1 ? UnitKerja::whereIn('nama', ['AKADEMIK', 'KEMAHASISWAAN'])->pluck('id') : [auth()->user()->pegawai->unit_kerja_id];
            $query->whereIn('unit_kerja_id', $unitKerjaId);
        })->get($format);
    }

    public function findById($folderId)
    {
        return $this->model->findOrFail($folderId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value
                );
        }) == FALSE), function($query) {
            $unitKerjaId = auth()->user()->pegawai->unit_kerja_id == 1 ? UnitKerja::whereIn('nama', ['AKADEMIK', 'KEMAHASISWAAN'])->pluck('id') : [auth()->user()->pegawai->unit_kerja_id];
            $query->whereIn('unit_kerja_id', $unitKerjaId);
        })->where($key, $value)
        ->orderBy('unit_kerja_id')
        ->get();
    }

    public function whereMultiple($where)
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value
                );
        }) == FALSE), function($query) {
            $unitKerjaId = auth()->user()->pegawai->unit_kerja_id == 1 ? UnitKerja::whereIn('nama', ['AKADEMIK', 'KEMAHASISWAAN'])->pluck('id') : [auth()->user()->pegawai->unit_kerja_id];
            $query->whereIn('unit_kerja_id', $unitKerjaId);
        })->where($where)->get();
    }

    public function store(array $folderData)
    {
        try {
            return $this->model->create($folderData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($folderId, array $newFolder)
    {
        try {
            $folder = $this->model->findOrFail($folderId);
            $folder->update($newFolder);
            return $folder;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($folderId)
    {
        try {
            $data = $this->model->findOrFail($folderId);
            $data->childs()->delete();
            return $data->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function allChildren($folderId)
    {
        return Folder::when($folderId == false, function($folder) {
            $folder->whereParentId(0);
        })->get(['id','nama','parent_id', 'unit_kerja_id'])->map(function($item) {
            $data['parent'] = $item->nama;
            $data['id'] = $item->id;
            $data['parentId'] = $item->parent_id_secret;
            $data['unit_kerja_id'] = $item->unit_kerja_id;
            $data['childs'] = $item->childs;
            return $data;
        });
    }
}