<?php

namespace App\Repositories\Folder;

interface FolderRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($folderId);

    public function whereBy($key, $value);

    public function whereMultiple($where);

    public function store(array $folderData);

    public function update($folderId, array $newFolder);
    
    public function delete($folderId);

    public function allChildren($folderId);
}