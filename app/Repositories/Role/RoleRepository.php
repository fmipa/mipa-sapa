<?php

namespace App\Repositories\Role;

use App\Repositories\Role\RoleRepositoryInterface;
use Spatie\Permission\Models\Role;

class RoleRepository implements RoleRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Role();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($roleId)
    {
        return $this->model->findOrFail($roleId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $roleData)
    {
        try {
            return $this->model->create($roleData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($roleId, array $newRole)
    {
        try {
            $role = $this->model->findOrFail($roleId);
            $role->update($newRole);
            return $role;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($roleId)
    {
        try {
            return $this->model->findOrFail($roleId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}