<?php

namespace App\Repositories\BerkasPegawai;

use App\Repositories\BerkasPegawai\BerkasPegawaiRepositoryInterface;
use App\Models\BerkasPegawai;

class BerkasPegawaiRepository implements BerkasPegawaiRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new BerkasPegawai();;
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($berkasPegawaiId)
    {
        return $this->model->withoutGlobalScopes()->findOrFail($berkasPegawaiId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $berkasPegawaiData)
    {
        try {
            return $this->model->create($berkasPegawaiData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($berkasPegawaiId, array $newBerkasPegawai)
    {
        try {
            $berkasPegawai = $this->model->findOrFail($berkasPegawaiId);
            $berkasPegawai->update($newBerkasPegawai);
            return $berkasPegawai;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($berkasPegawaiId)
    {
        try {
            return $this->model->findOrFail($berkasPegawaiId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}