<?php

namespace App\Repositories\KodeUnit;

use App\Repositories\KodeUnit\KodeUnitRepositoryInterface;
use App\Models\KodeUnit;

class KodeUnitRepository implements KodeUnitRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new KodeUnit();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($kodeUnitId)
    {
        return $this->model->findOrFail($kodeUnitId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $kodeUnitData)
    {
        try {
            return $this->model->create($kodeUnitData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($kodeUnitId, array $newKodeUnit)
    {
        try {
            $kodeUnit = $this->model->findOrFail($kodeUnitId);
            $kodeUnit->update($newKodeUnit);
            return $kodeUnit;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($kodeUnitId)
    {
        try {
            return $this->model->findOrFail($kodeUnitId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}