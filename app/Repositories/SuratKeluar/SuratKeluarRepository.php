<?php

namespace App\Repositories\SuratKeluar;

use App\Repositories\SuratKeluar\SuratKeluarRepositoryInterface;
use App\Models\SuratKeluar;

class SuratKeluarRepository implements SuratKeluarRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new SuratKeluar();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($suratKeluarId)
    {
        return $this->model->withoutGlobalScopes()->findOrFail($suratKeluarId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $suratKeluarData)
    {
        try {
            return $this->model->create($suratKeluarData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($suratKeluarId, array $newSuratKeluar)
    {
        try {
            $suratKeluar = $this->model->findOrFail($suratKeluarId);
            $suratKeluar->update($newSuratKeluar);
            return $suratKeluar;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($suratKeluarId)
    {
        try {
            return $this->model->findOrFail($suratKeluarId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}