<?php

namespace App\Repositories\SuratKeluar;

interface SuratKeluarRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($suratKeluarId);

    public function whereBy($key, $value);

    public function store(array $suratKeluarData);

    public function update($suratKeluarId, array $newSuratKeluar);
    
    public function delete($suratKeluarId);
}