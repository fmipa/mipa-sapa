<?php

namespace App\Repositories\Box;

interface BoxRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($boxId);

    public function whereBy($key, $value);

    public function store(array $boxData);

    public function update($boxId, array $newBox);
    
    public function delete($boxId);
}