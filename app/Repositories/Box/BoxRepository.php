<?php

namespace App\Repositories\Box;

use App\Enums\RoleEnum;
use App\Repositories\Box\BoxRepositoryInterface;
use App\Models\Box;

class BoxRepository implements BoxRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Box();
    }

    public function getAll() 
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value ||
                    $value == RoleEnum::KEPEGAWAIAN->value
                );
        }) == FALSE), function($query) {
            $query->where('unit_kerja_id', auth()->user()->pegawai->unit_kerja_id);
        })->get();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value ||
                    $value == RoleEnum::KEPEGAWAIAN->value
                );
        }) == FALSE), function($query) {
            $query->where('unit_kerja_id', auth()->user()->pegawai->unit_kerja_id);
        })->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value ||
                    $value == RoleEnum::KEPEGAWAIAN->value
                );
        }) == FALSE), function($query) {
            $query->where('unit_kerja_id', auth()->user()->pegawai->unit_kerja_id);
        })->get($format);
    }

    public function findById($boxId)
    {
        return $this->model->findOrFail($boxId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::SUPERADMIN->value ||
                    $value == RoleEnum::ADMIN->value ||
                    $value == RoleEnum::DEKAN->value ||
                    $value == RoleEnum::WD->value ||
                    $value == RoleEnum::KTU->value ||
                    $value == RoleEnum::SUBKOOR->value ||
                    $value == RoleEnum::KEPEGAWAIAN->value
                );
        }) == FALSE), function($query) {
            $query->where('unit_kerja_id', auth()->user()->pegawai->unit_kerja_id);
        })->where($key, $value)->get();
    }

    public function store(array $boxData)
    {
        try {
            return $this->model->create($boxData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($boxId, array $newBox)
    {
        try {
            $box = $this->model->findOrFail($boxId);
            $box->update($newBox);
            return $box;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($boxId)
    {
        try {
            return $this->model->findOrFail($boxId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}