<?php

namespace App\Repositories\Permission;

use App\Repositories\Permission\PermissionRepositoryInterface;
use Spatie\Permission\Models\Permission;

class PermissionRepository implements PermissionRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Permission();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($permissionId)
    {
        return $this->model->findOrFail($permissionId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $permissionData)
    {
        try {
            return $this->model->create($permissionData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($permissionId, array $newPermission)
    {
        try {
            $permission = $this->model->findOrFail($permissionId);
            $permission->update($newPermission);
            return $permission;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($permissionId)
    {
        try {
            return $this->model->findOrFail($permissionId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}