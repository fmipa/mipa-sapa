<?php

namespace App\Repositories\Jurusan;

use App\Repositories\Jurusan\JurusanRepositoryInterface;
use App\Models\Jurusan;

class JurusanRepository implements JurusanRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Jurusan();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($jurusanId)
    {
        return $this->model->findOrFail($jurusanId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $jurusanData)
    {
        try {
            return $this->model->create($jurusanData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($jurusanId, array $newJurusan)
    {
        try {
            $jurusan = $this->model->findOrFail($jurusanId);
            $jurusan->update($newJurusan);
            return $jurusan;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($jurusanId)
    {
        try {
            return $this->model->findOrFail($jurusanId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}