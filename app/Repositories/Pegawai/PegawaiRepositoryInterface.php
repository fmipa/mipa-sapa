<?php

namespace App\Repositories\Pegawai;

interface PegawaiRepositoryInterface 
{
    public function getAll($isNotAdmin);

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($pegawaiId);

    public function whereBy($key, $value);

    public function store(array $pegawaiData);

    public function update($pegawaiId, array $newPegawai);
    
    public function delete($pegawaiId);
}