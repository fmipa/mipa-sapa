<?php

namespace App\Repositories\Pegawai;

use App\Enums\RoleEnum;
use App\Repositories\Pegawai\PegawaiRepositoryInterface;
use App\Models\Pegawai;
use App\Models\Prodi;
use Illuminate\Support\Facades\DB;

class PegawaiRepository implements PegawaiRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Pegawai();
    }

    public function getAll($isNotAdmin = false) 
    {
        return $this->model->when($isNotAdmin, function($query) {
            $query->whereNotIn('id' , [1,2]);
        })->get();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->when((auth()->user()->getRoleNames()->contains(function($value, $key) {
            return (
                    $value == RoleEnum::ADJUR->value
                );
        }) == TRUE), function($query) { 
            $jurusanId = auth()->user()->pegawai->prodi->jurusan_id;
            $prodis = Prodi::where('jurusan_id', $jurusanId)->pluck('id');
            $query->whereIn('prodi_id', $prodis);
        })->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($pegawaiId)
    {
        return $this->model->findOrFail($pegawaiId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $pegawaiData)
    {
        try {
            $pegawai = $this->model->create($pegawaiData);
            return $pegawai;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($pegawaiId, array $newPegawai)
    {
        try {
            $pegawai = $this->model->findOrFail($pegawaiId);
            $pegawai->update($newPegawai);
            return $pegawai;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($pegawaiId)
    {
        try {
            $data = $this->model->findOrFail($pegawaiId);
            $data->user ? $data->user->delete() : NULL;
            return $data->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}