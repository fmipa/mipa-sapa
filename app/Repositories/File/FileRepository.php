<?php

namespace App\Repositories\File;

use App\Repositories\File\FileRepositoryInterface;
use App\Models\File;
use Illuminate\Support\Facades\DB;

class FileRepository implements FileRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new File();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($fileId)
    {
        return $this->model->findOrFail($fileId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $fileData)
    {
        try {
            DB::beginTransaction();
            $file = $this->model->create($fileData);
            DB::commit();
            return $file;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function update($fileId, array $newFile)
    {
        try {
            DB::beginTransaction();
            $file = $this->model->findOrFail($fileId);
            $file->update($newFile);
            DB::commit();
            return $file;
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    
    public function delete($fileId)
    {
        try {
            DB::beginTransaction();
            $data = $this->model->findOrFail($fileId);
            $data->pegawais() ? $data->pegawais()->detach() : NULL;
            $data->boxes() ? $data->boxes()->detach() : NULL;
            DB::commit();
            return $data->delete();
        } catch (\Throwable $th) {
            DB::rollback();
            return $th->getMessage();
        }
    }
    

}