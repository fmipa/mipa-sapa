<?php

namespace App\Repositories\File;

interface FileRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($fileId);

    public function whereBy($key, $value);

    public function store(array $fileData);

    public function update($fileId, array $newFile);
    
    public function delete($fileId);
}