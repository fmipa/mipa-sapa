<?php

namespace App\Repositories\MasterAgama;

use App\Repositories\MasterAgama\MasterAgamaRepositoryInterface;
use App\Models\MasterAgama;

class MasterAgamaRepository implements MasterAgamaRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new MasterAgama();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($masterAgamaId)
    {
        return $this->model->findOrFail($masterAgamaId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $masterAgamaData)
    {
        try {
            return $this->model->create($masterAgamaData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($masterAgamaId, array $newMasterAgama)
    {
        try {
            $agama = $this->model->findOrFail($masterAgamaId);
            $agama->update($newMasterAgama);
            return $agama;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($masterAgamaId)
    {
        try {
            return $this->model->findOrFail($masterAgamaId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}