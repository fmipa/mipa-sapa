<?php

namespace App\Repositories\Prodi;

use App\Repositories\Prodi\ProdiRepositoryInterface;
use App\Models\Prodi;

class ProdiRepository implements ProdiRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new Prodi();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($prodiId)
    {
        return $this->model->findOrFail($prodiId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $prodiData)
    {
        try {
            return $this->model->create($prodiData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($prodiId, array $newProdi)
    {
        try {
            $prodi = $this->model->findOrFail($prodiId);
            $prodi->update($newProdi);
            return $prodi;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($prodiId)
    {
        try {
            return $this->model->findOrFail($prodiId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}