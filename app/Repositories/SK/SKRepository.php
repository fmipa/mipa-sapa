<?php

namespace App\Repositories\SK;

use App\Repositories\SK\SKRepositoryInterface;
use App\Models\SK;

class SKRepository implements SKRepositoryInterface 
{
    private $model;

    public function __construct()
    {
        $this->model = new SK();
    }

    public function getAll() 
    {
        return $this->model->all();
    }

    public function allWithOrder($key, $orderby)
    {
        return $this->model->orderBy($key, $orderby)->get();
    }

    public function allWithFomat(array $format)
    {
        return $this->model->get($format);
    }

    public function findById($SKId)
    {
        return $this->model->withoutGlobalScopes()->findOrFail($SKId);
    }

    public function whereBy($key, $value)
    {
        return $this->model->where($key, $value)->get();
    }

    public function store(array $SKData)
    {
        try {
            return $this->model->create($SKData);
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function update($SKId, array $newSK)
    {
        try {
            $SK = $this->model->findOrFail($SKId);
            $SK->update($newSK);
            return $SK;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    
    public function delete($SKId)
    {
        try {
            return $this->model->findOrFail($SKId)->delete();
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    

}