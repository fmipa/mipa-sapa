<?php

namespace App\Repositories\SK;

interface SKRepositoryInterface 
{
    public function getAll();

    public function allWithOrder($key, $orderby);

    public function allWithFomat(array $format);

    public function findById($SKId);

    public function whereBy($key, $value);

    public function store(array $SKData);

    public function update($SKId, array $newSK);
    
    public function delete($SKId);
}