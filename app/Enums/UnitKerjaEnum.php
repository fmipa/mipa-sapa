<?php

namespace App\Enums;

enum UnitKerjaEnum: string
{ 
    case AKADEMIK       = 'AKADEMIK' ;
    case DOSEN          = 'DOSEN' ;
    case JURUSAN        = 'JURUSAN' ;
    case KEMAHASISWAAN  = 'KEMAHASISWAAN' ;
    case KEPEGAWAIAN    = 'KEPEGAWAIAN' ;
    case KEUANGAN       = 'KEUANGAN' ;
    case LABORATORIUM   = 'LABORATORIUM' ;
    case MITRA          = 'MITRA' ;
    case RUANGBACA      = 'RUANG BACA' ;
    case UMPER          = 'UMUM DAN PERLENGKAPAN' ;
    case TENDIK         = 'TENAGA KEPENDIDIKAN' ;
    case LAINNYA        = 'LAINNYA' ;
}

?>