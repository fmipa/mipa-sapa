<?php

namespace App\Enums;

enum StatusPegawaiEnum: string
{ 
    case PNS    = 'PNS';
    case PPPK   = 'PPPK';
    case KONTRAK    = 'KONTRAK';
    case HONORER    = 'HONORER';
    case PHL        = 'PHL';
    case ASISTEN    = 'ASISTEN';
    case OUTSOURCING   = 'OUTSOURCING';
}

?>