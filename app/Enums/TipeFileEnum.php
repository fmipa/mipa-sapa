<?php

namespace App\Enums;

enum TipeFileEnum: string
{ 
    case BERKASPEGAWAI  = 'berkas-pegawai';
    case DOKUMEN        = 'dokumen';
    case SURATMASUK     = 'surat-masuk';
    case SURATKELUAR    = 'surat-keluar';
    case SK             = 'sk';
}

?>