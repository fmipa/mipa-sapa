<?php

namespace App\Enums;

enum FolderStorageDefaultEnum: int
{ 
    case BERKASPEGAWAI  = 1;
    case DOKUMEN        = 2;
    case SURATMASUK     = 3;
    case SURATKELUAR    = 4;
    case SK             = 5;
}

?>