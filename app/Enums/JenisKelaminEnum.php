<?php

namespace App\Enums;

enum JenisKelaminEnum: string
{ 
    case LAKI    = 'L';
    case PEREMPUAN   = 'P';
}

?>