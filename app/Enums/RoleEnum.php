<?php

namespace App\Enums;

enum RoleEnum: string
{ 
    case SUPERADMIN = 'super-admin';
    case ADMIN      = 'admin';
    case DEKAN      = 'dekan';
    case WD         = 'wakil-dekan';
    case KTU        = 'ktu';
    case SUBKOOR    = 'sub-koordinator';
    case KAJUR      = 'ketua-jurusan';
    case SEKJUR     = 'sekretaris-jurusan';
    case KAPORDI    = 'ketua-prodi';
    case KALAB      = 'kepala-lab';
    case DOSEN      = 'dosen';
    case AKADEMIK   = 'akademik';
    case KEPEGAWAIAN    = 'kepegawaian';
    case KEUANGAN   = 'keuangan';
    case UMPER      = 'umper';
    case ADJUR      = 'admin-jurusan';
    case LABORAN    = 'laboran';
    case RUANGBACA  = 'ruang-baca';
    case TIK        = 'tik';
    case LUAR       = 'luar';
}

?>