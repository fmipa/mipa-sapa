<?php

namespace App\Enums;

enum JenisAksesEnum: int
{ 
    case PUBLIC     = 1;
    case PRIVACY    = 2;
    case TAG        = 3;
    case UNITKERJA  = 4;
    case TIM        = 5;

    public static function fromName(string $name){
        
        return constant("self::$name");
    }
}

?>