<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreKategoriBerkasRequest;
use App\Http\Requests\UpdateKategoriBerkasRequest;
use App\Models\KategoriBerkas;
use App\Services\KategoriBerkas\KategoriBerkasServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class KategoriBerkasController extends Controller
{
    use TraitsResponse, DecryptId;

    public $routeIndex  = 'kategori-berkas.index';
    public $folder;

    public function __construct(
        protected KategoriBerkasServiceInterface $kategoriBerkasService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.kategori-berkas';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('kberkas-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
        'kberkass' => $this->kategoriBerkasService->allWithOrder('kategori', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('kberkas-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreKategoriBerkasRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('kberkas-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->kategoriBerkasService->store($request->validated());
            if (!$saving instanceof KategoriBerkas) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(KategoriBerkas $kBerkas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $kBerkasId): View
    {
        abort_if(Gate::denies('kberkas-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
        'data'  => $this->kategoriBerkasService->findById($this->decrypt($kBerkasId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateKategoriBerkasRequest $request, string $kBerkasId): RedirectResponse
    {
        abort_if(Gate::denies('kberkas-update'), Response::HTTP_FORBIDDEN);
        
        $kBerkasData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->kategoriBerkasService->update($request->id, $kBerkasData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $kBerkasId): RedirectResponse
    {
        try {
            $saving = $this->kategoriBerkasService->delete($this->decrypt($kBerkasId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
