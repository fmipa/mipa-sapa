<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Requests\StoreFileRequest;
use App\Http\Requests\UpdateFileRequest;
use App\Models\File;
use App\Models\Folder;
use App\Models\PegawaiFile;
use App\Models\User;
use App\Services\BerkasPegawai\BerkasPegawaiServiceInterface;
use App\Services\Dokumen\DocumentServiceInterface;
use App\Services\File\FileServiceInterface;
use App\Services\Folder\FolderServiceInterface;
use App\Services\SK\SKServiceInterface;
use App\Services\SuratKeluar\SuratKeluarServiceInterface;
use App\Services\SuratMasuk\SuratMasukServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Mutators\EncryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Database\Query\JoinClause;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class FileController extends Controller
{
    use TraitsResponse, DecryptId, EncryptId;

    public $routeIndex  = 'file.index';
    public $folder;

    public function __construct(
        protected FileServiceInterface $fileService,
        protected SKServiceInterface $SKService,
        protected SuratMasukServiceInterface $suratMasukService,
        protected SuratKeluarServiceInterface $suratKeluarService,
        protected DocumentServiceInterface $dokumenService,
        protected BerkasPegawaiServiceInterface $berkasPegawaiService,
        protected FolderServiceInterface $folderService,
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.file';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('file-index'), Response::HTTP_FORBIDDEN);

        // $cekUser = (auth()->user()->getRoleNames()->contains(function($value, $key) {
        //     return (
        //             $value == RoleEnum::SUPERADMIN->value ||
        //             $value == RoleEnum::ADMIN->value ||
        //             $value == RoleEnum::DEKAN->value ||
        //             $value == RoleEnum::WD->value ||
        //             $value == RoleEnum::KTU->value ||
        //             $value == RoleEnum::SUBKOOR->value
        //         );
        // }) == TRUE) ?? FALSE;
        
        // if ($cekUser) {
        //     return view($this->folder.'.index-folder');
        // } else {
            return view($this->folder.'.index', [
                'files' => $this->fileService->allTypeFiles(),
            ]);
        // }
    }

    /**
     * Display a listing of the resource.
     */
    public function folderView(): View
    {
        abort_if(Gate::denies('file-index'), Response::HTTP_FORBIDDEN);
        return view($this->folder.'.index-folder');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('file-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFileRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('file-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->fileService->store($request->validated());
            if (!$saving instanceof File) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(File $file)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $fileId): View
    {
        abort_if(Gate::denies('file-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
            'data'  => $this->fileService->findById($this->decrypt($fileId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFileRequest $request, string $fileId): RedirectResponse
    {
        abort_if(Gate::denies('file-update'), Response::HTTP_FORBIDDEN);
        
        $fileData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->fileService->update($request->id, $fileData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $fileId): RedirectResponse
    {
        abort_if(Gate::denies('file-delete'), Response::HTTP_FORBIDDEN);

        try {
            $saving = $this->fileService->delete($this->decrypt($fileId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
    //! Folder Unit Kerja
    public function unitFolder()
    {
        return $this->fileService->unitFolder();
    }
    //! Folder
    public function folder(string $idSecret, ?string $idUnitKerja = NULL)
    {
        return $this->fileService->folder($idSecret, $idUnitKerja);
    }
    //! Pegawai Folder
    // ? Folder dengan nama Pegawai
    public function pegawaiFolder(string $idSecret, ?string $idUnitKerja = NULL)
    {
        return $this->fileService->pegawaiFolder($idSecret, $idUnitKerja);
    }
    //! Folder Pegawai
    // ? Folder sesuai dengan status pegawai
    public function folderPegawai(string $parentFolderPegawai, string $idSecret, ?string $idUnitKerja = NULL)
    {
        return $this->fileService->folderPegawai($parentFolderPegawai, $idSecret, $idUnitKerja);
    }
    //! Data Pegawai
    public function dataPegawai(string $idSecret, string $idPegawai)
    {
        return $this->fileService->dataPegawai($idSecret, $idPegawai);
    }

    //!  Kembali
    public function kembali(string $idSecret, ?string $idUnitKerja = NULL)
    {
        return $this->fileService->kembali($idSecret, $idUnitKerja);
    }

    //! Pencarian Berkas
    public function cari(?string $kataKunci = NULL, ?string $idUnitKerja = NULL, ?string $idFolder = NULL)
    {
        return $this->fileService->cari($kataKunci, $idUnitKerja, $idFolder);
    }

    //! Pencarian Berkas Spesifik Pegawai
    public function cariByPegawai(?string $kataKunci = NULL, ?string $idFolder = NULL, ?string $idPegawai = NULL)
    {
        return $this->fileService->cariByPegawai($kataKunci, $idFolder, $idPegawai);
    }
    //! Detail Berkas
    public function detail($idSecret, $tipeDokumen)
    {
        switch ($tipeDokumen) {
            case 'sk':
                return $this->SKService->detail($this->decrypt($idSecret));
                break;
            
            case 'surat-masuk':
                return $this->suratMasukService->detail($this->decrypt($idSecret));
                break;
                
            case 'surat-keluar':
                return $this->suratKeluarService->detail($this->decrypt($idSecret));
                break;
            
            case 'dokumen':
                return $this->dokumenService->detail($this->decrypt($idSecret));
                break;
            
            case 'berkas-pegawai':
                return $this->berkasPegawaiService->detail($this->decrypt($idSecret));
                break;
            
            default:
                return $this->SKService->detail($this->decrypt($idSecret));
                break;
        }
    }
}
