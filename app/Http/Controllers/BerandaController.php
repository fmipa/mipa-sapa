<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Services\Beranda\BerandaServiceInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class BerandaController extends Controller
{
    public $routeIndex  = 'beranda';
    public $folder;

    public function __construct(
        protected BerandaServiceInterface $berandaService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.beranda';
    }

    public function index()
    {
    //     switch (auth()->user()->operator) {
    //         case true:
    //             return $datas =  $this->berandaService->indexOperator();
    //             break;
            
    //         default:
                $datas =  $this->berandaService->indexPimpinan();
                // break;
        // }
        return view($this->folder.'.index', [
            'datas'  => $datas,
        ]);
    }

    public function cekLogin(Request $request)
    {
        if(Auth::check())
        {
            if (Auth::user()->activation) {
                return $this->index();
            } else {
                Auth::logout();
                $request->session()->invalidate();
                $request->session()->regenerateToken();
                return redirect()->route('login')->withErrors([
                    'message', 'Akun Tidak Aktif, silahkan menghubungi admin.',
                ]);
            }
        }
        
        return redirect()->route('login')
            ->withErrors([
            'message', 'Tolong Login Terlebih Dahulu.',
        ]);
    }
    
}
