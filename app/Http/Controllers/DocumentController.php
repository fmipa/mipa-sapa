<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Models\Document;
use App\Models\Box;
use App\Models\Extension;
use App\Models\Folder;
use App\Models\JenisAkses;
use App\Models\Pegawai;
use App\Models\PegawaiFile;
use App\Services\Dokumen\DocumentServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use App\Traits\Variables;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class DocumentController extends Controller
{
    use TraitsResponse, DecryptId, Variables;

    public $routeIndex  = 'dokumen.index';
    public $folder;
    protected $jenisAkses, $folders, $boxes, $roles, $pegawais, $accepts;

    public function __construct(
        protected DocumentServiceInterface $documentService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.dokumen';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('document-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'documents' => $this->documentService->allWithOrder('created_at', 'desc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('document-create'), Response::HTTP_FORBIDDEN);auth()->id();
        return view($this->folder.'.create',[
            'jenisAkses'    => $this->jenisAkses(),
            'boxes'         => $this->myBox(),
            'folders'       => $this->folders(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'accepts'       => $this->accepts(),
            'creators'      => $this->users(),
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreDocumentRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('document-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->documentService->store($request->validated());
            if (!$saving instanceof Document) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Document $document)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $documentId)
    {
        abort_if(Gate::denies('document-update'), Response::HTTP_FORBIDDEN);
        
        $data = $this->documentService->findById($this->decrypt($documentId));
        if ($data->file) {
            // ! selectJenisAkses
            $pegawaiId = $data->creator->pegawai_id ?? auth()->user()->pegawai_id;
            $selectJenisAkses = match ($data->file->jenis_akses_id) {
                3,'3' => $data->file->pegawais()->where('pegawai_id', '!=', $pegawaiId)->get()->pluck('id_secret'),
                4,'4' => PegawaiFile::whereFileId($data->file_id)->get()->pluck('unit_kerja_id_secret'),
                5,'5' => [auth()->user()->pegawai->unit_kerja_id],
                default => [$pegawaiId],
            };
            
            // ! selectBox
            $selectBox = $data->file->boxes ? $data->file->boxes->pluck('id_secret') : NULL;
            // ! selectFolder
            $selectFolder = $data->file->folder ? $data->file->folder->id_secret : NULL;

        }
        $selectCreator = $data->created_by ? $data->creator->id_secret : NULL;

        
        return view($this->folder.'.edit', [
            'jenisAkses'    => $this->jenisAkses(),
            'boxes'         =>$this->myBox(),
            'folders'       => $this->folders(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'accepts'       => $this->accepts(),
            'creators'      => $this->users(),
            'data'          => $data,
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
            'selectJenisAkses'  => $selectJenisAkses ?? NULL,
            'selectBox'         => $selectBox ?? NULL,
            'selectFolder'      => $selectFolder ?? NULL,
            'selectCreator'     => $selectCreator ?? NULL,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateDocumentRequest $request, string $documentId): RedirectResponse
    {
        abort_if(Gate::denies('document-update'), Response::HTTP_FORBIDDEN);
        
        $documentData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->documentService->update($request->id, $documentData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $documentId): RedirectResponse
    {
        abort_if(Gate::denies('document-delete'), Response::HTTP_FORBIDDEN);

        try {
            $saving = $this->documentService->delete($this->decrypt($documentId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
