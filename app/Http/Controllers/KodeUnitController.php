<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreKodeUnitRequest;
use App\Http\Requests\UpdateKodeUnitRequest;
use App\Models\KodeUnit;
use App\Services\KodeUnit\KodeUnitServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class KodeUnitController extends Controller
{
    use TraitsResponse, DecryptId;

    public $routeIndex  = 'kode-unit.index';
    public $folder;

    public function __construct(
        protected KodeUnitServiceInterface $kodeUnitService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.kode-unit';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('kodeunit-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'kodeunits' => $this->kodeUnitService->allWithOrder('kode', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('kodeunit-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreKodeUnitRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('kodeunit-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->kodeUnitService->store($request->validated());
            if (!$saving instanceof KodeUnit) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(KodeUnit $kodeUnit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $kodeUnitId): View
    {
        abort_if(Gate::denies('kodeunit-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
        'data'  => $this->kodeUnitService->findById($this->decrypt($kodeUnitId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateKodeUnitRequest $request, string $kodeUnitId): RedirectResponse
    {
        abort_if(Gate::denies('kodeunit-update'), Response::HTTP_FORBIDDEN);
        
        $kodeUnitData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->kodeUnitService->update($request->id, $kodeUnitData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $kodeUnitId): RedirectResponse
    {
        try {
            $saving = $this->kodeUnitService->delete($this->decrypt($kodeUnitId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
