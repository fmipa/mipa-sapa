<?php

namespace App\Http\Controllers;

use App\Enums\JenisKelaminEnum;
use App\Enums\StatusPegawaiEnum;
use App\Http\Requests\StorePegawaiRequest;
use App\Http\Requests\UpdatePegawaiRequest;
use App\Models\MasterAgama;
use App\Models\Pegawai;
use App\Models\Prodi;
use App\Services\Pegawai\PegawaiServiceInterface;
use App\Services\User\UserServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Helper;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class PegawaiController extends Controller
{
    use TraitsResponse, DecryptId, Helper;

    public $routeIndex  = 'pegawai.index';
    public $folder;

    public function __construct(
        protected PegawaiServiceInterface $pegawaiService,
        protected UserServiceInterface $userService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.pegawai';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('pegawai-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
        'pegawais' => $this->pegawaiService->allWithOrder('firstname', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('pegawai-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create', [
            'roles'     => Role::View(true)->toArray(),
            'agamas'    => MasterAgama::View()->toArray(),
            'prodis'    => Prodi::View()->toArray(),
            'jenisKelamin'  => $this->enumView(JenisKelaminEnum::cases(), false),
            'status'    => $this->enumView(StatusPegawaiEnum::cases(), false),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePegawaiRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('pegawai-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->pegawaiService->store($request->validated());
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Pegawai $pegawai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $pegawaiId): View
    {
        abort_if(Gate::denies('pegawai-update'), Response::HTTP_FORBIDDEN);
        
        $data = $this->pegawaiService->findById($this->decrypt($pegawaiId));
        return view($this->folder.'.edit', [
            'data'      => $data,
            'roles'     => Role::View(true)->toArray(),
            'agamas'    => MasterAgama::View()->toArray(),
            'prodis'    => Prodi::View()->toArray(),
            'jenisKelamin'  => $this->enumView(JenisKelaminEnum::cases(), false),
            'status'    => $this->enumView(StatusPegawaiEnum::cases(), false),
            'selectRoles'  => $data->user ? array_column($data->user->roles->toArray(), 'id_secret') : NULL,
            'selectProdi'  => $data->prodi_id ? $this->encrypt($data->prodi_id) : NULL,
            'selectAgama'  => $data->agama_id ? $this->encrypt($data->agama_id) : NULL,
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePegawaiRequest $request, string $pegawaiId): RedirectResponse
    {
        abort_if(Gate::denies('pegawai-update'), Response::HTTP_FORBIDDEN);
        
        $pegawaiData = array_filter($request->validated(), fn ($value) => !is_null($value));
        $pegawaiData['prodi_id'] = $request->prodi_id ?? NULL;
        try {
            $saving = $this->pegawaiService->update($request->id, $pegawaiData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $pegawaiId): RedirectResponse
    {
        try {
            $saving = $this->pegawaiService->delete($this->decrypt($pegawaiId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    public function eRapat()
    {
        return view($this->folder.'.erapat', [
            'pegawais' => $this->pegawaiService->exportERapat(),
        ]);
    }
}
