<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreFolderStorageRequest;
use App\Http\Requests\UpdateFolderStorageRequest;
use App\Models\Folder;
use App\Models\FolderStorage;
use App\Models\JenisAkses;
use App\Models\Pegawai;
use App\Services\FolderStorage\FolderStorageServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Helper;
use App\Traits\Response as TraitsResponse;
use App\Traits\Variables;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class FolderStorageController extends Controller
{
    use TraitsResponse, DecryptId, Helper, Variables;

    public $routeIndex  = 'folder-storage.index';
    public $folder;

    public function __construct(
        protected FolderStorageServiceInterface $folderStorageService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.folder-storage';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('index-folderstorage'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'folderStorages' => $this->folderStorageService->allWithOrder('nama', 'asc', ['id', 'nama', 'jenis_akses_id', 'created_by']),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('create-folderstorage'), Response::HTTP_FORBIDDEN);
        return view($this->folder.'.create',[
            'unitKerjas'    => $this->unitKerjas(),
            'pegawais'      => $this->pegawais(),
            'jenisAkses'    => $this->jenisAkses(),
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFolderStorageRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('create-folderstorage'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->folderStorageService->store($request->validated());
            if (!$saving instanceof FolderStorage) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(FolderStorage $folder)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $folderStorageId): View
    {
        abort_if(Gate::denies('update-folderstorage'), Response::HTTP_FORBIDDEN);
        
        $data = $this->folderStorageService->findById($this->decrypt($folderStorageId));
        switch ($data->jenis_akses_id) {
            case 3:
                $selected = $data->pegawais()->where('pegawai_id', '!=', auth()->user()->pegawai_id)->get()->pluck('id_secret');
                $selected->toArray();
                break;
            
            case 4:
                $selected = auth()->user()->roles->pluck('id_secret');
                break;
            
            default:
                $selected = [auth()->user()->pegawai_id];
                break;
        }
        
        return view($this->folder.'.edit', [
            'unitKerjas'    => $this->unitKerjas(),
            'pegawais'      => $this->pegawais(),
            'jenisAkses'    => $this->jenisAkses(),
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
            'data'      => $data,
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFolderStorageRequest $request, string $folderStorageId): RedirectResponse
    {
        abort_if(Gate::denies('update-folderstorage'), Response::HTTP_FORBIDDEN);
        
        $folderStorageData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->folderStorageService->update($request->id, $folderStorageData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $folderStorageId): RedirectResponse
    {
        try {
            $saving = $this->folderStorageService->delete($this->decrypt($folderStorageId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
