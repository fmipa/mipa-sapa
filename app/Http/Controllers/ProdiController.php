<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProdiRequest;
use App\Http\Requests\UpdateProdiRequest;
use App\Models\Jurusan;
use App\Models\Prodi;
use App\Services\Prodi\ProdiServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class ProdiController extends Controller
{
    use TraitsResponse, DecryptId;

    public $routeIndex  = 'prodi.index';
    public $folder;

    public function __construct(
        protected ProdiServiceInterface $prodiService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.prodi';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('prodi-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'prodis' => $this->prodiService->allWithOrder('nama', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('prodi-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create', [
            'jurusans' => Jurusan::View()->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProdiRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('prodi-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->prodiService->store($request->validated());
            if (!$saving instanceof Prodi) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Prodi $prodi)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $prodiId)
    {
        abort_if(Gate::denies('prodi-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
            'jurusans' => Jurusan::View()->toArray(),
            'data'  => $this->prodiService->findById($this->decrypt($prodiId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProdiRequest $request, string $prodiId): RedirectResponse
    {
        abort_if(Gate::denies('prodi-update'), Response::HTTP_FORBIDDEN);
        
        $prodiData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->prodiService->update($request->id, $prodiData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $prodiId): RedirectResponse
    {
        try {
            $saving = $this->prodiService->delete($this->decrypt($prodiId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
