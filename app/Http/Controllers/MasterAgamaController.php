<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMasterAgamaRequest;
use App\Http\Requests\UpdateMasterAgamaRequest;
use App\Models\MasterAgama;
use App\Services\MasterAgama\MasterAgamaServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;

class MasterAgamaController extends Controller
{
    use TraitsResponse, DecryptId;
    public $routeIndex  = 'master-agama.index';
    public $folder;

    public function __construct(
        protected MasterAgamaServiceInterface $agamaService,
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.master-agama';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('agama-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'agamas' => $this->agamaService->allWithOrder('agama', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('agama-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreMasterAgamaRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('agama-create'), Response::HTTP_FORBIDDEN);

        try {
            $saving = $this->agamaService->store($request->validated());
            if (!$saving instanceof MasterAgama) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(MasterAgama $agama)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit($agamaId): View
    {
        abort_if(Gate::denies('agama-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
            'data'  => $this->agamaService->findById($this->decrypt($agamaId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateMasterAgamaRequest $request, $agamaId): RedirectResponse
    {
        abort_if(Gate::denies('agama-update'), Response::HTTP_FORBIDDEN);

        $agamaData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->agamaService->update($this->decrypt($agamaId), $agamaData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy($agamaId): RedirectResponse
    {
        try {
            $saving = $this->agamaService->delete($this->decrypt($agamaId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
