<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreExtensionRequest;
use App\Http\Requests\UpdateExtensionRequest;
use App\Models\Extension;
use App\Services\Extension\ExtensionServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class ExtensionController extends Controller
{
    use TraitsResponse, DecryptId;

    public $routeIndex  = 'ekstensi.index';
    public $folder;

    public function __construct(
        protected ExtensionServiceInterface $extensionService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.extension';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('extension-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'extensions' => $this->extensionService->allWithOrder('extension', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('extension-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreExtensionRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('extension-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->extensionService->store($request->validated());
            if (!$saving instanceof Extension) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Extension $extension)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $extensionId): View
    {
        abort_if(Gate::denies('extension-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
            'data'  => $this->extensionService->findById($this->decrypt($extensionId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateExtensionRequest $request, string $extensionId): RedirectResponse
    {
        abort_if(Gate::denies('extension-update'), Response::HTTP_FORBIDDEN);
        
        $extensionData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->extensionService->update($request->id, $extensionData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $extensionId): RedirectResponse
    {
        abort_if(Gate::denies('extension-delete'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->extensionService->delete($this->decrypt($extensionId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
