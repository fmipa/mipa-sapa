<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSuratKeluarRequest;
use App\Http\Requests\UpdateSuratKeluarRequest;
use App\Models\Box;
use App\Models\Extension;
use App\Models\Folder;
use App\Models\JenisAkses;
use App\Models\KodeNaskah;
use App\Models\Pegawai;
use App\Models\PegawaiFile;
use App\Models\SuratKeluar;
use App\Services\SuratKeluar\SuratKeluarServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use App\Traits\Variables;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class SuratKeluarController extends Controller
{
    use TraitsResponse, DecryptId, Variables;

    public $routeIndex  = 'surat-keluar.index';
    public $folder;

    public function __construct(
        protected SuratKeluarServiceInterface $suratKeluarService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.surat-keluar';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('suratkeluar-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'suratKeluars' => $this->suratKeluarService->allWithOrder('created_at', 'desc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('suratkeluar-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create', [
            'jenisAkses'    => $this->jenisAkses(),
            'folders'       => $this->folders(),
            'boxes'         => $this->boxes(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'accepts'       => $this->accepts(),
            'kodeNaskahs'   => $this->kodeNaskahs(),
            'creators'      => $this->users(),
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
            'selectFolder'  => $this->myFolder()->first()->parent_id_secret,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSuratKeluarRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('suratkeluar-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->suratKeluarService->store($request->validated());
            if (!$saving instanceof SuratKeluar) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(SuratKeluar $suratKeluar)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $suratKeluarId): View
    {
        abort_if(Gate::denies('suratkeluar-update'), Response::HTTP_FORBIDDEN);
        
        $data = $this->suratKeluarService->findById($this->decrypt($suratKeluarId));
        if ($data->file) {
            // ! selectJenisAkses
            $pegawaiId = $data->creator->pegawai_id ?? auth()->user()->pegawai_id;
            $selectJenisAkses = match ($data->file->jenis_akses_id) {
                3,'3' => $data->file->pegawais()->where('pegawai_id', '!=', $pegawaiId)->get()->pluck('id_secret'),
                4,'4' => PegawaiFile::whereFileId($data->file_id)->get()->pluck('unit_kerja_id_secret'),
                5,'5' => [auth()->user()->pegawai->unit_kerja_id],
                default => [$pegawaiId],
            };
            
            // ! selectBox
            $selectBox = $data->file->boxes ? $data->file->boxes->pluck('id_secret') : NULL;
            // ! selectFolder
            $selectFolder = $data->file->folder ? $data->file->folder->id_secret : NULL;
        }
        $selectCreator = $data->created_by ? $data->creator->id_secret : NULL;


        return view($this->folder.'.edit', [
            'jenisAkses'    => $this->jenisAkses(),
            'folders'       => $this->folders(),
            'boxes'         => $this->boxes(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'accepts'       => $this->accepts(),
            'kodeNaskahs'   => $this->kodeNaskahs(),
            'creators'      => $this->users(),
            'data'          => $data,
            'selectJenisAkses'  => $selectJenisAkses ?? NULL,
            'selectBox'         => $selectBox ?? NULL,
            'selectFolder'      => $selectFolder ?? NULL,
            'selectCreator'     => $selectCreator ?? NULL,
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSuratKeluarRequest $request, string $suratKeluarId): RedirectResponse
    {
        abort_if(Gate::denies('suratkeluar-update'), Response::HTTP_FORBIDDEN);
        
        $suratKeluarData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->suratKeluarService->update($request->id, $suratKeluarData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $suratKeluarId): RedirectResponse
    {
        abort_if(Gate::denies('suratkeluar-delete'), Response::HTTP_FORBIDDEN);

        try {
            $saving = $this->suratKeluarService->delete($this->decrypt($suratKeluarId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
