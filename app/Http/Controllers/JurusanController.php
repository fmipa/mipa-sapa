<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreJurusanRequest;
use App\Http\Requests\UpdateJurusanRequest;
use App\Models\Jurusan;
use App\Services\Jurusan\JurusanServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class JurusanController extends Controller
{
    use TraitsResponse, DecryptId;

    public $routeIndex  = 'jurusan.index';
    public $folder;

    public function __construct(
        protected JurusanServiceInterface $jurusanService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.jurusan';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('jurusan-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'jurusans' => $this->jurusanService->allWithOrder('nama', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('jurusan-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreJurusanRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('jurusan-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->jurusanService->store($request->validated());
            if (!$saving instanceof Jurusan) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Jurusan $jurusan)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $jurusanId): View
    {
        abort_if(Gate::denies('jurusan-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
            'data'  => $this->jurusanService->findById($this->decrypt($jurusanId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateJurusanRequest $request, string $jurusanId): RedirectResponse
    {
        abort_if(Gate::denies('jurusan-update'), Response::HTTP_FORBIDDEN);
        
        $jurusanData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->jurusanService->update($request->id, $jurusanData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $jurusanId): RedirectResponse
    {
        try {
            $saving = $this->jurusanService->delete($this->decrypt($jurusanId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
