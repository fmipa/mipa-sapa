<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Services\Role\RoleServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Helper;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    use TraitsResponse, DecryptId, Helper;

    public $routeIndex  = 'hak-akses.index';
    public $folder;

    public function __construct(
        protected RoleServiceInterface $roleService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.role';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('hakakses-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'roles' => $this->roleService->allWithOrder('name', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('hakakses-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create', [
            'permissions' => Permission::orderBy('id', 'asc')->View()->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRoleRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('hakakses-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->roleService->store($request->validated());
            if (!$saving instanceof Role) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $roleId): View
    {
        abort_if(Gate::denies('hakakses-update'), Response::HTTP_FORBIDDEN);
        
        $data = $this->roleService->findById($this->decrypt($roleId));
        $data['permissions'] = array_column($this->enumView($data->permissions->pluck('id'), false, true), 'key');
        return view($this->folder.'.edit', [
            'data'          => $data,
            'permissions'   => Permission::orderBy('id', 'asc')->View()->toArray(),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRoleRequest $request, string $roleId): RedirectResponse
    {
        abort_if(Gate::denies('hakakses-update'), Response::HTTP_FORBIDDEN);
        
        $roleData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->roleService->update($request->id, $roleData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $roleId): RedirectResponse
    {
        abort_if(Gate::denies('hakakses-delete'), Response::HTTP_FORBIDDEN);

        try {
            $saving = $this->roleService->delete($this->decrypt($roleId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
