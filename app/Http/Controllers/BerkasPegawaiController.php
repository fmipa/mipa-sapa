<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBerkasPegawaiRequest;
use App\Http\Requests\UpdateBerkasPegawaiRequest;
use App\Models\BerkasPegawai;
use App\Models\PegawaiFile;
use App\Services\BerkasPegawai\BerkasPegawaiServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use App\Traits\Variables;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;


class BerkasPegawaiController extends Controller
{
    use TraitsResponse, DecryptId, Variables;

    public $routeIndex  = 'berkas-pegawai.index';
    public $folder;
    public $title       = 'Berkas Pegawai';

    public function __construct(
        protected BerkasPegawaiServiceInterface $berkasPegawaiService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.berkas-pegawai';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('berkaspegawai-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'berkas' => $this->berkasPegawaiService->allWithOrder('created_at', 'desc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('berkaspegawai-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create',[
            'title'         => $this->title,
            'jenisAkses'    => $this->jenisAkses(),
            'folders'       => $this->folders(),
            'boxes'         => $this->boxes(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'accepts'       => $this->accepts(),
            'kategoris'     => $this->kategoriBerkas(),
            'creators'      => $this->users(),
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBerkasPegawaiRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('berkaspegawai-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->berkasPegawaiService->store($request->validated());
            if (!$saving instanceof BerkasPegawai) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(BerkasPegawai $berkasPegawai)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $berkasPegawaiId)
    {
        abort_if(Gate::denies('berkaspegawai-update'), Response::HTTP_FORBIDDEN);
        
        $data = $this->berkasPegawaiService->findById($this->decrypt($berkasPegawaiId));
        if ($data->file) {
            // ! selectJenisAkses
            // switch ($data->file->jenis_akses_id) {
            //     case 3:
            //         $selectJenisAkses = $data->file->pegawais()->where('pegawai_id', '!=', auth()->user()->pegawai_id)->get()->pluck('id_secret');
            //         $selectJenisAkses->toArray();
            //         break;
                
            //     case 4:
            //         $selectJenisAkses = auth()->user()->roles->pluck('id_secret');
            //         break;
                
            //     default:
            //         $selectJenisAkses = [auth()->user()->pegawai_id];
            //         break;
            // }
            // return $data->file->unitKerjas()->get();
            $pegawaiId = $data->creator->pegawai_id ?? auth()->user()->pegawai_id;
            $selectJenisAkses = match ($data->file->jenis_akses_id) {
                3,'3' => $data->file->pegawais()->where('pegawai_id', '!=', $pegawaiId)->get()->pluck('id_secret'),
                4,'4' => PegawaiFile::whereFileId($data->file_id)->get()->pluck('unit_kerja_id_secret'),
                5,'5' => [auth()->user()->pegawai->unit_kerja_id],
                default => [$pegawaiId],
            };
            // ! selectBox
            $selectBox = $data->file->boxes ? $data->file->boxes->pluck('id_secret') : NULL;
            // ! selectFolder
            $selectFolder = $data->file->folder ? $data->file->folder->id_secret : NULL;
        }
        $selectCreator = $data->created_by ? $data->creator->id_secret : NULL;

        return view($this->folder.'.edit', [
            'title'         => $this->title,
            'jenisAkses'    => $this->jenisAkses(),
            'folders'       => $this->folders(),
            'boxes'         => $this->boxes(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'creators'      => $this->users(),
            'kategoris'     => $this->kategoriBerkas(),
            'accepts'       => $this->accepts(),
            'data'          => $data,
            'selectJenisAkses'  => $selectJenisAkses ?? NULL,
            'selectBox'         => $selectBox ?? NULL,
            'selectFolder'      => $selectFolder ?? NULL,
            'selectCreator'     => $selectCreator ?? NULL,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBerkasPegawaiRequest $request, string $berkasPegawaiId)
    {
        abort_if(Gate::denies('berkaspegawai-update'), Response::HTTP_FORBIDDEN);
        
        $berkasPegawaiData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->berkasPegawaiService->update($request->id, $berkasPegawaiData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $berkasPegawaiId): RedirectResponse
    {
        abort_if(Gate::denies('berkaspegawai-delete'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->berkasPegawaiService->delete($this->decrypt($berkasPegawaiId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
