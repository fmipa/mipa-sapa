<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreKodeNaskahRequest;
use App\Http\Requests\UpdateKodeNaskahRequest;
use App\Models\KodeNaskah;
use App\Services\KodeNaskah\KodeNaskahServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class KodeNaskahController extends Controller
{
    use TraitsResponse, DecryptId;

    public $routeIndex  = 'kode-naskah.index';
    public $folder;
    public $title       = 'Kode Naskah';

    public function __construct(
        protected KodeNaskahServiceInterface $kodeNaskahService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.kode-naskah';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('kodenaskah-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'kodeNaskahs'  => $this->kodeNaskahService->allWithOrder('id', 'asc'),
            'title' => $this->title,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('kodenaskah-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create', [
            'title' => $this->title,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreKodeNaskahRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('kodenaskah-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->kodeNaskahService->store($request->validated());
            if (!$saving instanceof KodeNaskah) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(KodeNaskah $kSK)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $kodeNaskahId): View
    {
        abort_if(Gate::denies('kodenaskah-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
            'data'  => $this->kodeNaskahService->findById($this->decrypt($kodeNaskahId)),
            'title' => $this->title,
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateKodeNaskahRequest $request, string $kodeNaskahId): RedirectResponse
    {
        abort_if(Gate::denies('kodenaskah-update'), Response::HTTP_FORBIDDEN);
        
        $kodeNaskahData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->kodeNaskahService->update($request->id, $kodeNaskahData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $kodeNaskahId): RedirectResponse
    {
        abort_if(Gate::denies('kodenaskah-delete'), Response::HTTP_FORBIDDEN);

        try {
            $saving = $this->kodeNaskahService->delete($this->decrypt($kodeNaskahId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
