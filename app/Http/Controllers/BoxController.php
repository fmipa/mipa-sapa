<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBoxRequest;
use App\Http\Requests\UpdateBoxRequest;
use App\Models\Box;
use App\Services\Box\BoxServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Helper;
use App\Traits\QR;
use App\Traits\Response as TraitsResponse;
use App\Traits\Variables;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;
use Barryvdh\DomPDF\Facade\Pdf;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class BoxController extends Controller
{
    use TraitsResponse, DecryptId, Helper, QR, Variables;

    public $routeIndex  = 'box.index';
    public $folder;

    public function __construct(
        protected BoxServiceInterface $boxService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.box';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('box-index'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.index', [
            'boxes' => $this->boxService->allWithOrder('nomor', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('box-create'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.create', [
            'nomorUrut' => ( DB::table('boxes')->max('nomor') ?? 0 ) + 1,
            'tahuns'    => $this->generateTahun(1997, (date('Y') + 1), true, true),
            'unitKerjas'    => $this->unitKerjas(true),
            // 'selected'  => array_column(auth()->user()->roles->toArray(), 'id_secret'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreBoxRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('box-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->boxService->store($request->validated());
            if (!$saving instanceof Box) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Box $box)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $boxId): View
    {
        abort_if(Gate::denies('box-update'), Response::HTTP_FORBIDDEN);
        $data =  $this->boxService->findById($this->decrypt($boxId));
        $selectTahun = explode(',', $data->tahun);
        return view($this->folder.'.edit', [
            'data'          => $data,
            'tahuns'        => $this->generateTahun(1997, (date('Y') + 1), true, true),
            'unitKerjas'    => $this->unitKerjas(true, true),
            'selectTahun'   => $selectTahun,
            'selectUnit'    => $data->unit_kerja_id ? $this->encrypt($data->unit_kerja_id) : NULL,
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBoxRequest $request, string $boxId): RedirectResponse
    {
        abort_if(Gate::denies('box-update'), Response::HTTP_FORBIDDEN);
        
        $boxData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->boxService->update($request->id, $boxData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $boxId): RedirectResponse
    {
        try {
            $saving = $this->boxService->delete($this->decrypt($boxId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    public function printLabel($uniqueId, $tipeLabel = 'box')
    {
        $data   = Box::whereUniqueId($uniqueId)->first(['id', 'unique_id', 'nama', 'nomor', 'bagian', 'tahun', 'lokasi'])->toArray();
        $ukuran = match ($tipeLabel) {
            'gobi'      => 100,
            'box-file'  => 120,
            'map'       => 100,
            default     => 200,
        };
        $qrcode = $this->generateSVG($data['unique_id'], $ukuran, 'round', '#00008B');
        // $data['qr_code']    = ($qrcode);
        $data['qr_code']    = base64_encode($qrcode);
        $data['tipeLabel']  = ucwords($tipeLabel);
        $pages = match ($tipeLabel) {
            'gobi'      => 'pages.label-box.gobi-label',
            'box-file'  => 'pages.label-box.box-file-label',
            'map'       => 'pages.label-box.map-label',
            default     => 'pages.label-box.kotak-label',
        };
        // return $data;
        // return view($pages, [ 'data' => $data]);
        $pdf = Pdf::loadView($pages, ['data' => $data])
            ->setPaper('a4', 'landscape')
            ->setWarnings(false)
            ->setOption(['dpi' => 150, 'defaultFont' => 'sans-serif']);
        return $pdf->download('Label '.$data['tipeLabel'].' Nomor '.$data['nomor'].'.pdf');
    }

    public function textToImg()
    {
        $label = "Y-Axis Label";
        $labelfont = 2;

        // pixel-width of label
        $txtsz = imagefontwidth($labelfont) * strlen($label);
        // pixel-height of label
        $txtht = imagefontheight($labelfont);

        // define temp image
        $labelimage = imagecreate($txtsz, $txtsz);
        $white = imagecolorallocate($labelimage, 0xFF, 0xFF, 0xFF);
        $black = imagecolorallocate($labelimage, 0x00, 0x00, 0x00);

        // write to the temp image
        imagestring($labelimage, $labelfont, 0, $txtsz/2 - $txtht/2, $label , $black);

        // rotate the temp imagetransform-origin: left bottom 0;
        $labelimage1 = imagerotate($labelimage, 90, $white);

        // copy the temp image back to the real image
        imagecopy($image, $labelimage1, 3, $vmargin + $ysize/2 - $txtsz/2, $txtsz/2 - $txtht/2, 0, $txtht, $txtsz);

        // destroy temp images, clear memory
        imagedestroy($labelimage);
        imagedestroy($labelimage1);
    }

}
