<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;
use App\Services\Permission\PermissionServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


class PermissionController extends Controller
{
    use TraitsResponse, DecryptId;

    public $routeIndex  = 'izin.index';
    public $folder;

    public function __construct(
        protected PermissionServiceInterface $permissionService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.permission';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('permission-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'permissions' => $this->permissionService->allWithOrder('id', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('permission-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePermissionRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('permission-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->permissionService->store($request->validated());
            if (!$saving instanceof Permission) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $permissionId): View
    {
        abort_if(Gate::denies('permission-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
            'data'  => $this->permissionService->findById($this->decrypt($permissionId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePermissionRequest $request, string $permissionId): RedirectResponse
    {
        abort_if(Gate::denies('permission-update'), Response::HTTP_FORBIDDEN);
        
        $permissionData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->permissionService->update($request->id, $permissionData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $permissionId): RedirectResponse
    {
        abort_if(Gate::denies('permission-delete'), Response::HTTP_FORBIDDEN);

        try {
            $saving = $this->permissionService->delete($this->decrypt($permissionId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
