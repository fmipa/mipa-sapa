<?php

namespace App\Http\Controllers;

use App\Enums\FolderAdminJurusanEnum;
use App\Enums\RoleEnum;
use App\Http\Requests\StoreFolderRequest;
use App\Http\Requests\UpdateFolderRequest;
use App\Models\Folder;
use App\Models\FolderStorage;
use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use App\Services\Folder\FolderServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Jurusan;
use App\Traits\JurusanFolder;
use App\Traits\Mutators\EncryptId;
use App\Traits\Response as TraitsResponse;
use App\Traits\Variables;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;

class FolderController extends Controller
{
    use TraitsResponse, DecryptId, EncryptId, JurusanFolder, Variables;

    public $routeIndex  = 'folder.index';
    public $folder;

    public function __construct(
        protected FolderServiceInterface $folderService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.folder';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('folder-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'parentFolders' => $this->folders(),
            'prodis'        => $this->prodis(),
            'users'         => $this->users(),
            'folders'       => $this->folderService->whereBy('parent_id', 0),
            'ranting'       => Folder::pluck('nama', 'id')->all(),
            'unitKerjas'    => $this->unitKerjas(true),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('folder-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create', [
            'bagians' => Role::whereIn('name', ['dosen','akademik','kepegawaian','keuangan','umper','admin-jurusan','laboran','mitra'])->ViewUnit(true)->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreFolderRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('folder-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->folderService->store($request->validated());
            if (!$saving instanceof Folder) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show($folderId)
    {
        $data = $this->folderService->findById($this->decrypt($folderId));
        $data['idSecret_unitKerja'] = $this->encrypt($data->unit_kerja_id);
        $data['idSecret_prodi']     = $data->prodi_id ? $this->encrypt($data->prodi_id) : NULL;
        $data['idSecret_creator']   = $data->created_by ? $this->encrypt($data->created_by) : NULL;
        return $data;
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $folderId): View
    {
        abort_if(Gate::denies('folder-update'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.edit', [
            'data'  => $this->folderService->findById($this->decrypt($folderId)),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateFolderRequest $request, string $folderId): RedirectResponse
    {
        abort_if(Gate::denies('folder-update'), Response::HTTP_FORBIDDEN);
        
        $folderData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->folderService->update($request->id, $folderData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $folderId): RedirectResponse
    {
        abort_if(Gate::denies('folder-delete'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->folderService->delete($this->decrypt($folderId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

}
