<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUserProfileRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserProfileRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\User;
use App\Services\User\UserServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class UserController extends Controller
{
    use TraitsResponse, DecryptId;
    public $routeIndex  = 'pengguna.index';
    public $folder;


    public function __construct(
        protected UserServiceInterface $userService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.user';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('user-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'roles' => Role::View()->toArray(),
            'users' => $this->userService->allWithOrder('name', 'asc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('user-create'), Response::HTTP_FORBIDDEN);

        return view($this->folder.'.create', [
            'roles' => Role::View()->toArray(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {
        // return $request->validated();
        abort_if(Gate::denies('user-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->userService->store($request->validated());
            if (!$saving instanceof User) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $userId): View
    {
        abort_if(Gate::denies('user-update'), Response::HTTP_FORBIDDEN);
        
        $data = $this->userService->findById($this->decrypt($userId));
        return view($this->folder.'.edit', [
            'roles' => Role::View()->toArray(),
            'data'  => $data,
            'selected'  => array_column($data->roles->toArray(), 'id_secret'),
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, string $userId): RedirectResponse
    {
        abort_if(Gate::denies('user-update'), Response::HTTP_FORBIDDEN);
        
        $userData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->userService->update($request->id, $userData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $userId): RedirectResponse
    {
        try {
            $saving = $this->userService->delete($this->decrypt($userId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Update Profile.
     */
    public function updateProfile(UpdateUserProfileRequest $request): RedirectResponse
    {
        try {
            $saving = $this->userService->update(auth()->id(), $request->validated());
            if (!$saving instanceof User) {
                throw new Exception($saving);
            }
            return redirect()->back()->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }
}
