<?php

namespace App\Http\Controllers;

use App\Enums\TipeFileEnum;
use App\Models\BerkasPegawai;
use App\Models\Box;
use App\Models\Document;
use App\Models\SK;
use App\Models\SuratKeluar;
use App\Models\SuratMasuk;
use App\Services\BerkasPegawai\BerkasPegawaiServiceInterface;
use App\Services\Dokumen\DocumentServiceInterface;
use App\Services\SK\SKServiceInterface;
use App\Services\SuratKeluar\SuratKeluarServiceInterface;
use App\Services\SuratMasuk\SuratMasukServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class PencarianController extends Controller
{
    use TraitsResponse, DecryptId;

    protected $berkasPegawaiService, $documentService, $suratMasukService, $suratKeluarService, $SKService;
    public function __construct(
        BerkasPegawaiServiceInterface $berkasPegawaiService,
        DocumentServiceInterface $documentService,
        SuratMasukServiceInterface $suratMasukService,
        SuratKeluarServiceInterface $suratKeluarService,
        SKServiceInterface $SKService,
    ) {
        $this->berkasPegawaiService = $berkasPegawaiService;
        $this->documentService      = $documentService;
        $this->suratMasukService    = $suratMasukService;
        $this->suratKeluarService   = $suratKeluarService;
        $this->SKService            = $SKService;
    }
    public function byBox(string $boxId)
    {   
        $berkasPegawai = BerkasPegawai::whereHas('file.boxes', function($query) use ($boxId) {
            $query->whereUniqueId($boxId);
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama;
            $map['kategori'] = 'Berkas Pegawai';
            $map['tipeFile'] = 'berkas-pegawai';
            return $map;
        });

        $document = Document::whereHas('file.boxes', function($query) use ($boxId) {
            $query->whereUniqueId($boxId);
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nama;
            $map['judul'] = $map->nama;
            $map['kategori'] = 'Dokumen';
            $map['tipeFile'] = 'dokumen';
            return $map;
        });


        $suratMasuk = SuratMasuk::whereHas('file.boxes', function($query) use ($boxId) {
            $query->whereUniqueId($boxId);
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomor;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Masuk';
            $map['tipeFile'] = 'surat-masuk';
            return $map;
        });

        $suratKeluar = SuratKeluar::whereHas('file.boxes', function($query) use ($boxId) {
            $query->whereUniqueId($boxId);
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomorSurat;
            $map['judul'] = $map->perihal;
            $map['kategori'] = 'Surat Keluar';
            $map['tipeFile'] = 'surat-keluar';
            return $map;
        });

        $SK = SK::whereHas('file.boxes', function($query) use ($boxId) {
            $query->whereUniqueId($boxId);
        })
        ->with('file.folder','file.boxes')->get()->map(function($map){
            $map['nomor'] = $map->nomorSk;
            $map['judul'] = $map->judul;
            $map['kategori'] = 'SK';
            $map['tipeFile'] = 'sk';
            return $map;
        });
        
        $datas = new \Illuminate\Database\Eloquent\Collection; //Create empty collection which we know has the concat() method
        $datas = $datas->concat($suratMasuk);
        $datas = $datas->concat($suratKeluar);
        $datas = $datas->concat($SK);
        $datas = $datas->concat($berkasPegawai);
        $datas = $datas->concat($document);
        
        return view('pages.'.config('variables.templateName').'.box.qrcode', [
            'datas' => $datas,
            'box'   => Box::whereUniqueId($boxId)->first(['id', 'nama', 'lokasi', 'tahun', 'unique_id', 'password']),
        ]);
    }

    protected function getDataFile($id, $tipeFile)
    {
        switch ($tipeFile) {
            case TipeFileEnum::BERKASPEGAWAI->value:
                $data = $this->berkasPegawaiService->findById($id);
                $data['nama_file_download'] = $data->nama;
                break;
            
            case TipeFileEnum::DOKUMEN->value:
                $data = $this->documentService->findById($id);
                $data['nama_file_download'] = $data->nama;
                break;
            
            case TipeFileEnum::SURATMASUK->value:
                $data = $this->suratMasukService->findById($id);
                $data['nama_file_download'] = $data->nomor;
                break;
            
            case TipeFileEnum::SURATKELUAR->value:
                $data = $this->suratKeluarService->findById($id);
                $data['nama_file_download'] = $data->nomor;
                break;

            case TipeFileEnum::SK->value:
                $data = $this->SKService->findById($id);
                $data['nama_file_download'] = $data->nomor_sk;
                break;
            
            default:
                $data = $this->SKService->findById($id);
                $data['nama_file_download'] = $data->nomor_sk;
                break;
        }

        return $data ?? 'false';
    }

    public function viewFile($idSecret, $tipeFile)
    {
        $id     = $this->decrypt($idSecret);
        $data   = $this->getDataFile($id, $tipeFile);
        
        try {
            if ($data) {
                DB::beginTransaction();
                $data->file()->update(['view' => ($data->file->view+1)]);
                DB::commit();
                $file = storage_path($data->urlFile);
                return response()->file($file, ['Content-Type: '.$data->file->mime_type]);
            } else {
                DB::rollback();
                return redirect()->back()->with('status', 'Data tidak ditemukan');
            }
        } catch (\Throwable $th) {
            // return $th->getMessage();
            return redirect()->back()->with('status', 'Ada Kesalahan pada Server');
            return response()->with('status', 'Ada Kesalahan pada Server');
        }
    }

    public function downloadFile($idSecret, $tipeFile)
    {
        $id     = $this->decrypt($idSecret);
        $data   = $this->getDataFile($id, $tipeFile);

        try {
            if ($data) {
                DB::beginTransaction();
                $file = storage_path($data->urlFile);
                $newNama = str_replace(array("/", "\\", ":", "*", "?", "«", "<", ">", "|"), "-", ($data->nama_file_download.'.'.$data->file->extension));
                $data->file()->update(['download' => ($data->file->download+1)]);
                DB::commit();
                return response()->download($file, $newNama, ['Content-Type: '.$data->file->mime_type]);
            } else {
                DB::rollback();
                return redirect()->back()->with('status', 'Data tidak ditemukan');
            }
        } catch (\Throwable $th) {
            // return $th->getMessage();
            return redirect()->back()->with('status', 'Ada Kesalahan pada Server');
            return response()->with('status', 'Ada Kesalahan pada Server');
        }
    }

    public function viewFileBox($uniqueId, $idSecret, $tipeFile, $password)
    {
        $id     = $this->decrypt($idSecret);
        $data   = false;
        if ($password == 'fmipa' && !auth()->check()) {
            return redirect()->route('login'); 
        }
        if ($password) {
            if (Box::whereUniqueId($uniqueId)->when($password != 'fmipa', function($query) use ($password) {
                $query->wherePassword($password);
            })
            ->count() > 0) {
                $data   = $this->getDataFile($id, $tipeFile);
            } else {
                return redirect()->back()->withErrors('Password Box/Gobi Salah, Silahkan tanya ke Admin atau Operator yang mempunyai Box tersebut.');
            }
        }
        try {
            if ($data) {
                DB::beginTransaction();
                $data->file()->update(['view' => ($data->file->view+1)]);
                DB::commit();
                $file = storage_path($data->urlFile);
                return response()->file($file, ['Content-Type: '.$data->file->mime_type]);
            } else {
                DB::rollback();
                return redirect()->back()->withErrors('Data tidak ditemukan');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors('Ada Kesalahan pada Server');
            return response()->with('status', 'Ada Kesalahan pada Server');
        }
    }

    public function downloadFileBox($uniqueId, $idSecret, $tipeFile, $password)
    {
        $id     = $this->decrypt($idSecret);
        $data   = false;
        if ($password == 'fmipa' && !auth()->check()) {
            return redirect()->route('login'); 
        }
        if ($password) {
            if (Box::whereUniqueId($uniqueId)->when($password != 'fmipa', function($query) use ($password) {
                $query->wherePassword($password);
            })
            ->count() > 0) {
                $data   = $this->getDataFile($id, $tipeFile);
            } else {
                return redirect()->back()->withErrors('Password Box/Gobi Salah, Silahkan tanya ke Admin atau Operator yang mempunyai Box tersebut.');
            }
        }
        try {
            if ($data) {
                DB::beginTransaction();
                $file = storage_path($data->urlFile);
                $newNama = str_replace(array("/", "\\", ":", "*", "?", "«", "<", ">", "|"), "-", ($data->nama_file_download.'.'.$data->file->extension));
                $data->file()->update(['download' => ($data->file->download+1)]);
                DB::commit();
                return response()->download($file, $newNama, ['Content-Type: '.$data->file->mime_type]);
            } else {
                DB::rollback();
                return redirect()->back()->withErrors('Data tidak ditemukan');
            }
        } catch (\Throwable $th) {
            return redirect()->back()->withErrors('Ada Kesalahan pada Server');
            return response()->with('status', 'Ada Kesalahan pada Server');
        }
    }
}
