<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSuratMasukRequest;
use App\Http\Requests\UpdateSuratMasukRequest;
use App\Models\Box;
use App\Models\Extension;
use App\Models\Folder;
use App\Models\JenisAkses;
use App\Models\Pegawai;
use App\Models\PegawaiFile;
use App\Models\SuratMasuk;
use App\Services\SuratMasuk\SuratMasukServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use App\Traits\Variables;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class SuratMasukController extends Controller
{
    use TraitsResponse, DecryptId, Variables;

    public $routeIndex  = 'surat-masuk.index';
    public $folder;

    public function __construct(
        protected SuratMasukServiceInterface $suratMasukService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.surat-masuk';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('suratmasuk-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'suratMasuks' => $this->suratMasukService->allWithOrder('created_at', 'desc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('suratmasuk-create'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.create', [
            'jenisAkses'    => $this->jenisAkses(),
            'folders'       => $this->folders(),
            'boxes'         => $this->boxes(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'accepts'       => $this->accepts(),
            'creators'      => $this->users(),
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSuratMasukRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('suratmasuk-create'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->suratMasukService->store($request->validated());
            if (!$saving instanceof SuratMasuk) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(SuratMasuk $suratMasuk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $suratMasukId): View
    {
        abort_if(Gate::denies('suratmasuk-update'), Response::HTTP_FORBIDDEN);

        $data = $this->suratMasukService->findById($this->decrypt($suratMasukId));
        if ($data->file) {
            // ! selectJenisAkses
            $pegawaiId = $data->creator->pegawai_id ?? auth()->user()->pegawai_id;
            $selectJenisAkses = match ($data->file->jenis_akses_id) {
                3,'3' => $data->file->pegawais()->where('pegawai_id', '!=', $pegawaiId)->get()->pluck('id_secret'),
                4,'4' => PegawaiFile::whereFileId($data->file_id)->get()->pluck('unit_kerja_id_secret'),
                5,'5' => [auth()->user()->pegawai->unit_kerja_id],
                default => [$pegawaiId],
            };
            
            // ! selectBox
            $selectBox = $data->file->boxes ? $data->file->boxes->pluck('id_secret') : NULL;
            // ! selectFolder
            $selectFolder = $data->file->folder ? $data->file->folder->id_secret : NULL;
        }
        $selectCreator = $data->created_by ? $data->creator->id_secret : NULL;

        return view($this->folder.'.edit', [
            'jenisAkses'    => $this->jenisAkses(),
            'folders'       => $this->folders(),
            'boxes'         => $this->boxes(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'creators'      => $this->users(),
            'accepts'       => $this->accepts(),
            'data'          => $data,
            'selectJenisAkses'  => $selectJenisAkses ?? NULL,
            'selectBox'         => $selectBox ?? NULL,
            'selectFolder'      => $selectFolder ?? NULL,
            'selectCreator'     => $selectCreator ?? NULL,
        ]);

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSuratMasukRequest $request, string $suratMasukId): RedirectResponse
    {
        abort_if(Gate::denies('suratmasuk-update'), Response::HTTP_FORBIDDEN);

        $suratMasukData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->suratMasukService->update($request->id, $suratMasukData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $suratMasukId): RedirectResponse
    {
        abort_if(Gate::denies('suratmasuk-delete'), Response::HTTP_FORBIDDEN);
        
        try {
            $saving = $this->suratMasukService->delete($this->decrypt($suratMasukId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
