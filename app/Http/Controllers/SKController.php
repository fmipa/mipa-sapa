<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreSKRequest;
use App\Http\Requests\UpdateSKRequest;
use App\Models\Box;
use App\Models\Extension;
use App\Models\Folder;
use App\Models\JenisAkses;
use App\Models\KodeNaskah;
use App\Models\KodeUnit;
use App\Models\Pegawai;
use App\Models\PegawaiFile;
use App\Models\SK;
use App\Services\SK\SKServiceInterface;
use App\Traits\Accessors\DecryptId;
use App\Traits\Response as TraitsResponse;
use App\Traits\Variables;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\View\View;
use Spatie\Permission\Models\Role;


class SKController extends Controller
{
    use TraitsResponse, DecryptId, Variables;

    public $routeIndex  = 'sk.index';
    public $folder;
    public $title       = 'SK';

    public function __construct(
        protected SKServiceInterface $skService
    ) {
        $this->middleware('auth');
        $this->folder = 'pages.'.config('variables.templateName').'.sk';
    }

    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        abort_if(Gate::denies('sk-index'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.index', [
            'sks' => $this->skService->allWithOrder('created_at', 'desc'),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        abort_if(Gate::denies('sk-create'), Response::HTTP_FORBIDDEN);
        
        return view($this->folder.'.create', [
            'title'         => $this->title,
            'jenisAkses'    => $this->jenisAkses(),
            'folders'       => $this->folders(),
            'boxes'         => $this->boxes(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'accepts'       => $this->accepts(),
            'kodeNaskahs'   => $this->kodeNaskahs(),
            'kodeUnits'     => $this->kodeUnits(),
            'creators'      => $this->users(),
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
            'selectKodeUnit'    => $this->kodeUnits()[1]['key'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreSKRequest $request): RedirectResponse
    {
        abort_if(Gate::denies('sk-create'), Response::HTTP_FORBIDDEN);
        try {
            $saving = $this->skService->store($request->validated());
            if (!$saving instanceof SK) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(200),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);            
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(SK $sk)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $skId): View
    {
        abort_if(Gate::denies('sk-update'), Response::HTTP_FORBIDDEN);
        
        $data = $this->skService->findById($this->decrypt($skId));
        if ($data->file) {
            // ! selectJenisAkses
            $pegawaiId = $data->creator->pegawai_id ?? $pegawaiId;
            $selectJenisAkses = match ($data->file->jenis_akses_id) {
                3,'3' => $data->file->pegawais()->where('pegawai_id', '!=', $pegawaiId)->get()->pluck('id_secret'),
                4,'4' => PegawaiFile::whereFileId($data->file_id)->get()->pluck('unit_kerja_id_secret'),
                5,'5' => [auth()->user()->pegawai->unit_kerja_id],
                default => [$pegawaiId],
            };
            // ! selectBox
            $selectBox = $data->file->boxes ? $data->file->boxes->pluck('id_secret') : NULL;
            // ! selectFolder
            $selectFolder = $data->file->folder ? $data->file->folder->id_secret : NULL;
        }
        $selectCreator = $data->created_by ? $data->creator->id_secret : NULL;


        return view($this->folder.'.edit', [
            'title'         => $this->title,
            'jenisAkses'    => $this->jenisAkses(),
            'folders'       => $this->folders(),
            'boxes'         => $this->boxes(),
            'unitKerjas'    => $this->unitKerjas(false, true),
            'pegawais'      => $this->pegawais(),
            'accepts'       => $this->accepts(),
            'kodeNaskahs'   => $this->kodeNaskahs(),
            'kodeUnits'     => $this->kodeUnits(),
            'creators'      => $this->users(),
            'data'          => $data,
            'selected'      => $this->myUnitKerja()->pluck('id_Secret'),
            'selectJenisAkses'  => $selectJenisAkses ?? NULL,
            'selectBox'         => $selectBox ?? NULL,
            'selectFolder'      => $selectFolder ?? NULL,
            'selectCreator'     => $selectCreator ?? NULL,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateSKRequest $request, string $skId): RedirectResponse
    {
        abort_if(Gate::denies('sk-update'), Response::HTTP_FORBIDDEN);
        
        $skData = array_filter($request->validated(), fn ($value) => !is_null($value));
        try {
            $saving = $this->skService->update($request->id, $skData);
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(201),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $skId): RedirectResponse
    {
        abort_if(Gate::denies('sk-delete'), Response::HTTP_FORBIDDEN);

        try {
            $saving = $this->skService->delete($this->decrypt($skId));
            if (!$saving) {
                throw new Exception($saving);
            }
            return redirect()->route($this->routeIndex)->with([
                'data'      => $saving,
                'status'    => true,
                'message'   => $this->pesanFlash(202),
            ]);
        } catch (\Exception $e) {
            return redirect()->back()->with([
                'status'    => false,
                'message'   => $this->pesanFlash(),
            ]);
        }
    }
}
