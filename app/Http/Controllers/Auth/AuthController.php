<?php

namespace App\Http\Controllers\Auth;

use App\Enums\RoleEnum;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\MasterAgama;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    /**
     * Instantiate a new AuthController instance.
     */
    public function __construct()
    {
        $this->middleware('guest')->except([
            'logout', 'beranda', 'profile'
        ]);
    }

    /**
     * Display a login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('pages.auth.login', [
            'title' => 'Login Page',
        ]);
    }


    /**
     * Display a registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function register()
    {
        return view('pages.auth.register', [
            'title' => 'Register Page',
        ]);
    }

    /**
     * Store a new user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'      => 'required|string|max:250',
            'username'  => 'required|string|max:250',
            'email'     => 'required|email|max:250|unique:users',
            'password'  => 'required|min:8|confirmed'
        ]);
        DB::transaction(function () use ($request) {
            $user = User::create([
                'name'      => $request->name,
                'username'  => $request->username,
                'email'     => $request->email,
                'password'  => Hash::make($request->password)
            ]);
    
            
            if ($user->activation) {
                $credentials = $request->only('email', 'password');
                Auth::attempt($credentials);
                $request->session()->regenerate();
                return redirect()->route('dashboard')
                ->withSuccess('You have successfully registered & logged in!');
            } else {
                return back()->withErrors([
                    'active' => 'Account not active.',
                ]);
            }
            
        });
        return back()->withErrors([
            'active' => 'Account not active.',
        ]);

    }

    /**
     * Authenticate the user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doLogin(LoginRequest $request)
    {
        $credentials = $request->getCredentials();

        if(!Auth::validate($credentials)):
            return back()
                ->withErrors([
                    'message' => 'Username dan Password Tidak Cocok'
                ])->withInput();
        endif;

        // $user = Auth::getProvider()->retrieveByCredentials($credentials);

        // return Auth::login($user);

        return $this->authenticated($request);
    } 

    /**
     * Handle response after user authenticated
     * 
     * @param Request $request
     * @param Auth $user
     * 
     * @return \Illuminate\Http\Response
     */
    protected function authenticated(Request $request) 
    {
        if (Auth::attempt($request->validated())) {
            if (Auth::user()->activation) {
                $request->session()->regenerate();
                $cekUser = (auth()->user()->getRoleNames()->contains(function($value, $key) {
                    return (
                            $value == RoleEnum::DEKAN->value ||
                            $value == RoleEnum::WD->value ||
                            $value == RoleEnum::KTU->value ||
                            $value == RoleEnum::SUBKOOR->value
                        );
                }) == TRUE) ?? FALSE;
        
                if ($cekUser) {
                    return redirect()->route('file.folderView');
                } else {
                    return redirect()->intended('beranda')
                        ->withSuccess('Login Berhasil!');
                }
            } else {
                return back()->withErrors([
                    'message', 'Akun Tidak Aktif, silahkan menghubungi admin.',
                ]);
            }
        } 
        return back()->withErrors([
            'message', 'Username dan Password Tidak Cocok.',
        ]);
    }
    
    /**
     * Display a dashboard to authenticated users.
     *
     * @return \Illuminate\Http\Response
     */
    public function beranda(Request $request)
    {
        if(Auth::check())
        {
            if (Auth::user()->activation) {
                return view('pages.beranda');
            } else {
                Auth::logout();
                $request->session()->invalidate();
                $request->session()->regenerateToken();
                return redirect()->route('login')->withErrors([
                    'message', 'Akun Tidak Aktif, silahkan menghubungi admin.',
                ]);
            }
        }
        
        return redirect()->route('login')
            ->withErrors([
            'message', 'Tolong Login Terlebih Dahulu.',
        ]);
    } 

    /**
     * Display a Profile users.
     *
     * @return \Illuminate\Http\Response
     */
    public function profile()
    {
        $data = auth()->user();
        $data->pegawai = auth()->user()->pegawai;   
        $data->prodiName = $data->pegawai->prodi->nama ?? 'Fakultas';
        return view('pages.'.config('variables.templateName').'.user.profile', [ 
            'data'          => $data,
            'masterAgama'   => MasterAgama::View()->toArray(),
        ]);
    }

    /**
     * Log out the user from application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Session::flush();
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect()->route('login')
            ->withSuccess('Anda Berhasil Keluar Aplikasi!');
    }    

}