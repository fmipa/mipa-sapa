<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreRoleRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('hakakses-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'              => ['required', 'string', 'unique:roles,name'],
            'guard_name'        => ['required', 'string'],
            'permission_id'       => ['nullable'],
            'permission_id.*'   => ['nullable', 'numeric', 'exists:permissions,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'permission_id.*.numeric' => ':attribute tidak ditemukan',
            'permission_id.*.exist'   => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'name'          => 'Hak Akses',
            'guard_name'    => 'Guard Sistem',
            'permission_id'   => 'Pilihan Izin',
            'permission_id.*' => 'Izin',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'guard_name'        => config('auth.defaults.guard'),
            'permission_id'     => $this->permission_id ? $this->arraysId($this->permission_id) : NULL,
        ]);
    }

    public function arraysId($arrays)
    {
        $data = [];
        foreach ($arrays as $key => $value) {
            $data[] = $this->decrypt((string)$value);
        }
        return $data ?? NULL;
    }
}
