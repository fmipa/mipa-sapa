<?php

namespace App\Http\Requests;

use App\Models\Folder;
use App\Traits\Accessors\DecryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class UpdateFolderRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('folder-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id'            => ['required', 'numeric', 'exists:folders,id'],
            'nama'          => ['required', 'string'],
            'urutan'        => ['nullable', 'integer'],
            'parent_id'     => ['nullable ', 'numeric', 'exists:folders,id'],
            'unit_kerja_id' => ['nullable', 'numeric', 'exists:unit_kerjas,id'],
            'slug'          => ['nullable', 'string'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'id.required'           => ':attribute tidak ada',
            'id.numeric'            => ':attribute tidak ditemukan',
            'id.exist'              => ':attribute tidak terdaftar',
            'parent_id.numeric'     => ':attribute tidak ditemukan',
            'parent_id.exist'       => ':attribute tidak terdaftar',
            'unit_kerja_id.numeric' => ':attribute tidak ditemukan',
            'unit_kerja_id.exist'   => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'id'            => 'Data Folder',
            'nama'          => 'Nama',
            'urutan'        => 'Urutan',
            'parent_id'     => 'Kepala',
            'unit_kerja_id' => 'Unit Kerja',
            'slug'          => 'Slug',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'id'            => $this->decrypt((string)$this->id),
            // 'urutan'        => Folder::max('urutan'),
            'parent_id'     => $this->parent_id ? $this->decrypt((string)$this->parent_id) : NULL,
            'unit_kerja_id' => $this->unit_kerja_id ?  $this->decrypt($this->unit_kerja_id) :  DB::table('unit_role')->where('role_id',  auth()->user()->roles()->first()->id)->where('unit_kerja_id', '!=', 10)->first(['id', 'unit_kerja_id'])->unit_kerja_id,
            'slug'          => Str::lower(Str::slug($this->nama)),
        ]);
    }
}
