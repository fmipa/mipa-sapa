<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateUserRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('user-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id'        => ['required', 'numeric', 'exists:users,id'],
            'name'      => ['required', 'string', 'max:60'],
            'username'  => ['required', 'max:40', Rule::unique('users', 'username')->ignore($this->id)],
            'password'  => ['nullable', 'max:40', 'confirmed'],
            'email'     => ['required', 'email', Rule::unique('users', 'email')->ignore($this->id)],
            'role_id'   => ['required', 'array'],
            'role_id.*' => ['required', 'numeric', 'exists:roles,id'],
            'active'    => ['boolean'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'id.required'       => ':attribute tidak ada',
            'id.numeric'        => ':attribute tidak ditemukan',
            'id.exist'          => ':attribute tidak terdaftar',
            'role_id.array'     => ':attribute tidak sesuai',
            'role_id.*.numeric' => ':attribute tidak ada',
            'role_id.*.exist'   => ':attribute tidak terdaftar',
            'active.boolean'    => ':attribute tidak dapat dilakukan',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'id'        => 'User',
            'name'      => 'Nama Lengkap',
            'username'  => 'Username',
            'password'  => 'Password/Kata Sandi',
            'email'     => 'E-mail',
            'role_id'   => 'Pilihan Hak Akses',
            'role_id.*' => 'Hak Akses',
            'active'    => 'Aktivasi Akun',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'id'        => $this->decrypt((string)$this->id),
            'role_id'   => $this->roles(),
        ]);
    }

    public function roles(): array
    {
        foreach ($this->role_id as $key => $value) {
            $role_id[] = $this->decrypt((string)$value);
        }
        return $role_id;
    }
}
