<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateRoleRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('hakakses-update);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id'            => ['required', 'numeric', 'exists:roles,id'],
            'name'          => ['required', 'string', Rule::unique('roles', 'name')->ignore($this->id)],
            'guard_name'    => ['required', 'string'],
            'permission_id'     => ['nullable'],
            'permission_id.*'   => ['nullable', 'numeric', 'exists:permissions,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'id.required'   => ':attribute tidak ada',
            'id.numeric'    => ':attribute tidak ditemukan',
            'id.exist'      => ':attribute tidak terdaftar',
            'permission_id.*.numeric' => ':attribute tidak ditemukan',
            'permission_id.*.exist'   => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'id'            => 'Data Hak Akses',
            'name'          => 'Hak Akses',
            'guard_name'    => 'Guard Sistem',
            'permission_id'   => 'Pilihan Izin',
            'permission_id.*' => 'Izin',

        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'id'            => $this->decrypt((string)$this->id),
            'guard_name'    => config('auth.defaults.guard'),
            'permission_id' => $this->permission_id ? $this->arraysId($this->permission_id) : NULL,
        ]);
    }

    public function arraysId($arrays)
    {
        $data = [];
        foreach ($arrays as $key => $value) {
            $data[] = $this->decrypt((string)$value);
        }
        return $data ?? NULL;
    }
}
