<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateKodeNaskahRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('kodenaskah-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id'        => ['required', 'numeric', 'exists:kode_naskahs,id'],
            'substansi' => ['required', 'string'],
            'kode'      => ['required', 'string', 'min:2', 'max:20'],
            'deskripsi' => ['nullable', 'string'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'id.required'   => ':attribute tidak ada',
            'id.numeric'    => ':attribute tidak ditemukan',
            'id.exist'      => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'id'        => 'Data Kategori',
            'substansi' => 'Substansi',
            'kode'      => 'Kode Substansi',
            'deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'id' => $this->decrypt((string)$this->id),
        ]);
    }
}
