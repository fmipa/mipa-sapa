<?php

namespace App\Http\Requests;

use App\Models\Folder;
use App\Traits\Accessors\DecryptId;
use App\Traits\JurusanFolder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;

class StoreFolderRequest extends FormRequest
{
    use DecryptId, JurusanFolder;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('folder-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'nama'          => ['required', 'string'],
            'urutan'        => ['required', 'integer'],
            'parent_id'     => ['nullable ', 'numeric', 'exists:folders,id'],
            'unit_kerja_id' => ['nullable', 'numeric', 'exists:unit_kerjas,id'],
            'prodi_id'      => ['nullable', 'numeric', 'exists:prodis,id'],
            'slug'          => ['nullable', 'string'],
            'created_by'    => ['required', 'numeric', 'exists:users,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'nama'          => 'Nama',
            'urutan'        => 'Urutan',
            'parent_id'     => 'Kepala',
            'unit_kerja_id' => 'Unit Kerja',
            'prodi_id'      => 'Prodi',
            'slug'          => 'Slug',
            'created_by'    => 'Pembuat',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'parent_id'     => $this->cekDefaultFolder($this->parent_id),
            'unit_kerja_id' => $this->unit_kerja_id ?  $this->decrypt($this->unit_kerja_id) :  DB::table('unit_role')->where('role_id',  auth()->user()->roles()->first()->id)->where('unit_kerja_id', '!=', 10)->first(['id', 'unit_kerja_id'])->unit_kerja_id,
            'prodi_id'      => $this->cekProdi($this->prodi_id),
            'urutan'        => Folder::whereUnitKerjaId($this->unit_kerja_id)->when($this->prodi_id, function($query) {
                $query->whereProdiId($this->prodi_id);
            })->when($this->parent_id, function($query) {
                $query->whereParentId($this->parent_id);
            })->max('urutan') + 1,
            'slug'          => Str::lower(Str::slug($this->nama)),
            'created_by'    => $this->created_by ? $this->decrypt((string)$this->created_by) : auth()->id(),
        ]);
    }
}
