<?php

namespace App\Http\Requests;

use App\Traits\Mutators\EncryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreBoxRequest extends FormRequest
{
    use EncryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('box-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'nama'          => ['required', 'string'],
            'nomor'         => ['required', 'integer', 'unique:boxes,nomor'],
            'tahun'         => ['nullable', 'string'],
            'unit_kerja_id' => ['nullable', 'numeric', 'exists:unit_kerjas,id'],
            'lokasi'        => ['required', 'string'],
            'password'      => ['nullable', 'string'],
            'unique_id'     => ['required', 'string', 'unique:boxes,unique_id'],
            'created_by'    => ['required', 'numeric', 'exists:users,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'nomor.exist'       => ':attribute tidak terdaftar',
            'unique_id.exist'   => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'nama'      => 'Nama',
            'nomor'     => 'Nomor',
            'tahun'     => 'Tahun',
            'unit_kerja_id' => 'Unit Kerja',
            'lokasi'    => 'Lokasi Penyimpanan',
            'password'  => 'Password Box',
            'unique_id' => 'Unik ID QRCode',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'created_by'    => $this->created_by ? $this->decrypt((string)$this->created_by) : auth()->id(),
            'unit_kerja_id' => $this->unit_kerja_id ? $this->decrypt((string)$this->unit_kerja_id) : (auth()->user()->operator == 1 ? auth()->user()->pegawai->unit_kerja_id : NULL),
            'unique_id'     => $this->encrypt((string)$this->nomor.date('ymdHis'), 'qrcode-box'),
            'tahun'         => $this->tahuns ? implode(",", $this->tahuns) : NULL,
        ]);
    }
}
