<?php

namespace App\Http\Requests;

use App\Enums\JenisKelaminEnum;
use App\Enums\StatusPegawaiEnum;
use App\Traits\Accessors\DecryptId;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;
use Illuminate\Validation\Rules\Password;

class StorePegawaiRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('pegawai-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'firstname'         => ['required', 'string'],
            'middlename'        => ['nullable', 'string'],
            'lastname'          => ['nullable', 'string'],
            'gelar_depan'       => ['nullable', 'string'],
            'gelar_belakang'    => ['nullable', 'string'],
            'nomor_unik'        => ['nullable', 'string', 'unique:pegawais,nomor_unik'],
            'tempat_lahir'      => ['nullable', 'string'],
            'tanggal_lahir'     => ['nullable', 'date_format:Y-m-d'],
            'agama_id'          => ['nullable', 'numeric', 'exists:master_agamas,id'],
            'prodi_id'          => ['nullable', 'numeric', 'exists:prodis,id'],
            'jenis_kelamin'     => ['nullable'],
            'status'            => ['nullable'],
            
            'username'  => ['nullable', 'max:40', 'alpha_num:ascii', 'unique:users,username'],
            'name'      => ['required', 'string', 'max:100'],
            'password'  => ['nullable', 'max:20', Password::min(6), 'confirmed'],
            'email'     => ['nullable', 'email', 'unique:users,email'],
            'role_id'   => ['required', 'array'],
            'role_id.*' => ['required', 'numeric', 'exists:roles,id'],
            'active'    => ['boolean'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'agama_id.required'     => ':attribute tidak ada',
            'agama_id.numeric'      => ':attribute tidak ditemukan',
            'agama_id.exist'        => ':attribute tidak terdaftar',
            'jenis_kelamin.enum'    => ':attribute tidak ada',
            'status.enum'           => ':attribute tidak ada',
            'prodi_id.required'     => ':attribute tidak ada',
            'prodi_id.numeric'      => ':attribute tidak ditemukan',
            'prodi_id.exist'        => ':attribute tidak terdaftar',

            'role_id.array'     => ':attribute tidak sesuai',
            'role_id.*.numeric' => ':attribute tidak ada',
            'role_id.*.exist'   => ':attribute tidak terdaftar',
            'active.boolean'    => ':attribute tidak dapat dilakukan',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'firstname'         => 'Nama Depan',
            'middlename'        => 'Nama Tengah',
            'lastname'          => 'Nama Belakang',
            'gelar_depan'       => 'Gelar Depan',
            'gelar_belakang'    => 'Gelar Belakang',
            'nomor_unik'        => 'Nomor Identitas',
            'tempat_lahir'      => 'Tempat Lahir',
            'tanggal_lahir'     => 'Tanggal Lahir',
            'agama_id'          => 'Agama',
            'prodi_id'          => 'Program Studi',
            'jenis_kelamin'     => 'Jenis Kelamin',
            'status'            => 'Status Pegawai',

            'name'              => 'Nama Lengkap',
            'username'          => 'Username',
            'password'          => 'Password/Kata Sandi',
            'email'             => 'E-mail',
            'unit_kerja_id'     => 'Pilihan Unit Kerja',
            'unit_kerja_id.*'   => 'Unit Kerja',
            'active'            => 'Aktivasi Akun',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'tanggal_lahir' => $this->tanggal_lahir ? $this->tanggalFormat($this->tanggal_lahir) : NULL,
            'agama_id'      => $this->agama_id ? $this->decrypt((string)$this->agama_id) : NULL,
            'prodi_id'      => $this->prodi_id ? $this->decrypt((string)$this->prodi_id) : NULL,
            'jenis_kelamin' => $this->jenis_kelamin ? (string) Str::upper($this->jenis_kelamin) : NULL,
            'status'        => $this->status ? (string) Str::upper($this->status) : NULL,

            'name'          => strtoupper($this->fullName()),
            'role_id'       => $this->roles(),

        ]);
    }

    public function tanggalFormat($date, $format = "Y-m-d")
    {
        return Carbon::createFromFormat('d-M-Y', $date)->format($format);
    }

    public function fullName(): string
    {
        return (($this->gelar_depan ? ($this->gelar_depan .' ') : NULL) . $this->firstname . ($this->middlename ? (' '. $this->middlename) : NULL) . ($this->lastname ? (' '. $this->lastname) : NULL) . ($this->gelar_belakang ? (', '. $this->gelar_belakang) : NULL));
    }

    public function roles(): array
    {
        foreach ($this->role_id as $key => $value) {
            $role_id[] = $this->decrypt((string)$value);
        }
        return $role_id;
    }
}
