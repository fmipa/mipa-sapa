<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use App\Traits\Mutators\EncryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateBoxRequest extends FormRequest
{
    use DecryptId, EncryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('box-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id'            => ['required', 'numeric', 'exists:boxes,id'],
            'nama'          => ['required', 'string'],
            'nomor'         => ['required', 'integer', Rule::unique('boxes', 'nomor')->ignore($this->id)],
            'tahun'         => ['nullable', 'string'],
            'unit_kerja_id' => ['nullable', 'numeric', Rule::unique('unit_kerjas', 'id')->ignore($this->unit_kerja_id)],
            'lokasi'        => ['required', 'string'],
            'password'      => ['nullable', 'string'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'id.required'       => ':attribute tidak ada',
            'id.numeric'        => ':attribute tidak ditemukan',
            'id.exist'          => ':attribute tidak terdaftar',
            'nomor.exist'       => ':attribute tidak terdaftar',
            // 'unique_id.exist'   => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'id'        => 'Data Box/Gobi',
            'nama'      => 'Nama',
            'nomor'     => 'Nomor',
            'tahun'     => 'Tahun',
            'unit_kerja_id' => 'Unit Kerja',
            'lokasi'    => 'Lokasi Penyimpanan',
            'unique_id' => 'Unik ID QRCode',
            'password'  => 'Password Box',

        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'id'    => $this->decrypt((string)$this->id),
            'tahun' => $this->tahuns ? implode(",", $this->tahuns) : NULL,
            'unit_kerja_id' => $this->unit_kerja_id ? $this->decrypt((string)$this->unit_kerja_id) : (auth()->user()->operator == 1 ? auth()->user()->pegawai->unit_kerja_id : NULL),
        ]);
    }
}
