<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class UpdateUserProfileRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id'        => ['required', 'numeric', 'exists:users,id'],
            'username'  => ['required', 'max:40', Rule::unique('users', 'username')->ignore($this->id)],
            'password'  => ['nullable', 'max:40', 'confirmed'],
            'email'     => ['required', 'email', Rule::unique('users', 'email')->ignore($this->id)],
            'active'    => ['boolean'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'id.required'       => ':attribute tidak ada',
            'id.numeric'        => ':attribute tidak ditemukan',
            'id.exist'          => ':attribute tidak terdaftar',

        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'id'        => 'Akun',
            'username'  => 'Username',
            'password'  => 'Password/Kata Sandi',
            'email'     => 'E-mail',
            'active'    => 'Aktivasi Akun',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'id'        => auth()->id(),
            'active'    => true,
        ]);
    }

}
