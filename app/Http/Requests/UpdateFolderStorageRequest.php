<?php

namespace App\Http\Requests;

use App\Enums\JenisAksesEnum;
use App\Traits\Accessors\DecryptId;
use App\Traits\Folder;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class UpdateFolderStorageRequest extends FormRequest
{
    use DecryptId, Folder;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('folderstorage-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id'            => ['required', 'numeric', 'exists:folders,id'],
            'nama'          => ['required', 'string'],
            'disk'          => ['required', 'string'],
            'path'          => ['required', 'string'],
            'jenis_akses_id'    => ['required', 'string', 'exists:jenis_akses,id'],
            'role_id'       => ['required_if:jenis_akses_id,'.(JenisAksesEnum::UNITKERJA->value)],
            'role_id.*'     => ['required_if:jenis_akses_id,'.(JenisAksesEnum::UNITKERJA->value), 'numeric', 'exists:roles,id'],
            'pegawai_id'    => ['required_if:jenis_akses_id,'.(JenisAksesEnum::TAG->value)],
            'pegawai_id.*'  => ['required_if:jenis_akses_id,'.(JenisAksesEnum::TAG->value), 'numeric', 'exists:pegawais,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'id.required'       => ':attribute tidak ada',
            'id.numeric'        => ':attribute tidak ditemukan',
            'id.exist'          => ':attribute tidak terdaftar',
            'disk.required'         => 'Kesalahan pada sistem',
            'path.required'         => 'Kesalahan pada sistem',
            'unit_kerja_id.*.numeric'   => ':attribute tidak ada',
            'unit_kerja_id.*.exist'     => ':attribute tidak terdaftar',
            'pegawai_id.*.numeric'  => ':attribute tidak ada',
            'pegawai_id.*.exist'    => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'id'    => 'Data Folder',
            'nama'  => 'Nama Folder',
            'disk'  => 'Folder Utama',
            'path'  => 'Lokasi Folder',
            'jenis_akses_id'    => 'Jenis Akses Folder',
            'role_id'       => 'Pilihan Hak Akses',
            'role_id.*'     => 'Hak Akses',
            'pegawai_id'    => 'Pilihan Pegawai',
            'pegawai_id.*'  => 'Pegawai',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $roleIdValidate     = $this->jenis_akses_id == (JenisAksesEnum::UNITKERJA->value) ? ($this->role_id ? $this->arraysId($this->role_id) : NULL) : NULL;
        $pegawaiIdValidate  = $this->jenis_akses_id == (JenisAksesEnum::TAG->value) ? ($this->pegawai_id ? $this->arraysId($this->pegawai_id) : NULL) : NULL;
        if ($this->jenis_akses_id == (JenisAksesEnum::TAG->value)) {
            $pegawaiIdValidate[] = auth()->id();
        }if ($this->jenis_akses_id == (JenisAksesEnum::PRIVACY->value)) {
            $pegawaiIdValidate[] = auth()->id();
        }
        $this->merge([
            'id'            => $this->decrypt((string)$this->id),
            'role_id'       => $roleIdValidate,
            'pegawai_id'    => $pegawaiIdValidate,
            'disk'          => config('filesystems.disks.local.driver'),
            'path'          => $this->penetapan($this->createKodeUnik($this->jenis_akses_id)),
        ]);
    }
    
    public function createKodeUnik(string $aksesFile)
    {
        switch ($aksesFile) {
            case 'tim':
                $kodeUnik = $this->generateKode($this->role_id);
                break;
            
            case 'tag':
                $kodeUnik = $this->generateKode($this->pegawai_id);
                break;
            
            default:
                $kodeUnik = auth()->user()->pegawai_id;
                break;
        }
        return $kodeUnik;
    }

    public function generateKode($arrays)
    {
        $data = NULL;
        foreach ($arrays as $key => $value) {
            $data += $this->decrypt((string)$value);
        }
        return $data ?? NULL;
    }

    public function arraysId($arrays)
    {
        $data = [];
        foreach ($arrays as $key => $value) {
            $data[] = $this->decrypt((string)$value);
        }
        return $data ?? NULL;
    }
}
