<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use App\Traits\Helper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rule;

class UpdateMasterAgamaRequest extends FormRequest
{
    use DecryptId, Helper;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('agama-update');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'id'    => ['required', 'numeric', 'exists:master_agamas,id'],
            'kode'  => ['nullable', 'string', Rule::unique('master_agamas', 'kode')->ignore($this->id)],
            'agama' => ['required', 'string'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'id.required'   => ':attribute tidak ada',
            'id.numeric'    => ':attribute tidak ditemukan',
            'id.exist'      => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'id'    => 'Data Agama',
            'agama' => 'Agama',
            'kode'  => 'Kode/Singkatan',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'id'    => $this->decrypt((string)$this->id),
            'kode'  => $this->kode ?? $this->acronym(strtoupper($this->agama)),
        ]);
    }
}
