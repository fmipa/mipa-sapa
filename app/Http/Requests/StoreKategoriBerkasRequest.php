<?php

namespace App\Http\Requests;

use App\Models\KategoriBerkas;
use App\Traits\Accessors\DecryptId;
use App\Traits\Helper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreKategoriBerkasRequest extends FormRequest
{
    use DecryptId, Helper;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('kberkas-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'kategori'      => ['required', 'string'],
            'kode'          => ['nullable', 'string', 'unique:kategori_berkas,kode'],
            'deskripsi'     => ['nullable', 'string'],
            'created_by'    => ['required', 'numeric', 'exists:users,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'kategori'  => 'Nama Kategori',
            'kode'      => 'Kode/Singkatan',
            'deskripsi' => 'Deskripsi',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'kode'          => $this->kode ?? $this->cekKode($this->acronym(strtoupper($this->kategori))),
            'created_by'    => $this->created_by ? $this->decrypt((string)$this->created_by) : auth()->id(),
        ]);
    }

    protected function cekKode($kode)
    {
        $total = KategoriBerkas::whereKode($kode)->count();
        if($total > 0)
        {
            return $kode.'-'.($total+1);
        } else {
            return $kode;
        }

    }
}
