<?php

namespace App\Http\Requests;

use App\Enums\AksesFileEnum;
use App\Enums\FolderStorageDefaultEnum;
use App\Enums\JenisAksesEnum;
use App\Enums\SizeFileEnum;
use App\Rules\ExtensionDB;
use App\Traits\Accessors\DecryptId;
use App\Traits\JurusanFolder;
use App\Traits\Mutators\JenisAkses;
use App\Traits\ValidateSizeFile;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Enum;

class StoreDocumentRequest extends FormRequest
{
    use DecryptId, JenisAkses, JurusanFolder, ValidateSizeFile;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('document-create');
    }
    

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'attachment'        => ['required', 'file', new ExtensionDB, 'max:'.$this->rulesHelper(2)],
            'nama'              => ['required', 'string'],
            'folder_storage_id' => ['required', 'numeric', 'exists:folder_storages,id'],
            'folder_id'         => ['nullable', 'numeric', 'exists:folders,id'],
            'box_id'            => ['nullable'],
            'box_id.*'          => ['nullable', 'numeric', 'exists:boxes,id'],
            'jenis_akses_id'    => ['required', 'numeric', 'exists:jenis_akses,id'],
            'unit_kerja_id'     => ['required_if:jenis_akses_id,'.(JenisAksesEnum::UNITKERJA->value)],
            'unit_kerja_id.*'   => ['required_if:jenis_akses_id,'.(JenisAksesEnum::UNITKERJA->value), 'numeric', 'exists:unit_kerjas,id'],
            'pegawai_id'        => ['required_if:jenis_akses_id,'.(JenisAksesEnum::TAG->value)],
            'pegawai_id.*'      => ['required_if:jenis_akses_id,'.(JenisAksesEnum::TAG->value), 'numeric', 'exists:pegawais,id'],
            'created_by'        => ['required', 'numeric', 'exists:users,id'],
            'ids'               => ['nullable'],

        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'attachment.max'        => ':attribute tidak boleh lebih dari '.$this->messagesHelper(2),
            'box_id.*.numeric'      => ':attribute tidak ditemukan',
            'box_id.*.exist'        => ':attribute tidak terdaftar',
            'unit_kerja_id.*.numeric'   => ':attribute tidak ada',
            'unit_kerja_id.*.exist'     => ':attribute tidak terdaftar',
            'pegawai_id.*.numeric'  => ':attribute tidak ada',
            'pegawai_id.*.exist'    => ':attribute tidak terdaftar',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'attachment'        => 'Dokumen',
            'nama'              => 'Nama Dokumen',
            'box_id'            => 'Pilihan Box/Gobi',
            'box_id.*'          => 'Box/Gobi',
            'folder_id'         => 'Pilihan Folder',
            'jenis_akses_id'    => 'Jenis Akses Folder',
            'unit_kerja_id'     => 'Pilihan Unit Kerja',
            'unit_kerja_id.*'   => 'Unit Kerja',
            'pegawai_id'        => 'Pilihan Pegawai',
            'pegawai_id.*'      => 'Pegawai',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $ids = match ($this->jenis_akses_id) {
            '3' => $this->prosesAkses($this->jenis_akses_id, $this->pegawai_id),
            '4' => $this->prosesAkses($this->jenis_akses_id, $this->unit_kerja_id),
            '5' => $this->prosesAkses($this->jenis_akses_id, false),
            default => $this->prosesAkses($this->jenis_akses_id, false),
        };
        $this->merge([
            'created_by'    => $this->created_by ? $this->decrypt((string)$this->created_by) : auth()->id(),
            'unit_kerja_id' => ($this->jenis_akses_id == JenisAksesEnum::UNITKERJA->value ? $ids : ($this->jenis_akses_id == JenisAksesEnum::TIM->value ? $ids : NULL)),
            'pegawai_id'    => ($this->jenis_akses_id == JenisAksesEnum::TAG->value) ? $ids : NULL,
            'folder_storage_id' => FolderStorageDefaultEnum::DOKUMEN->value,
            'folder_id'         => $this->cekDefaultFolder($this->folder_id),
            'box_id'            => $this->box_id ? $this->arraysId($this->box_id) : NULL,
            'ids'               => $ids ?? NULL,
        ]);
    }
}
