<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Validation\Rules\Password;

class StoreUserRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('user-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name'      => ['required', 'string', 'max:100'],
            'username'  => ['required', 'max:40', 'alpha_num:ascii', 'unique:users,username'],
            'password'  => ['required', 'max:20', Password::min(6), 'confirmed'],
            'email'     => ['required', 'email', 'unique:users,email'],
            'role_id'   => ['required', 'array'],
            'role_id.*' => ['required', 'numeric', 'exists:roles,id'],
            'active'    => ['boolean'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'role_id.array'     => ':attribute tidak sesuai',
            'role_id.*.numeric' => ':attribute tidak ada',
            'role_id.*.exist'   => ':attribute tidak terdaftar',
            'active.boolean'    => ':attribute tidak dapat dilakukan',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'name'      => 'Nama Lengkap',
            'username'  => 'Username',
            'password'  => 'Password/Kata Sandi',
            'email'     => 'E-mail',
            'role_id'   => 'Pilihan Hak Akses',
            'role_id.*' => 'Hak Akses',
            'active'    => 'Aktivasi Akun',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'role_id' => $this->roles(),
        ]);
    }

    public function roles(): array
    {
        foreach ($this->role_id as $key => $value) {
            $role_id[] = $this->decrypt((string)$value);
        }
        return $role_id;
    }

}
