<?php

namespace App\Http\Requests;

use App\Traits\Accessors\DecryptId;
use App\Traits\Helper;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;

class StoreMasterAgamaRequest extends FormRequest
{
    use DecryptId, Helper;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('agama-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'agama' => ['required', 'string'],
            'kode'  => ['nullable', 'string', 'unique:master_agamas,kode'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'agama' => 'Agama',
            'kode'  => 'Kode/Singkatan',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'kode'  => $this->kode ?? $this->acronym(strtoupper($this->agama)),
        ]);
    }
}
