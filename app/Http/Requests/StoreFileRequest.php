<?php

namespace App\Http\Requests;

use App\Enums\AksesFileEnum;
use App\Rules\ExtensionDB;
use App\Traits\Accessors\DecryptId;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Str;
use Illuminate\Validation\Rules\Enum;

class StoreFileRequest extends FormRequest
{
    use DecryptId;
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return Gate::allows('file-create');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'nama_file'         => ['required', 'string'],
            'file'              => ['required', 'file', new ExtensionDB],
            'folder_storage_id' => ['required', 'integer', 'exists:folder_storages,id'],
            'akses'             => ['required', [new Enum(AksesFileEnum::class)]],
            'user_tags'         => ['required_if:akses,TAG', 'array'],
            'created_by'        => ['required', 'numeric', 'exists:users,id'],
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array<string, string>
     */
    public function messages(): array
    {
        return [
            'akses.enum'        => ':attribute tidak ada',
            'role_id.exist'     => ':attribute tidak terdaftar',
            'user_tags.array'   => ':attribute tidak sesuai'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes(): array
    {
        return [
            'nama_file' => 'Nama File',
            'file'      => 'File',
            'folder_storage_id' => 'Folder',
            'akses'     => 'Jenis Akses File',
            'user_tags' => 'Pengguna',
        ];
    }

    /**
     * Sebelum di Validasi
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $this->merge([
            'created_by'        => auth()->id(),
            'akses'             => Str::upper($this->akses),
            'folder_storage_id' => $this->decrypt((string)$this->folder_storage_id),
        ]);
    }
}
