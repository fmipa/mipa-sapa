<?php

namespace App\Providers;

use App\Services\Beranda\BerandaService;
use App\Services\Beranda\BerandaServiceInterface;
use App\Services\BerkasPegawai\BerkasPegawaiService;
use App\Services\BerkasPegawai\BerkasPegawaiServiceInterface;
use App\Services\Box\BoxService;
use App\Services\Box\BoxServiceInterface;
use App\Services\Dokumen\DocumentService;
use App\Services\Dokumen\DocumentServiceInterface;
use App\Services\Extension\ExtensionService;
use App\Services\Extension\ExtensionServiceInterface;
use App\Services\File\FileService;
use App\Services\File\FileServiceInterface;
use App\Services\Folder\FolderService;
use App\Services\Folder\FolderServiceInterface;
use App\Services\FolderStorage\FolderStorageService;
use App\Services\FolderStorage\FolderStorageServiceInterface;
use App\Services\JenisAkses\JenisAksesService;
use App\Services\JenisAkses\JenisAksesServiceInterface;
use App\Services\Jurusan\JurusanService;
use App\Services\Jurusan\JurusanServiceInterface;
use App\Services\KategoriBerkas\KategoriBerkasService;
use App\Services\KategoriBerkas\KategoriBerkasServiceInterface;
use App\Services\KodeNaskah\KodeNaskahService;
use App\Services\KodeNaskah\KodeNaskahServiceInterface;
use App\Services\KodeUnit\KodeUnitService;
use App\Services\KodeUnit\KodeUnitServiceInterface;
use App\Services\MasterAgama\MasterAgamaService;
use App\Services\MasterAgama\MasterAgamaServiceInterface;
use App\Services\Pegawai\PegawaiService;
use App\Services\Pegawai\PegawaiServiceInterface;
use App\Services\Permission\PermissionService;
use App\Services\Permission\PermissionServiceInterface;
use App\Services\Prodi\ProdiService;
use App\Services\Prodi\ProdiServiceInterface;
use App\Services\Role\RoleService;
use App\Services\Role\RoleServiceInterface;
use App\Services\SK\SKService;
use App\Services\SK\SKServiceInterface;
use App\Services\SuratKeluar\SuratKeluarService;
use App\Services\SuratKeluar\SuratKeluarServiceInterface;
use App\Services\SuratMasuk\SuratMasukService;
use App\Services\SuratMasuk\SuratMasukServiceInterface;
use App\Services\User\UserService;
use App\Services\User\UserServiceInterface;
use Illuminate\Support\ServiceProvider;

class ServicesProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->app->bind(
            BerandaServiceInterface::class,
            BerandaService::class
        );
        $this->app->bind(
            BerkasPegawaiServiceInterface::class,
            BerkasPegawaiService::class
        );
        $this->app->bind(
            BoxServiceInterface::class,
            BoxService::class
        );
        $this->app->bind(
            DocumentServiceInterface::class,
            DocumentService::class
        );
        $this->app->bind(
            ExtensionServiceInterface::class,
            ExtensionService::class
        );
        $this->app->bind(
            FileServiceInterface::class,
            FileService::class
        );
        $this->app->bind(
            FolderServiceInterface::class,
            FolderService::class
        );
        $this->app->bind(
            FolderStorageServiceInterface::class,
            FolderStorageService::class
        );
        $this->app->bind(
            JenisAksesServiceInterface::class,
            JenisAksesService::class
        );
        $this->app->bind(
            JurusanServiceInterface::class,
            JurusanService::class
        );
        $this->app->bind(
            KategoriBerkasServiceInterface::class,
            KategoriBerkasService::class
        );
        $this->app->bind(
            KodeNaskahServiceInterface::class,
            KodeNaskahService::class
        );
        $this->app->bind(
            KodeUnitServiceInterface::class,
            KodeUnitService::class
        );
        $this->app->bind(
            MasterAgamaServiceInterface::class,
            MasterAgamaService::class
        );
        $this->app->bind(
            PegawaiServiceInterface::class,
            PegawaiService::class
        );
        $this->app->bind(
            PermissionServiceInterface::class,
            PermissionService::class
        );
        $this->app->bind(
            ProdiServiceInterface::class,
            ProdiService::class
        );
        $this->app->bind(
            RoleServiceInterface::class,
            RoleService::class
        );
        $this->app->bind(
            SKServiceInterface::class,
            SKService::class
        );
        $this->app->bind(
            SuratKeluarServiceInterface::class,
            SuratKeluarService::class
        );
        $this->app->bind(
            SuratMasukServiceInterface::class,
            SuratMasukService::class
        );
        $this->app->bind(
            UserServiceInterface::class,
            UserService::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }
}
