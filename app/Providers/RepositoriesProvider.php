<?php

namespace App\Providers;

use App\Models\FolderStorage;
use App\Repositories\BerkasPegawai\BerkasPegawaiRepository;
use App\Repositories\BerkasPegawai\BerkasPegawaiRepositoryInterface;
use App\Repositories\Box\BoxRepository;
use App\Repositories\Box\BoxRepositoryInterface;
use App\Repositories\Dokumen\DocumentRepository;
use App\Repositories\Dokumen\DocumentRepositoryInterface;
use App\Repositories\Extension\ExtensionRepository;
use App\Repositories\Extension\ExtensionRepositoryInterface;
use App\Repositories\File\FileRepository;
use App\Repositories\File\FileRepositoryInterface;
use App\Repositories\Folder\FolderRepository;
use App\Repositories\Folder\FolderRepositoryInterface;
use App\Repositories\FolderStorage\FolderStorageRepository;
use App\Repositories\FolderStorage\FolderStorageRepositoryInterface;
use App\Repositories\Jurusan\JurusanRepository;
use App\Repositories\Jurusan\JurusanRepositoryInterface;
use App\Repositories\KategoriBerkas\KategoriBerkasRepository;
use App\Repositories\KategoriBerkas\KategoriBerkasRepositoryInterface;
use App\Repositories\KodeNaskah\KodeNaskahRepository;
use App\Repositories\KodeNaskah\KodeNaskahRepositoryInterface;
use App\Repositories\KodeUnit\KodeUnitRepository;
use App\Repositories\KodeUnit\KodeUnitRepositoryInterface;
use App\Repositories\MasterAgama\MasterAgamaRepository;
use App\Repositories\MasterAgama\MasterAgamaRepositoryInterface;
use App\Repositories\Pegawai\PegawaiRepository;
use App\Repositories\Pegawai\PegawaiRepositoryInterface;
use App\Repositories\Permission\PermissionRepository;
use App\Repositories\Permission\PermissionRepositoryInterface;
use App\Repositories\Prodi\ProdiRepository;
use App\Repositories\Prodi\ProdiRepositoryInterface;
use App\Repositories\Role\RoleRepository;
use App\Repositories\Role\RoleRepositoryInterface;
use App\Repositories\SK\SKRepository;
use App\Repositories\SK\SKRepositoryInterface;
use App\Repositories\SuratKeluar\SuratKeluarRepository;
use App\Repositories\SuratKeluar\SuratKeluarRepositoryInterface;
use App\Repositories\SuratMasuk\SuratMasukRepository;
use App\Repositories\SuratMasuk\SuratMasukRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoriesProvider extends ServiceProvider
{
    /**
     * Register Repository.
     */
    public function register(): void
    {
        $this->app->bind(
            BerkasPegawaiRepositoryInterface::class,
            BerkasPegawaiRepository::class
        );
        $this->app->bind(
            BoxRepositoryInterface::class,
            BoxRepository::class
        );
        $this->app->bind(
            DocumentRepositoryInterface::class,
            DocumentRepository::class
        );
        $this->app->bind(
            ExtensionRepositoryInterface::class,
            ExtensionRepository::class
        );
        $this->app->bind(
            FileRepositoryInterface::class,
            FileRepository::class
        );
        $this->app->bind(
            FolderRepositoryInterface::class,
            FolderRepository::class
        );
        $this->app->bind(
            FolderStorageRepositoryInterface::class,
            FolderStorageRepository::class
        );
        $this->app->bind(
            JurusanRepositoryInterface::class,
            JurusanRepository::class
        );
        $this->app->bind(
            KategoriBerkasRepositoryInterface::class,
            KategoriBerkasRepository::class
        );
        $this->app->bind(
            KodeNaskahRepositoryInterface::class,
            KodeNaskahRepository::class
        );
        $this->app->bind(
            KodeUnitRepositoryInterface::class,
            KodeUnitRepository::class
        );
        $this->app->bind(
            MasterAgamaRepositoryInterface::class,
            MasterAgamaRepository::class
        );
        $this->app->bind(
            PegawaiRepositoryInterface::class,
            PegawaiRepository::class
        );
        $this->app->bind(
            PermissionRepositoryInterface::class,
            PermissionRepository::class
        );
        $this->app->bind(
            ProdiRepositoryInterface::class,
            ProdiRepository::class
        );
        $this->app->bind(
            RoleRepositoryInterface::class,
            RoleRepository::class
        );
        $this->app->bind(
            SKRepositoryInterface::class,
            SKRepository::class
        );
        $this->app->bind(
            SuratKeluarRepositoryInterface::class,
            SuratKeluarRepository::class
        );
        $this->app->bind(
            SuratMasukRepositoryInterface::class,
            SuratMasukRepository::class
        );
        $this->app->bind(
            UserRepositoryInterface::class,
            UserRepository::class
        );
    }

    /**
     * Bootstrap Repositorys.
     */
    public function boot(): void
    {
        //
    }
}
