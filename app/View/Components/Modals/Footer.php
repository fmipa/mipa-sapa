<?php

namespace App\View\Components\Modals;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Footer extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public ?string $class   = NULL, 
        public ?string $id      = NULL,
        public ?string $onclick = NULL,
        public string $closeText    = 'Tutup', 
        public string $saveText     = 'Simpan', 
        public string $typeButton   = 'button', 
        public string $classButton  = 'success', 
        public string $saveButton   = 'true', 
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.modals.footer');
    }
}
