<?php

namespace App\View\Components\Modals;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Button extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $class    = 'secondary', 
        public ?string $size    = NULL, 
        public ?string $target  = NULL, 
        public ?string $id      = NULL,
        public ?string $onclick = NULL,
        public string $text     = 'Modal', 
    )
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.modals.button');
    }
}
