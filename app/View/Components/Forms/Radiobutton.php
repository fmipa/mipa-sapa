<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Radiobutton extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public string $type = 'button', 
        public string $text, 
        public string $class = 'secondary', 
        public string $id = '',
    )
    {
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.env('TEMPLATE').'.forms.radiobutton');
    }
}
