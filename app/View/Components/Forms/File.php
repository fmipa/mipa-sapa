<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class File extends Component
{
    public $classDiv, $class, $id, $label, $name, $required, $readonly, $smallText, $disabled, $accept;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $label,
        $name    = NULL,
        $classDiv= false,
        $class   = false,
        $id      = false,
        $required   = false,
        $readonly   = false,
        $disabled   = false,
        $accept     = false,
        $smallText  = false,
    )
    {
        $this->classDiv = $classDiv;
        $this->class        = $class;
        $this->id           = $id;
        $this->label        = $label;
        $this->name         = $name;
        $this->required     = $required;
        $this->readonly     = $readonly;
        $this->disabled     = $disabled;
        $this->accept       = $accept;
        $this->smallText    = $smallText;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.forms.'.config('variables.templateForm').'.file');
    }
}
