<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class JenisAkses extends Component
{
    public $classDiv, $create, $checked, $pegawais, $unitKerjas, $jenisAksesId, $selectJenisAkses, $smallText;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $classDiv       = false,
        $create         = false,
        $pegawais       = false,
        $unitKerjas     = false,
        $checked        = false,
        $jenisAksesId   = false,
        $selectJenisAkses   = false,
        $smallText      = false,
    )
    {
        $this->classDiv         = $classDiv;
        $this->create           = $create;
        $this->checked          = $checked;
        $this->pegawais         = $pegawais;
        $this->unitKerjas       = $unitKerjas;
        $this->jenisAksesId     = $jenisAksesId;
        $this->selectJenisAkses = $selectJenisAkses;
        $this->smallText        = $smallText;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.forms.'.config('variables.templateForm').'.jenisakses');
    }
}
