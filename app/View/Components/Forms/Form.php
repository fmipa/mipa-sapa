<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Form extends Component
{
    public $class, $id, $action, $method, $ecntype;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $class   = false,
        $id      = false,
        $action   = '#',
        $method   = 'GET',
        $ecntype = false,
    )
    {
        $this->class    = $class;
        $this->id       = $id;
        $this->action   = $action;
        $this->method   = $method;
        $this->ecntype  = $ecntype;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.forms.form');
    }
}
