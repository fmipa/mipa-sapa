<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Selectbox extends Component
{
    public $selector, $classDiv, $class, $label, $name, $id, $datas, $selected, $placeholder, $required, $modal, $readonly, $disabled, $autofocus, $hasMultiple, $smallText;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $label, 
        $name, 
        $selector       = false,
        $classDiv       = false,
        $class          = NULL, 
        $id             = NULL,
        $datas          = [],
        $selected       = false,
        $placeholder    = NULL,
        $required       = false,
        $modal          = false,
        $readonly       = false,
        $disabled       = false,
        $autofocus      = false,
        $hasMultiple    = false,
        $smallText      = false,
    )
    {
        $this->label        = $label;
        $this->name         = $name;
        $this->selector     = $selector;
        $this->classDiv     = $classDiv;
        $this->class        = $class;
        $this->id           = $id;
        $this->datas        = $datas;
        $this->selected     = $selected;
        $this->placeholder  = $placeholder;
        $this->required     = $required;
        $this->modal        = $modal;
        $this->readonly     = $readonly;
        $this->disabled     = $disabled;
        $this->autofocus    = $autofocus;
        $this->hasMultiple  = $hasMultiple;
        $this->smallText    = $smallText;
    }

    /**
     * Determine if the given option is the currently selected option.
     */
    public function isSelected(string $option): bool
    {
        return $option === $this->selected;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.forms.'.config('variables.templateForm').'.selectbox');
    }

}
