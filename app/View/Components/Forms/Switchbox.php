<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class SwitchBox extends Component
{
    public $class, $id, $label, $name, $value, $checked, $required, $readonly, $disabled, $smallText;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $label,
        $name    = NULL,
        $class   = false,
        $id      = false,
        $value   = NULL,
        $checked = true,
        $required   = false,
        $readonly   = false,
        $disabled   = false,
        $smallText  = false,
    )
    {
        $this->class        = $class;
        $this->id           = $id;
        $this->label        = $label;
        $this->name         = $name;
        $this->value        = $value;
        $this->checked      = $checked;
        $this->required     = $required;
        $this->readonly     = $readonly;
        $this->disabled     = $disabled;
        $this->smallText    = $smallText;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        $this->value = $this->value ?? old($this->name);
        return view('components.'.config('variables.templateName').'.forms.'.config('variables.templateForm').'.switchbox');
    }
}
