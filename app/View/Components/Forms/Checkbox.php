<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Checkbox extends Component
{
    public $classDiv, $label ,$name ,$class ,$id ,$text ,$value ,$checked ,$required ,$readonly ,$disabled ,$autofocus, $smallText;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $label, 
        $name    = false, 
        $classDiv= false,
        $class   = false, 
        $id      = false,
        $text,
        $value,
        $checked    = false,
        $required   = false,
        $readonly   = false,
        $disabled   = false,
        $autofocus  = false,
        $smallText  = false,
    )
    {
        $this->label    = $label; 
        $this->name     = $name; 
        $this->classDiv = $classDiv;
        $this->class    = $class; 
        $this->id       = $id;
        $this->text     = $text;
        $this->value    = $value;
        $this->checked  = $checked;
        $this->required = $required;
        $this->readonly = $readonly;
        $this->disabled = $disabled;
        $this->autofocus = $autofocus;
        $this->smallText    = $smallText;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.forms.'.config('variables.templateForm').'.checkbox');
    }
}
