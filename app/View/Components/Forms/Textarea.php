<?php

namespace App\View\Components\Forms;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Textarea extends Component
{
    public $classDiv, $class, $id, $label, $name, $value, $placeholder, $required, $readonly, $disabled, $autofocus, $rows, $smallText;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $label,
        $name    = NULL,
        $classDiv= false,
        $class   = false,
        $id      = false,
        $value   = NULL,
        $placeholder = NULL,
        $required   = false,
        $readonly   = false,
        $disabled   = false,
        $autofocus  = false,
        $rows       = 3,
        $smallText  = false,
    )
    {
        $this->classDiv     = $classDiv;
        $this->class        = $class;
        $this->id           = $id;
        $this->label        = $label;
        $this->name         = $name;
        $this->value        = $value;
        $this->placeholder  = $placeholder;
        $this->required     = $required;
        $this->readonly     = $readonly;
        $this->disabled     = $disabled;
        $this->autofocus    = $autofocus;
        $this->rows         = $rows;
        $this->smallText    = $smallText;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.forms.'.config('variables.templateForm').'.textarea');
    }
}
