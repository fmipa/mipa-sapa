<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Button extends Component
{
    public $class, $id, $type, $label, $onClick;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $type   = 'button',
        $class  = false,
        $id     = false, 
        $label,
        $onClick    = false, 
    )
    {
        $this->type         = $type;
        $this->class        = $class;
        $this->id           = $id;
        $this->label        = $label;
        $this->onClick      = $onClick;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.button');
    }
} 