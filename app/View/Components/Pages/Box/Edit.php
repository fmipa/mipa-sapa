<?php

namespace App\View\Components\Pages\Box;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataBox, $tahuns, $unitKerjas, $selectTahun, $selectUnit;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataBox,
        $tahuns         = false,
        $unitKerjas     = false,
        $selectTahun    = false,
        $selectUnit     = false,
    )
    {
        $this->dataBox      = $dataBox;
        $this->tahuns       = $tahuns;
        $this->unitKerjas   = $unitKerjas;
        $this->selectTahun  = $selectTahun;
        $this->selectUnit   = $selectUnit;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.box.edit');
    }
}
