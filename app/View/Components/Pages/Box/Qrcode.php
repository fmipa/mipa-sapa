<?php

namespace App\View\Components\Pages\Box;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Qrcode extends Component
{
    public $datas, $box;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $datas = false,
        $box = false,
    )
    {
        $this->datas    = $datas;
        $this->box      = $box;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.box.qrcode');
    }
}
