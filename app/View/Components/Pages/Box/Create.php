<?php

namespace App\View\Components\Pages\Box;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $nomorUrut, $tahuns, $unitKerjas;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $nomorUrut  = 0,
        $tahuns     = false,
        $unitKerjas = false,
    )
    {
        $this->nomorUrut    = $nomorUrut;
        $this->tahuns       = $tahuns;
        $this->unitKerjas   = $unitKerjas;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.box.create');
    }
}
