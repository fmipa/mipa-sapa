<?php

namespace App\View\Components\Pages\SuratMasuk;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Detail extends Component
{
    public $dataSurat;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataSurat = false,
    )
    {
        $this->dataSurat = $dataSurat;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.surat-masuk.detail');
    }
}
