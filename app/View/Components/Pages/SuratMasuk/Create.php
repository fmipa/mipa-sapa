<?php

namespace App\View\Components\Pages\SuratMasuk;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $folders, $jenisAkses, $unitKerjas, $pegawais, $boxes, $accepts, $selected, $creators;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $folders        = false,
        $jenisAkses     = false,
        $unitKerjas     = false,
        $pegawais       = false,
        $boxes          = false,
        $accepts        = false,
        $selected       = false,
        $creators       = false,
    )
    {
        $this->folders          = $folders;
        $this->jenisAkses       = $jenisAkses;
        $this->unitKerjas       = $unitKerjas;
        $this->pegawais         = $pegawais;
        $this->boxes            = $boxes;
        $this->accepts          = $accepts;
        $this->selected         = $selected;
        $this->creators         = $creators;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.surat-masuk.create');
    }
}
