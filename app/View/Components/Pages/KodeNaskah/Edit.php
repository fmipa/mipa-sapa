<?php

namespace App\View\Components\Pages\KodeNaskah;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataKodeNaskah;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataKodeNaskah,
    )
    {
        $this->dataKodeNaskah      = $dataKodeNaskah;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.kode-naskah.edit');
    }
}
