<?php

namespace App\View\Components\Pages\MasterAgama;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataAgama;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataAgama,
    )
    {
        $this->dataAgama      = $dataAgama;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.master-agama.edit');
    }
}
