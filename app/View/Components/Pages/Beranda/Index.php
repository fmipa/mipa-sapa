<?php

namespace App\View\Components\Pages\Beranda;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Index extends Component
{
    public $datas;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $datas = false,
    )
    {
        $this->datas = $datas;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        // $namaFolder = match (auth()->user()->operator) {
        //     1 => 'operator',
        //     default => 'pimpinan',
        // };
        $namaFolder = 'pimpinan';
        return view('components.'.config('variables.templateName').'.pages.beranda.'.$namaFolder.'.index');
    }
}
