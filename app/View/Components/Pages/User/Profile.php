<?php

namespace App\View\Components\Pages\User;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Profile extends Component
{
    public $dataUser;
    public $masterAgamas;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataUser       = false,
        $masterAgamas   = false,
    )
    {
        $this->dataUser     = $dataUser;
        $this->masterAgamas = $masterAgamas;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.user.profile');
    }
}
