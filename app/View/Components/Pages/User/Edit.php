<?php

namespace App\View\Components\Pages\User;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataUser, $roles, $selectRole;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataUser,
        $roles          = false,
        $selectRole     = false,
    )
    {
        $this->dataUser     = $dataUser;
        $this->roles        = $roles;
        $this->selectRole   = $selectRole;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.user.edit');
    }
}
