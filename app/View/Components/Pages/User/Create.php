<?php

namespace App\View\Components\Pages\User;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $roles;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $roles  = false,
    )
    {
        $this->roles    = $roles;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.user.create');
    }
}
