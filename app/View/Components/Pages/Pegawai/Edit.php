<?php

namespace App\View\Components\Pages\Pegawai;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataPegawai, $roles, $jenisKelamin, $status, $agamas, $prodis, $selectRoles, $selectStatus, $selectAgama, $selectProdi;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataPegawai,
        $roles          = false,
        $jenisKelamin   = false,
        $status         = false,
        $agamas         = false,
        $prodis         = false,
        $selectRoles    = false,
        $selectStatus   = false,
        $selectAgama    = false,
        $selectProdi    = false,
    )
    {
        $this->dataPegawai  = $dataPegawai;
        $this->roles        = $roles;
        $this->jenisKelamin = $jenisKelamin;
        $this->status       = $status;
        $this->agamas       = $agamas;
        $this->prodis       = $prodis;
        $this->selectRoles  = $selectRoles;
        $this->selectStatus = $selectStatus;
        $this->selectAgama  = $selectAgama;
        $this->selectProdi  = $selectProdi;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.pegawai.edit');
    }
}
