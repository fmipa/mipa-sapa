<?php

namespace App\View\Components\Pages\Pegawai;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $roles, $jenisKelamin, $status, $agamas, $prodis;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $roles          = false,
        $jenisKelamin   = false,
        $status         = false,
        $agamas         = false,
        $prodis         = false,
    )
    {
        $this->roles        = $roles;
        $this->jenisKelamin = $jenisKelamin;
        $this->status       = $status;
        $this->agamas       = $agamas;
        $this->prodis       = $prodis;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.pegawai.create');
    }
}
