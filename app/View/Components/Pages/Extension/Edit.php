<?php

namespace App\View\Components\Pages\Extension;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataExtension;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataExtension,
    )
    {
        $this->dataExtension = $dataExtension;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.extension.edit');
    }
}
