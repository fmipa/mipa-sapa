<?php

namespace App\View\Components\Pages\File\Folder;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Folder extends Component
{
    public $folders, $kembaliId,  $folderId, $namaFolder;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $folders    = false,
        $kembaliId  = false,
        $folderId   = false,
        $namaFolder = false,
    )
    {
        $this->folders      = $folders;
        $this->kembaliId    = $kembaliId;
        $this->folderId     = $folderId;
        $this->namaFolder   = $namaFolder;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.file.folder.folder');
    }
}
