<?php

namespace App\View\Components\Pages\File\Folder;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Pegawai extends Component
{
    public $kembaliId, $namaFolder;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $kembaliId    = false,
        $namaFolder   = false,
    )
    {
        $this->kembaliId      = $kembaliId;
        $this->namaFolder     = $namaFolder;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.file.folder.pegawai');
    }
}
