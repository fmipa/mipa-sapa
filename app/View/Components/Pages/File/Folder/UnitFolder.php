<?php

namespace App\View\Components\Pages\File\Folder;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class UnitFolder extends Component
{
    public $folders;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $folders = false,
    )
    {
        $this->folders = $folders;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.file.folder.unit-folder');
    }
}
