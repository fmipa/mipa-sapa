<?php

namespace App\View\Components\Pages\File;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $nomorUrut, $tahuns, $unitKerja;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $nomorUrut  = 0,
        $tahuns     = false,
        $unitKerja  = false,
    )
    {
        $this->nomorUrut    = $nomorUrut;
        $this->tahuns       = $tahuns;
        $this->unitKerja    = $unitKerja;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.box.create');
    }
}
