<?php

namespace App\View\Components\Pages\File;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class DataTables extends Component
{
    public $datas, $kataKunci;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $datas     = false,
        $kataKunci  = false,
    )
    {
        $this->datas        = $datas;
        $this->kataKunci    = $kataKunci;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.file.dataTables');
    }
}
