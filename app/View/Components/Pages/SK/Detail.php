<?php

namespace App\View\Components\Pages\SK;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Detail extends Component
{
    public $dataSK;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataSK = false,
    )
    {
        $this->dataSK = $dataSK;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.sk.detail');
    }
}
