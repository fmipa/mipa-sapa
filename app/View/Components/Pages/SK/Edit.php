<?php

namespace App\View\Components\Pages\SK;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataSK, $kodeNaskahs, $kodeUnits, $folders, $jenisAkses, $unitKerjas, $pegawais, $boxes, $accepts, $selectFolder, $selectJenisAkses, $selectBox, $creators, $selectCreator;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataSK,
        $kodeNaskahs        = false,
        $kodeUnits          = false,
        $folders            = false,
        $jenisAkses         = false,
        $unitKerjas         = false,
        $pegawais           = false,
        $boxes              = false,
        $accepts            = false,
        $selectFolder       = false,
        $selectJenisAkses   = false,
        $selectBox          = false,
        $creators           = false,
        $selectCreator      = false,
    )
    {
        $this->dataSK           = $dataSK;
        $this->kodeNaskahs      = $kodeNaskahs;
        $this->kodeUnits        = $kodeUnits;
        $this->folders          = $folders;
        $this->jenisAkses       = $jenisAkses;
        $this->unitKerjas       = $unitKerjas;
        $this->pegawais         = $pegawais;
        $this->boxes            = $boxes;
        $this->accepts          = $accepts;
        $this->selectFolder     = $selectFolder;
        $this->selectJenisAkses = $selectJenisAkses;
        $this->selectBox        = $selectBox;
        $this->creators         = $creators;
        $this->selectCreator    = $selectCreator;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.sk.edit');
    }
}
