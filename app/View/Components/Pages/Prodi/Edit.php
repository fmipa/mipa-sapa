<?php

namespace App\View\Components\Pages\Prodi;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataProdi, $jurusans;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataProdi,
        $jurusans = false,
    )
    {
        $this->dataProdi    = $dataProdi;
        $this->jurusans     = $jurusans;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.prodi.edit');
    }
}
