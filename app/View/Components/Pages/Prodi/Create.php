<?php

namespace App\View\Components\Pages\Prodi;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $jurusans;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $jurusans = false,
    )
    {
        $this->jurusans = $jurusans;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.prodi.create');
    }
}
