<?php

namespace App\View\Components\Pages\Dokumen;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $jenisAkses, $unitKerjas, $pegawais, $boxes, $folders, $selected, $accepts, $creators;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $jenisAkses = false,
        $unitKerjas = false,
        $pegawais   = false,
        $boxes      = false,
        $folders    = false,
        $selected   = false,
        $accepts    = false,
        $creators   = false,
    )
    {
        $this->jenisAkses   = $jenisAkses;
        $this->unitKerjas   = $unitKerjas;
        $this->pegawais     = $pegawais;
        $this->boxes        = $boxes;
        $this->folders      = $folders;
        $this->selected     = $selected;
        $this->accepts      = $accepts;
        $this->creators     = $creators;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.dokumen.create');
    }
}
