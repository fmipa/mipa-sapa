<?php

namespace App\View\Components\Pages\Dokumen;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataDokumen, $jenisAkses, $unitKerjas, $pegawais, $boxes, $folders, $selectJenisAkses, $selectBox, $selectFolder, $accepts, $creators, $selectCreator;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataDokumen,
        $jenisAkses         = false,
        $unitKerjas         = false,
        $pegawais           = false,
        $boxes              = false,
        $folders            = false,
        $selectJenisAkses   = false,
        $selectBox          = false,
        $selectFolder       = false,
        $accepts            = false,
        $creators           = false,
        $selectCreator      = false,
    )
    {
        $this->dataDokumen      = $dataDokumen;
        $this->jenisAkses       = $jenisAkses;
        $this->unitKerjas       = $unitKerjas;
        $this->pegawais         = $pegawais;
        $this->boxes            = $boxes;
        $this->folders          = $folders;
        $this->selectJenisAkses = $selectJenisAkses;
        $this->selectBox        = $selectBox;
        $this->selectFolder     = $selectFolder;
        $this->accepts          = $accepts;
        $this->creators         = $creators;
        $this->selectCreator    = $selectCreator;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.dokumen.edit');
    }
}
