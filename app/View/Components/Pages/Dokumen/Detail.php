<?php

namespace App\View\Components\Pages\Dokumen;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Detail extends Component
{
    public $dataDokumen;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataDokumen = false,
    )
    {
        $this->dataDokumen = $dataDokumen;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.dokumen.detail');
    }
}
