<?php

namespace App\View\Components\Pages\SuratKeluar;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $kodeNaskahs, $kodeUnits, $folders, $jenisAkses, $unitKerjas, $pegawais, $boxes, $accepts, $selected, $selectKodeUnit, $selectFolder, $creators;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $kodeNaskahs    = false,
        $kodeUnits      = false,
        $folders        = false,
        $jenisAkses     = false,
        $unitKerjas     = false,
        $pegawais       = false,
        $boxes          = false,
        $accepts        = false,
        $selected       = false,
        $selectKodeUnit = false,
        $selectFolder   = false,
        $creators       = false,
    )
    {
        $this->kodeNaskahs      = $kodeNaskahs;
        $this->kodeUnits        = $kodeUnits;
        $this->folders          = $folders;
        $this->jenisAkses       = $jenisAkses;
        $this->unitKerjas       = $unitKerjas;
        $this->pegawais         = $pegawais;
        $this->boxes            = $boxes;
        $this->accepts          = $accepts;
        $this->selected         = $selected;
        $this->selectKodeUnit   = $selectKodeUnit;
        $this->selectFolder     = $selectFolder;
        $this->creators         = $creators;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.surat-keluar.create');
    }
}
