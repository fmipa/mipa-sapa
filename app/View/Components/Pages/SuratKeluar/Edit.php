<?php

namespace App\View\Components\Pages\SuratKeluar;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataSurat, $kodeNaskahs, $folders, $jenisAkses, $unitKerjas, $pegawais, $boxes, $accepts, $selectFolder, $selectJenisAkses, $selectBox, $creators, $selectCreator;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataSurat,
        $kodeNaskahs        = false,
        $folders            = false,
        $jenisAkses         = false,
        $unitKerjas         = false,
        $pegawais           = false,
        $boxes              = false,
        $accepts            = false,
        $selectFolder       = false,
        $selectJenisAkses   = false,
        $selectBox          = false,
        $creators           = false,
        $selectCreator      = false,
    )
    {
        $this->dataSurat        = $dataSurat;
        $this->kodeNaskahs      = $kodeNaskahs;
        $this->folders          = $folders;
        $this->jenisAkses       = $jenisAkses;
        $this->unitKerjas       = $unitKerjas;
        $this->pegawais         = $pegawais;
        $this->boxes            = $boxes;
        $this->accepts          = $accepts;
        $this->selectFolder     = $selectFolder;
        $this->selectJenisAkses = $selectJenisAkses;
        $this->selectBox        = $selectBox;
        $this->creators         = $creators;
        $this->selectCreator    = $selectCreator;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.surat-keluar.edit');
    }
}
