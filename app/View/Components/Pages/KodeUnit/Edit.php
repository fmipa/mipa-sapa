<?php

namespace App\View\Components\Pages\KodeUnit;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataKodeUnit;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataKodeUnit,
    )
    {
        $this->dataKodeUnit      = $dataKodeUnit;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.kode-unit.edit');
    }
}
