<?php

namespace App\View\Components\Pages\KategoriBerkas;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataKBerkas;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataKBerkas,
    )
    {
        $this->dataKBerkas = $dataKBerkas;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.kategori-berkas.edit');
    }
}
