<?php

namespace App\View\Components\Pages\BerkasPegawai;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataBerkas, $kategoris, $folders, $jenisAkses, $unitKerjas, $pegawais, $boxes, $accepts, $creators, $selectFolder, $selectJenisAkses, $selectBox, $selectCreator;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataBerkas         = false,
        $kategoris          = false,
        $folders            = false,
        $jenisAkses         = false,
        $unitKerjas         = false,
        $pegawais           = false,
        $boxes              = false,
        $accepts            = false,
        $creators           = false,
        $selectFolder       = false,
        $selectJenisAkses   = false,
        $selectBox          = false,
        $selectCreator      = false,
    )
    {
        $this->dataBerkas       = $dataBerkas;
        $this->kategoris        = $kategoris;
        $this->folders          = $folders;
        $this->jenisAkses       = $jenisAkses;
        $this->unitKerjas       = $unitKerjas;
        $this->pegawais         = $pegawais;
        $this->boxes            = $boxes;
        $this->creators         = $creators;
        $this->accepts          = $accepts;
        $this->selectFolder     = $selectFolder;
        $this->selectJenisAkses = $selectJenisAkses;
        $this->selectBox        = $selectBox;
        $this->selectCreator    = $selectCreator;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.berkas-pegawai.edit');
    }
}
