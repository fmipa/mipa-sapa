<?php

namespace App\View\Components\Pages\BerkasPegawai;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Detail extends Component
{
    public $dataBerkas;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataBerkas = false,
    )
    {
        $this->dataBerkas = $dataBerkas;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.berkas-pegawai.detail');
    }
}
