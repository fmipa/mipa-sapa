<?php

namespace App\View\Components\Pages\Role;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $permissions;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $permissions = false,
    )
    {
        $this->permissions = $permissions;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.role.create');
    }
}
