<?php

namespace App\View\Components\Pages\Jurusan;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataJurusan;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataJurusan,
    )
    {
        $this->dataJurusan = $dataJurusan;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.jurusan.edit');
    }
}
