<?php

namespace App\View\Components\Pages\Folder;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Index extends Component
{
    public $datas, $parentFolders, $unitKerjas, $prodis, $users;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $datas,
        $parentFolders  = false,
        $unitKerjas     = false,
        $prodis         = false,
        $users          = false,
    )
    {
        $this->datas            = $datas;
        $this->parentFolders    = $parentFolders;
        $this->unitKerjas       = $unitKerjas;
        $this->prodis           = $prodis;
        $this->users            = $users;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.folder.index');
    }
}
