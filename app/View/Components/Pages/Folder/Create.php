<?php

namespace App\View\Components\Pages\Folder;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $parentFolders;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $parentFolders  = false,
    )
    {
        $this->parentFolders    = $parentFolders;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.folder.create');
    }
}
