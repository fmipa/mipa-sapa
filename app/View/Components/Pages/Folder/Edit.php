<?php

namespace App\View\Components\Pages\Folder;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataBox, $tahuns, $unitKerja, $selectTahun;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataBox,
        $tahuns         = false,
        $unitKerja      = false,
        $selectTahun    = false,
    )
    {
        $this->dataBox      = $dataBox;
        $this->tahuns       = $tahuns;
        $this->unitKerja    = $unitKerja;
        $this->selectTahun  = $selectTahun;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.folder.edit');
    }
}
