<?php

namespace App\View\Components\Pages\Permission;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataPermission;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataPermission,
    )
    {
        $this->dataPermission = $dataPermission;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.permission.edit');
    }
}
