<?php

namespace App\View\Components\Pages\FolderStorage;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Index extends Component
{
    public $datas;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $datas = false,
    )
    {
        $this->datas = $datas;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.folder-storage.index');
    }
}
