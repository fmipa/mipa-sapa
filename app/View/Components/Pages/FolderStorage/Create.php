<?php

namespace App\View\Components\Pages\FolderStorage;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Create extends Component
{
    public $jenisAkses, $unitKerjas, $pegawais, $selected;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $pegawais   = false,
        $jenisAkses = false,
        $unitKerjas = false,
        $selected   = false,
    )
    {
        $this->jenisAkses   = $jenisAkses;
        $this->unitKerjas   = $unitKerjas;
        $this->pegawais     = $pegawais;
        $this->selected     = $selected;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.folder-storage.create');
    }
}
