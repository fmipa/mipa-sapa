<?php

namespace App\View\Components\Pages\FolderStorage;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Edit extends Component
{
    public $dataFolder, $jenisAkses, $roles, $pegawais, $selected;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $dataFolder,
        $jenisAkses = false,
        $roles      = false,
        $pegawais   = false,
        $selected   = false,
    )
    {
        $this->dataFolder   = $dataFolder;
        $this->jenisAkses   = $jenisAkses;
        $this->roles        = $roles;
        $this->pegawais     = $pegawais;
        $this->selected     = $selected;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.pages.folder-storage.edit');
    }
}
