<?php

namespace App\View\Components\Tables;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class ButtonAction extends Component
{
    public $route, $id, $labelDelete;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $route = '',
        $id,
        $labelDelete = '',
    )
    {
        $this->route        = $route;
        $this->id           = $id;
        $this->labelDelete  = $labelDelete;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.modals.button-action');
    }
}
