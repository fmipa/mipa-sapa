<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Ahref extends Component
{
    public $class, $id, $link, $target, $text;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $link   = '#', 
        $target = false, 
        $text   = false, 
        $class  = false, 
        $id     = false,
    )
    {
        $this->class    = $class;
        $this->id       = $id;
        $this->link     = $link;
        $this->target   = $target;
        $this->text     = $text;
        }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.ahref');
    }
}
