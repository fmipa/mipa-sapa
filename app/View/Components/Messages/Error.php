<?php

namespace App\View\Components\Messages;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Error extends Component
{
    public $class, $message;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $class      = false,
        $message    = NULL,
    )
    {
        $this->class    = $class;
        $this->message  = $message;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.messages.error');
    }
}
