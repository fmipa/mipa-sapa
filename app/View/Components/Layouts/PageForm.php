<?php

namespace App\View\Components\Layouts;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PageForm extends Component
{
    public $title, $menu, $formLabel, $submenu, $urlback;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $title, 
        $formLabel = '', 
        $menu = '', 
        $submenu = false,
        $urlback = false,
    )
    {
        $this->title        = $title;
        $this->formLabel    = $formLabel;
        $this->menu         = $menu;
        $this->submenu      = $submenu;
        $this->urlback      = $urlback;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.env('TEMPLATE').'.layouts.page-form');
    }
}
