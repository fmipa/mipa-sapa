<?php

namespace App\View\Components\Layouts;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class ContentHeader extends Component
{
    public $title, $menu, $submenu;
    /**
     * Create a new component instance.
     */
    public function __construct($title, $menu = false, $submenu = false)
    {
        $this->title    = $title;
        $this->menu     = $menu;
        $this->submenu  = $submenu;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.env('TEMPLATE').'.layouts.content-header');
    }
}
