<?php

namespace App\View\Components\Layouts;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CardApp extends Component
{
    /**
     * Create a new component instance.
     */
    public function __construct(
        public bool $card           = false,
        public bool $create         = false,
        public string $createUrl    = '#',
        public bool $back           = false,
        public string $backUrl      = '#',
        public string $pageName     = 'Data',
        public string $subPageName  = 'Data',
    )
    {
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.layouts.sections.card-app');
    }
}
