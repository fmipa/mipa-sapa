<?php

namespace App\View\Components\Layouts;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Notification extends Component
{
    public $notification;
    /**
     * Create a new component instance.
     */
    public function __construct($notification = false)
    {
        $this->notification = $notification;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.env('TEMPLATE').'.layouts.notification');
    }
}
