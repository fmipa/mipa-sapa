<?php

namespace App\View\Components\Layouts;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class PageIndexTable extends Component
{
    public $title, $route, $menu, $submenu, $buttonTambah;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $title, 
        $route = false, 
        $menu = '', 
        $submenu = false,
        $buttonTambah = false,
    )
    {
        $this->title        = $title;
        $this->route        = $route;
        $this->menu         = $menu;
        $this->submenu      = $submenu;
        $this->buttonTambah = $buttonTambah;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.env('TEMPLATE').'.layouts.page-index-table');
    }
}
