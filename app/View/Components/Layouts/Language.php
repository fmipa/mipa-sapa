<?php

namespace App\View\Components\Layouts;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Language extends Component
{
    public $language;
    /**
     * Create a new component instance.
     */
    public function __construct($language = false)
    {
        $this->language = $language;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.env('TEMPLATE').'.layouts.language');
    }
}
