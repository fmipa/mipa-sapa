<?php

namespace App\View\Components\Layouts\Vertical\Menus;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Menu extends Component
{
    public $nameMenu, $route, $active, $iconMenu, $dropdownMenu, $alertMenu;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($nameMenu, $route = false, $active = false, $iconMenu = false, $dropdownMenu = false, $alertMenu = false)
    {
        $this->nameMenu     = $nameMenu;
        $this->route        = $route;
        $this->active       = $active;
        $this->iconMenu     = $iconMenu;
        $this->dropdownMenu = $dropdownMenu;
        $this->alertMenu    = $alertMenu;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.layouts.sections.vertical.menus.menu');
    }
}
