<?php

namespace App\View\Components\Layouts\Vertical\Menus;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class DropdownMenu extends Component
{
    public $parentMenu, $parentIcon, $parentAlert;
    /**
     * Create a new component instance.
     */
    public function __construct($parentMenu, $parentIcon = false, $parentAlert = false)
    {
        $this->parentMenu     = $parentMenu;
        $this->parentIcon     = $parentIcon;
        $this->parentAlert    = $parentAlert;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.layouts.sections.vertical.menus.dropdown-menu');
    }
}
