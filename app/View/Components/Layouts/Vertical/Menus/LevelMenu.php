<?php

namespace App\View\Components\Components\Layouts\Vertical\Menus;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class LevelMenu extends Component
{
    public $titleMenu, $route, $active, $classMenu;
    /**
     * Create a new component instance.
     */
    public function __construct($titleMenu, $route = false, $active = false, $classMenu = '')
    {
        $this->titleMenu    = $titleMenu;
        $this->route        = $route;
        $this->active       = $active;
        $this->classMenu    = $classMenu;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.env('TEMPLATE').'.menus.level-menu');
    }
}
