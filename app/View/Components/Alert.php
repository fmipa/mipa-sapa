<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Alert extends Component
{
    public $typeAlert, $messages;
    /**
     * Create a new component instance.
     */
    public function __construct(
        $typeAlert = 'success',
        $messages,
    )
    {
        $this->typeAlert    = $typeAlert;
        $this->messages     = $messages;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.'.config('variables.templateName').'.alert');
    }
}
