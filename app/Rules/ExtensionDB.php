<?php

namespace App\Rules;

use App\Models\Extension;
use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class ExtensionDB implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $extensions = Extension::get(['extension'])->pluck('extension')->toArray();
        $extensionsUppercase = Extension::get(['extension'])->map(function($item) {
            return strtoupper($item->extension);
        })->toArray();
        $extensionFile = $value->getClientOriginalExtension();
        if (!in_array($extensionFile, array_merge($extensions, $extensionsUppercase)))
        {
            $fail('Jenis File tidak diijinkan.');
        }
    }
}
