<?php

use App\Models\Box;
use App\Models\File;
use App\Models\Folder;
use App\Models\UnitKerja;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('files', function(Blueprint $table) {
            $table->dropForeign('files_folder_id_foreign');
            $table->dropIndex('files_folder_id_foreign');
            
            $table->renameColumn('folder_id', 'folder_storage_id');

            $table->foreign('folder_storage_id')->references('id')->on('folder_storages')->onDelete('cascade');
        });

        Schema::table('files', function (Blueprint $table) {
            $table->foreignIdFor(Folder::class)->nullable()->after('jenis_akses_id')->constrained();
        });

        Schema::table('pegawai_file', function (Blueprint $table) {
            $table->unsignedBigInteger('pegawai_id')->nullable()->change();
            $table->foreignIdFor(UnitKerja::class)->nullable()->after('pegawai_id')->constrained();
        });


    }

    public function down()
    {
        Schema::table('files', function(Blueprint $table) {
            $table->dropForeign('files_folder_storage_id_foreign');
            $table->renameColumn('folder_id', 'folder_storage_id');

            $table->foreign('folder_id')->references('id')->on('folders')->onDelete('cascade');
        });

        Schema::table('pegawai_file', function (Blueprint $table) {
            $table->unsignedBigInteger('pegawai_id')->nullable(false)->change();
            $table->foreignIdFor(UnitKerja::class)->nullable()->after('pegawai_id')->constrained();
        });


    }
};
