<?php

use App\Models\Box;
use App\Models\File;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('boxes', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->integer('nomor');
            $table->string('tahun')->nullable();
            $table->foreignId('unit_kerja_id')->nullable()->comment('Unit Kerja yang mempunyai Box/Gobi, jika NULL bearti milik pribadi');
            $table->string('lokasi')->comment('Lokasi Box Tersimpan');
            $table->string('unique_id')->comment('Diperuntukan link QRCode');
            $table->string('password')->nullable()->comment('Untuk mengkases QRCode/keamanan berkas jika di scan');
            $table->foreignId('created_by');
            $table->timestamps();
            
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('unit_kerja_id')->references('id')->on('unit_kerjas');
        });

        Schema::create('file_box', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(File::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignIdFor(Box::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignId('created_by');
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('boxes');
        Schema::dropIfExists('file_box');
    }
};
