<?php

use App\Models\File;
use App\Models\KodeNaskah;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('surat_keluar', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(File::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->string('nomor');
            $table->string('perihal');
            $table->year('tahun');
            $table->string('lampiran')->nullable();
            $table->date('tanggal_diterbitkan')->default(date("Y/m/d"));
            $table->string('tujuan');
            $table->foreignIdFor(KodeNaskah::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignId('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('surat_keluar');
    }
};
