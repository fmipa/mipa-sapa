<?php

use App\Models\FolderStorage;
use App\Models\JenisAkses;
use App\Models\Pegawai;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('folder_storages', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->foreignIdFor(JenisAkses::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->string('path');
            $table->string('disk')->default(config('filesystems.default'));
            $table->foreignId('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('pegawai_folder_storage', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Pegawai::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignIdFor(FolderStorage::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignId('created_by');
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('folder_storages');
        Schema::dropIfExists('pegawai_folder_storage');
    }
};
