<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('pegawais', function (Blueprint $table) {
            $table->id();
            $table->string('firstname');
            $table->string('middlename')->nullable();
            $table->string('lastname')->nullable();
            $table->string('gelar_depan')->nullable();
            $table->string('gelar_belakang')->nullable();
            $table->string('nomor_unik')->comment('NIK/NIDK/NIM/NIP')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->foreignId('agama_id')->nullable();
            $table->foreignId('team_id')->nullable();
            $table->foreignId('prodi_id')->nullable();
            $table->foreignId('unit_kerja_id')->nullable();
            $table->enum('jenis_kelamin', ['L', 'P'])->nullable();
            $table->enum('status', ['PNS', 'PPPK', 'KONTRAK', 'HONORER', 'PHL', 'ASISTEN', 'OUTSOURCING'])->nullable();
            // $table->foreignId('created_by')->nullable();
            $table->timestamps();
            $table->softDeletes($column = 'deleted_at', $precision = 0);

            $table->foreign('agama_id')->references('id')->on('master_agamas');
            $table->foreign('team_id')->references('id')->on('teams');
            $table->foreign('prodi_id')->references('id')->on('prodis');
            $table->foreign('unit_kerja_id')->references('id')->on('unit_kerjas');
            // $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('pegawais');
    }
};
