<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('extensions', function (Blueprint $table) {
            $table->id();
            $table->string('extension');
            $table->enum('tipe_file', ['*', 'berkas-pegawai', 'dokumen', 'surat-masuk', 'surat-keluar', 'sk'])->default('*')->comment('Tipe File sama dengan TIPEFILEENUM');
            $table->string('deskripsi')->nullable();
            $table->foreignId('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('extensions');
    }
};
