<?php

use App\Models\Box;
use App\Models\File;
use App\Models\Folder;
use App\Models\Pegawai;
use App\Models\Prodi;
use App\Models\UnitKerja;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Role;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // ! Remove Foreign Id
        Schema::table('pohons', function (Blueprint $table) {
            $table->dropForeign(['role_id']);
            $table->dropColumn('role_id');
        });

        // ! Rename Tables
        Schema::rename('berkas', 'berkas_pegawai');
        Schema::rename('folders', 'folder_storages');
        Schema::rename('pohons', 'folders');
        Schema::rename('pegawai_folder', 'pegawai_folder_storage');

        // ! Add Tables
        Schema::create('unit_kerjas', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->integer('urutan');
            $table->integer('is_folder')->default(0);
            $table->foreignId('prodi_id')->nullable();
            $table->string('keterangan')->nullable();
            // $table->foreignId('created_by');
            $table->timestamps();
            
            $table->foreign('prodi_id')->references('id')->on('prodis');
            // $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('unit_folder', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(UnitKerja::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignIdFor(Folder::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignId('created_by');
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('unit_role', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(UnitKerja::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignIdFor(Role::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignId('created_by');
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });

        // ! Modification Column
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('operator')->default(0)->after('pegawai_id');
        });

        Schema::table('pegawais', function (Blueprint $table) {
            $table->foreignIdFor(UnitKerja::class)->nullable()->after('prodi_id')->constrained();
        });

        Schema::table('folders', function (Blueprint $table) {
            $table->foreignIdFor(UnitKerja::class)->nullable()->after('parent_id')->constrained();
            $table->foreignIdFor(Prodi::class)->nullable()->after('unit_kerja_id')->constrained();
        });

        Schema::table('boxes', function (Blueprint $table) {
            // $table->dropColumn('bagian');
            $table->foreignIdFor(UnitKerja::class)->nullable()->after('bagian')->constrained();
        });


        Schema::table('pegawai_folder_storage', function (Blueprint $table) {
            $table->renameColumn('folder_id', 'folder_storage_id');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('unit_kerjas');
        Schema::dropIfExists('unit_folder');
    }
};
