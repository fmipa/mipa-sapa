<?php

use App\Models\File;
use App\Models\Folder;
use App\Models\FolderStorage;
use App\Models\JenisAkses;
use App\Models\Pegawai;
use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('nama_file')->comment('Surat menggunakan nomor surat lengkap, SK menggunakan nama SK');
            $table->string('extension')->nullable();
            $table->string('mime_type')->nullable();
            $table->integer('size')->nullable();
            $table->integer('view')->default(0);
            $table->integer('download')->default(0);
            $table->foreignIdFor(FolderStorage::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignIdFor(JenisAkses::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignId('created_by');
            $table->timestamps();
            $table->softDeletes($column = 'deleted_at', $precision = 0);
        
            $table->foreign('created_by')->references('id')->on('users');
        });

        Schema::create('pegawai_file', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(Pegawai::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignIdFor(File::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->foreignId('created_by');
            $table->timestamps();
        
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
        Schema::dropIfExists('pegawai_file');
    }
};
