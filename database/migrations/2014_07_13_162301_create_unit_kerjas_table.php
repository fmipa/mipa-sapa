<?php

use App\Models\Folder;
use App\Models\MasterFolder;
use App\Models\UnitKerja;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('unit_kerjas', function (Blueprint $table) {
            $table->id();
            $table->string('nama');
            $table->integer('urutan')->nullable();
            $table->integer('is_folder')->default(0);
            $table->foreignId('prodi_id')->nullable();
            $table->string('keterangan')->nullable();
            // $table->foreignId('created_by');
            $table->timestamps();
            
            $table->foreign('prodi_id')->references('id')->on('prodis');
            // $table->foreign('created_by')->references('id')->on('users');
        });

    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('unit_kerjas');
    }
};
