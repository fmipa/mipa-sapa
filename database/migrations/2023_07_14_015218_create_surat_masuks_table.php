<?php

use App\Models\File;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('surat_masuk', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(File::class)->constrained()
            ->cascadeOnUpdate()
            ->cascadeOnDelete();
            $table->string('nomor');
            $table->string('perihal');
            $table->year('tahun');
            $table->string('instansi');
            $table->date('tanggal_diterbitkan');
            $table->date('tanggal_diterima')->default(date("Y/m/d"));
            $table->foreignId('created_by');
            $table->timestamps();

            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('surat_masuk');
    }
};
