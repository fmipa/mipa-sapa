<?php

namespace Database\Seeders;

use App\Models\AksesFile;
use App\Models\JenisAkses;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JenisAksesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $jenisAkses = [
            ['nama' => 'publik'],
            ['nama' => 'pribadi'],
            ['nama' => 'tag'],
            ['nama' => 'tim'],
            ['nama' => 'unit-kerja'],
        ];

        JenisAkses::insert($jenisAkses);
    }
}
