<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // ! Deletet All Role
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('permissions')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        $permissions = [

            ['name' => 'activation'],
            ['name' => 'activation-index'],
            ['name' => 'activation-create'],
            ['name' => 'activation-update'],
            ['name' => 'activation-delete'],
                        
            ['name' => 'agama'],
            ['name' => 'agama-index'],
            ['name' => 'agama-create'],
            ['name' => 'agama-update'],
            ['name' => 'agama-delete'],
                                    
            ['name' => 'berkaspegawai'],
            ['name' => 'berkaspegawai-index'],
            ['name' => 'berkaspegawai-create'],
            ['name' => 'berkaspegawai-update'],
            ['name' => 'berkaspegawai-delete'],

            ['name' => 'box'],
            ['name' => 'box-index'],
            ['name' => 'box-create'],
            ['name' => 'box-update'],
            ['name' => 'box-delete'],
            
            ['name' => 'document'],
            ['name' => 'document-index'],
            ['name' => 'document-create'],
            ['name' => 'document-update'],
            ['name' => 'document-delete'],
            
            ['name' => 'extension'],
            ['name' => 'extension-index'],
            ['name' => 'extension-create'],
            ['name' => 'extension-update'],
            ['name' => 'extension-delete'],

            ['name' => 'file'],
            ['name' => 'file-index'],
            ['name' => 'file-create'],
            ['name' => 'file-update'],
            ['name' => 'file-delete'],
            
            ['name' => 'fileshare'],
            ['name' => 'fileshare-index'],
            ['name' => 'fileshare-create'],
            ['name' => 'fileshare-update'],
            ['name' => 'fileshare-delete'],
            
            ['name' => 'folder'],
            ['name' => 'folder-index'],
            ['name' => 'folder-create'],
            ['name' => 'folder-update'],
            ['name' => 'folder-delete'],
            
            ['name' => 'folderstorage'],
            ['name' => 'folderstorage-index'],
            ['name' => 'folderstorage-create'],
            ['name' => 'folderstorage-update'],
            ['name' => 'folderstorage-delete'],
                        
            ['name' => 'hakakses'],
            ['name' => 'hakakses-index'],
            ['name' => 'hakakses-create'],
            ['name' => 'hakakses-update'],
            ['name' => 'hakakses-delete'],
                        
            ['name' => 'jurusan'],
            ['name' => 'jurusan-index'],
            ['name' => 'jurusan-create'],
            ['name' => 'jurusan-update'],
            ['name' => 'jurusan-delete'],
            
            ['name' => 'pegawai'],
            ['name' => 'pegawai-index'],
            ['name' => 'pegawai-create'],
            ['name' => 'pegawai-update'],
            ['name' => 'pegawai-delete'],
            
            ['name' => 'kberkas'],
            ['name' => 'kberkas-index'],
            ['name' => 'kberkas-create'],
            ['name' => 'kberkas-update'],
            ['name' => 'kberkas-delete'],
            
            ['name' => 'kodenaskah'],
            ['name' => 'kodenaskah-index'],
            ['name' => 'kodenaskah-create'],
            ['name' => 'kodenaskah-update'],
            ['name' => 'kodenaskah-delete'],
            
            ['name' => 'kodeunit'],
            ['name' => 'kodeunit-index'],
            ['name' => 'kodeunit-create'],
            ['name' => 'kodeunit-update'],
            ['name' => 'kodeunit-delete'],
            
            ['name' => 'permission'],
            ['name' => 'permission-index'],
            ['name' => 'permission-create'],
            ['name' => 'permission-update'],
            ['name' => 'permission-delete'],

            ['name' => 'prodi'],
            ['name' => 'prodi-index'],
            ['name' => 'prodi-create'],
            ['name' => 'prodi-update'],
            ['name' => 'prodi-delete'],

            ['name' => 'sk'],
            ['name' => 'sk-index'],
            ['name' => 'sk-create'],
            ['name' => 'sk-update'],
            ['name' => 'sk-delete'],
            
            ['name' => 'suratmasuk'],
            ['name' => 'suratmasuk-index'],
            ['name' => 'suratmasuk-create'],
            ['name' => 'suratmasuk-update'],
            ['name' => 'suratmasuk-delete'],
            
            ['name' => 'suratkeluar'],
            ['name' => 'suratkeluar-index'],
            ['name' => 'suratkeluar-create'],
            ['name' => 'suratkeluar-update'],
            ['name' => 'suratkeluar-delete'],
            
            ['name' => 'team'],
            ['name' => 'team-index'],
            ['name' => 'team-create'],
            ['name' => 'team-update'],
            ['name' => 'team-delete'],

            ['name' => 'user'],
            ['name' => 'user-index'],
            ['name' => 'user-create'],
            ['name' => 'user-update'],
            ['name' => 'user-delete'],

        ];
        Permission::insert($permissions);
    }
}
