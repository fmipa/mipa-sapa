<?php

namespace Database\Seeders;

use App\Models\Prodi;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProdiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $prodis = [
            ['nama' => 'Biologi', 'strata' => 'S1', 'jurusan_id' => 1],
            ['nama' => 'Fisika', 'strata' => 'S1', 'jurusan_id' => 2],
            ['nama' => 'Geofisika', 'strata' => 'S1', 'jurusan_id' => 2],
            ['nama' => 'Ilmu Kelautan', 'strata' => 'S1', 'jurusan_id' => 3],
            ['nama' => 'Kimia', 'strata' => 'S1', 'jurusan_id' => 4],
            ['nama' => 'Kimia S2', 'strata' => 'S2', 'jurusan_id' => 4],
            ['nama' => 'Matematika', 'strata' => 'S1', 'jurusan_id' => 5],
            ['nama' => 'Statistika', 'strata' => 'S1', 'jurusan_id' => 5],
            ['nama' => 'Rekayasa Sistem Komputer', 'strata' => 'S1', 'jurusan_id' => 6],
            ['nama' => 'Sistem Informasi', 'strata' => 'S1', 'jurusan_id' => 7],
            
        ];

        Prodi::insert($prodis);

    }
}
