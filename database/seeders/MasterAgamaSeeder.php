<?php

namespace Database\Seeders;

use App\Models\MasterAgama;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class MasterAgamaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $masterAgamas = [
            ['agama' => 'Islam', 'kode' => 'IL'],
            ['agama' => 'Protestan', 'kode' => 'PT'],
            ['agama' => 'Katolik', 'kode' => 'KT'],
            ['agama' => 'Hindu', 'kode' => 'HD'],
            ['agama' => 'Buddha', 'kode' => 'BD'],
            ['agama' => 'Khonghucu', 'kode' => 'KH'],
            
        ];

        MasterAgama::insert($masterAgamas);
    }
}
