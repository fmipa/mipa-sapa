<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;
use PhpParser\Node\Stmt\Foreach_;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
    //     User::create([
    //         'name' => 'Super Admin',
    //         'username' => 'superadmin',
    //         'email' => 'superadmin@email.com',
    //         'email_verified_at' => now(),
    //         'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
    //         'remember_token' => Str::random(10),
    //     ]);
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);
        // $users = User::all();
        // foreach ($users as $key => $value) {
        //     $value->atta
        // }

        $this->call([
            // PermissionSeeder::class,
            // RoleSeeder::class,
            // Teamseeder::class,
            // JurusanSeeder::class,
            // ProdiSeeder::class,
            // UnitKerjaSeeder::class,

            // Userseeder::class,
            // UserTendikASNseeder::class,
            // UserTendiknonASNseeder::class,
            // UserDosenBiologiseeder::class,
            // UserDosenFisikaseeder::class,
            // UserDosenGeofisikaseeder::class,
            // UserDosenIlmuKelautanseeder::class,
            // UserDosenKimiaS1seeder::class,
            // UserDosenKimiaS2seeder::class,
            // UserDosenMatematikaseeder::class,
            // UserDosenResiskomseeder::class,
            // UserDosenSisfoseeder::class,
            // UserDosenStatistikaseeder::class,
            // PerbaikanPegawaiSeeder::class,

            // ExtensionSeeder::class,
            // JenisAksesSeeder::class,
            // KodeNaskahSeeder::class,
            // MasterAgamaSeeder::class,
            // KodeUnitSeeder::class,
            // KategoriBerkasSeeder::class,
            FolderSeeder::class,
            // BoxSeeder::class,
            // FolderStorageSeeder::class,

            // ChangeCreatordanUnitKerjaseeder::class,
            // ChangePermissionBerkasSeeder::class,
            AddFolderJurusanSeeder::class,
            AddFolderUmperSeeder::class,
            AddFolderAdminJurusanSeeder::class,
            AddFolderAkademikSeeder::class,
            AddFolderKemahasiswaanSeeder::class,
            AddFolderDosenTendikSeeder::class,

            AddFolderUnitKerjaSeeder::class,
        ]);
    }
}
