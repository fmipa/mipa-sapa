<?php

namespace Database\Seeders;

use App\Models\KategoriBerkas;
use App\Traits\Helper;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KategoriBerkasSeeder extends Seeder
{
    use Helper;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $kategoriiBerkass = [
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN MATEMATIKA TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN FISIKA TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN KIMIA TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN BIOLOGI TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN REKAYASA SISTEM KOMPUTER TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN ILMU KELAUTAN TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN SISTEM INFORMASI TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN MATEMATIKA TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN FISIKA TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN KIMIA TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN BIOLOGI TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN REKAYASA SISTEM KOMPUTER TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN ILMU KELAUTAN TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN SISTEM INFORMASI TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN MATEMATIKA TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN FISIKA TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN KIMIA TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN BIOLOGI TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN REKAYASA SISTEM KOMPUTER TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN ILMU KELAUTAN TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN JURUSAN SISTEM INFORMASI TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN TENDIK TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN TENDIK TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SKP DOSEN TENDIK TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'ARSIP DOSEN JURUSAN MATEMATIKA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'ARSIP DOSEN JURUSAN FISIKA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'ARSIP DOSEN JURUSAN KIMIA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'ARSIP DOSEN JURUSAN BIOLOGI',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'ARSIP DOSEN JURUSAN REKAYASA SISTEM KOMPUTER',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'ARSIP DOSEN JURUSAN ILMU KELAUTAN',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'ARSIP DOSEN JURUSAN SISTEM INFORMASI',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'ARSIP TENDIK',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK PANGKAT DOSEN JURUSAN MATEMATIKA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK PANGKAT DOSEN JURUSAN FISIKA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK PANGKAT DOSEN JURUSAN KIMIA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK PANGKAT DOSEN JURUSAN BIOLOGI',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK PANGKAT DOSEN JURUSAN REKAYASA SISTEM KOMPUTER',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK PANGKAT DOSEN JURUSAN ILMU KELAUTAN',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK PANGKAT DOSEN JURUSAN SISTEM INFORMASI',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK PANGKAT TENDIK',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK JABATAN FUNGSIONAL DOSEN JURUSAN MATEMATIKA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK JABATAN FUNGSIONAL DOSEN JURUSAN FISIKA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK JABATAN FUNGSIONAL DOSEN JURUSAN KIMIA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK JABATAN FUNGSIONAL DOSEN JURUSAN BIOLOGI',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK JABATAN FUNGSIONAL DOSEN JURUSAN REKAYASA SISTEM KOMPUTER',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK JABATAN FUNGSIONAL DOSEN JURUSAN ILMU KELAUTAN',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK JABATAN FUNGSIONAL DOSEN JURUSAN SISTEM INFORMASI',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK JABATAN FUNGSIONAL TENDIK',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SATYALANCANA DOSEN JURUSAN MATEMATIKA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SATYALANCANA DOSEN JURUSAN FISIKA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SATYALANCANA DOSEN JURUSAN KIMIA',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SATYALANCANA DOSEN JURUSAN BIOLOGI',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SATYALANCANA DOSEN JURUSAN REKAYASA SISTEM KOMPUTER',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SATYALANCANA DOSEN JURUSAN ILMU KELAUTAN',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SATYALANCANA DOSEN JURUSAN SISTEM INFORMASI',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SATYALANCANA TENDIK',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK FAKULTAS TAHUN 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK FAKULTAS TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK FAKULTAS TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK UNIVERSITAS TAHUN 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK UNIVERSITAS TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SK UNIVERSITAS TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN JANUARI 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN FEBRUARI 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN MARET 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN APRIL 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN MEI 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN JUNI 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN JULI 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN AGUSTUS 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN SEPTEMBER 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN OKTOBER 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN NOVEMBER 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN DESEMBER 2023',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN JANUARI 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN FEBRUARI 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN MARET 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN APRIL 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN MEI 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN JUNI 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN JULI 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN AGUSTUS 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN SEPTEMBER 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN OKTOBER 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN NOVEMBER 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'SURAT CUTI TAHUN DESEMBER 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'KP-4 TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'KP-4 TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'KP-4 TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'LAKIN FMIPA TAHUN 2022',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'LAKIN FMIPA TAHUN 2021',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        //     [
        //         'kategori' => 'LAKIN FMIPA TAHUN 2020',
        //         'kode' => $this->acronym(strtoupper($this->kategori)),
        //         'created_by' => 1
        //     ],
        // ];
        
        $kategoriiBerkass = [
            "SKP DOSEN JURUSAN MATEMATIKA TAHUN 2022",
            "SKP DOSEN JURUSAN FISIKA TAHUN 2022",
            "SKP DOSEN JURUSAN KIMIA TAHUN 2022",
            "SKP DOSEN JURUSAN BIOLOGI TAHUN 2022",
            "SKP DOSEN JURUSAN REKAYASA SISTEM KOMPUTER TAHUN 2022",
            "SKP DOSEN JURUSAN ILMU KELAUTAN TAHUN 2022",
            "SKP DOSEN JURUSAN SISTEM INFORMASI TAHUN 2022",
            "SKP DOSEN JURUSAN MATEMATIKA TAHUN 2021",
            "SKP DOSEN JURUSAN FISIKA TAHUN 2021",
            "SKP DOSEN JURUSAN KIMIA TAHUN 2021",
            "SKP DOSEN JURUSAN BIOLOGI TAHUN 2021",
            "SKP DOSEN JURUSAN REKAYASA SISTEM KOMPUTER TAHUN 2021",
            "SKP DOSEN JURUSAN ILMU KELAUTAN TAHUN 2021",
            "SKP DOSEN JURUSAN SISTEM INFORMASI TAHUN 2021",
            "SKP DOSEN JURUSAN MATEMATIKA TAHUN 2020",
            "SKP DOSEN JURUSAN FISIKA TAHUN 2020",
            "SKP DOSEN JURUSAN KIMIA TAHUN 2020",
            "SKP DOSEN JURUSAN BIOLOGI TAHUN 2020",
            "SKP DOSEN JURUSAN REKAYASA SISTEM KOMPUTER TAHUN 2020",
            "SKP DOSEN JURUSAN ILMU KELAUTAN TAHUN 2020",
            "SKP DOSEN JURUSAN SISTEM INFORMASI TAHUN 2020",
            "SKP DOSEN TENDIK TAHUN 2022",
            "SKP DOSEN TENDIK TAHUN 2021",
            "SKP DOSEN TENDIK TAHUN 2020",
            "ARSIP DOSEN JURUSAN MATEMATIKA",
            "ARSIP DOSEN JURUSAN FISIKA",
            "ARSIP DOSEN JURUSAN KIMIA",
            "ARSIP DOSEN JURUSAN BIOLOGI",
            "ARSIP DOSEN JURUSAN REKAYASA SISTEM KOMPUTER",
            "ARSIP DOSEN JURUSAN ILMU KELAUTAN",
            "ARSIP DOSEN JURUSAN SISTEM INFORMASI",
            "ARSIP TENDIK",
            "SK PANGKAT DOSEN JURUSAN MATEMATIKA",
            "SK PANGKAT DOSEN JURUSAN FISIKA",
            "SK PANGKAT DOSEN JURUSAN KIMIA",
            "SK PANGKAT DOSEN JURUSAN BIOLOGI",
            "SK PANGKAT DOSEN JURUSAN REKAYASA SISTEM KOMPUTER",
            "SK PANGKAT DOSEN JURUSAN ILMU KELAUTAN",
            "SK PANGKAT DOSEN JURUSAN SISTEM INFORMASI",
            "SK PANGKAT TENDIK",
            "SK JABATAN FUNGSIONAL DOSEN JURUSAN MATEMATIKA ",
            "SK JABATAN FUNGSIONAL DOSEN JURUSAN FISIKA ",
            "SK JABATAN FUNGSIONAL DOSEN JURUSAN KIMIA",
            "SK JABATAN FUNGSIONAL DOSEN JURUSAN BIOLOGI",
            "SK JABATAN FUNGSIONAL DOSEN JURUSAN REKAYASA SISTEM KOMPUTER",
            "SK JABATAN FUNGSIONAL DOSEN JURUSAN ILMU KELAUTAN",
            "SK JABATAN FUNGSIONAL DOSEN JURUSAN SISTEM INFORMASI",
            "SK JABATAN FUNGSIONAL TENDIK",
            "SATYALANCANA DOSEN JURUSAN MATEMATIKA",
            "SATYALANCANA DOSEN JURUSAN FISIKA",
            "SATYALANCANA DOSEN JURUSAN KIMIA",
            "SATYALANCANA DOSEN JURUSAN BIOLOGI",
            "SATYALANCANA DOSEN JURUSAN REKAYASA SISTEM KOMPUTER",
            "SATYALANCANA DOSEN JURUSAN ILMU KELAUTAN",
            "SATYALANCANA DOSEN JURUSAN SISTEM INFORMASI",
            "SATYALANCANA TENDIK ",
            "SK FAKULTAS TAHUN 2023",
            "SK FAKULTAS TAHUN 2022",
            "SK FAKULTAS TAHUN 2021",
            "SK UNIVERSITAS TAHUN 2023",
            "SK UNIVERSITAS TAHUN 2022",
            "SK UNIVERSITAS TAHUN 2021",
            "SURAT CUTI TAHUN JANUARI 2023",
            "SURAT CUTI TAHUN FEBRUARI 2023",
            "SURAT CUTI TAHUN MARET 2023",
            "SURAT CUTI TAHUN APRIL 2023",
            "SURAT CUTI TAHUN MEI 2023",
            "SURAT CUTI TAHUN JUNI 2023",
            "SURAT CUTI TAHUN JULI 2023",
            "SURAT CUTI TAHUN AGUSTUS 2023",
            "SURAT CUTI TAHUN SEPTEMBER 2023",
            "SURAT CUTI TAHUN OKTOBER 2023",
            "SURAT CUTI TAHUN NOVEMBER 2023",
            "SURAT CUTI TAHUN DESEMBER 2023",
            "SURAT CUTI TAHUN JANUARI 2022",
            "SURAT CUTI TAHUN FEBRUARI 2022",
            "SURAT CUTI TAHUN MARET 2022",
            "SURAT CUTI TAHUN APRIL 2022",
            "SURAT CUTI TAHUN MEI 2022",
            "SURAT CUTI TAHUN JUNI 2022",
            "SURAT CUTI TAHUN JULI 2022",
            "SURAT CUTI TAHUN AGUSTUS 2022",
            "SURAT CUTI TAHUN SEPTEMBER 2022",
            "SURAT CUTI TAHUN OKTOBER 2022",
            "SURAT CUTI TAHUN NOVEMBER 2022",
            "SURAT CUTI TAHUN DESEMBER 2022",
            "KP-4 TAHUN 2022",
            "KP-4 TAHUN 2021",
            "KP-4 TAHUN 2020",
            "LAKIN FMIPA TAHUN 2022",
            "LAKIN FMIPA TAHUN 2021",
            "LAKIN FMIPA TAHUN 2020",
        ];
        $dataKategori = [];
        foreach ($kategoriiBerkass as $key => $value) {
            $dataKategori[$key]['kategori'] = $value; 
            $dataKategori[$key]['kode'] =  $this->acronym(strtoupper($value)); 
            $dataKategori[$key]['created_by'] = 1; 
        }
        KategoriBerkas::insert($dataKategori);
    }
}
