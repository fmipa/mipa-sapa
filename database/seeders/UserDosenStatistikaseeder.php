<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenStatistikaseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $dadan = Pegawai::firstOrCreate([
            'firstname'         => 'Dadan', 
            'middlename'        => 'Kusnandar', 
            'gelar_depan'       => 'Ir.',
            'gelar_belakang'    => 'M.Sc., Ph.D.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('195907081987031014'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $dadan->user()->firstOrCreate([
            'name' => strtoupper('Ir. Dadan Kusnandar, M.Sc., Ph.D.'),
            'username' => strtolower('195907081987031014'),
            'email' => strtolower('dadankusnandar@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $dadan->user->activation()->create(['user_id' => $dadan->user->id, 'kode' => Str::random(20)]);
        $dadan->user->assignRole('dosen');

        $neva = Pegawai::firstOrCreate([
            'firstname'         => 'Neva', 
            'middlename'        => 'Satyahadewi', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('198212042005012001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $neva->user()->firstOrCreate([
            'name' => strtoupper('Neva Satyahadewi, S.Si., M.Sc.'),
            'username' => strtolower('198212042005012001'),
            'email' => strtolower('nevasatyahadewi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $neva->user->activation()->create(['user_id' => $neva->user->id, 'kode' => Str::random(20)]);
        $neva->user->assignRole('dosen');

        $naomi = Pegawai::firstOrCreate([
            'firstname'         => 'Naomi', 
            'middlename'        => 'Nessyana', 
            'lastname'          => 'Debataraja', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('198811232012122004'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $naomi->user()->firstOrCreate([
            'name' => strtoupper('Naomi Nessyana Debataraja, S.Si., M.Si.'),
            'username' => strtolower('198811232012122004'),
            'email' => strtolower('naominessyanadebataraja@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $naomi->user->activation()->create(['user_id' => $naomi->user->id, 'kode' => Str::random(20)]);
        $naomi->user->assignRole(['sekretaris-jurusan', 'dosen']);

        $setyo = Pegawai::firstOrCreate([
            'firstname'         => 'Setyo', 
            'middlename'        => 'Wira', 
            'lastname'          => 'Rizki', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('198410312015042003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $setyo->user()->firstOrCreate([
            'name' => strtoupper('Setyo Wira Rizki, S.Si., M.Sc.'),
            'username' => strtolower('198410312015042003'),
            'email' => strtolower('setyowirarizki@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $setyo->user->activation()->create(['user_id' => $setyo->user->id, 'kode' => Str::random(20)]);
        $setyo->user->assignRole('dosen');

        $evy = Pegawai::firstOrCreate([
            'firstname'         => 'Evy', 
            'middlename'        => 'Sulistianingsih',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('198502172008122006'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $evy->user()->firstOrCreate([
            'name' => strtoupper('Dr. Evy Sulistianingsih, S.Si., M.Sc.'),
            'username' => strtolower('198502172008122006'),
            'email' => strtolower('evysulistianingsih@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $evy->user->activation()->create(['user_id' => $evy->user->id, 'kode' => Str::random(20)]);
        $evy->user->assignRole('dosen');

        $shantika = Pegawai::firstOrCreate([
            'firstname'         => 'Shantika', 
            'middlename'        => 'Martha',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('198403082008122003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $shantika->user()->firstOrCreate([
            'name' => strtoupper('Shantika Martha, S.Si., M.Si.'),
            'username' => strtolower('198403082008122003'),
            'email' => strtolower('shantikamartha@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $shantika->user->activation()->create(['user_id' => $shantika->user->id, 'kode' => Str::random(20)]);
        $shantika->user->assignRole('dosen');

        $nurfitri = Pegawai::firstOrCreate([
            'firstname'         => 'Nurfitri', 
            'middlename'        => "Imro'ah", 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('198907182019032021'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $nurfitri->user()->firstOrCreate([
            'name' => strtoupper("Nurfitri Imro'ah, S.Si., M.Si"),
            'username' => strtolower('198907182019032021'),
            'email' => strtolower('nurfitriimroah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $nurfitri->user->activation()->create(['user_id' => $nurfitri->user->id, 'kode' => Str::random(20)]);
        $nurfitri->user->assignRole('dosen');

        $hendra = Pegawai::firstOrCreate([
            'firstname'         => 'Hendra', 
            'middlename'        => 'Perdana',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('198810102019031020'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $hendra->user()->firstOrCreate([
            'name' => strtoupper('Hendra Perdana, S.Si., M.Sc.'),
            'username' => strtolower('198810102019031020'),
            'email' => strtolower('hendraperdana@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $hendra->user->activation()->create(['user_id' => $hendra->user->id, 'kode' => Str::random(20)]);
        $hendra->user->assignRole('dosen');

        $wirda = Pegawai::firstOrCreate([
            'firstname'         => 'Wirda', 
            'middlename'        => 'Andani',
            'gelar_belakang'    => 'M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 8, 
            'nomor_unik'        => strtolower('199411152022032016'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $wirda->user()->firstOrCreate([
            'name' => strtoupper('Wirda Andani, M.Si.'),
            'username' => strtolower('199411152022032016'),
            'email' => strtolower('wirdaandani@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $wirda->user->activation()->create(['user_id' => $wirda->user->id, 'kode' => Str::random(20)]);
        $wirda->user->assignRole('dosen');

    }
}
