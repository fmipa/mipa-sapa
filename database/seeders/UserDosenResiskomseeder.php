<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenResiskomseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $cucu = Pegawai::firstOrCreate([
            'firstname'         => 'Cucu', 
            'middlename'        => 'Suhery', 
            'gelar_depan'       => 'Drs.',
            'gelar_belakang'    => 'MA.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9, 
            'nomor_unik'        => strtolower('196108291989031002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $cucu->user()->firstOrCreate([
            'name' => strtoupper('Drs. Cucu Suhery, MA.'),
            'username' => strtolower('196108291989031002'),
            'email' => strtolower('cucusuhery@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $cucu->user->activation()->create(['user_id' => $cucu->user->id, 'kode' => Str::random(20)]);
        $cucu->user->assignRole(['wakil-dekan', 'dosen']);

        $dedi = Pegawai::firstOrCreate([
            'firstname'         => 'Dedi', 
            'middlename'        => 'Triyanto', 
            'gelar_belakang'    => 'ST., MT.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9, 
            'nomor_unik'        => strtolower('198112242009121003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $dedi->user()->firstOrCreate([
            'name' => strtoupper('Dedi Triyanto, ST., MT.'),
            'username' => strtolower('198112242009121003'),
            'email' => strtolower('deditriyanto@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $dedi->user->activation()->create(['user_id' => $dedi->user->id, 'kode' => Str::random(20)]);
        $dedi->user->assignRole('dosen');

        $tedy = Pegawai::firstOrCreate([
            'firstname'         => 'Tedy', 
            'middlename'        => 'Rismawan', 
            'gelar_belakang'    => 'S.Kom., M.Cs.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9, 
            'nomor_unik'        => strtolower('198609222014041002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $tedy->user()->firstOrCreate([
            'name' => strtoupper('Tedy Rismawan, S.Kom., M.Cs.'),
            'username' => strtolower('198609222014041002'),
            'email' => strtolower('tedyrismawan@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $tedy->user->activation()->create(['user_id' => $tedy->user->id, 'kode' => Str::random(20)]);
        $tedy->user->assignRole('dosen');

        $dwiMarisa = Pegawai::firstOrCreate([
            'firstname'         => 'Dwi', 
            'middlename'        => 'Marisa', 
            'lastname'          => 'Midyanti', 
            'gelar_belakang'    => 'ST., M.Cs.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 9, 
            'nomor_unik'        => strtolower('198003192015042001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $dwiMarisa->user()->firstOrCreate([
            'name' => strtoupper('Dwi Marisa Midyanti, ST., M.Cs.'),
            'username' => strtolower('198003192015042001'),
            'email' => strtolower('dwimarisamidyanti@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $dwiMarisa->user->activation()->create(['user_id' => $dwiMarisa->user->id, 'kode' => Str::random(20)]);
        $dwiMarisa->user->assignRole('dosen');

        $syamsul = Pegawai::firstOrCreate([
            'firstname'         => 'Syamsul', 
            'middlename'        => 'Bahri',
            'gelar_belakang'    => 'S.Kom., M.Cs.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9, 
            'nomor_unik'        => strtolower('198802272015041001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $syamsul->user()->firstOrCreate([
            'name' => strtoupper('Syamsul Bahri, S.Kom., M.Cs.'),
            'username' => strtolower('198802272015041001'),
            'email' => strtolower('syamsulbahri@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $syamsul->user->activation()->create(['user_id' => $syamsul->user->id, 'kode' => Str::random(20)]);
        $syamsul->user->assignRole('dosen');

        $ikhwan = Pegawai::firstOrCreate([
            'firstname'         => 'Ikhwan', 
            'middlename'        => 'Ruslianto',
            'gelar_belakang'    => 'S.Kom., M.Cs.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9, 
            'nomor_unik'        => strtolower('198607012014041001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ikhwan->user()->firstOrCreate([
            'name' => strtoupper('Ikhwan Ruslianto, S.Kom., M.Cs.'),
            'username' => strtolower('198607012014041001'),
            'email' => strtolower('ikhwanruslianto@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ikhwan->user->activation()->create(['user_id' => $ikhwan->user->id, 'kode' => Str::random(20)]);
        $ikhwan->user->assignRole(['ketua-jurusan', 'ketua-prodi', 'dosen']);

        $rahmi = Pegawai::firstOrCreate([
            'firstname'         => 'Rahmi', 
            'middlename'        => 'Hidayati', 
            'gelar_belakang'    => 'S.Kom., M.Cs.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('198607202015042001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $rahmi->user()->firstOrCreate([
            'name' => strtoupper('Rahmi Hidayati, S.Kom., M.Cs.'),
            'username' => strtolower('198607202015042001'),
            'email' => strtolower('rahmihidayati@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $rahmi->user->activation()->create(['user_id' => $rahmi->user->id, 'kode' => Str::random(20)]);
        $rahmi->user->assignRole(['sekretaris-jurusan', 'dosen']);

        $sampe = Pegawai::firstOrCreate([
            'firstname'         => 'Sampe', 
            'middlename'        => 'Hotlan', 
            'lastname'          => 'Sitorus', 
            'gelar_belakang'    => 'S.Si., M.Kom.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('197108092006041001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $sampe->user()->firstOrCreate([
            'name' => strtoupper('Sampe Hotlan Sitorus, S.Si., M.Kom.'),
            'username' => strtolower('197108092006041001'),
            'email' => strtolower('sampehotlansitorus@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $sampe->user->activation()->create(['user_id' => $sampe->user->id, 'kode' => Str::random(20)]);
        $sampe->user->assignRole('dosen');

        $irma = Pegawai::firstOrCreate([
            'firstname'         => 'Irma', 
            'middlename'        => 'Nirmala', 
            'gelar_belakang'    => 'ST., MT.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('198404052019032015'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $irma->user()->firstOrCreate([
            'name' => strtoupper('Irma Nirmala, ST., MT.'),
            'username' => strtolower('198404052019032015'),
            'email' => strtolower('irmanirmala@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $irma->user->activation()->create(['user_id' => $irma->user->id, 'kode' => Str::random(20)]);
        $irma->user->assignRole('dosen');

        $uray = Pegawai::firstOrCreate([
            'firstname'         => 'Uray', 
            'middlename'        => 'Ristian', 
            'gelar_belakang'    => 'S.Kom., M.Kom.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('199012012019031017'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $uray->user()->firstOrCreate([
            'name' => strtoupper('Uray Ristian, S.Kom., M.Kom.'),
            'username' => strtolower('199012012019031017'),
            'email' => strtolower('urayristian@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $uray->user->activation()->create(['user_id' => $uray->user->id, 'kode' => Str::random(20)]);
        $uray->user->assignRole('dosen');

        $suhardi = Pegawai::firstOrCreate([
            'firstname'         => 'Suhardi', 
            'gelar_belakang'    => 'ST., M.Eng.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('198606182020121006'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $suhardi->user()->firstOrCreate([
            'name' => strtoupper('Suhardi, ST., M.Eng.'),
            'username' => strtolower('198606182020121006'),
            'email' => strtolower('suhardi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $suhardi->user->activation()->create(['user_id' => $suhardi->user->id, 'kode' => Str::random(20)]);
        $suhardi->user->assignRole('dosen');

        $hirzen = Pegawai::firstOrCreate([
            'firstname'         => 'Hirzen', 
            'middlename'        => 'Hasfani', 
            'gelar_belakang'    => 'M.Cs.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('199305032022031003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $hirzen->user()->firstOrCreate([
            'name' => strtoupper('Hirzen Hasfani, M.Cs.'),
            'username' => strtolower('199305032022031003'),
            'email' => strtolower('hirzenhasfani@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $hirzen->user->activation()->create(['user_id' => $hirzen->user->id, 'kode' => Str::random(20)]);
        $hirzen->user->assignRole('dosen');

        $kartika = Pegawai::firstOrCreate([
            'firstname'         => 'Kartika', 
            'middlename'        => 'Sari', 
            'gelar_belakang'    => 'M.Cs.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('199206162022032014'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $kartika->user()->firstOrCreate([
            'name' => strtoupper('Kartika Sari, M.Cs.'),
            'username' => strtolower('199206162022032014'),
            'email' => strtolower('kartikasari@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $kartika->user->activation()->create(['user_id' => $kartika->user->id, 'kode' => Str::random(20)]);
        $kartika->user->assignRole('dosen');

        $kasliono = Pegawai::firstOrCreate([
            'firstname'         => 'Kasliono', 
            'gelar_belakang'    => 'S.Mat., M.Cs.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('199306202022031005'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $kasliono->user()->firstOrCreate([
            'name' => strtoupper('Kasliono, S.Mat., M.Cs.'),
            'username' => strtolower('199306202022031005'),
            'email' => strtolower('kasliono@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $kasliono->user->activation()->create(['user_id' => $kasliono->user->id, 'kode' => Str::random(20)]);
        $kasliono->user->assignRole('dosen');

        $hafiz = Pegawai::firstOrCreate([
            'firstname'         => 'Hafiz', 
            'middlename'        => 'Muhardi', 
            'gelar_belakang'    => 'S.T., M.Kom.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PPPK', 
            'prodi_id'          => 9,
            'nomor_unik'        => strtolower('199007232023211017'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $hafiz->user()->firstOrCreate([
            'name' => strtoupper('Hafiz Muhardi, S.T., M.Kom.'),
            'username' => strtolower('199007232023211017'),
            'email' => strtolower('harizmuhardi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $hafiz->user->activation()->create(['user_id' => $hafiz->user->id, 'kode' => Str::random(20)]);
        $hafiz->user->assignRole('dosen');
    }
}
