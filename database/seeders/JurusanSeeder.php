<?php

namespace Database\Seeders;

use App\Models\Jurusan;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class JurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $jurusans = [
            ['nama' => 'Biologi'],
            ['nama' => 'Fisika'],
            ['nama' => 'Ilmu Kelautan'],
            ['nama' => 'Kimia'],
            ['nama' => 'Matematika'],
            ['nama' => 'Rekayasa Sistem Komputer'],
            ['nama' => 'Sistem Informasi'],
            
        ];

        Jurusan::insert($jurusans);
    }
}
