<?php

namespace Database\Seeders;

use App\Models\Box;
use App\Models\UnitKerja;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BoxSeeder extends Seeder
{
    use EncryptId;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $boxes = [
            [
                'nama'          => 'SK' , 
                'nomor'         => 1, 
                'lokasi'        => 'Ruang Arsiparis', 
                'tahun'         => '2023', 
                'unit_kerja_id' => UnitKerja::whereNama('KEPEGAWAIAN')->value('id'), 
                'unique_id'     =>  $this->encrypt((string) '1'.date('ymdHis'), 'qrcode-box'), 
                'created_by'    => 1,
            ],
            [
                'nama'          => 'Surat Masuk' , 
                'nomor'         => 2, 
                'lokasi'        => 'Ruang Arsiparis', 
                'tahun'         => '2023', 
                'unit_kerja_id' => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'), 
                'unique_id'     =>  $this->encrypt((string) '2'.date('ymdHis'), 'qrcode-box'), 
                'created_by'    => 1,
            ],
            [
                'nama'          => 'Surat Keluar' , 
                'nomor'         => 3, 
                'lokasi'        => 'Ruang Arsiparis', 
                'tahun'         => '2023', 
                'unit_kerja_id' => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'), 
                'unique_id'     =>  $this->encrypt((string) '3'.date('ymdHis'), 'qrcode-box'), 
                'created_by'    => 1,
            ],
        ];

        Box::insert($boxes);
    }
}
