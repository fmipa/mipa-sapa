<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Models\Prodi;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Seeder;

class AddFolderDosenTendikSeeder extends Seeder
{
    use EncryptId;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $unitKejaIdDosen    = 2;
        $unitKejaIdTendik   = 10;
        // Dosen
        Folder::create(['nama' => 'PNS', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => $unitKejaIdDosen, 'prodi_id' => null, 'slug' => 'pns', 'created_by' => 1]);
        Folder::create(['nama' => 'PPPK', 'urutan' => 2, 'parent_id' => 0, 'unit_kerja_id' => $unitKejaIdDosen, 'prodi_id' => null, 'slug' => 'pppk', 'created_by' => 1]);
        // Tendik
        Folder::create(['nama' => 'PNS', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => $unitKejaIdTendik, 'prodi_id' => null, 'slug' => 'pns', 'created_by' => 1]);
        Folder::create(['nama' => 'PPPK', 'urutan' => 2, 'parent_id' => 0, 'unit_kerja_id' => $unitKejaIdTendik, 'prodi_id' => null, 'slug' => 'pppk', 'created_by' => 1]);
        Folder::create(['nama' => 'KONTRAK', 'urutan' => 3, 'parent_id' => 0, 'unit_kerja_id' => $unitKejaIdTendik, 'prodi_id' => null, 'slug' => 'kontrak', 'created_by' => 1]);
        Folder::create(['nama' => 'PHL', 'urutan' => 4, 'parent_id' => 0, 'unit_kerja_id' => $unitKejaIdTendik, 'prodi_id' => null, 'slug' => 'phl', 'created_by' => 1]);
        
    }
}
