<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Models\Prodi;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AddFolderAdminJurusanSeeder extends Seeder
{
    use EncryptId;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $prodis         = Prodi::all();
        $operator       = [171, 172, 172, 173, 174, 174, 176, 176, 177, 178];
        $parentProdi    = [155, 156, 157, 160, 161, 162, 158, 159, 163, 164];
        $unitKejaId     = 3;

        foreach ($prodis as $key => $value) {
            $Psempro = Folder::create(['nama' => 'Seminar Proposal', 'urutan' => 1, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'seminar-proposal', 'created_by' => $operator[$key]]);

            $Psemhas = Folder::create(['nama' => 'Seminar Hasil', 'urutan' => 2, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'seminar-hasil', 'created_by' => $operator[$key]]);
            $Psidang = Folder::create(['nama' => 'Sidang', 'urutan' => 3, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'sidang', 'created_by' => $operator[$key]]);
            $PKP = Folder::create(['nama' => 'Kerja Praktik (KP)', 'urutan' => 4, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'kerja-praktik-(kp)', 'created_by' => $operator[$key]]);
            $Ppenelitian = Folder::create(['nama' => 'Penelitian', 'urutan' => 5, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'penelitian', 'created_by' => $operator[$key]]);
            $Ppengujian = Folder::create(['nama' => 'Pengujian Sampel Penelitian', 'urutan' => 6, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'pengujian-sampel-penelitian', 'created_by' => $operator[$key]]);
            $Pmbkm = Folder::create(['nama' => 'Magang MBKM', 'urutan' => 7, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'magang-mbkm', 'created_by' => $operator[$key]]);
            $Pkulap = Folder::create(['nama' => 'Kuliah Lapangan', 'urutan' => 8, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'kuliah-lapangan', 'created_by' => $operator[$key]]);
            $Pdpk = Folder::create(['nama' => 'Dosen Pembimbing Kerja Mandiri Terpantau', 'urutan' => 9, 'parent_id' => $parentProdi[$key], 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'dosen-pembimbing-kerja-mandiri-terpantau', 'created_by' => $operator[$key]]);
            
            // Seminar Proposal
            $PUnSempro = Folder::create(['nama' => 'Undangan Seminar Proposal', 'urutan' => 1, 'parent_id' => $Psempro->id, 'parent_utama' => $Psempro->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'undangan-seminar-proposal', 'created_by' => $operator[$key]]);
            $PSKSempro = Folder::create(['nama' => 'SK Pelaksanaan Seminar Proposal', 'urutan' => 2, 'parent_id' => $Psempro->id, 'parent_utama' => $Psempro->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'sk-pelaksanaan-seminar-proposal', 'created_by' => $operator[$key]]);
    
            // Seminar hasil
            $PUnSemhas = Folder::create(['nama' => 'Undangan Seminar Hasil', 'urutan' => 1, 'parent_id' => $Psemhas->id, 'parent_utama' => $Psemhas->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'undangan-seminar-hasil', 'created_by' => $operator[$key]]);
            $PSKSemhas = Folder::create(['nama' => 'SK Pelaksanaan Seminar Hasil', 'urutan' => 2, 'parent_id' => $Psemhas->id, 'parent_utama' => $Psemhas->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'sk-pelaksanaan-seminar-hasil', 'created_by' => $operator[$key]]);
    
            // Sidang
            $PUnSidang= Folder::create(['nama' => 'Undangan Sidang', 'urutan' => 1, 'parent_id' => $Psidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'undangan-sidang', 'created_by' => $operator[$key]]);
            $PPemSidang= Folder::create(['nama' => 'SK Pembimbing Tugas Akhir', 'urutan' => 2, 'parent_id' => $Psidang->id, 'parent_utama' => $Psidang->id, 'prodi_id' => $value->id, 'slug' => 'sk-pembimbing-tugas-akhir', 'created_by' => $operator[$key]]);
            $PPengSidang= Folder::create(['nama' => 'SK Penguji Tugas Akhir', 'urutan' => 3, 'parent_id' => $Psidang->id, 'parent_utama' => $Psidang->id, 'prodi_id' => $value->id, 'slug' => 'sk-penguji-tugas-akhir', 'created_by' => $operator[$key]]);
    
            // Kerja Praktik (KP)
            $PIzinKP = Folder::create(['nama' => 'Permohonan Izin Kerja Praktik Mahasiswa', 'urutan' => 1, 'parent_id' => $PKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'permohonan-izin-kerja-praktik-mahasiswa', 'created_by' => $operator[$key]]);
            $PSuratKP = Folder::create(['nama' => 'Surat Tugas KP', 'urutan' => 2, 'parent_id' => $PKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'surat-tugas-kp', 'created_by' => $operator[$key]]);
            $PDataKP = Folder::create(['nama' => 'Permohonan Data Kerja Praktik', 'urutan' => 3, 'parent_id' => $PKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'permohonan-data-kerja-praktik', 'created_by' => $operator[$key]]);
            $PPembimbingKP = Folder::create(['nama' => 'SK Dosen Pembimbing Kerja Praktik', 'urutan' => 4, 'parent_id' => $PKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'sk-dosen-pembimbing-kerja-praktik', 'created_by' => $operator[$key]]);
            // Penelitian
            $PDataPenelitian = Folder::create(['nama' => 'Permintaan Data Penelitian Tugas Akhir', 'urutan' => 1, 'parent_id' => $Ppenelitian->id, 'parent_utama' => $Ppenelitian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'permintaan-data-penelitian-tugas-akhir', 'created_by' => $operator[$key]]);
            // Pengujian Sampel Penelitian
            $PSampelPenelitian = Folder::create(['nama' => 'Pengujian Sampel Penelitian Tugas Akhir', 'urutan' => 1, 'parent_id' => $Ppengujian->id, 'parent_utama' => $Ppengujian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'pengujian-sampel-penelitian-tugas-akhir', 'created_by' => $operator[$key]]);
            // Magang MBKM
            $PIzinMBKM = Folder::create(['nama' => 'Permohonan Izin Magang MBKM', 'urutan' => 1, 'parent_id' => $Pmbkm->id, 'parent_utama' => $Pmbkm->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'permohonan-izin-magang-mbkm', 'created_by' => $operator[$key]]);
            // Kuliah Lapangan
            $PPendampingLapangan = Folder::create(['nama' => 'SK Dosen Pendamping Kuliah Lapangan', 'urutan' => 1, 'parent_id' => $Pkulap->id, 'parent_utama' => $Pkulap->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'sk-dosen-pendamping-kuliah-lapangan', 'created_by' => $operator[$key]]);
            // Dosen Pembimbing Kerja Mandiri Terpantau
            $PPembimbingMandiri = Folder::create(['nama' => 'SK Dosen Pembimbing Kerja Mandiri Terpantau', 'urutan' => 1, 'parent_id' => $Pdpk->id, 'parent_utama' => $Pdpk->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'sk-dosen-pembimbing-kerja-mandiri-terpantau', 'created_by' => $operator[$key]]);
    
            $tahun2 = [
                // Undangan Seminar Proposal
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PUnSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PUnSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PUnSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PUnSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PUnSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // SK Pelaksanaan Seminar Proposal
                ['nama' => 'Tahun 2018/2019', 'urutan' => 1, 'parent_id' => $PSKSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2018/2019', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2019/2020', 'urutan' => 2, 'parent_id' => $PSKSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2019/2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2020/2021', 'urutan' => 3, 'parent_id' => $PSKSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020/2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021/2022', 'urutan' => 4, 'parent_id' => $PSKSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021/2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022/2023', 'urutan' => 5, 'parent_id' => $PSKSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022/2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023/2024', 'urutan' => 6, 'parent_id' => $PSKSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023/2024', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024/2025', 'urutan' => 7, 'parent_id' => $PSKSempro->id, 'parent_utama' => $Psempro->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024/2025', 'created_by' => $operator[$key]],
    
                // Undangan Seminar Hasil
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PUnSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PUnSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PUnSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PUnSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PUnSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // SK Pelaksanaan Seminar Hasil
                ['nama' => 'Tahun 2018/2019', 'urutan' => 1, 'parent_id' => $PSKSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2018/2019', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2019/2020', 'urutan' => 2, 'parent_id' => $PSKSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2019/2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2020/2021', 'urutan' => 3, 'parent_id' => $PSKSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020/2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021/2022', 'urutan' => 4, 'parent_id' => $PSKSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021/2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022/2023', 'urutan' => 5, 'parent_id' => $PSKSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022/2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023/2024', 'urutan' => 6, 'parent_id' => $PSKSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023/2024', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024/2025', 'urutan' => 7, 'parent_id' => $PSKSemhas->id, 'parent_utama' => $Psemhas->id,  'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024/2025', 'created_by' => $operator[$key]],
    
                // Undangan Sidang
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PUnSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PUnSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PUnSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PUnSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PUnSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // SK Pembimbing Tugas Akhir
                ['nama' => 'Tahun 2018/2019', 'urutan' => 1, 'parent_id' => $PPemSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2018/2019', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2019/2020', 'urutan' => 2, 'parent_id' => $PPemSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2019/2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2020/2021', 'urutan' => 3, 'parent_id' => $PPemSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020/2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021/2022', 'urutan' => 4, 'parent_id' => $PPemSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021/2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022/2023', 'urutan' => 5, 'parent_id' => $PPemSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022/2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023/2024', 'urutan' => 6, 'parent_id' => $PPemSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023/2024', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024/2025', 'urutan' => 7, 'parent_id' => $PPemSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024/2025', 'created_by' => $operator[$key]],
    
                // SK Penguji Tugas Akhir
                ['nama' => 'Tahun 2018/2019', 'urutan' => 1, 'parent_id' => $PPengSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2018/2019', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2019/2020', 'urutan' => 2, 'parent_id' => $PPengSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2019/2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2020/2021', 'urutan' => 3, 'parent_id' => $PPengSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020/2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021/2022', 'urutan' => 4, 'parent_id' => $PPengSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021/2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022/2023', 'urutan' => 5, 'parent_id' => $PPengSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022/2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023/2024', 'urutan' => 6, 'parent_id' => $PPengSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023/2024', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024/2025', 'urutan' => 7, 'parent_id' => $PPengSidang->id, 'parent_utama' => $Psidang->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024/2025', 'created_by' => $operator[$key]],
    
                // Permohonan Izin Kerja Praktik Mahasiswa
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PIzinKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PIzinKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PIzinKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PIzinKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PIzinKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // Surat Tugas KP
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PSuratKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PSuratKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PSuratKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PSuratKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PSuratKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // Permohonan Data Kerja Praktik
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PDataKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PDataKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PDataKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PDataKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PDataKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                 // SK Dosen Pembimbing Kerja Praktik
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PPembimbingKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PPembimbingKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PPembimbingKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PPembimbingKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PPembimbingKP->id, 'parent_utama' => $PKP->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // Permintaan Data Penelitian Tugas Akhir
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PDataPenelitian->id, 'parent_utama' => $Ppenelitian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PDataPenelitian->id, 'parent_utama' => $Ppenelitian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PDataPenelitian->id, 'parent_utama' => $Ppenelitian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PDataPenelitian->id, 'parent_utama' => $Ppenelitian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PDataPenelitian->id, 'parent_utama' => $Ppenelitian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // Pengujian Sampel Penelitian Tugas Akhir
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PSampelPenelitian->id, 'parent_utama' => $Ppengujian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PSampelPenelitian->id, 'parent_utama' => $Ppengujian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PSampelPenelitian->id, 'parent_utama' => $Ppengujian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PSampelPenelitian->id, 'parent_utama' => $Ppengujian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PSampelPenelitian->id, 'parent_utama' => $Ppengujian->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // Permohonan Izin Magang MBKM
                ['nama' => 'Tahun 2020', 'urutan' => 1, 'parent_id' => $PIzinMBKM->id, 'parent_utama' => $Pmbkm->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => $PIzinMBKM->id, 'parent_utama' => $Pmbkm->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022', 'urutan' => 3, 'parent_id' => $PIzinMBKM->id, 'parent_utama' => $Pmbkm->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023', 'urutan' => 4, 'parent_id' => $PIzinMBKM->id, 'parent_utama' => $Pmbkm->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024', 'urutan' => 5, 'parent_id' => $PIzinMBKM->id, 'parent_utama' => $Pmbkm->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024', 'created_by' => $operator[$key]],
    
                // SK Dosen Pendamping Kuliah Lapangan
                ['nama' => 'Tahun 2018/2019', 'urutan' => 1, 'parent_id' => $PPendampingLapangan->id, 'parent_utama' => $Pkulap->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2018/2019', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2019/2020', 'urutan' => 2, 'parent_id' => $PPendampingLapangan->id, 'parent_utama' => $Pkulap->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2019/2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2020/2021', 'urutan' => 3, 'parent_id' => $PPendampingLapangan->id, 'parent_utama' => $Pkulap->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020/2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021/2022', 'urutan' => 4, 'parent_id' => $PPendampingLapangan->id, 'parent_utama' => $Pkulap->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021/2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022/2023', 'urutan' => 5, 'parent_id' => $PPendampingLapangan->id, 'parent_utama' => $Pkulap->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022/2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023/2024', 'urutan' => 6, 'parent_id' => $PPendampingLapangan->id, 'parent_utama' => $Pkulap->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023/2024', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024/2025', 'urutan' => 7, 'parent_id' => $PPendampingLapangan->id, 'parent_utama' => $Pkulap->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024/2025', 'created_by' => $operator[$key]],
    
                // SK Dosen Pembimbing Kerja Mandiri Terpantau
                ['nama' => 'Tahun 2018/2019', 'urutan' => 1, 'parent_id' => $PPembimbingMandiri->id, 'parent_utama' => $Pdpk->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2018/2019', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2019/2020', 'urutan' => 2, 'parent_id' => $PPembimbingMandiri->id, 'parent_utama' => $Pdpk->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2019/2020', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2020/2021', 'urutan' => 3, 'parent_id' => $PPembimbingMandiri->id, 'parent_utama' => $Pdpk->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2020/2021', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2021/2022', 'urutan' => 4, 'parent_id' => $PPembimbingMandiri->id, 'parent_utama' => $Pdpk->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2021/2022', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2022/2023', 'urutan' => 5, 'parent_id' => $PPembimbingMandiri->id, 'parent_utama' => $Pdpk->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2022/2023', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2023/2024', 'urutan' => 6, 'parent_id' => $PPembimbingMandiri->id, 'parent_utama' => $Pdpk->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2023/2024', 'created_by' => $operator[$key]],
                ['nama' => 'Tahun 2024/2025', 'urutan' => 7, 'parent_id' => $PPembimbingMandiri->id, 'parent_utama' => $Pdpk->id, 'unit_kerja_id' => $unitKejaId, 'prodi_id' => $value->id, 'slug' => 'tahun-2024/2025', 'created_by' => $operator[$key]],
            ];
    
            foreach ($tahun2 as $tahun) {
                Folder::create($tahun);
            }
        }
    }
}
