<?php

namespace Database\Seeders;

use App\Models\BerkasPegawai;
use App\Models\Box;
use App\Models\Document;
use App\Models\File;
use App\Models\Folder;
use App\Models\JenisAkses;
use App\Models\KategoriBerkas;
use App\Models\SK;
use App\Models\SuratKeluar;
use App\Models\SuratMasuk;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Role;

class ChangeCreatordanUnitKerjaseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // ! Super Admin & Admin change to Operator
        User::whereIn('id', [1,2])->update(['operator' => 1]);

        // ! Get User Operator Utama
        $operators = ['biologi', 'fisika' , 'ilmukelautan', 'kimia', 'kimias2', 'matematika', 'resiskom', 'sisfo', 'akademik', 'kepegawaian', 'keuangan', 'umper', 'laboranbiologi', 'laboranfisika', 'laboranilkel', 'laboranmtk', 'laborankimia', 'laboransiskom', 'laboransisfo'];
        foreach ($operators as $key => $item) { 
            $userOperator[$item] = User::whereUsername($item)->value('id');
        }

        // ! Get Nama Role
        $roles = DB::table('roles')
                ->select('name')
                ->whereNotIn('name', ['super-admin', 'admin', 'mitra', 'dekan', 'wakil-dekan', 'ketua-jurusan', 'ketua-prodi', 'sekretaris-jurusan', 'ktu', 'sub-koordinator', 'tik', 'ruang-baca'])
                ->orderBy('name')->get();
        
        // ! Get User Operator Per Unit Kerja
        $userRoles  = [];
        $user       = User::query();
        $box        = Box::query();
        $unitKerja  = UnitKerja::query();
        foreach ($roles as $key => $value) {
            switch ($value->name) {
                case 'admin-jurusan':
                    foreach ($operators as $key => $item) {
                        $keyName = $item;
                        if ($item == 'biologi') 
                        {
                            $userRoles[$keyName] = $user->select('id', 'name')->clone()->whereHas('pegawai', function($query) {
                                $query->whereProdiId(1);
                            })->role($value->name)->pluck('id')->toArray();
                        }
                        if ($item == 'fisika') 
                        {
                            $userRoles[$keyName] = $user->select('id', 'name')->clone()->whereHas('pegawai', function($query) {
                                $query->whereProdiId(2);
                            })->role($value->name)->pluck('id')->toArray();
                        }
                        if ($item == 'ilmukelautan') 
                        {
                            $userRoles[$keyName] = $user->select('id', 'name')->clone()->whereHas('pegawai', function($query) {
                                $query->whereProdiId(4);
                            })->role($value->name)->pluck('id')->toArray();
                        }
                        if ($item == 'kimia') 
                        {
                            $userRoles[$keyName] = $user->select('id', 'name')->clone()->whereHas('pegawai', function($query) {
                                $query->whereProdiId(5);
                            })->role($value->name)->pluck('id')->toArray();
                        }
                        if ($item == 'kimias2') 
                        {
                            $userRoles[$keyName] = $user->select('id', 'name')->clone()->whereHas('pegawai', function($query) {
                                $query->whereProdiId(6);
                            })->role($value->name)->pluck('id')->toArray();
                        }
                        if ($item == 'matematika') 
                        {
                            $userRoles[$keyName] = $user->select('id', 'name')->clone()->whereHas('pegawai', function($query) {
                                $query->whereProdiId(7);
                            })->role($value->name)->pluck('id')->toArray();
                        }
                        if ($item == 'resiskom') 
                        {
                            $userRoles[$keyName] = $user->select('id', 'name')->clone()->whereHas('pegawai', function($query) {
                                $query->whereProdiId(9);
                            })->role($value->name)->pluck('id')->toArray();
                        }
                        if ($item == 'sisfo') 
                        {
                            $userRoles[$keyName] = $user->select('id', 'name')->clone()->whereHas('pegawai', function($query) {
                                $query->whereProdiId(10);
                            })->role($value->name)->pluck('id')->toArray();
                        }

                    }
                    $box->clone()->whereBagian('admin-jurusan')->update(['unit_kerja_id' => $unitKerja->clone()->whereNama('JURUSAN')->value('id')]);
                    break;

                default:
                    $userRoles[$value->name] = $user->select('id')->clone()->role($value->name)->pluck('id')->toArray();
                    switch ($value->name) {
                        case 'umper':
                            $unitKerjaName = 'UMUM DAN PERLENGKAPAN';
                            break;
                        case 'laboran':
                            $unitKerjaName = 'LABORATORIUM';
                            break;
                            
                        default:
                            $unitKerjaName = strtoupper($value->name);
                            break;
                    }
                    $box->clone()->whereBagian($value->name)->update(['unit_kerja_id' => $unitKerja->clone()->whereNama($unitKerjaName)->value('id')]);

                    break;
            }
        }
        // return $userRoles;
        // ! Update Data Creator Operator Menjadi Operator Utama 
        foreach ($operators as $value) {
            try {
                DB::beginTransaction();
                File::whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                BerkasPegawai::whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                SuratMasuk::whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                SuratKeluar::whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                SK::whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                Document::whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);

                Box::whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                Folder::whereIn('created_by', $userRoles[$value])->update(['unit_kerja_id' => $unitKerja->clone()->whereNama('KEPEGAWAIAN')->value('id'), 'created_by' => $userOperator[$value]]);
                KategoriBerkas::whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                DB::table('file_box')->whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                DB::table('pegawai_file')->whereIn('created_by', $userRoles[$value])->update(['created_by' => $userOperator[$value]]);
                DB::commit();
            } catch (\Throwable $th) {
                DB::rollback();
            }
        }
        // ! Save Unit Kerja Folder
        foreach (Folder::all() as $value) {
            $value->unitKerja()->attach($unitKerja->clone()->whereNama('KEPEGAWAIAN')->value('id'), ['created_by' => $userOperator['kepegawaian']]);
        }

        // ! Save Unit Kerja Role
        $role = Role::query();
        foreach (UnitKerja::all() as $value) {
            $rolesNama = match ($value->nama) {
                'AKADEMIK'              => ['akademik'],
                'DOSEN'                 => ['dosen', 'dekan', 'wakil-dekan', 'ketua-jurusan', 'sekretaris-jurusan', 'ketua-prodi'],
                'JURUSAN'               => ['admin-jurusan'],
                'KEPEGAWAIAN'           => ['kepegawaian'],
                'KEUANGAN'              => ['keuangan'],
                'LABORATORIUM'          => ['laboran'],
                'MITRA'                 => ['mitra'],
                'RUANG BACA'            => ['ruang-baca'],
                'UMUM DAN PERLENGKAPAN' => ['umper'],
                'TENAGA KEPENDIDIKAN'   => ['ktu', 'sub-koordinator', 'admin-jurusan', 'kepegawaian', 'keuangan', 'laboran', 'ruang-baca', 'umper'],
                'SEMUA PEGAWAI'         => ['dosen', 'dekan', 'wakil-dekan', 'ketua-jurusan', 'sekretaris-jurusan', 'ketua-prodi', 'kepala-lab', 'ktu', 'sub-koordinator', 'admin-jurusan', 'kepegawaian', 'keuangan', 'laboran', 'ruang-baca', 'umper', 'tik'],
                'PIMPINAN FAKULTAS'     => ['dekan', 'wakil-dekan', 'ktu', 'sub-koordinator'],
                'KETUA JURUSAN'         => ['ketua-jurusan'],
                'SEKRETARIS JURUSAN'    => ['sekretaris-jurusan'],
                'KETUA PRODI'           => ['ketua-prodi'],
                'KEPALA LAB'            => ['kepala-lab'],
                'DOSEN BIOLOGI'         => ['dosen'],
                'DOSEN FISIKA'          => ['dosen'],
                'DOSEN GEOFISIKA'       => ['dosen'],
                'DOSEN ILMU KELAUTAN'   => ['dosen'],
                'DOSEN KIMIA'           => ['dosen'],
                'DOSEN S2 KIMIA'        => ['dosen'],
                'DOSEN MATEMATIKA'      => ['dosen'],
                'DOSEN REKAYASA SISTEM KOMPUTER'    => ['dosen'],
                'DOSEN SISTEM INFORMASI'            => ['dosen'],
                'DOSEN STATISTIK'       => ['dosen'],
                'TIM TIK'               => ['tik'],
                default                 => [NULL],
            };
            foreach ($role->clone()->whereIn('name', $rolesNama)->get('id') as $key => $item) {
                $value->roles()->attach($item->id, ['created_by' => 1]);
            }
        }

        // ! Add Jenis Akses 
        JenisAkses::insert(['nama' => 'unit-kerja']);
    }
}
