<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenMatematikaseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $helmi = Pegawai::firstOrCreate([
            'firstname'         => 'Helmi', 
            'gelar_depan'       => 'Drs.',
            'gelar_belakang'    => 'M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('196410171998021001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $helmi->user()->firstOrCreate([
            'name' => strtoupper('Drs. Helmi, M.Si.'),
            'username' => strtolower('196410171998021001'),
            'email' => strtolower('helmi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $helmi->user->activation()->create(['user_id' => $helmi->user->id, 'kode' => Str::random(20)]);
        $helmi->user->assignRole('dosen');

        $mariatul = Pegawai::firstOrCreate([
            'firstname'         => 'Mariatul', 
            'middlename'        => 'Kiftiah', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('198512262008122004'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $mariatul->user()->firstOrCreate([
            'name' => strtoupper('Mariatul Kiftiah, S.Si., M.Sc.'),
            'username' => strtolower('198512262008122004'),
            'email' => strtolower('mariatulkiftiah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $mariatul->user->activation()->create(['user_id' => $mariatul->user->id, 'kode' => Str::random(20)]);
        $mariatul->user->assignRole('dosen');

        $yundari = Pegawai::firstOrCreate([
            'firstname'         => 'Yundari', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('198310202008012012'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $yundari->user()->firstOrCreate([
            'name' => strtoupper('Dr. Yundari, S.Si., M.Sc.'),
            'username' => strtolower('198310202008012012'),
            'email' => strtolower('yundari@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $yundari->user->activation()->create(['user_id' => $yundari->user->id, 'kode' => Str::random(20)]);
        $yundari->user->assignRole(['ketua-jurusan', 'dosen']);

        $bayu = Pegawai::firstOrCreate([
            'firstname'         => 'Bayu', 
            'middlename'        => 'Prihandono', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('197911152005011003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $bayu->user()->firstOrCreate([
            'name' => strtoupper('Dr. Bayu Prihandono, S.Si., M.Sc.'),
            'username' => strtolower('197911152005011003'),
            'email' => strtolower('bayuprihandono@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $bayu->user->activation()->create(['user_id' => $bayu->user->id, 'kode' => Str::random(20)]);
        $bayu->user->assignRole(['ketua-prodi', 'dosen']);

        $nilam = Pegawai::firstOrCreate([
            'firstname'         => 'Nilamsari', 
            'middlename'        => 'Kusumastuti',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('198105102005012003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $nilam->user()->firstOrCreate([
            'name' => strtoupper('Dr. Nilamsari Kusumastuti, S.Si., M.Sc.'),
            'username' => strtolower('198105102005012003'),
            'email' => strtolower('nilamsarikusumastuti@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $nilam->user->activation()->create(['user_id' => $nilam->user->id, 'kode' => Str::random(20)]);
        $nilam->user->assignRole(['sekretaris-jurusan', 'dosen']);

        $evi = Pegawai::firstOrCreate([
            'firstname'         => 'Evi', 
            'middlename'        => 'Noviani',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('197208181998022001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $evi->user()->firstOrCreate([
            'name' => strtoupper('Dr. Evi Noviani, S.Si., M.Si.'),
            'username' => strtolower('197208181998022001'),
            'email' => strtolower('evinoviani@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $evi->user->activation()->create(['user_id' => $evi->user->id, 'kode' => Str::random(20)]);
        $evi->user->assignRole(['wakil-dekan', 'dosen']);

        $fran = Pegawai::firstOrCreate([
            'firstname'         => 'Fransiskus', 
            'middlename'        => 'Fran', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('198804152019031014'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $fran->user()->firstOrCreate([
            'name' => strtoupper('Fransiskus Fran, S.Si., M.Si.'),
            'username' => strtolower('198804152019031014'),
            'email' => strtolower('fransiskusfran@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $fran->user->activation()->create(['user_id' => $fran->user->id, 'kode' => Str::random(20)]);
        $fran->user->assignRole('dosen');

        $yudhi = Pegawai::firstOrCreate([
            'firstname'         => 'Yudhi', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('198504072019031004'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $yudhi->user()->firstOrCreate([
            'name' => strtoupper('Yudhi, S.Si., M.Si.'),
            'username' => strtolower('198504072019031004'),
            'email' => strtolower('yudhi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $yudhi->user->activation()->create(['user_id' => $yudhi->user->id, 'kode' => Str::random(20)]);
        $yudhi->user->assignRole('dosen');

        $miftahul = Pegawai::firstOrCreate([
            'firstname'         => "Nur’ainul", 
            'middlename'        => 'Miftahul',
            'lastname'          => 'Huda',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('199411142020122014'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $miftahul->user()->firstOrCreate([
            'name' => strtoupper("Nur’ainul Miftahul Huda, S.Si., M.Si."),
            'username' => strtolower('199411142020122014'),
            'email' => strtolower('nurainulmiftahulhuda@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $miftahul->user->activation()->create(['user_id' => $miftahul->user->id, 'kode' => Str::random(20)]);
        $miftahul->user->assignRole('dosen');

        $meliana = Pegawai::firstOrCreate([
            'firstname'         => 'Meliana', 
            'middlename'        => 'Pasaribu',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 7, 
            'nomor_unik'        => strtolower('198710192019032006'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $meliana->user()->firstOrCreate([
            'name' => strtoupper('Meliana Pasaribu, S.Pd., M.Sc.'),
            'username' => strtolower('198710192019032006'),
            'email' => strtolower('melianapasaribu@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $meliana->user->activation()->create(['user_id' => $meliana->user->id, 'kode' => Str::random(20)]);
        $meliana->user->assignRole('dosen');

        $ray = Pegawai::firstOrCreate([
            'firstname'         => 'Ray', 
            'middlename'        => 'Tamtama',
            'gelar_belakang'    => 'M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PHL', 
            'prodi_id'          => 7, 
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ray->user()->firstOrCreate([
            'name' => strtoupper('Ray Tamtama, M.Si.'),
            'username' => strtolower('raytamtama'),
            'email' => strtolower('raytamtama@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ray->user->activation()->create(['user_id' => $ray->user->id, 'kode' => Str::random(20)]);
        $ray->user->assignRole('dosen');

    }
}
