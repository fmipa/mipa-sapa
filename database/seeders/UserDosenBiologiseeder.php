<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenBiologiseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $siti = Pegawai::firstOrCreate([
            'firstname'         => 'Siti', 
            'middlename'        => 'Khotimah',
            'gelar_depan'       => 'Dr. Dra.',
            'gelar_belakang'    => 'M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('196702021997022001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $siti->user()->firstOrCreate([
            'name' => strtoupper('Dr. Dra. Siti Khotimah, M.Si.'),
            'username' => strtolower('196702021997022001'),
            'email' => strtolower('sitikhotimah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $siti->user->activation()->create(['user_id' => $siti->user->id, 'kode' => Str::random(20)]);
        $siti->user->assignRole('dosen');

        $rafdinal = Pegawai::firstOrCreate([
            'firstname'         => 'Rafdinal', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197108311999031002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $rafdinal->user()->firstOrCreate([
            'name' => strtoupper('Dr. Rafdinal, S.Si., M.Si.'),
            'username' => strtolower('197108311999031002'),
            'email' => strtolower('rafdinal@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $rafdinal->user->activation()->create(['user_id' => $rafdinal->user->id, 'kode' => Str::random(20)]);
        $rafdinal->user->assignRole('dosen');

        $junardi = Pegawai::firstOrCreate([
            'firstname'         => 'Junardi', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197206132000031001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $junardi->user()->firstOrCreate([
            'name' => strtoupper('Dr. Junardi, S.Si., M.Si.'),
            'username' => strtolower('197206132000031001'),
            'email' => strtolower('junardi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $junardi->user->activation()->create(['user_id' => $junardi->user->id, 'kode' => Str::random(20)]);
        $junardi->user->assignRole('dosen');

        $kustiati = Pegawai::firstOrCreate([
            'firstname'         => 'Kustiati', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197212102000032001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $kustiati->user()->firstOrCreate([
            'name' => strtoupper('Dr. Kustiati, S.Si., M.Si.'),
            'username' => strtolower('197212102000032001'),
            'email' => strtolower('kustiati@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $kustiati->user->activation()->create(['user_id' => $kustiati->user->id, 'kode' => Str::random(20)]);
        $kustiati->user->assignRole(['ketua-jurusan', 'ketua-prodi', 'dosen']);

        $elvi = Pegawai::firstOrCreate([
            'firstname'         => 'Elvi', 
            'middlename'        => 'Rusmiyanto',
            'lastname'          => 'Pancaning Wardoyo',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197109012000031003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $elvi->user()->firstOrCreate([
            'name' => strtoupper('Dr. Elvi Rusmiyanto Pancaning Wardoyo, S.Si., M.Si.'),
            'username' => strtolower('197109012000031003'),
            'email' => strtolower('elvirusmiyantopancaningwardoyo@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $elvi->user->activation()->create(['user_id' => $elvi->user->id, 'kode' => Str::random(20)]);
        $elvi->user->assignRole('dosen');

        $masnur = Pegawai::firstOrCreate([
            'firstname'         => 'Masnur', 
            'middlename'        => 'Turnip',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197208181998022001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $masnur->user()->firstOrCreate([
            'name' => strtoupper('Masnur Turnip, S.Si., M.Sc.'),
            'username' => strtolower('197208181998022001'),
            'email' => strtolower('masnurturnip@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $masnur->user->activation()->create(['user_id' => $masnur->user->id, 'kode' => Str::random(20)]);
        $masnur->user->assignRole('dosen');

        $mukarlina = Pegawai::firstOrCreate([
            'firstname'         => 'Mukarlina', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('196804062000032001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $mukarlina->user()->firstOrCreate([
            'name' => strtoupper('Mukarlina, S.Si., M.Si.'),
            'username' => strtolower('196804062000032001'),
            'email' => strtolower('mukarlina@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $mukarlina->user->activation()->create(['user_id' => $mukarlina->user->id, 'kode' => Str::random(20)]);
        $mukarlina->user->assignRole('dosen');

        $zulfa = Pegawai::firstOrCreate([
            'firstname'         => 'Zulfa', 
            'middlename'        => 'Zakiah',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197306242000032001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $zulfa->user()->firstOrCreate([
            'name' => strtoupper('Dr. Zulfa Zakiah, S.Si., M.Si.'),
            'username' => strtolower('197306242000032001'),
            'email' => strtolower('zulfazakiah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $zulfa->user->activation()->create(['user_id' => $zulfa->user->id, 'kode' => Str::random(20)]);
        $zulfa->user->assignRole('dosen');

        $riza = Pegawai::firstOrCreate([
            'firstname'         => 'Riza', 
            'middlename'        => 'Linda',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197005071999032001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $riza->user()->firstOrCreate([
            'name' => strtoupper('Riza Linda, S.Si., M.Si.'),
            'username' => strtolower('197005071999032001'),
            'email' => strtolower('rizalinda@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $riza->user->activation()->create(['user_id' => $riza->user->id, 'kode' => Str::random(20)]);
        $riza->user->assignRole('dosen');

        $sitiIfadatin = Pegawai::firstOrCreate([
            'firstname'         => 'Siti', 
            'middlename'        => 'Ifadatin',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197103272000032001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $sitiIfadatin->user()->firstOrCreate([
            'name' => strtoupper('Dr. Siti Ifadatin, S.Si., M.Si.'),
            'username' => strtolower('197103272000032001'),
            'email' => strtolower('sitiifadatin@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $sitiIfadatin->user->activation()->create(['user_id' => $sitiIfadatin->user->id, 'kode' => Str::random(20)]);
        $sitiIfadatin->user->assignRole(['sekretaris-jurusan', 'dosen']);

        $triRima = Pegawai::firstOrCreate([
            'firstname'         => 'Tri', 
            'middlename'        => 'Rima',
            'lastname'          => 'Setyawati',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197403071999032002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $triRima->user()->firstOrCreate([
            'name' => strtoupper('Tri Rima Setyawati, S.Si., M.Si.'),
            'username' => strtolower('197403071999032002'),
            'email' => strtolower('tririmasetyawati@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $triRima->user->activation()->create(['user_id' => $triRima->user->id, 'kode' => Str::random(20)]);
        $triRima->user->assignRole('dosen');

        $irwan = Pegawai::firstOrCreate([
            'firstname'         => 'Irwan', 
            'middlename'        => 'Lovadi',
            'gelar_belakang'    => 'S.Si., M.App.Sc., Ph.D.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('197803192001121002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $irwan->user()->firstOrCreate([
            'name' => strtoupper('Irwan Lovadi, S.Si., M.App.Sc., Ph.D.'),
            'username' => strtolower('197803192001121002'),
            'email' => strtolower('irwanlovadi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $irwan->user->activation()->create(['user_id' => $irwan->user->id, 'kode' => Str::random(20)]);
        $irwan->user->assignRole('dosen');

        $ariHepi = Pegawai::firstOrCreate([
            'firstname'         => 'Ari', 
            'middlename'        => 'Hepi',
            'lastname'          => 'Yanti',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('198404152008012008'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ariHepi->user()->firstOrCreate([
            'name' => strtoupper('Ari Hepi Yanti, S.Si., M.Sc.'),
            'username' => strtolower('198404152008012008'),
            'email' => strtolower('arihepiyanti@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ariHepi->user->activation()->create(['user_id' => $ariHepi->user->id, 'kode' => Str::random(20)]);
        $ariHepi->user->assignRole('dosen');

        $diah = Pegawai::firstOrCreate([
            'firstname'         => 'Diah', 
            'middlename'        => 'Wulandari',
            'lastname'          => 'Rousdy',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('198510212008122003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $diah->user()->firstOrCreate([
            'name' => strtoupper('Diah Wulandari Rousdy, S.Si., M.Sc.'),
            'username' => strtolower('198510212008122003'),
            'email' => strtolower('diahwulandarirousdy@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $diah->user->activation()->create(['user_id' => $diah->user->id, 'kode' => Str::random(20)]);
        $diah->user->assignRole('dosen');

        $rahmawati = Pegawai::firstOrCreate([
            'firstname'         => 'Rahmawati', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('198404092008122002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $rahmawati->user()->firstOrCreate([
            'name' => strtoupper('Rahmawati, S.Si., M.Sc.'),
            'username' => strtolower('198404092008122002'),
            'email' => strtolower('rahmawati@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $rahmawati->user->activation()->create(['user_id' => $rahmawati->user->id, 'kode' => Str::random(20)]);
        $rahmawati->user->assignRole('dosen');

        $dwiGusmalawati = Pegawai::firstOrCreate([
            'firstname'         => 'Dwi', 
            'middlename'        => 'Gusmalawati',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('198408072014042002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $dwiGusmalawati->user()->firstOrCreate([
            'name' => strtoupper('Dr. Dwi Gusmalawati, S.Si., M.Si.'),
            'username' => strtolower('198408072014042002'),
            'email' => strtolower('dwigusmalawati@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $dwiGusmalawati->user->activation()->create(['user_id' => $dwiGusmalawati->user->id, 'kode' => Str::random(20)]);
        $dwiGusmalawati->user->assignRole('dosen');

        $riyandi = Pegawai::firstOrCreate([
            'firstname'         => 'Riyandi', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('198606182015041001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $riyandi->user()->firstOrCreate([
            'name' => strtoupper('Riyandi, S.Si., M.Si.'),
            'username' => strtolower('198606182015041001'),
            'email' => strtolower('riyandi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $riyandi->user->activation()->create(['user_id' => $riyandi->user->id, 'kode' => Str::random(20)]);
        $riyandi->user->assignRole('dosen');

        $firman = Pegawai::firstOrCreate([
            'firstname'         => 'Firman', 
            'middlename'        => 'Saputra',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('198302112008121003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $firman->user()->firstOrCreate([
            'name' => strtoupper('Firman Saputra, S.Si., M.Sc.'),
            'username' => strtolower('198302112008121003'),
            'email' => strtolower('firmansaputra@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $firman->user->activation()->create(['user_id' => $firman->user->id, 'kode' => Str::random(20)]);
        $firman->user->assignRole('dosen');

        $rikhsan = Pegawai::firstOrCreate([
            'firstname'         => 'Rikhsan', 
            'middlename'        => 'Kurniatuhadi',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PPPK', 
            'prodi_id'          => 1, 
            'nomor_unik'        => strtolower('198903042023211018'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $rikhsan->user()->firstOrCreate([
            'name' => strtoupper('Rikhsan Kurniatuhadi, S.Si, M.Si'),
            'username' => strtolower('198903042023211018'),
            'email' => strtolower('rikhsankurniatuhadi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $rikhsan->user->activation()->create(['user_id' => $rikhsan->user->id, 'kode' => Str::random(20)]);
        $rikhsan->user->assignRole('dosen');

    }
}
