<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\Team;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PerbaikanPegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $pegawais = Pegawai::all();

        //! Pegawai Kepegawaian
        $kepegawaian = [170,6,7,8];
        //! Pegawai umper
        $umper = [171,4,5,18,19,20,21];
        //! Pegawai akademik
        $akademik = [168,9,22,23,24,47];
        //! Pegawai keuangan
        $keuangan = [169,12,16,17,42,43,46];
        //! Pegawai ruang baca
        $ruangBaca = [190,44];
        //! Pegawai laboran
        $laboran = [180,181,182,183,184,185,186,187,188,189,13,15,30,32,33,35,37,39,40,48];
        //! Pegawai admin jurusan
        $adminjurusan = [172,173,174,175,176,177,178,179,10,25,26,27,28,29,34,38,45];
        //! Pegawai tik
        $tik = [191,36];

        // ! Pimpinan
        $pimpinan = [41,3,49,70,136];

        foreach ($pegawais as $key => $pegawai) {
            if ($pegawai->team_id == 4) {
                $pegawai->update(['unit_kerja_id' => 10]);
            }
            if (!in_array($pegawai->id, $pimpinan) && $pegawai->team_id == 2) {
                $pegawai->update(['unit_kerja_id' => 2]);
            }


            if (in_array($pegawai->id, $pimpinan)) {
                $pegawai->update(['unit_kerja_id' => 12]);
            }
            if (in_array($pegawai->id, $kepegawaian)) {
                $pegawai->update(['unit_kerja_id' => 4]);
            }
            if (in_array($pegawai->id, $umper)) {
                $pegawai->update(['unit_kerja_id' => 9]);
            }
            if (in_array($pegawai->id, $akademik)) {
                $pegawai->update(['unit_kerja_id' => 1]);
            }
            if (in_array($pegawai->id, $keuangan)) {
                $pegawai->update(['unit_kerja_id' => 5]);
            }
            if (in_array($pegawai->id, $ruangBaca)) {
                $pegawai->update(['unit_kerja_id' => 8]);
            }
            if (in_array($pegawai->id, $laboran)) {
                // Updata Data Tim Solihin
                if ($pegawai->id == 15) {
                    $pegawai->update(['team_id' => 4, 'unit_kerja_id' => 6]);
                } else {
                    $pegawai->update(['unit_kerja_id' => 6]);
                }
            }
            if (in_array($pegawai->id, $adminjurusan)) {
                $pegawai->update(['unit_kerja_id' => 3]);
            }
            if (in_array($pegawai->id, $tik)) {
                $pegawai->update(['unit_kerja_id' => 27]);
            }
            if ($pegawai->id == 11) {
                $pegawai->update(['unit_kerja_id' => 7]);
            }
            if (in_array($pegawai->id, [1,2])) {
                $pegawai->update(['unit_kerja_id' => null]);
            }
        }
    }
}
