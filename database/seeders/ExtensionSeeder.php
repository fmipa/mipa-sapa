<?php

namespace Database\Seeders;

use App\Models\Extension;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ExtensionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $extensions = [
            ['extension' => 'pdf' , 'tipe_file' => "*", 'created_by' => 1],
            ['extension' => 'doc' , 'tipe_file' => "berkas-pegawai", 'created_by' => 1],
            ['extension' => 'docx' , 'tipe_file' => "berkas-pegawai", 'created_by' => 1],
            ['extension' => 'xls' , 'tipe_file' => "berkas-pegawai", 'created_by' => 1],
            ['extension' => 'xlsx' , 'tipe_file' => "berkas-pegawai", 'created_by' => 1],
            ['extension' => 'rar' , 'tipe_file' => "berkas-pegawai", 'created_by' => 1],
            ['extension' => 'ppt' , 'tipe_file' => "berkas-pegawai", 'created_by' => 1],
            
        ];

        Extension::insert($extensions);
    }
}
