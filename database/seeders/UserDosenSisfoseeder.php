<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenSisfoseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $ilhamsyah = Pegawai::firstOrCreate([
            'firstname'         => 'Ilhamsyah', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 10, 
            'nomor_unik'        => strtolower('198405102012121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ilhamsyah->user()->firstOrCreate([
            'name' => strtoupper('Ilhamsyah, S.Si., M.Cs.'),
            'username' => strtolower('198405102012121001'),
            'email' => strtolower('ilhamsyah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ilhamsyah->user->activation()->create(['user_id' => $ilhamsyah->user->id, 'kode' => Str::random(20)]);
        $ilhamsyah->user->assignRole('dosen');

        $renny = Pegawai::firstOrCreate([
            'firstname'         => 'Renny', 
            'middlename'        => 'Puspita', 
            'lastname'          => 'Sari', 
            'gelar_belakang'    => 'ST., MT.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 10, 
            'nomor_unik'        => strtolower('198704182015042001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $renny->user()->firstOrCreate([
            'name' => strtoupper('Renny Puspita Sari, ST., MT.'),
            'username' => strtolower('198704182015042001'),
            'email' => strtolower('rennypuspitasari@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $renny->user->activation()->create(['user_id' => $renny->user->id, 'kode' => Str::random(20)]);
        $renny->user->assignRole(['ketua-jurusan', 'ketua-prodi', 'dosen']);

        $nurul = Pegawai::firstOrCreate([
            'firstname'         => 'Nurul', 
            'middlename'        => 'Mutiah', 
            'gelar_belakang'    => 'ST., MT.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 10, 
            'nomor_unik'        => strtolower('198711182015042002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $nurul->user()->firstOrCreate([
            'name' => strtoupper('Nurul Mutiah, ST., MT.'),
            'username' => strtolower('198711182015042002'),
            'email' => strtolower('nurulmutiah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $nurul->user->activation()->create(['user_id' => $nurul->user->id, 'kode' => Str::random(20)]);
        $nurul->user->assignRole('dosen');

        $ibnur = Pegawai::firstOrCreate([
            'firstname'         => 'Ibnur', 
            'middlename'        => 'Rusi', 
            'gelar_belakang'    => 'S.Kom., MM.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 10, 
            'nomor_unik'        => strtolower('198907282019031008'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ibnur->user()->firstOrCreate([
            'name' => strtoupper('Ibnur Rusi, S.Kom., MM.'),
            'username' => strtolower('198907282019031008'),
            'email' => strtolower('ibnurrusi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ibnur->user->activation()->create(['user_id' => $ibnur->user->id, 'kode' => Str::random(20)]);
        $ibnur->user->assignRole(['sekretaris-jurusan', 'dosen']);

        $dian = Pegawai::firstOrCreate([
            'firstname'         => 'Dian', 
            'middlename'        => 'Prawira',
            'gelar_belakang'    => 'ST., M.Eng.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 10, 
            'nomor_unik'        => strtolower('198411132015041001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $dian->user()->firstOrCreate([
            'name' => strtoupper('Dian Prawira, ST., M.Eng.'),
            'username' => strtolower('198411132015041001'),
            'email' => strtolower('dianprawira@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $dian->user->activation()->create(['user_id' => $dian->user->id, 'kode' => Str::random(20)]);
        $dian->user->assignRole('dosen');

        $ferdy = Pegawai::firstOrCreate([
            'firstname'         => 'Ferdy', 
            'middlename'        => 'Febriyanto',
            'gelar_belakang'    => 'S.Kom., M.Kom.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 10, 
            'nomor_unik'        => strtolower('198902012019031008'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ferdy->user()->firstOrCreate([
            'name' => strtoupper('Ferdy Febriyanto, S.Kom., M.Kom.'),
            'username' => strtolower('198902012019031008'),
            'email' => strtolower('ferdyfebriyanto@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ferdy->user->activation()->create(['user_id' => $ferdy->user->id, 'kode' => Str::random(20)]);
        $ferdy->user->assignRole('dosen');

        $syahru = Pegawai::firstOrCreate([
            'firstname'         => 'Syahru', 
            'middlename'        => 'Rahmayuda', 
            'gelar_belakang'    => 'S.Kom., M.Kom.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'prodi_id'          => 10, 
            'nomor_unik'        => strtolower('8884370018'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $syahru->user()->firstOrCreate([
            'name' => strtoupper('Syahru Rahmayuda, S.Kom., M.Kom.'),
            'username' => strtolower('8884370018'),
            'email' => strtolower('syahrurahmayuda@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $syahru->user->activation()->create(['user_id' => $syahru->user->id, 'kode' => Str::random(20)]);
        $syahru->user->assignRole('dosen');

    }
}
