<?php

namespace Database\Seeders;

use App\Models\Team;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Teamseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $teams = [
            ['nama' => 'admin'],
            ['nama' => 'pimpinan'],
            ['nama' => 'dosen'],
            ['nama' => 'tendik'],
            ['nama' => 'luar'],
            
        ];

        Team::insert($teams);
    }
}
