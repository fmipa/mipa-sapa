<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Models\Prodi;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddFolderUnitKerjaSeeder extends Seeder
{
    use EncryptId;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $dosens = [1, 2, 3, 4, 5, 7, 8, 10]; 
        $tendiks = [1,2,3,4,5,7,8,10,12];
        $unitTendik = [1,3,4,5,6,8,9,10,27];
        
        foreach ($dosens as $key => $dosen) {
            $folderDosen[] = ['unit_kerja_id' => 2, 'folder_id' => $dosen, 'created_by' => 1];
        }
        foreach ($unitTendik as $key => $value) {
            foreach ($tendiks as $tendik) {
                $folderTendik[$key][] = ['unit_kerja_id' => $value, 'folder_id' => $tendik, 'created_by' => 1];
            }
        }
        
        DB::table('unit_folder')->insert($folderDosen);
        foreach ($folderTendik as $key => $value) {
            DB::table('unit_folder')->insert($folderTendik[$key]);
        }

        // Tambahan Folder Kepegawaian
        $folderKepegawaian = [
            ['nama' => 'Pensiun', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'pensiun', 'created_by' => 5],
            ['nama' => 'Pindah / Alih Status', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'pindah-atau-alih-status', 'created_by' => 5],
            ['nama' => 'Kenaikan Gaji Berkala', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'kenaikan-gaji-berkala', 'created_by' => 5],
        ];
        DB::table('folders')->insert($folderKepegawaian);

    }
}
