<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenKimiaS2seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $thamrin = Pegawai::firstOrCreate([
            'firstname'         => 'Thamrin', 
            'middlename'        => 'Usman',
            'gelar_depan'       => 'Prof. Dr. H.',
            'gelar_belakang'    => 'DEA',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 6, 
            'nomor_unik'        => strtolower('196211101988111001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $thamrin->user()->firstOrCreate([
            'name' => strtoupper('Prof. Dr. H. Thamrin Usman, DEA.'),
            'username' => strtolower('196211101988111001'),
            'email' => strtolower('thamrinusman@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $thamrin->user->activation()->create(['user_id' => $thamrin->user->id, 'kode' => Str::random(20)]);
        $thamrin->user->assignRole('dosen');

        $risa = Pegawai::firstOrCreate([
            'firstname'         => 'Risa', 
            'middlename'        => 'Nofiani', 
            'gelar_depan'       => 'Prof.',
            'gelar_belakang'    => 'S.Si., M.Si., Ph.D.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 6, 
            'nomor_unik'        => strtolower('197411152000122001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $risa->user()->firstOrCreate([
            'name' => strtoupper('Prof. Risa Nofiani, S.Si., M.Si., Ph.D.'),
            'username' => strtolower('197411152000122001'),
            'email' => strtolower('risanofiani@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $risa->user->activation()->create(['user_id' => $risa->user->id, 'kode' => Str::random(20)]);
        $risa->user->assignRole('dosen');

        $gusrizal = Pegawai::firstOrCreate([
            'firstname'         => 'Gusrizal', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 6, 
            'nomor_unik'        => strtolower('197108022000031001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $gusrizal->user()->firstOrCreate([
            'name' => strtoupper('Dr. Gusrizal, S.Si., M.Si.'),
            'username' => strtolower('197108022000031001'),
            'email' => strtolower('gusrizal@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $gusrizal->user->activation()->create(['user_id' => $gusrizal->user->id, 'kode' => Str::random(20)]);
        $gusrizal->user->assignRole(['dekan', 'dosen']);

        $agusWibowo = Pegawai::firstOrCreate([
            'firstname'         => 'Muhamad', 
            'middlename'        => 'Agus', 
            'lastname'          => 'Wibowo', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 6, 
            'nomor_unik'        => strtolower('197301092000031002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $agusWibowo->user()->firstOrCreate([
            'name' => strtoupper('Dr. Muhamad Agus Wibowo, S.Si., M.Si.'),
            'username' => strtolower('197301092000031002'),
            'email' => strtolower('muhamadaguswibowo@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $agusWibowo->user->activation()->create(['user_id' => $agusWibowo->user->id, 'kode' => Str::random(20)]);
        $agusWibowo->user->assignRole('dosen');

        $rudiyansyah = Pegawai::firstOrCreate([
            'firstname'         => 'Rudiyansyah', 
            'gelar_belakang'    => 'S.Si., M.Si., Ph.D.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 6, 
            'nomor_unik'        => strtolower('197201242000121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $rudiyansyah->user()->firstOrCreate([
            'name' => strtoupper('Rudiyansyah, S.Si., M.Si., Ph.D.'),
            'username' => strtolower('197201242000121001'),
            'email' => strtolower('rudiyansyah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $rudiyansyah->user->activation()->create(['user_id' => $rudiyansyah->user->id, 'kode' => Str::random(20)]);
        $rudiyansyah->user->assignRole('dosen');

        $andi = Pegawai::firstOrCreate([
            'firstname'         => 'Andi', 
            'middlename'        => 'Hairil',
            'lastname'          => 'Alimuddin',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 6, 
            'nomor_unik'        => strtolower('197109202000121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $andi->user()->firstOrCreate([
            'name' => strtoupper('Dr. Andi Hairil Alimuddin, S.Si., M.Si.'),
            'username' => strtolower('197109202000121001'),
            'email' => strtolower('andihairilalimuddin@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $andi->user->activation()->create(['user_id' => $andi->user->id, 'kode' => Str::random(20)]);
        $andi->user->assignRole(['ketua-jurusan', 'dosen']);

        $anis = Pegawai::firstOrCreate([
            'firstname'         => 'Anis', 
            'middlename'        => 'Shofiyani', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 6, 
            'nomor_unik'        => strtolower('197311152000122001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $anis->user()->firstOrCreate([
            'name' => strtoupper('Dr. Anis Shofiyani, S.Si., M.Si.'),
            'username' => strtolower('197311152000122001'),
            'email' => strtolower('anisshofiyani@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $anis->user->activation()->create(['user_id' => $anis->user->id, 'kode' => Str::random(20)]);
        $anis->user->assignRole(['ketua-prodi', 'dosen']);

        $ari = Pegawai::firstOrCreate([
            'firstname'         => 'Ari', 
            'middlename'        => 'Widiyantoro',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 6, 
            'nomor_unik'        => strtolower('197304012000121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ari->user()->firstOrCreate([
            'name' => strtoupper('Dr. Ari Widiyantoro, S.Si., M.Si.'),
            'username' => strtolower('197304012000121001'),
            'email' => strtolower('ariwidiyantoro@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ari->user->activation()->create(['user_id' => $ari->user->id, 'kode' => Str::random(20)]);
        $ari->user->assignRole('dosen');

    }
}
