<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Models\Prodi;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Seeder;

class AddFolderKemahasiswaanSeeder extends Seeder
{
    use EncryptId;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // id Folder 

        $prodis             = Prodi::all();
        $operatorProdi      = [171, 172, 172, 173, 174, 174, 176, 176, 177, 178];
        $operatorAkademik   = 168;
        $unitKerjaId        = 28;
        
        // SK Pengurus Himpunan
        $PSKPHim = Folder::create(['nama' => 'SK Pengurus Himpunan', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => 'sk-pengurus-himpunan', 'created_by' => 168]);
        for ($i=0; $i < 7; $i++) { 
            $namaTA = 'Tahun '.(2018+$i);
            $PTA = Folder::create(['nama' => $namaTA, 'urutan' => ($i+1), 'parent_id' => $PSKPHim->id, 'parent_utama' => $PSKPHim->id, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => NULL, 'slug' => strtolower(str_replace(' ', '-', $namaTA)), 'created_by' => $operatorAkademik]);
        }

        // SK MBKM
        $PSKMBKM = Folder::create(['nama' => 'SK MBKM', 'urutan' => 2, 'parent_id' => 0, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => 'sk-mbkm', 'created_by' => $operatorAkademik]);
        foreach ($prodis as $key => $value) {
            $prodiMBKM = 'Prodi '.$value->nama;
            $PMenggajarProdi = Folder::create(['nama' => $prodiMBKM, 'urutan' => ($key+1), 'parent_id' => $PSKMBKM->id, 'parent_utama' => $PSKMBKM->id, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $prodiMBKM)), 'created_by' => $operatorAkademik]);

            for ($i=0; $i < 3; $i++) { 
            $tahunMBKM = 'Tahun '.(2022+$i);
            $PTMBKM = Folder::create(['nama' => $tahunMBKM, 'urutan' => ($i+1), 'parent_id' => $PMenggajarProdi->id, 'parent_utama' => $PSKMBKM->id, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $tahunMBKM)), 'created_by' => $operatorAkademik]);
        }
        }
        
        // SK Yudisium
        $PSKYudis = Folder::create(['nama' => 'SK Yudisium', 'urutan' => 3, 'parent_id' => 0, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => 'sk-yudisium', 'created_by' => $operatorAkademik]);
        for ($i=0; $i < 6; $i++) { 
            $namaTAYudis = 'TA '.(2019 + $i).'/'.(2020 + $i);
            $PTAYudis = Folder::create(['nama' => $namaTAYudis, 'urutan' => ($i+1), 'parent_id' => $PSKYudis->id, 'parent_utama' => $PSKYudis->id, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $namaTA)), 'created_by' => $operatorAkademik]);

            $periodes = ['I', 'II', 'III', 'IV'];
            foreach ($periodes as $key => $periode) {
                $namaPeriode = 'Periode '.$periode;
                $PUtsUas = Folder::create(['nama' => $namaPeriode, 'urutan' => ($key+1), 'parent_id' => $PTAYudis->id, 'parent_utama' => $PSKYudis->id, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $namaPeriode)), 'created_by' => $operatorAkademik]);
            }
        }
    }
}
