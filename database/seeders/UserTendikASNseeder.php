<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserTendikASNseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $eva = Pegawai::firstOrCreate([
            'firstname'         => 'Eva', 
            'middlename'        => 'Novianti',  
            'lastname'          => 'Hestivera',  
            'gelar_belakang'    => 'ST., S.E., M.M.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'nomor_unik'        => strtolower('197907142006042001'),
            'unit_kerja_id'     => UnitKerja::whereNama('TENAGA KEPENDIDIKAN')->value('id'),
        ]);
        $eva->user()->firstOrCreate([
            'name' => strtoupper('Eva Novianti Hestivera, ST., S.E., M.M.'),
            'username' => strtolower('197907142006042001'),
            'email' => strtolower('evanoviantihestivera@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $eva->user->activation()->create(['user_id' => $eva->user->id, 'kode' => Str::random(20)]);
        $eva->user->assignRole('ktu');

        $rinny = Pegawai::firstOrCreate([
            'firstname'         => 'Rinny', 
            'middlename'        => 'Yusnita',  
            'lastname'          => 'Absari',  
            'gelar_belakang'    => 'S.E., M.M.',
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'nomor_unik'        => strtolower('197706192005012003'),
            'unit_kerja_id'     => UnitKerja::whereNama('KEUANGAN')->value('id'),
        ]);
        $rinny->user()->firstOrCreate([
            'name' => strtoupper('Rinny Yusnita Absari, S.E., M.M.'),
            'username' => strtolower('197706192005012003'),
            'email' => strtolower('rinnyyusnitaabsari@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $rinny->user->activation()->create(['user_id' => $rinny->user->id, 'kode' => Str::random(20)]);
        $rinny->user->assignRole('keuangan');

        $rachmat = Pegawai::firstOrCreate([
            'firstname'         => 'Rachmat', 
            'middlename'        => 'Jamaluddinn',  
            'gelar_belakang'    => 'A.Md.',
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS',  
            'nomor_unik'        => strtolower('197810202000121002'),
            'unit_kerja_id'     => UnitKerja::whereNama('KEUANGAN')->value('id'),
        ]);
        $rachmat->user()->firstOrCreate([
            'name' => strtoupper('Rachmat Jamaluddinn, A.Md.'),
            'username' => strtolower('197810202000121002'),
            'email' => 'rachmat@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(10),
        ]);

        $ervina = Pegawai::firstOrCreate([
            'firstname'         => 'Ervina', 
            'middlename'        => 'Indriati',  
            'gelar_belakang'    => 'S.Far., M.Si.',
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'nomor_unik'        => strtolower('198110212008012008'),
            'unit_kerja_id'     => UnitKerja::whereNama('KEUANGAN')->value('id'),
        ]);
        $ervina->user()->firstOrCreate([
            'name' => strtoupper('Ervina Indriati, S.Far., M.Si.'),
            'username' => strtolower('198110212008012008'),
            'email' => strtolower('ervinaindriati@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ervina->user->activation()->create(['user_id' => $ervina->user->id, 'kode' => Str::random(20)]);
        $ervina->user->assignRole('keuangan');

        $sumadi = Pegawai::firstOrCreate([
            'firstname'         => 'Muhammad', 
            'middlename'        => 'Sumadi',  
            'gelar_belakang'    => 'S.E.',
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'nomor_unik'        => strtolower('197405262009101001'),
            'unit_kerja_id'     => UnitKerja::whereNama('RUANG BACA')->value('id'),
        ]);
        $sumadi->user()->firstOrCreate([
            'name' => strtoupper('Muhammad Sumadi, S.E.'),
            'username' => strtolower('197405262009101001'),
            'email' => strtolower('muhammadsumadi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $sumadi->user->activation()->create(['user_id' => $sumadi->user->id, 'kode' => Str::random(20)]);
        $sumadi->user->assignRole('ruang-baca');

        $maryulin = Pegawai::firstOrCreate([
            'firstname'         => 'Maryulin', 
            'middlename'        => 'Setianingsih',  
            'gelar_belakang'    => 'S.Sos.',
            'prodi_id'          => 9,
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'nomor_unik'        => strtolower('198103302009102001'),
            'unit_kerja_id'     => UnitKerja::whereNama('JURUSAN')->value('id'),
        ]);
        $maryulin->user()->firstOrCreate([
            'name' => strtoupper('Maryulin Setianingsih, S.Sos.'),
            'username' => strtolower('198103302009102001'),
            'email' => strtolower('maryulinsetianingsih@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $maryulin->user->activation()->create(['user_id' => $maryulin->user->id, 'kode' => Str::random(20)]);
        $maryulin->user->assignRole('admin-jurusan');

        $eko = Pegawai::firstOrCreate([
            'firstname'         => 'Eko', 
            'middlename'        => 'Sri',  
            'lastname'          => 'Haryati',  
            'gelar_belakang'    => 'A.Md.',
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'nomor_unik'        => strtolower('196912172007012001'),
            'unit_kerja_id'     => UnitKerja::whereNama('KEUANGAN')->value('id'),
        ]);
        $eko->user()->firstOrCreate([
            'name' => strtoupper('Eko Sri Haryati, A.Md.'),
            'username' => strtolower('196912172007012001'),
            'email' => strtolower('ekosriharyati@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $eko->user->activation()->create(['user_id' => $eko->user->id, 'kode' => Str::random(20)]);
        $eko->user->assignRole('keuangan');

        $sakdiana = Pegawai::firstOrCreate([
            'firstname'         => 'Sakdiana', 
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'nomor_unik'        => strtolower('198307072009102001'),
            'unit_kerja_id'     => UnitKerja::whereNama('AKADEMIK')->value('id'),
        ]);
        $sakdiana->user()->firstOrCreate([
            'name' => strtoupper('sakdiana'),
            'username' => strtolower('198307072009102001'),
            'email' => strtolower('sakdiana@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $sakdiana->user->activation()->create(['user_id' => $sakdiana->user->id, 'kode' => Str::random(20)]);
        $sakdiana->user->assignRole('akademik');

        $yoga = Pegawai::firstOrCreate([
            'firstname'         => 'Yoga', 
            'middlename'        => 'Pratama',  
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PPPK', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('199408032023211018'),
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
        ]);
        $yoga->user()->firstOrCreate([
            'name' => strtoupper('Yoga Pratama, S.Si.'),
            'username' => strtolower('199408032023211018'),
            'email' => strtolower('yogapratama@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $yoga->user->activation()->create(['user_id' => $yoga->user->id, 'kode' => Str::random(20)]);
        $yoga->user->assignRole('laboran');

    }
}
