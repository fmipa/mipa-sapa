<?php

namespace Database\Seeders;

use App\Models\KodeUnit;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KodeUnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $kodeUnits = [
            ['kode' => 'UN22' , 'deskripsi' => 'Universitas Tanjungpura', 'created_by' => 1],
            ['kode' => 'UN22.8' , 'deskripsi' => 'Fakultas Matematika dan Ilmu Pengetahuan Alam Universitas Tanjungpura', 'created_by' => 1],
            
        ];

        KodeUnit::insert($kodeUnits);
    }
}
