<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Models\UnitKerja;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FolderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $parentDPersonalP = 1;
        $parentKepangkatan = 2;
        $parentJafung = 3;
        $parentRiwayat = 4;
        $parentSKinerjaP = 5;
        $parentLakin = 6;
        $parentKP4 = 7;
        $parentTubel = 8;
        $parentSkeluar = 9;
        $parentsatya = 10;
        $parentsCuti = 11;
        $parentsKontrak = 12;
        $parentsSertifikat = 13;
        $parentsKehadiran = 14;

        $folders = [
            ['nama' => 'Data Personal Pegawai', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'data-personal-pegawai', 'created_by' => 5], // 1
            ['nama' => 'Riwayat Kepangkatan/Gol', 'urutan' => 2, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'riwayat-kepangkatan/gol', 'created_by' => 5], // 2
            ['nama' => 'Riwayat Jabatan Fungsional/Jabatan Administrasi', 'urutan' => 3, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'riwayat-jabatan-fungsional-jabatan-administrasi', 'created_by' => 5], // 3
            ['nama' => 'Riwayat Dosen/Tendik dengan Tugas Tambahan', 'urutan' => 4, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'riwayat-dosen/tendik-dengan-tugas-tambahan', 'created_by' => 5], // 4
            ['nama' => 'Sasaran Kinerja Pegawai (SKP)', 'urutan' => 5, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'riwayat-kinerja-pegawai-skp', 'created_by' => 5], // 5
            ['nama' => 'Laporan Kinerja (LAKIN) FMIPA UNTAN', 'urutan' => 6, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'laporan-kinerja-(lakin)-fmipa-untan', 'created_by' => 5], // 6
            ['nama' => 'Surat Tunjangan Keluarga (KP4)', 'urutan' => 7, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'surat-tunjangan-keluarga', 'created_by' => 5], // 7
            ['nama' => 'Riwayat Tugas Belajar dan Pengaktifan Kembali', 'urutan' => 8, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'riwayat-tugas-belajar-dan-pengaktifan-kembali', 'created_by' => 5], // 8
            ['nama' => 'Surat Keluar', 'urutan' => 8, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'surat-keluar', 'created_by' => 5], // 9
            ['nama' => 'Piagam Satya Lancana Karya Satya', 'urutan' => 10, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'piagam-satya-lancana-karya-satya', 'created_by' => 5], // 10
            ['nama' => 'Riwayat Cuti', 'urutan' => 11, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'riwayat-cuti', 'created_by' => 5], // 11
            ['nama' => 'Kontrak Kerja', 'urutan' => 12, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'kontrak-kerja', 'created_by' => 5], // 12
            ['nama' => 'Sertifikat Pelatihan/Sertifikasi', 'urutan' => 13, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'sertifikat-pelatihan-sertifikasi', 'created_by' => 5], // 13
            ['nama' => 'Rekapitulasi Kehadiran Pegawai', 'urutan' => 14, 'parent_id' => 0, 'unit_kerja_id' => 4, 'slug' => 'rekapitulasi-kehadiran-pegawai', 'created_by' => 5],  // 14

            // Sub Menu 1
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 1, 'parent_utama' => $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 15
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 1, 'parent_utama' => $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 16
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 2, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 17
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 2, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 18
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 3, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 19
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 3, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 20
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 4, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 21
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 4, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 22
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 5, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 23
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 5, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 24
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 6, 'parent_utama' => $parentLakin, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 25
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 6, 'parent_utama' => $parentLakin, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 26
            ['nama' => 'Tahun 2020', 'urutan' => 3, 'parent_id' => 6, 'parent_utama' => $parentLakin, 'unit_kerja_id' => 4, 'slug' => 'tahun-2020', 'created_by' => 5], // 27
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 7, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 28
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 7, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 29
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 8, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 30
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 8, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 31
            ['nama' => 'Tahun 2023', 'urutan' => 1, 'parent_id' => 9, 'parent_utama' => $parentSkeluar, 'unit_kerja_id' => 4, 'slug' => 'tahun-2023', 'created_by' => 5], // 32
            ['nama' => 'Tahun 2022', 'urutan' => 2, 'parent_id' => 9, 'parent_utama' => $parentSkeluar, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 33
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 10, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 34
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 10, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 35
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 11, 'parent_utama' => $parentsCuti, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 36
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 11, 'parent_utama' => $parentsCuti, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 37
            ['nama' => 'Dosen', 'urutan' => 1, 'parent_id' => 12, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'dosen', 'created_by' => 5], // 38
            ['nama' => 'Tendik', 'urutan' => 2, 'parent_id' => 12, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'tendik', 'created_by' => 5], // 39
            ['nama' => 'Tahun 2023', 'urutan' => 1, 'parent_id' => 13, 'parent_utama' => $parentsSertifikat, 'unit_kerja_id' => 4, 'slug' => 'tahun-2023', 'created_by' => 5], // 40
            ['nama' => 'Tahun 2022', 'urutan' => 2, 'parent_id' => 13, 'parent_utama' => $parentsSertifikat, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 41
            
            // Sub Menu 2
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 15, 'parent_utama' =>  $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 42
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 15, 'parent_utama' =>  $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 43
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 15, 'parent_utama' =>  $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 44
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 15, 'parent_utama' =>  $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 45
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 15, 'parent_utama' =>  $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 46
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 15, 'parent_utama' =>  $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 47
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 15, 'parent_utama' =>  $parentDPersonalP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 48
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 17, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 49
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 17, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 50
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 17, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 51
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 17, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 52
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 17, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 53
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 17, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 54
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 17, 'parent_utama' => $parentKepangkatan, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 55
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 19, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 56
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 19, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 57
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 19, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 58
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 19, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 59
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 19, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 60
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 19, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 61
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 19, 'parent_utama' => $parentJafung, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 62
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 21, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 43
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 21, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 64
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 21, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 65
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 21, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 66
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 21, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 67
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 21, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 68
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 21, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 69
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 23, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 70
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 23, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 71
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 23, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 72
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 23, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 73
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 23, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 74
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 23, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 75
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 23, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 76
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 24, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 77
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 24, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 78
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 28, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 79
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 28, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 80
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 28, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 81
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 28, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 82
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 28, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 83
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 28, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 84
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 28, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 85
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 29, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 86
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 29, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 87
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 30, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 88
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 30, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 89
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 30, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 90
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 30, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 91
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 30, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 92
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 30, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 93
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 30, 'parent_utama' => $parentTubel, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 94
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 34, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 95
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 34, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 96
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 34, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 97
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 34, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 98
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 34, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 99
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 34, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 100
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 34, 'parent_utama' => $parentsatya, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 101
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 36, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 102
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 36, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 103
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 36, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 104
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 36, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 105
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 36, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 106
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 36, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 107
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 36, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 108
            ['nama' => 'Tahun 2023', 'urutan' => 1, 'parent_id' => 37, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'tahun-2023', 'created_by' => 5], // 109
            ['nama' => 'Tahun 2022', 'urutan' => 2, 'parent_id' => 37, 'parent_utama' => $parentRiwayat, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 110
            ['nama' => 'Tahun 2023', 'urutan' => 1, 'parent_id' => 39, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'tahun-2023', 'created_by' => 5], // 111
            ['nama' => 'Tahun 2022', 'urutan' => 2, 'parent_id' => 39, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 112
            ['nama' => 'Jurusan Matematika', 'urutan' => 1, 'parent_id' => 38, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'jurusan-matematika', 'created_by' => 5], // 113
            ['nama' => 'Jurusan Fisika', 'urutan' => 2, 'parent_id' => 38, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'jurusan-fisika', 'created_by' => 5], // 114
            ['nama' => 'Jurusan Kimia', 'urutan' => 3, 'parent_id' => 38, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'jurusan-kimia', 'created_by' => 5], // 115
            ['nama' => 'Jurusan Biologi', 'urutan' => 4, 'parent_id' => 38, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'jurusan-biologi', 'created_by' => 5], // 9116
            ['nama' => 'Jurusan Rekayasa Sistem Komputer', 'urutan' => 5, 'parent_id' => 38, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'jurusan-rekayasa-sistem-komputer', 'created_by' => 5], // 117
            ['nama' => 'Jurusan Ilmu Kelautan', 'urutan' => 6, 'parent_id' => 38, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'jurusan-ilmu-kelautan', 'created_by' => 5], // 118
            ['nama' => 'Jurusan Sistem Informasi', 'urutan' => 7, 'parent_id' => 38, 'parent_utama' => $parentsKontrak, 'unit_kerja_id' => 4, 'slug' => 'jurusan-sistem-informasi', 'created_by' => 5], // 119

            // Sub Menu 3
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 70, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 120
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 70, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 121
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 71, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 122
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 71, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 123
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 72, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 124
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 72, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 125
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 73, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 126
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 73, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 127
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 74, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 128
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 74, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 129
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 75, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 130
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 75, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 131
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 76, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 132
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 76, 'parent_utama' => $parentSKinerjaP, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 133
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 79, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 134
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 79, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 135
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 80, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 136
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 80, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 137
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 81, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 138
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 81, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 139
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 82, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 140
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 82, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 141
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 83, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 142
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 83, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 143
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 84, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 144
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 84, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 145
            ['nama' => 'Tahun 2022', 'urutan' => 1, 'parent_id' => 85, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2022', 'created_by' => 5], // 146
            ['nama' => 'Tahun 2021', 'urutan' => 2, 'parent_id' => 85, 'parent_utama' => $parentKP4, 'unit_kerja_id' => 4, 'slug' => 'tahun-2021', 'created_by' => 5], // 147
        ];

        foreach ($folders as $folder) {
            Folder::create($folder);
        }

        // $getFolderKepegawaian = Folder::whereParentId(0)->pluck('id');
        // $kepegawaian = UnitKerja::find(4);
        // $kepegawaian->folder()->attach($getFolderKepegawaian, ['created_by' => 1]);

    }
}
