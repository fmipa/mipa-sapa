<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Models\Prodi;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Seeder;

class AddFolderAkademikSeeder extends Seeder
{
    use EncryptId;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // id Folder 1222

        $prodis             = Prodi::all();
        $operatorProdi      = [171, 172, 172, 173, 174, 174, 176, 176, 177, 178];
        $operatorAkademik   = 168;
        
        // SK Pembimbing Akademik
        $PSKPA = Folder::create(['nama' => 'SK Pembimbing Akademik', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => 1, 'prodi_id' => null, 'slug' => 'sk-pembimbing-akademik', 'created_by' => 168]);
        for ($i=0; $i < 7; $i++) { 
            $namaTA = 'Tahun '.(2018+$i);
            $PTA = Folder::create(['nama' => $namaTA, 'urutan' => ($i+1), 'parent_id' => $PSKPA->id, 'parent_utama' => $PSKPA->id, 'unit_kerja_id' => 1, 'prodi_id' => NULL, 'slug' => strtolower(str_replace(' ', '-', $namaTA)), 'created_by' => $operatorAkademik]);
        }

        // SK Mengajar
        $PSKMengajar = Folder::create(['nama' => 'SK Mengajar', 'urutan' => 2, 'parent_id' => 0, 'unit_kerja_id' => 1, 'prodi_id' => null, 'slug' => 'sk-mengajar', 'created_by' => $operatorAkademik]);
        $gajilgenap = ['GANJIL', 'GENAP'];
        foreach ($gajilgenap as $key => $value) {
            $mengajarGG = Folder::create(['nama' => $value, 'urutan' => ($key+1), 'parent_id' => $PSKMengajar->id, 'parent_utama' => $PSKMengajar->id, 'unit_kerja_id' => 1, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $value)), 'created_by' => $operatorAkademik]);
            
            for ($i=0; $i < 8; $i++) { 
                $namaTA = 'TA '.(2017 + $i).'/'.(2018 + $i);
                $PTA = Folder::create(['nama' => $namaTA, 'urutan' => ($i+1), 'parent_id' => $mengajarGG->id, 'parent_utama' => $PSKMengajar->id, 'unit_kerja_id' => 1, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $namaTA)), 'created_by' => $operatorAkademik]);
            }
        }
        
        // SK Ujian
        $PSKUjian = Folder::create(['nama' => 'SK Ujian', 'urutan' => 3, 'parent_id' => 0, 'unit_kerja_id' => 1, 'prodi_id' => null, 'slug' => 'sk-mengajar', 'created_by' => $operatorAkademik]);
        $gajilgenap = ['GANJIL', 'GENAP'];
        foreach ($gajilgenap as $key => $value) {
            $PUjianGG = Folder::create(['nama' => $value, 'urutan' => ($key+1), 'parent_id' => $PSKUjian->id, 'parent_utama' => $PSKUjian->id, 'unit_kerja_id' => 1, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $value)), 'created_by' => $operatorAkademik]);
            
            for ($i=0; $i < 6; $i++) { 
                $namaTA = 'TA '.(2019 + $i).'/'.(2020 + $i);
                $PTA = Folder::create(['nama' => $namaTA, 'urutan' => ($i+1), 'parent_id' => $PUjianGG->id, 'parent_utama' => $PSKUjian->id, 'unit_kerja_id' => 1, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $namaTA)), 'created_by' => $operatorAkademik]);

                $utsuas = ['UTS', 'UAS'];
                foreach ($utsuas as $key => $UU) {
                    $PUtsUas = Folder::create(['nama' => $UU, 'urutan' => ($key+1), 'parent_id' => $PTA->id, 'parent_utama' => $PSKUjian->id, 'unit_kerja_id' => 1, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $UU)), 'created_by' => $operatorAkademik]);
                }
            }
        }
    }
}
