<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Models\UnitKerja;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UnitKerjaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $unitKerjas = [
            ['nama' => 'AKADEMIK' , 'urutan' => 1, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'DOSEN' , 'urutan' => 6, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'JURUSAN' , 'urutan' => 8, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'KEPEGAWAIAN' , 'urutan' => 3, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'KEUANGAN' , 'urutan' => 4, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'LABORATORIUM' , 'urutan' => 9, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'MITRA' , 'urutan' => 11, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'RUANG BACA' , 'urutan' => 10, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'UMUM DAN PERLENGKAPAN' , 'urutan' => 5, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'TENAGA KEPENDIDIKAN' , 'urutan' => 7, 'is_folder' => 1, 'prodi_id' => NULL],
            ['nama' => 'SEMUA PEGAWAI', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => NULL],
            ['nama' => 'PIMPINAN FAKULTAS', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => NULL],
            ['nama' => 'KETUA JURUSAN', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => NULL],
            ['nama' => 'SEKRETARIS JURUSAN', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => NULL],
            ['nama' => 'KETUA PRODI', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => NULL],
            ['nama' => 'KEPALA LAB', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => NULL],
            ['nama' => 'DOSEN BIOLOGI', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 1],
            ['nama' => 'DOSEN FISIKA', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 2],
            ['nama' => 'DOSEN GEOFISIKA', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 3],
            ['nama' => 'DOSEN ILMU KELAUTAN', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 4],
            ['nama' => 'DOSEN KIMIA', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 5],
            ['nama' => 'DOSEN S2 KIMIA', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 6],
            ['nama' => 'DOSEN MATEMATIKA', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 7],
            ['nama' => 'DOSEN STATISTIK', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 8],
            ['nama' => 'DOSEN REKAYASA SISTEM KOMPUTER', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 9],
            ['nama' => 'DOSEN SISTEM INFORMASI', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => 10],
            ['nama' => 'TIM TIK', 'urutan' => 0, 'is_folder' => 0, 'prodi_id' => NULL],
            ['nama' => 'KEMAHASISWAAN', 'urutan' => 2, 'is_folder' => 1, 'prodi_id' => NULL],
            
        ];

        UnitKerja::insert($unitKerjas);
    }
}
