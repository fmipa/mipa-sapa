<?php

namespace Database\Seeders;

use App\Enums\JenisAksesEnum;
use App\Models\FolderStorage;
use App\Models\JenisAkses;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;

class FolderStorageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $disk   = config('filesystems.disks.local.driver');
        $path   = 'default';
        $publik = JenisAksesEnum::PUBLIC->value;
        $jenisAksesNama = strtolower(JenisAkses::find($publik)->nama);
        $folderStorages = [
            ['nama' => 'Berkas Pegawai', 'disk' => $disk, 'jenis_akses_id' => $publik, 'path' => $path.'/Berkas Pegawai', 'created_by' => 1],
            ['nama' => 'Dokumen', 'disk' => $disk, 'jenis_akses_id' => $publik, 'path' => $path.'/Dokumen', 'created_by' => 1],
            ['nama' => 'Surat Masuk', 'disk' => $disk, 'jenis_akses_id' => $publik, 'path' => $path.'/Surat Masuk', 'created_by' => 1],
            ['nama' => 'Surat Keluar', 'disk' => $disk, 'jenis_akses_id' => $publik, 'path' => $path.'/Surat Keluar', 'created_by' => 1],
            ['nama' => 'SK', 'disk' => $disk, 'jenis_akses_id' => $publik, 'path' => $path.'/SK', 'created_by' => 1],
        ];
        
        Storage::disk($disk)->deleteDirectory($jenisAksesNama);
        Storage::disk($disk)->makeDirectory($jenisAksesNama.'/'.$path.'/Berkas Pegawai');
        Storage::disk($disk)->makeDirectory($jenisAksesNama.'/'.$path.'/Dokumen');
        Storage::disk($disk)->makeDirectory($jenisAksesNama.'/'.$path.'/Surat Masuk');
        Storage::disk($disk)->makeDirectory($jenisAksesNama.'/'.$path.'/Surat Keluar');
        Storage::disk($disk)->makeDirectory($jenisAksesNama.'/'.$path.'/SK');

        FolderStorage::insert($folderStorages);
    }
}
