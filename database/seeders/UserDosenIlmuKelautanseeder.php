<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenIlmuKelautanseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $warsidah = Pegawai::firstOrCreate([
            'firstname'         => 'Warsidah', 
            'gelar_belakang'    => 'S.Si., M.Si., Apt.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('197304122000032001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $warsidah->user()->firstOrCreate([
            'name' => strtoupper('Warsidah, S.Si., M.Si., Apt.'),
            'username' => strtolower('197304122000032001'),
            'email' => strtolower('warsidah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $warsidah->user->activation()->create(['user_id' => $warsidah->user->id, 'kode' => Str::random(20)]);
        $warsidah->user->assignRole('dosen');

        $nora = Pegawai::firstOrCreate([
            'firstname'         => 'Nora', 
            'middlename'         => 'Idiawati', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('197510152006042001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $nora->user()->firstOrCreate([
            'name' => strtoupper('Nora Idiawati, S.Si., M.Si.'),
            'username' => strtolower('197510152006042001'),
            'email' => strtolower('noraidiawati@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $nora->user->activation()->create(['user_id' => $nora->user->id, 'kode' => Str::random(20)]);
        $nora->user->assignRole('dosen');

        $apriansyah = Pegawai::firstOrCreate([
            'firstname'         => 'Apriansyah', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('198604292014041001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $apriansyah->user()->firstOrCreate([
            'name' => strtoupper('Dr. Apriansyah, S.Si., M.Si.'),
            'username' => strtolower('198604292014041001'),
            'email' => strtolower('apriansyah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $apriansyah->user->activation()->create(['user_id' => $apriansyah->user->id, 'kode' => Str::random(20)]);
        $apriansyah->user->assignRole('dosen');

        $arie = Pegawai::firstOrCreate([
            'firstname'         => 'Arie', 
            'middlename'        => 'Antasari', 
            'lastname'          => 'Kushadiwijayanto', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('198609072015041001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $arie->user()->firstOrCreate([
            'name' => strtoupper('Arie Antasari Kushadiwijayanto, S.Si., M.Si.'),
            'username' => strtolower('198609072015041001'),
            'email' => strtolower('arieantasarikushadiwijayanto@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $arie->user->activation()->create(['user_id' => $arie->user->id, 'kode' => Str::random(20)]);
        $arie->user->assignRole(['ketua-jurusan', 'ketua-prodi', 'dosen']);

        $yusuf = Pegawai::firstOrCreate([
            'firstname'         => 'Yusuf', 
            'middlename'        => 'Arief',
            'lastname'          => 'Nurrahman',
            'gelar_belakang'    => 'S.Kel., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('198903172018031001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $yusuf->user()->firstOrCreate([
            'name' => strtoupper('Yusuf Arief Nurrahman, S.Kel., M.Si.'),
            'username' => strtolower('198903172018031001'),
            'email' => strtolower('yusufariefnurrahman@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $yusuf->user->activation()->create(['user_id' => $yusuf->user->id, 'kode' => Str::random(20)]);
        $yusuf->user->assignRole(['sekretaris-jurusan', 'dosen']);

        $megaSari = Pegawai::firstOrCreate([
            'firstname'         => 'Mega', 
            'middlename'        => 'Sari',
            'lastname'          => 'Juane Sofiana',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('198606242019032017'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $megaSari->user()->firstOrCreate([
            'name' => strtoupper('Mega Sari Juane Sofiana, S.Si., M.Sc.'),
            'username' => strtolower('198606242019032017'),
            'email' => strtolower('megasarijuanesofiana@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $megaSari->user->activation()->create(['user_id' => $megaSari->user->id, 'kode' => Str::random(20)]);
        $megaSari->user->assignRole('dosen');

        $sukal = Pegawai::firstOrCreate([
            'firstname'         => 'Sukal', 
            'middlename'        => 'Minsas', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('198507192019032007'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $sukal->user()->firstOrCreate([
            'name' => strtoupper('Sukal Minsas, S.Si., M.Si.'),
            'username' => strtolower('198507192019032007'),
            'email' => strtolower('sukalminsas@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $sukal->user->activation()->create(['user_id' => $sukal->user->id, 'kode' => Str::random(20)]);
        $sukal->user->assignRole('dosen');

        $syarif = Pegawai::firstOrCreate([
            'firstname'         => 'Syarif', 
            'middlename'        => 'Irwan',
            'lastname'          => 'Nurdiansyah',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PPPK', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('198606272023211014'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $syarif->user()->firstOrCreate([
            'name' => strtoupper('Syarif Irwan Nurdiansyah, S.Si., M.Si.'),
            'username' => strtolower('198606272023211014'),
            'email' => strtolower('syarifirwannurdiansyah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $syarif->user->activation()->create(['user_id' => $syarif->user->id, 'kode' => Str::random(20)]);
        $syarif->user->assignRole('dosen');

        $ikha = Pegawai::firstOrCreate([
            'firstname'         => 'Ikha', 
            'middlename'        => 'Safitri',
            'gelar_belakang'    => 'S.Pi., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'KONTRAK', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('8886270018'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ikha->user()->firstOrCreate([
            'name' => strtoupper('Ikha Safitri, S.Pi., M.Si.'),
            'username' => strtolower('8886270018'),
            'email' => strtolower('ikhasafitri@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ikha->user->activation()->create(['user_id' => $ikha->user->id, 'kode' => Str::random(20)]);
        $ikha->user->assignRole('dosen');

        $shifa = Pegawai::firstOrCreate([
            'firstname'         => 'Shifa', 
            'middlename'        => 'Helena',
            'gelar_belakang'    => 'S.Kel., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'KONTRAK', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('8817270018'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $shifa->user()->firstOrCreate([
            'name' => strtoupper('Shifa Helena, S.Kel., M.Si.'),
            'username' => strtolower('8817270018'),
            'email' => strtolower('shifahelena@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $shifa->user->activation()->create(['user_id' => $shifa->user->id, 'kode' => Str::random(20)]);
        $shifa->user->assignRole('dosen');

        $dwiImam = Pegawai::firstOrCreate([
            'firstname'         => 'Dwi', 
            'middlename'        => 'Imam',
            'lastname'          => 'Prayitno',
            'gelar_belakang'    => 'S.Kel., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'prodi_id'          => 4, 
            'nomor_unik'        => strtolower('8876270018'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $dwiImam->user()->firstOrCreate([
            'name' => strtoupper('Dwi Imam Prayitno. S.Kel., M.Si.'),
            'username' => strtolower('8876270018'),
            'email' => strtolower('dwiimamprayitno@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $dwiImam->user->activation()->create(['user_id' => $dwiImam->user->id, 'kode' => Str::random(20)]);
        $dwiImam->user->assignRole('dosen');

        
    }
}
