<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserTendiknonASNseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $toni = Pegawai::firstOrCreate([
            'firstname'         => 'Toni', 
            'team_id'           => 4, 
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('KEPEGAWAIAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $toni->user()->firstOrCreate([
            'name' => strtoupper('Toni'),
            'username' => strtolower('toni'),
            'email' => 'toni@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(10),
        ]);

        $toni->user->activation()->create(['user_id' => $toni->user->id, 'kode' => Str::random(20)]);
        $toni->user->assignRole('kepegawaian');


        $wiwid = Pegawai::firstOrCreate([
            'firstname'         => 'Wiwid', 
            'middlename'        => 'Widyana',
            'gelar_belakang'    => 'S.Si.',
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'unit_kerja_id'     => UnitKerja::whereNama('KEPEGAWAIAN')->value('id'),
            'status'            => 'KONTRAK',
        ]);
        $wiwid->user()->firstOrCreate([
            'name' => strtoupper('Wiwid Widyana, S.Si.'),
            'username' => strtolower('wiwid'),
            'email' => 'wiwid@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(10),
        ]);

        $wiwid->user->activation()->create(['user_id' => $wiwid->user->id, 'kode' => Str::random(20)]);
        $wiwid->user->assignRole('kepegawaian');


        $budi = Pegawai::firstOrCreate([
            'firstname'         => 'Budi', 
            'middlename'        => 'Suryadarma',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('KEUANGAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $budi->user()->firstOrCreate([
            'name' => strtoupper('Budi Suryadarma'),
            'username' => strtolower('budi'),
            'email' => strtolower('budisuryadarma@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $budi->user->activation()->create(['user_id' => $budi->user->id, 'kode' => Str::random(20)]);
        $budi->user->assignRole('keuangan');

        $prima = Pegawai::firstOrCreate([
            'firstname'         => 'Prima', 
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('KEUANGAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $prima->user()->firstOrCreate([
            'name' => strtoupper('Prima'),
            'username' => strtolower('prima'),
            'email' => strtolower('prima@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $prima->user->activation()->create(['user_id' => $prima->user->id, 'kode' => Str::random(20)]);
        $prima->user->assignRole('keuangan');

        $nayla = Pegawai::firstOrCreate([
            'firstname'         => 'Nayla', 
            'middlename'        => 'Afifah',  
            'gelar_belakang'    => 'S.Hut.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'unit_kerja_id'     => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $nayla->user()->firstOrCreate([
            'name' => strtoupper('Nayla Afifah, S.Hut.'),
            'username' => strtolower('nayla'),
            'email' => 'nayla@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(10),
        ]);

        $nayla->user->activation()->create(['user_id' => $nayla->user->id, 'kode' => Str::random(20)]);
        $nayla->user->assignRole('umper');

        $supriani = Pegawai::firstOrCreate([
            'firstname'         => 'Supriani', 
            'gelar_belakang'    => 'S.Hut.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'unit_kerja_id'     => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $supriani->user()->firstOrCreate([
            'name' => strtoupper('Supriani, S.Hut.'),
            'username' => strtolower('supriani'),
            'email' => 'supriani@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(10),
        ]);

        $supriani->user->activation()->create(['user_id' => $supriani->user->id, 'kode' => Str::random(20)]);
        $supriani->user->assignRole('umper');

        $peri = Pegawai::firstOrCreate([
            'firstname'         => 'Peri', 
            'middlename'        => 'Suhendra',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $peri->user()->firstOrCreate([
            'name' => strtoupper('Peri Suhendra'),
            'username' => strtolower('peri'),
            'email' => strtolower('perisuhendra@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $peri->user->activation()->create(['user_id' => $peri->user->id, 'kode' => Str::random(20)]);
        $peri->user->assignRole('umper');

        $sahroni = Pegawai::firstOrCreate([
            'firstname'         => 'sahroni', 
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $sahroni->user()->firstOrCreate([
            'name' => strtoupper('Sahroni'),
            'username' => strtolower('sahroni'),
            'email' => strtolower('sahroni@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $sahroni->user->activation()->create(['user_id' => $sahroni->user->id, 'kode' => Str::random(20)]);
        $sahroni->user->assignRole('umper');

        $susanti = Pegawai::firstOrCreate([
            'firstname'         => 'Susanti', 
            'gelar_belakang'    => 'S.Pd.', 
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'unit_kerja_id'     => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $susanti->user()->firstOrCreate([
            'name' => strtoupper('Susanti, S.Pd.'),
            'username' => strtolower('susanti'),
            'email' => strtolower('susanti@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $susanti->user->activation()->create(['user_id' => $susanti->user->id, 'kode' => Str::random(20)]);
        $susanti->user->assignRole('umper');

        $hajjar = Pegawai::firstOrCreate([
            'firstname'         => 'Hajjar', 
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $hajjar->user()->firstOrCreate([
            'name' => strtoupper('Hajjar'),
            'username' => strtolower('hajjar'),
            'email' => strtolower('hajjar@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $hajjar->user->activation()->create(['user_id' => $hajjar->user->id, 'kode' => Str::random(20)]);
        $hajjar->user->assignRole('umper');

        $onny = Pegawai::firstOrCreate([
            'firstname'         => 'Onny', 
            'middlename'        => 'Suryana',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('AKADEMIK')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $onny->user()->firstOrCreate([
            'name' => strtoupper('Onny Suryana'),
            'username' => strtolower('onny'),
            'email' => strtolower('onnysuuryana@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $onny->user->activation()->create(['user_id' => $onny->user->id, 'kode' => Str::random(20)]);
        $onny->user->assignRole('akademik');

        $primanita = Pegawai::firstOrCreate([
            'firstname'         => 'Primanita',
            'middlename'        => 'Putri',  
            'lastname'          => 'Darmanto',  
            'gelar_belakang'    => 'M.Pd',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'unit_kerja_id'     => UnitKerja::whereNama('AKADEMIK')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $primanita->user()->firstOrCreate([
            'name' => strtoupper('Primanita Putri Darmanto, M.Pd.'),
            'username' => strtolower('primanita'),
            'email' => 'primanita@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(10),
        ]);

        $primanita->user->activation()->create(['user_id' => $primanita->user->id, 'kode' => Str::random(20)]);
        $primanita->user->assignRole('akademik');

        $agung = Pegawai::firstOrCreate([
            'firstname'         => 'Agung', 
            'middlename'        => 'Setyowahyu',  
            'gelar_belakang'    => 'A.Md.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('AKADEMIK')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $agung->user()->firstOrCreate([
            'name' => strtoupper('Agung Setyowahyu, A.Md.'),
            'username' => strtolower('agung'),
            'email' => strtolower('agungsetyowahyu@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $agung->user->activation()->create(['user_id' => $agung->user->id, 'kode' => Str::random(20)]);
        $agung->user->assignRole('akademik');

        $hendri = Pegawai::firstOrCreate([
            'firstname'         => 'Hendri', 
            'middlename'        => 'Purwanto',  
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('AKADEMIK')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $hendri->user()->firstOrCreate([
            'name' => strtoupper('Hendri Purwanto, S.Si.'),
            'username' => strtolower('hendri'),
            'email' => strtolower('hendripurwanto@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $hendri->user->activation()->create(['user_id' => $hendri->user->id, 'kode' => Str::random(20)]);
        $hendri->user->assignRole('akademik');

        $wafha = Pegawai::firstOrCreate([
            'firstname'         => 'Wafha', 
            'middlename'        => 'Fardiah',  
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('JURUSAN')->value('id'),
            'prodi_id'          => 7, 
        ]);
        $wafha->user()->firstOrCreate([
            'name' => strtoupper('Wafha Fardiah, S.Si.'),
            'username' => strtolower('wafha'),
            'email' => strtolower('wafhafardiah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $wafha->user->activation()->create(['user_id' => $wafha->user->id, 'kode' => Str::random(20)]);
        $wafha->user->assignRole('admin-jurusan');

        $suandi = Pegawai::firstOrCreate([
            'firstname'         => 'Suandi', 
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('JURUSAN')->value('id'),
            'prodi_id'          => 2, 
        ]);
        $suandi->user()->firstOrCreate([
            'name' => strtoupper('Suandi, S.Si..'),
            'username' => strtolower('suandi'),
            'email' => 'suandi@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(10),
        ]);

        $suandi->user->activation()->create(['user_id' => $suandi->user->id, 'kode' => Str::random(20)]);
        $suandi->user->assignRole('admin-jurusan');

        $rumiris = Pegawai::firstOrCreate([
            'firstname'         => 'Rumiris', 
            'middlename'        => 'Sitorus',  
            'gelar_belakang'    => 'S.Kom.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('JURUSAN')->value('id'),
            'prodi_id'          => 5, 
        ]);
        $rumiris->user()->firstOrCreate([
            'name' => strtoupper('Rumiris Sitorus, S.Kom.'),
            'username' => strtolower('rumiris'),
            'email' => strtolower('rumirissitorus@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $rumiris->user->activation()->create(['user_id' => $rumiris->user->id, 'kode' => Str::random(20)]);
        $rumiris->user->assignRole('admin-jurusan');
        
        $agus = Pegawai::firstOrCreate([
            'firstname'         => 'Agus', 
            'middlename'        => 'Setiawan',  
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('JURUSAN')->value('id'),
            'prodi_id'          => 10, 
        ]);
        $agus->user()->firstOrCreate([
            'name' => strtoupper('Agus Setiawan, S.Si.'),
            'username' => strtolower('agus'),
            'email' => strtolower('agussetiawan@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $agus->user->activation()->create(['user_id' => $agus->user->id, 'kode' => Str::random(20)]);
        $agus->user->assignRole('admin-jurusan');
        
        $surya = Pegawai::firstOrCreate([
            'firstname'         => 'Surya', 
            'middlename'        => 'Darma',  
            'gelar_belakang'    => 'A.Md.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('JURUSAN')->value('id'),
            'prodi_id'          => 4, 
        ]);
        $surya->user()->firstOrCreate([
            'name' => strtoupper('Surya Darma, A.Md.'),
            'username' => strtolower('surya'),
            'email' => strtolower('suryadarma@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $surya->user->activation()->create(['user_id' => $surya->user->id, 'kode' => Str::random(20)]);
        $surya->user->assignRole('admin-jurusan');
        
        $warsi = Pegawai::firstOrCreate([
            'firstname'         => 'Warsi', 
            'middlename'        => 'Kurnia',  
            'lastname'          => 'Rahayu',  
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('JURUSAN')->value('id'),
            'prodi_id'          => 6, 
        ]);
        $warsi->user()->firstOrCreate([
            'name' => strtoupper('Warsi Kurnia Rahayu, S.Si.'),
            'username' => strtolower('warsi'),
            'email' => strtolower('warsikurniarahayu@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $warsi->user->activation()->create(['user_id' => $warsi->user->id, 'kode' => Str::random(20)]);
        $warsi->user->assignRole('admin-jurusan');
        
        $thareq = Pegawai::firstOrCreate([
            'firstname'         => 'Thareq', 
            'middlename'        => 'Abdul',  
            'lastname'          => 'Aziz',  
            'gelar_belakang'    => 'A.Md.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 7, 
        ]);
        $thareq->user()->firstOrCreate([
            'name' => strtoupper('Thareq Abdul Aziz, A.Md.'),
            'username' => strtolower('thareq'),
            'email' => strtolower('thareqabdulaziz@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $thareq->user->activation()->create(['user_id' => $thareq->user->id, 'kode' => Str::random(20)]);
        $thareq->user->assignRole('laboran');
        
        $faurizal = Pegawai::firstOrCreate([
            'firstname'         => 'Faurizal', 
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 2, 
        ]);
        $faurizal->user()->firstOrCreate([
            'name' => strtoupper('Faurizal, S.Si.'),
            'username' => strtolower('faurizal'),
            'email' => strtolower('faurizal@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $faurizal->user->activation()->create(['user_id' => $faurizal->user->id, 'kode' => Str::random(20)]);
        $faurizal->user->assignRole('laboran');

        $raha = Pegawai::firstOrCreate([
            'firstname'         => 'Sri', 
            'middlename'        => 'Rahayu', 
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 1, 
        ]);
        $raha->user()->firstOrCreate([
            'name' => strtoupper('Sri Rahayu, S.Si.'),
            'username' => strtolower('sri'),
            'email' => strtolower('srirahayu@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $raha->user->activation()->create(['user_id' => $raha->user->id, 'kode' => Str::random(20)]);
        $raha->user->assignRole('laboran');

        $emma = Pegawai::firstOrCreate([
            'firstname'         => 'Emma', 
            'middlename'        => 'Khairiah', 
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 1, 
        ]);
        $emma->user()->firstOrCreate([
            'name' => strtoupper('Emma Khairiah, S.Si.'),
            'username' => strtolower('emma'),
            'email' => strtolower('emmakahiriah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $emma->user->activation()->create(['user_id' => $emma->user->id, 'kode' => Str::random(20)]);
        $emma->user->assignRole('laboran');

        $margie = Pegawai::firstOrCreate([
            'firstname'         => 'Margie', 
            'middlename'        => 'Surahman', 
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 1, 
        ]);
        $margie->user()->firstOrCreate([
            'name' => strtoupper('Margie Surahman, S.Si.'),
            'username' => strtolower('margie'),
            'email' => strtolower('margiesurahman@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $margie->user->activation()->create(['user_id' => $margie->user->id, 'kode' => Str::random(20)]);
        $margie->user->assignRole(['admin-jurusan']);

        $harianto = Pegawai::firstOrCreate([
            'firstname'         => 'Harianto', 
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'KONTRAK', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 4, 
        ]);
        $harianto->user()->firstOrCreate([
            'name' => strtoupper('Harianto, S.Si.'),
            'username' => strtolower('harianto'),
            'email' => strtolower('harianto@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $harianto->user->activation()->create(['user_id' => $harianto->user->id, 'kode' => Str::random(20)]);
        $harianto->user->assignRole('laboran');

        $rendi = Pegawai::firstOrCreate([
            'firstname'         => 'Rendi', 
            'gelar_belakang'    => 'A.Md.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'status'            => 'KONTRAK', 
        ]);
        $rendi->user()->firstOrCreate([
            'name' => strtoupper('Rendi, A.Md.'),
            'username' => strtolower('rendi'),
            'email' => strtolower('rendi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $rendi->user->activation()->create(['user_id' => $rendi->user->id, 'kode' => Str::random(20)]);
        $rendi->user->assignRole('tik');

        $weldi = Pegawai::firstOrCreate([
            'firstname'         => 'Weldi', 
            'gelar_belakang'    => 'S.Kom.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PHL', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 9, 
        ]);
        $weldi->user()->firstOrCreate([
            'name' => strtoupper('Weldi, S.Kom.'),
            'username' => strtolower('weldi'),
            'email' => strtolower('weldi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $weldi->user->activation()->create(['user_id' => $weldi->user->id, 'kode' => Str::random(20)]);
        $weldi->user->assignRole('laboran');

        $solihin = Pegawai::firstOrCreate([
            'firstname'         => 'Solihin', 
            'gelar_belakang'    => 'S.Kom.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PHL', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 10, 
        ]);
        $solihin->user()->firstOrCreate([
            'name' => strtoupper('Solihin, S.Kom.'),
            'username' => strtolower('solihin'),
            'email' => strtolower('solihin@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $solihin->user->activation()->create(['user_id' => $solihin->user->id, 'kode' => Str::random(20)]);
        $solihin->user->assignRole('laboran');

        $solihun = Pegawai::firstOrCreate([
            'firstname'         => 'Solihun', 
            'gelar_belakang'    => 'S.Kom.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PHL', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 9, 
        ]);
        $solihun->user()->firstOrCreate([
            'name' => strtoupper('Solihun, S.Kom.'),
            'username' => strtolower('solihun'),
            'email' => strtolower('solihun@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $solihun->user->activation()->create(['user_id' => $solihun->user->id, 'kode' => Str::random(20)]);
        $solihun->user->assignRole('laboran');

        $riyo = Pegawai::firstOrCreate([
            'firstname'         => 'Riyo', 
            'middlename'        => 'Riadi', 
            'gelar_belakang'    => 'S.Mat.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'unit_kerja_id'     => UnitKerja::whereNama('KEPEGAWAIAN')->value('id'),
            'status'            => 'PHL', 
        ]);
        $riyo->user()->firstOrCreate([
            'name' => strtoupper('Riyo Riadi, S.Mat.'),
            'username' => strtolower('riyo'),
            'email' => strtolower('riyoriadi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $riyo->user->activation()->create(['user_id' => $riyo->user->id, 'kode' => Str::random(20)]);
        $riyo->user->assignRole('kepegawaian');
        
        $yusuf = Pegawai::firstOrCreate([
            'firstname'         => 'Yusuf', 
            'middlename'        => 'Umar', 
            'lastname'          => 'Al Hakim', 
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PHL', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 3, 
        ]);
        $yusuf->user()->firstOrCreate([
            'name' => strtoupper('Yusuf Umar Al Hakim, S.Si.'),
            'username' => strtolower('yusuf'),
            'email' => strtolower('yusufumaralhakim@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $yusuf->user->activation()->create(['user_id' => $yusuf->user->id, 'kode' => Str::random(20)]);
        $yusuf->user->assignRole('laboran');
        
        $titik = Pegawai::firstOrCreate([
            'firstname'         => 'Titik', 
            'middlename'        => 'Lestari', 
            'gelar_belakang'    => 'S.Si.',  
            'team_id'           => 4,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PHL', 
            'unit_kerja_id'     => UnitKerja::whereNama('LABORATORIUM')->value('id'),
            'prodi_id'          => 5, 
        ]);
        $titik->user()->firstOrCreate([
            'name' => strtoupper('Titik Lestari, S.Si.'),
            'username' => strtolower('titik'),
            'email' => strtolower('titiklestari@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $titik->user->activation()->create(['user_id' => $titik->user->id, 'kode' => Str::random(20)]);
        $titik->user->assignRole('laboran');
    }
}
