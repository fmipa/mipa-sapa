<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AddFolderJurusanSeeder extends Seeder
{
    use EncryptId;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // ADMIN JURUSAN
        $folders = [
            ['nama' => 'BIOLOGI', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => 3, 'prodi_id' => 1, 'slug' => 'biologi', 'created_by' => 171], // 148
            ['nama' => 'FISIKA', 'urutan' => 2, 'parent_id' => 0, 'unit_kerja_id' => 3, 'prodi_id' => 2, 'slug' => 'fisika', 'created_by' => 172], // 149
            ['nama' => 'MATEMATIKA', 'urutan' => 4, 'parent_id' => 0, 'unit_kerja_id' => 3, 'prodi_id' => 7, 'slug' => 'matematika', 'created_by' => 176], // 150
            ['nama' => 'ILMU KELAUTAN', 'urutan' => 3, 'parent_id' => 0, 'unit_kerja_id' => 3, 'prodi_id' => 4, 'slug' => 'ilmu-kelautan', 'created_by' => 173], // 151
            ['nama' => 'KIMIA', 'urutan' => 5, 'parent_id' => 0, 'unit_kerja_id' => 3, 'prodi_id' => 5, 'slug' => 'kimia', 'created_by' => 174], // 152
            ['nama' => 'REKAYASA SISTEM KOMPUTER', 'urutan' => 6, 'parent_id' => 0, 'unit_kerja_id' => 3, 'prodi_id' => 9, 'slug' => 'rekayasa-sistem-komputer', 'created_by' => 177], // 153
            ['nama' => 'SISTEM INFORMASI', 'urutan' => 7, 'parent_id' => 0, 'unit_kerja_id' => 3, 'prodi_id' => 10, 'slug' => 'sistem-informasi', 'created_by' => 178], // 154

            // PRODI
            ['nama' => 'PRODI BIOLOGI', 'urutan' => 1, 'parent_id' => 148, 'unit_kerja_id' => 3, 'prodi_id' => 1, 'slug' => 'prodi-biologi', 'created_by' => 171], // 155
            ['nama' => 'PRODI FISIKA', 'urutan' => 1, 'parent_id' => 149, 'unit_kerja_id' => 3, 'prodi_id' => 2, 'slug' => 'prodi-fisika', 'created_by' => 172], // 156
            ['nama' => 'PRODI GEOFISIKA', 'urutan' => 2, 'parent_id' => 149, 'unit_kerja_id' => 3, 'prodi_id' => 3, 'slug' => 'prodi-geofisika', 'created_by' => 172], // 157
            ['nama' => 'PRODI MATEMATIKA', 'urutan' => 1, 'parent_id' => 150, 'unit_kerja_id' => 3, 'prodi_id' => 7, 'slug' => 'prodi-matematika', 'created_by' => 176], // 158
            ['nama' => 'PRODI STATISTIKA', 'urutan' => 2, 'parent_id' => 150, 'unit_kerja_id' => 3, 'prodi_id' => 8, 'slug' => 'prodi-statistika', 'created_by' => 176], // 159
            ['nama' => 'PRODI ILMU KELAUTAN', 'urutan' => 1, 'parent_id' => 151, 'unit_kerja_id' => 3, 'prodi_id' => 4, 'slug' => 'prodi-ilmu-kelautan', 'created_by' => 173], // 160
            ['nama' => 'PRODI KIMIA', 'urutan' => 1, 'parent_id' => 152, 'unit_kerja_id' => 3, 'prodi_id' => 5, 'slug' => 'prodi-kimia', 'created_by' => 174], // 161
            ['nama' => 'PRODI KIMIA S2', 'urutan' => 2, 'parent_id' => 152, 'unit_kerja_id' => 3, 'prodi_id' => 6, 'slug' => 'prodi-kimia-s2', 'created_by' => 174], // 162
            ['nama' => 'PRODI REKAYASA SISTEM KOMPUTER', 'urutan' => 1, 'parent_id' => 153, 'unit_kerja_id' => 3, 'prodi_id' => 9, 'slug' => 'prodi-rekayasa-sistem-komputer', 'created_by' => 177], // 163
            ['nama' => 'PRODI SISTEM INFORMASI', 'urutan' => 1, 'parent_id' => 154, 'unit_kerja_id' => 3, 'prodi_id' => 10, 'slug' => 'prodi-sistem-informasi', 'created_by' => 178], // 164

        ];

        foreach ($folders as $folder) {
            Folder::create($folder);
        }

        // LABORAN
        $folders = [
            ['nama' => 'BIOLOGI', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => 6, 'prodi_id' => 1, 'slug' => 'biologi', 'created_by' => 181], // 165
            ['nama' => 'FISIKA', 'urutan' => 2, 'parent_id' => 0, 'unit_kerja_id' => 6, 'prodi_id' => 2, 'slug' => 'fisika', 'created_by' => 182], // 166
            ['nama' => 'MATEMATIKA', 'urutan' => 4, 'parent_id' => 0, 'unit_kerja_id' => 6, 'prodi_id' => 7, 'slug' => 'matematika', 'created_by' => 185], // 167
            ['nama' => 'ILMU KELAUTAN', 'urutan' => 3, 'parent_id' => 0, 'unit_kerja_id' => 6, 'prodi_id' => 4, 'slug' => 'ilmu-kelautan', 'created_by' => 184], // 168
            ['nama' => 'KIMIA', 'urutan' => 5, 'parent_id' => 0, 'unit_kerja_id' => 6, 'prodi_id' => 5, 'slug' => 'kimia', 'created_by' => 187], // 169
            ['nama' => 'REKAYASA SISTEM KOMPUTER', 'urutan' => 6, 'parent_id' => 0, 'unit_kerja_id' => 6, 'prodi_id' => 9, 'slug' => 'rekayasa-sistem-komputer', 'created_by' => 180], // 170
            ['nama' => 'SISTEM INFORMASI', 'urutan' => 7, 'parent_id' => 0, 'unit_kerja_id' => 6, 'prodi_id' => 10, 'slug' => 'sistem-informasi', 'created_by' => 179], // 171
        ];

        foreach ($folders as $folder) {
            Folder::create($folder);
        }

        // DOSEN
        // $folders = [
        //     ['nama' => 'BIOLOGI', 'urutan' => 1, 'parent_id' => 0, 'unit_kerja_id' => 2, 'prodi_id' => 1, 'slug' => 'biologi', 'created_by' => 1], // 172
        //     ['nama' => 'FISIKA', 'urutan' => 2, 'parent_id' => 0, 'unit_kerja_id' => 2, 'prodi_id' => 2, 'slug' => 'fisika', 'created_by' => 1], // 173
        //     ['nama' => 'MATEMATIKA', 'urutan' => 4, 'parent_id' => 0, 'unit_kerja_id' => 2, 'prodi_id' => 7, 'slug' => 'matematika', 'created_by' => 1], // 174
        //     ['nama' => 'ILMU KELAUTAN', 'urutan' => 3, 'parent_id' => 0, 'unit_kerja_id' => 2, 'prodi_id' => 4, 'slug' => 'ilmu-kelautan', 'created_by' => 1], // 175
        //     ['nama' => 'KIMIA', 'urutan' => 5, 'parent_id' => 0, 'unit_kerja_id' => 2, 'prodi_id' => 5, 'slug' => 'kimia', 'created_by' => 1], // 176
        //     ['nama' => 'REKAYASA SISTEM KOMPUTER', 'urutan' => 6, 'parent_id' => 0, 'unit_kerja_id' => 2, 'prodi_id' => 9, 'slug' => 'rekayasa-sistem-komputer', 'created_by' => 1], // 177
        //     ['nama' => 'SISTEM INFORMASI', 'urutan' => 7, 'parent_id' => 0, 'unit_kerja_id' => 2, 'prodi_id' => 10, 'slug' => 'sistem-informasi', 'created_by' => 1], // 178

        //     // PRODI
        //     ['nama' => 'PRODI BIOLOGI', 'urutan' => 1, 'parent_id' => 148, 'unit_kerja_id' => 2, 'prodi_id' => 1, 'slug' => 'prodi-biologi', 'created_by' => 1], // 179
        //     ['nama' => 'PRODI FISIKA', 'urutan' => 1, 'parent_id' => 149, 'unit_kerja_id' => 2, 'prodi_id' => 2, 'slug' => 'prodi-fisika', 'created_by' => 1], // 180
        //     ['nama' => 'PRODI GEOFISIKA', 'urutan' => 2, 'parent_id' => 149, 'unit_kerja_id' => 2, 'prodi_id' => 3, 'slug' => 'prodi-geofisika', 'created_by' => 1], // 181
        //     ['nama' => 'PRODI MATEMATIKA', 'urutan' => 1, 'parent_id' => 150, 'unit_kerja_id' => 2, 'prodi_id' => 7, 'slug' => 'prodi-matematika', 'created_by' => 1], // 182
        //     ['nama' => 'PRODI STATISTIKA', 'urutan' => 2, 'parent_id' => 150, 'unit_kerja_id' => 2, 'prodi_id' => 8, 'slug' => 'prodi-statistika', 'created_by' => 1], // 183
        //     ['nama' => 'PRODI ILMU KELAUTAN', 'urutan' => 1, 'parent_id' => 151, 'unit_kerja_id' => 2, 'prodi_id' => 4, 'slug' => 'prodi-ilmu-kelautan', 'created_by' => 1], // 184
        //     ['nama' => 'PRODI KIMIA', 'urutan' => 1, 'parent_id' => 152, 'unit_kerja_id' => 2, 'prodi_id' => 5, 'slug' => 'prodi-kimia', 'created_by' => 1], // 185
        //     ['nama' => 'PRODI KIMIA S2', 'urutan' => 2, 'parent_id' => 152, 'unit_kerja_id' => 2, 'prodi_id' => 6, 'slug' => 'prodi-kimia-s2', 'created_by' => 1], // 185
        //     ['nama' => 'PRODI REKAYASA SISTEM KOMPUTER', 'urutan' => 1, 'parent_id' => 153, 'unit_kerja_id' => 2, 'prodi_id' => 9, 'slug' => 'prodi-rekayasa-sistem-komputer', 'created_by' => 1], // 186
        //     ['nama' => 'PRODI SISTEM INFORMASI', 'urutan' => 1, 'parent_id' => 154, 'unit_kerja_id' => 2, 'prodi_id' => 10, 'slug' => 'prodi-sistem-informasi', 'created_by' => 1], // 187

        // ];
        // foreach ($folders as $folder) {
        //     Folder::create($folder);
        // }
    }
}
