<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // ! Deletet All Role
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
        DB::table('roles')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
        // ! Create Role
        Role::create(['name' => 'super-admin']);
        $admin      = Role::create(['name' => 'admin']);
        $dekan      = Role::create(['name' => 'dekan']);
        $wd         = Role::create(['name' => 'wakil-dekan']);
        $ktu        = Role::create(['name' => 'ktu']);
        $subkoor    = Role::create(['name' => 'sub-koordinator']);
        $kajur      = Role::create(['name' => 'ketua-jurusan']);
        $sekjur     = Role::create(['name' => 'sekretaris-jurusan']);
        $kaprodi    = Role::create(['name' => 'ketua-prodi']);
        $dosen      = Role::create(['name' => 'dosen']);
        $akademik   = Role::create(['name' => 'akademik']);
        $kepegawaian = Role::create(['name' => 'kepegawaian']);
        $keuangan   = Role::create(['name' => 'keuangan']);
        $umper      = Role::create(['name' => 'umper']);
        $adjur      = Role::create(['name' => 'admin-jurusan']);
        $laboran    = Role::create(['name' => 'laboran']);
        $tik        = Role::create(['name' => 'tik']);
        $ruangbaca  = Role::create(['name' => 'ruang-baca']);
        $mitra      = Role::create(['name' => 'mitra']);

        // $permissions = [
        //     'activation','activation-index','activation-create','activation-update','activation-delete',
        //     'agama','agama-index','agama-create','agama-update','agama-delete',
        //     'berkaspegawai','berkaspegawai-index','berkaspegawai-create','berkaspegawai-update','berkaspegawai-delete',
        //     'box','box-index','box-create','box-update','box-delete',
        //     'document','document-index','document-create','document-update','document-delete',
        //     'extension','extension-index','extension-create','extension-update','extension-delete',
        //     'file','file-index','file-create','file-update','file-delete',
        //     'fileshare','fileshare-index','fileshare-create','fileshare-update','fileshare-delete',
        //     'folder','folder-index','folder-create','folder-update','folder-delete',
        //     'folderstorage','folderstorage-index','folderstorage-create','folderstorage-update','folderstorage-delete',
        //     'hakakses','hakakses-index','hakakses-create','hakakses-update','hakakses-delete',
        //     'kberkas','kberkas-index','kberkas-create','kberkas-update','kberkas-delete',
        //     'kodenaskah','kodenaskah-index','kodenaskah-create','kodenaskah-update','kodenaskah-delete',
        //     'kodeunit','kodeunit-index','kodeunit-create','kodeunit-update','kodeunit-delete',
        //     'pegawai','pegawai-index','pegawai-create','pegawai-update','pegawai-delete',
        //     'permission','permission-index','permission-create','permission-update','permission-delete',
        //     'sk','sk-index','sk-create','sk-update','sk-delete',
        //     'suratmasuk','suratmasuk-index','suratmasuk-create','suratmasuk-update','suratmasuk-delete',
        //     'suratkeluar','suratkeluar-index','suratkeluar-create','suratkeluar-update','suratkeluar-delete'
        //     'team','team-index','team-create','team-update','team-delete',
        //     'user','user-index','user-create','user-update','user-delete', 
        // ];
        
        $pimpinanPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index',
            'pegawai','pegawai-index',
        ];

        $dosenPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index',
        ];

        $akademikPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index','file-create','file-update','file-delete',
            'folder','folder-index','folder-create','folder-update','folder-delete',
            'sk','sk-index','sk-create','sk-update','sk-delete',
            'suratkeluar','suratkeluar-index','suratkeluar-create','suratkeluar-update','suratkeluar-delete',
        ];

        $mitraPermissions = [
            'file','file-index',
        ];

        $kepegawaianPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'berkaspegawai','berkaspegawai-index','berkaspegawai-create','berkaspegawai-update','berkaspegawai-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index','file-create','file-update','file-delete',
            'folder','folder-index','folder-create','folder-update','folder-delete',
            'kberkas','kberkas-index','kberkas-create','kberkas-update','kberkas-delete',
            'kodenaskah','kodenaskah-index','kodenaskah-create','kodenaskah-update','kodenaskah-delete',
            'pegawai','pegawai-index','pegawai-create','pegawai-update','pegawai-delete',
            'sk','sk-index','sk-create','sk-update','sk-delete',
            'suratkeluar','suratkeluar-index','suratkeluar-create','suratkeluar-update','suratkeluar-delete',
        ];

        $umperPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index','file-create','file-update','file-delete',
            'folder','folder-index','folder-create','folder-update','folder-delete',
            'kodenaskah','kodenaskah-index','kodenaskah-create','kodenaskah-update','kodenaskah-delete',
            'sk','sk-index','sk-create','sk-update','sk-delete',
            'suratmasuk','suratmasuk-index','suratmasuk-create','suratmasuk-update','suratmasuk-delete',
            'suratkeluar','suratkeluar-index','suratkeluar-create','suratkeluar-update','suratkeluar-delete',
        ];

        $keuanganPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index','file-create','file-update','file-delete',
            'folder','folder-index','folder-create','folder-update','folder-delete',
            'sk','sk-index','sk-create','sk-update','sk-delete',
            'suratmasuk','suratmasuk-index','suratmasuk-create','suratmasuk-update','suratmasuk-delete',
            // 'suratkeluar','suratkeluar-index','suratkeluar-create','suratkeluar-update','suratkeluar-delete'
        ];

        $adjurPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index','file-create','file-update','file-delete',
            'folder','folder-index','folder-create','folder-update','folder-delete',
            'pegawai','pegawai-index',
            'prodi','prodi-index','prodi-create','prodi-update',
            'sk','sk-index','sk-create','sk-update','sk-delete',
            'suratmasuk','suratmasuk-index',
            'suratkeluar','suratkeluar-index','suratkeluar-create','suratkeluar-update','suratkeluar-delete',
        ];

        $laboranPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index',
        ];

        $adminPermissions = [
            'agama','agama-index','agama-create','agama-update','agama-delete',
            'berkaspegawai','berkaspegawai-index','berkaspegawai-create','berkaspegawai-update','berkaspegawai-delete',
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index','file-create','file-update','file-delete',
            'folder','folder-index','folder-create','folder-update','folder-delete',
            'jurusan','jurusan-index','jurusan-create','jurusan-update','jurusan-delete',
            'kberkas','kberkas-index','kberkas-create','kberkas-update','kberkas-delete',
            'kodenaskah','kodenaskah-index','kodenaskah-create','kodenaskah-update','kodenaskah-delete',
            'kodeunit','kodeunit-index','kodeunit-create','kodeunit-update','kodeunit-delete',
            'pegawai','pegawai-index','pegawai-create','pegawai-update','pegawai-delete',
            'prodi','prodi-index','prodi-create','prodi-update','prodi-delete',
            'sk','sk-index','sk-create','sk-update','sk-delete',
            'suratmasuk','suratmasuk-index','suratmasuk-create','suratmasuk-update','suratmasuk-delete',
            'suratkeluar','suratkeluar-index','suratkeluar-create','suratkeluar-update','suratkeluar-delete',
        ];

        $ruangbacaPermissions = [
            'box','box-index','box-create','box-update','box-delete',
            'document','document-index','document-create','document-update','document-delete',
            'file','file-index',
        ];

        // ! Give Permission to Role
        $admin->syncPermissions($adminPermissions);
        $dekan->syncPermissions($pimpinanPermissions);
        $wd->syncPermissions($pimpinanPermissions);
        $ktu->syncPermissions($pimpinanPermissions);
        $subkoor->syncPermissions($pimpinanPermissions);
        $kajur->syncPermissions($dosenPermissions);
        $sekjur->syncPermissions($dosenPermissions);
        $kaprodi->syncPermissions($dosenPermissions);
        $dosen->syncPermissions($dosenPermissions);
        $akademik->syncPermissions($akademikPermissions);
        $kepegawaian->syncPermissions($kepegawaianPermissions);
        $keuangan->syncPermissions($keuanganPermissions);
        $umper->syncPermissions($umperPermissions);
        $adjur->syncPermissions($adjurPermissions);
        $laboran->syncPermissions($laboranPermissions);
        $tik->syncPermissions($adminPermissions);
        $ruangbaca->syncPermissions($ruangbacaPermissions);
        $mitra->syncPermissions($mitraPermissions);
    }
}
