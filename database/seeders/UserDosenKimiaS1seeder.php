<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenKimiaS1seeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $nelly = Pegawai::firstOrCreate([
            'firstname'         => 'Nelly', 
            'middlename'        => 'Wahyuni',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('197506022000032001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $nelly->user()->firstOrCreate([
            'name' => strtoupper('Dr. Nelly Wahyuni, S.Si., M.Si.'),
            'username' => strtolower('197506022000032001'),
            'email' => strtolower('nellywahyuni@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $nelly->user->activation()->create(['user_id' => $nelly->user->id, 'kode' => Str::random(20)]);
        $nelly->user->assignRole('dosen');

        $afghani = Pegawai::firstOrCreate([
            'firstname'         => 'Afghani', 
            'middlename'        => 'Jayuska', 
            'gelar_depan'       => 'H.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('197107072000121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $afghani->user()->firstOrCreate([
            'name' => strtoupper('H. Afghani Jayuska, S.Si., M.Si.'),
            'username' => strtolower('197107072000121001'),
            'email' => strtolower('afghanijayuska@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $afghani->user->activation()->create(['user_id' => $afghani->user->id, 'kode' => Str::random(20)]);
        $afghani->user->assignRole('dosen');

        $titin = Pegawai::firstOrCreate([
            'firstname'         => 'Titin', 
            'middlename'        => 'Anita', 
            'lastname'          => 'Zaharah', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('196904191996012002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $titin->user()->firstOrCreate([
            'name' => strtoupper('Titin Anita Zaharah, S.Si., M.Sc.'),
            'username' => strtolower('196904191996012002'),
            'email' => strtolower('titinanitazaharah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $titin->user->activation()->create(['user_id' => $titin->user->id, 'kode' => Str::random(20)]);
        $titin->user->assignRole('dosen');

        $puji = Pegawai::firstOrCreate([
            'firstname'         => 'Puji', 
            'middlename'        => 'Ardiningsih', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('197201271998022001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $puji->user()->firstOrCreate([
            'name' => strtoupper('Puji Ardiningsih, S.Si., M.Si.'),
            'username' => strtolower('197201271998022001'),
            'email' => strtolower('pujiardiningsih@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $puji->user->activation()->create(['user_id' => $puji->user->id, 'kode' => Str::random(20)]);
        $puji->user->assignRole('dosen');

        $endah = Pegawai::firstOrCreate([
            'firstname'         => 'Endah', 
            'middlename'        => 'Sayekti',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('197206222000122001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $endah->user()->firstOrCreate([
            'name' => strtoupper('Dr. Endah Sayekti, S.Si., M.Si.'),
            'username' => strtolower('197206222000122001'),
            'email' => strtolower('endahsayekti@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $endah->user->activation()->create(['user_id' => $endah->user->id, 'kode' => Str::random(20)]);
        $endah->user->assignRole(['sekretaris-jurusan', 'dosen']);

        $berlian = Pegawai::firstOrCreate([
            'firstname'         => 'Berlian', 
            'middlename'        => 'Sitorus',
            'gelar_belakang'    => 'S.Si., M.Si., M.Sc., Ph.D.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('197410102000122006'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $berlian->user()->firstOrCreate([
            'name' => strtoupper('Berlian Sitorus, S.Si., M.Si., M.Sc., Ph.D.'),
            'username' => strtolower('197410102000122006'),
            'email' => strtolower('serliansitorus@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $berlian->user->activation()->create(['user_id' => $berlian->user->id, 'kode' => Str::random(20)]);
        $berlian->user->assignRole('dosen');

        $anthoni = Pegawai::firstOrCreate([
            'firstname'         => 'Anthoni', 
            'middlename'        => 'Batahan', 
            'lastname'          => 'Aritonang', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('196803082000031001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $anthoni->user()->firstOrCreate([
            'name' => strtoupper('Dr. Anthoni Batahan Aritonang, S.Si., M.Si.'),
            'username' => strtolower('196803082000031001'),
            'email' => strtolower('anthonibatahanaritonang@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $anthoni->user->activation()->create(['user_id' => $anthoni->user->id, 'kode' => Str::random(20)]);
        $anthoni->user->assignRole('dosen');

        $imelda = Pegawai::firstOrCreate([
            'firstname'         => 'Imelda', 
            'middlename'        => 'Hotmarisi',
            'lastname'          => 'Silalahi',
            'gelar_belakang'    => 'S.Si., M.Si., Ph.D.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('197605062000122001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $imelda->user()->firstOrCreate([
            'name' => strtoupper('Imelda Hotmarisi Silalahi, S.Si., M.Si., Ph.D.'),
            'username' => strtolower('197605062000122001'),
            'email' => strtolower('imeldahotmarisisilalahi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $imelda->user->activation()->create(['user_id' => $imelda->user->id, 'kode' => Str::random(20)]);
        $imelda->user->assignRole(['ketua-prodi', 'dosen']);

        $harlia = Pegawai::firstOrCreate([
            'firstname'         => 'Harlia', 
            'gelar_depan'       => 'Dra.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('195909161989032002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $harlia->user()->firstOrCreate([
            'name' => strtoupper('Dra. Harlia, M.Si'),
            'username' => strtolower('195909161989032002'),
            'email' => strtolower('harlia@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $harlia->user->activation()->create(['user_id' => $harlia->user->id, 'kode' => Str::random(20)]);
        $harlia->user->assignRole('dosen');

        $ajuk = Pegawai::firstOrCreate([
            'firstname'         => 'Ajuk', 
            'middlename'        => 'Sapar',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('197112312000121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ajuk->user()->firstOrCreate([
            'name' => strtoupper('Dr. Ajuk Sapar, S.Si., M.Si.'),
            'username' => strtolower('197112312000121001'),
            'email' => strtolower('ajuksapar@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ajuk->user->activation()->create(['user_id' => $ajuk->user->id, 'kode' => Str::random(20)]);
        $ajuk->user->assignRole('dosen');

        $intan = Pegawai::firstOrCreate([
            'firstname'         => 'Intan', 
            'middlename'        => 'Syahbanu',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('198511042012122002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $intan->user()->firstOrCreate([
            'name' => strtoupper('Intan Syahbanu, S.Si., M.Si.'),
            'username' => strtolower('198511042012122002'),
            'email' => strtolower('intansyahbanu@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $intan->user->activation()->create(['user_id' => $intan->user->id, 'kode' => Str::random(20)]);
        $intan->user->assignRole('dosen');

        $winda = Pegawai::firstOrCreate([
            'firstname'         => 'Winda', 
            'middlename'        => 'Rahmalia',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('198402272008122004'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $winda->user()->firstOrCreate([
            'name' => strtoupper('Dr. Winda Rahmalia, S.Si., M.Si.'),
            'username' => strtolower('198402272008122004'),
            'email' => strtolower('windarahmalia@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $winda->user->activation()->create(['user_id' => $winda->user->id, 'kode' => Str::random(20)]);
        $winda->user->assignRole('dosen');

        $lia = Pegawai::firstOrCreate([
            'firstname'         => 'Lia', 
            'middlename'        => 'Destiarti',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('198312022008122002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $lia->user()->firstOrCreate([
            'name' => strtoupper('Lia Destiarti, S.Si., M.Si.'),
            'username' => strtolower('198312022008122002'),
            'email' => strtolower('liadestiarti@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $lia->user->activation()->create(['user_id' => $lia->user->id, 'kode' => Str::random(20)]);
        $lia->user->assignRole('dosen');

        $nurlina = Pegawai::firstOrCreate([
            'firstname'         => 'Nurlina', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('198510232012122002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $nurlina->user()->firstOrCreate([
            'name' => strtoupper('Nurlina, S.Si., M.Sc.'),
            'username' => strtolower('198510232012122002'),
            'email' => strtolower('nurlina@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $nurlina->user->activation()->create(['user_id' => $nurlina->user->id, 'kode' => Str::random(20)]);
        $nurlina->user->assignRole('dosen');

        $adhitiyawarman = Pegawai::firstOrCreate([
            'firstname'         => 'Adhitiyawarman', 
            'gelar_belakang'    => 'S.Si., M.Si., Ph.D.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('198409192008121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $adhitiyawarman->user()->firstOrCreate([
            'name' => strtoupper('Adhitiyawarman, S.Si., M.Si., Ph.D.'),
            'username' => strtolower('198409192008121001'),
            'email' => strtolower('adhitiyawarman@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $adhitiyawarman->user->activation()->create(['user_id' => $adhitiyawarman->user->id, 'kode' => Str::random(20)]);
        $adhitiyawarman->user->assignRole('dosen');

        $firman = Pegawai::firstOrCreate([
            'firstname'         => 'Firman', 
            'middlename'        => 'Shantya',
            'lastname'          => 'Budi',
            'gelar_belakang'    => 'M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PPPK', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('198905292023211027'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $firman->user()->firstOrCreate([
            'name' => strtoupper('Firman Shantya Budi, M.Sc.'),
            'username' => strtolower('198905292023211027'),
            'email' => strtolower('firmanshantyabudi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $firman->user->activation()->create(['user_id' => $firman->user->id, 'kode' => Str::random(20)]);
        $firman->user->assignRole('dosen');

        $risya = Pegawai::firstOrCreate([
            'firstname'         => 'Risya', 
            'middlename'        => 'Sasri', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PPPK', 
            'prodi_id'          => 5, 
            'nomor_unik'        => strtolower('199003102023212041'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $risya->user()->firstOrCreate([
            'name' => strtoupper('Risya Sasri, S.Si., M.Sc.'),
            'username' => strtolower('199003102023212041'),
            'email' => strtolower('risyasasri@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $risya->user->activation()->create(['user_id' => $risya->user->id, 'kode' => Str::random(20)]);
        $risya->user->assignRole('dosen');

    }
}
