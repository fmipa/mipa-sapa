<?php

namespace Database\Seeders;

use App\Models\KategoriSuratKeluar;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class KategoriSuratKeluarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $kategoriSuratKeluars = [
            ['kategori' => 'SURAT TUGAS' , 'kode' => 'ST', 'deskripsi' => 'Surat Tugas untuk Pegawai', 'created_by' => 1],
            ['kategori' => 'SURAT UNDANGAN' , 'kode' => 'SU', 'deskripsi' => 'Surat Undangan Pegawai', 'created_by' => 1],
            
        ];

        KategoriSuratKeluar::insert($kategoriSuratKeluars);
    }
}
