<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class Userseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        // $superAdmin = Pegawai::firstOrCreate([
        //     'firstname'     => 'Super Admin',
        //     'nomor_unik'    => 7,
        // ]);
        // $superAdmin->user()->firstOrCreate([
        //     'operator'          => 1,
        //     'name'              => strtoupper('Super Admin'),
        //     'username'          => strtolower('superadmin7'),
        //     'email'             => 'superadmin@email.com',
        //     'email_verified_at' => now(),
        //     'password'          => '1sampai8',
        //     'remember_token'    => Str::random(20),
        // ]);

        // $admin = Pegawai::firstOrCreate([
        //     'firstname'     => 'Administrator',
        //     'team_id'       => 1, 
        //     'nomor_unik'    => 8,
        // ]);
        // $admin->user()->firstOrCreate([
        //     'operator'          => 1,
        //     'name'              => strtoupper('Administrator'),
        //     'username'          => strtolower('admin8'),
        //     'email'             => 'admin@email.com',
        //     'email_verified_at' => now(),
        //     'password'          => '1sampai8',
        //     'remember_token'    => Str::random(20),
        // ]);

        // $superAdmin->user->activation()->create(['user_id' => $superAdmin->user->id, 'kode' => Str::random(20)]);
        // $superAdmin->user->assignRole('super-admin');

        // $admin->user->activation()->create(['user_id' => $admin->user->id, 'kode' => Str::random(20)]);
        // $admin->user->assignRole('admin');


        // ! AKUN OPERATOR UNIT KERJA FAKULTAS
        $akademik = Pegawai::firstOrCreate([
            'firstname'     => 'AKADEMIK',
            'middlename'    => 'FMIPA',
            'lastname'      => 'UNTAN',
            'team_id'       => 4,
            'unit_kerja_id' => UnitKerja::whereNama('AKADEMIK')->value('id'),
        ]);
        $akademik->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('AKADEMIK FMIPA UNTAN'),
            'username'          => strtolower('akademik'),
            'email'             => 'akademik@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $akademik->user->activation()->create(['user_id' => $akademik->user->id, 'kode' => Str::random(20)]);
        $akademik->user->assignRole('akademik');

        $keuangan = Pegawai::firstOrCreate([
            'firstname'     => 'KEUANGAN',
            'middlename'    => 'FMIPA',
            'lastname'      => 'UNTAN',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('KEUANGAN')->value('id'), 
        ]);
        $keuangan->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('KEUANGAN FMIPA UNTAN'),
            'username'          => strtolower('keuangan'),
            'email'             => 'keuangan@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $keuangan->user->activation()->create(['user_id' => $keuangan->user->id, 'kode' => Str::random(20)]);
        $keuangan->user->assignRole('keuangan');

        $kepegawaian = Pegawai::firstOrCreate([
            'firstname'     => 'KEPEGAWAIAN',
            'middlename'    => 'FMIPA',
            'lastname'      => 'UNTAN',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('KEPEGAWAIAN')->value('id'), 
        ]);
        $kepegawaian->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('KEPEGAWAIAN FMIPA UNTAN'),
            'username'          => strtolower('kepegawaian'),
            'email'             => 'kepegawaian@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $kepegawaian->user->activation()->create(['user_id' => $kepegawaian->user->id, 'kode' => Str::random(20)]);
        $kepegawaian->user->assignRole('kepegawaian');

        $umper = Pegawai::firstOrCreate([
            'firstname'     => 'UMPER',
            'middlename'    => 'FMIPA',
            'lastname'      => 'UNTAN',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('UMUM DAN PERLENGKAPAN')->value('id'), 
        ]);
        $umper->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('UMPER FMIPA UNTAN'),
            'username'          => strtolower('umper'),
            'email'             => 'umper@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $umper->user->activation()->create(['user_id' => $umper->user->id, 'kode' => Str::random(20)]);
        $umper->user->assignRole('umper');

        // ! AKUN OPERATOR JURUSAN
        $biologi = Pegawai::firstOrCreate([
            'firstname'     => 'ADMIN',
            'middlename'    => 'JURUSAN',
            'lastname'      => 'BIOLOGI',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('JURUSAN')->value('id'), 
            'prodi_id'      => 1, 
        ]);
        $biologi->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('ADMIN JURUSAN BIOLOGI'),
            'username'          => strtolower('biologi'),
            'email'             => 'biologi@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $biologi->user->activation()->create(['user_id' => $biologi->user->id, 'kode' => Str::random(20)]);
        $biologi->user->assignRole('admin-jurusan');

        $fisika = Pegawai::firstOrCreate([
            'firstname'     => 'ADMIN',
            'middlename'    => 'JURUSAN',
            'lastname'      => 'FISIKA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('JURUSAN')->value('id'), 
            'prodi_id'      => 2, 
        ]);
        $fisika->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('ADMIN JURUSAN FISIKA'),
            'username'          => strtolower('fisika'),
            'email'             => 'fisika@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $fisika->user->activation()->create(['user_id' => $fisika->user->id, 'kode' => Str::random(20)]);
        $fisika->user->assignRole('admin-jurusan');

        $ilmukelautan = Pegawai::firstOrCreate([
            'firstname'     => 'ADMIN',
            'middlename'    => 'JURUSAN',
            'lastname'      => 'ILMU KELAUTAN',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('JURUSAN')->value('id'), 
            'prodi_id'      => 4, 
        ]);
        $ilmukelautan->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('ADMIN JURUSAN ILMU KELAUTAN'),
            'username'          => strtolower('ilmukelautan'),
            'email'             => 'ilmukelautan@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $ilmukelautan->user->activation()->create(['user_id' => $ilmukelautan->user->id, 'kode' => Str::random(20)]);
        $ilmukelautan->user->assignRole('admin-jurusan');

        $kimia = Pegawai::firstOrCreate([
            'firstname'     => 'ADMIN',
            'middlename'    => 'JURUSAN',
            'lastname'      => 'KIMIA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('JURUSAN')->value('id'), 
            'prodi_id'      => 5, 
        ]);
        $kimia->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('ADMIN JURUSAN KIMIA'),
            'username'          => strtolower('kimia'),
            'email'             => 'kimia@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $kimia->user->activation()->create(['user_id' => $kimia->user->id, 'kode' => Str::random(20)]);
        $kimia->user->assignRole('admin-jurusan');

        $kimias2 = Pegawai::firstOrCreate([
            'firstname'     => 'ADMIN',
            'middlename'    => 'JURUSAN',
            'lastname'      => 'KIMIA S2',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('JURUSAN')->value('id'), 
            'prodi_id'      => 6, 
        ]);
        $kimias2->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('ADMIN JURUSAN KIMIA S2'),
            'username'          => strtolower('kimias2'),
            'email'             => 'kimias2@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $kimias2->user->activation()->create(['user_id' => $kimias2->user->id, 'kode' => Str::random(20)]);
        $kimias2->user->assignRole('admin-jurusan');

        $matematika = Pegawai::firstOrCreate([
            'firstname'     => 'ADMIN',
            'middlename'    => 'JURUSAN',
            'lastname'      => 'MATEMATIKA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('JURUSAN')->value('id'), 
            'prodi_id'      => 7, 
        ]);
        $matematika->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('ADMIN JURUSAN MATEMATIKA'),
            'username'          => strtolower('matematika'),
            'email'             => 'matematika@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $matematika->user->activation()->create(['user_id' => $matematika->user->id, 'kode' => Str::random(20)]);
        $matematika->user->assignRole('admin-jurusan');

        $resiskom = Pegawai::firstOrCreate([
            'firstname'     => 'ADMIN',
            'middlename'    => 'JURUSAN',
            'lastname'      => 'RESISKOM',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('JURUSAN')->value('id'), 
            'prodi_id'      => 9, 
        ]);
        $resiskom->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('ADMIN JURUSAN RESISKOM'),
            'username'          => strtolower('resiskom'),
            'email'             => 'resiskom@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $resiskom->user->activation()->create(['user_id' => $resiskom->user->id, 'kode' => Str::random(20)]);
        $resiskom->user->assignRole('admin-jurusan');

        $sisfo = Pegawai::firstOrCreate([
            'firstname'     => 'ADMIN',
            'middlename'    => 'JURUSAN',
            'lastname'      => 'SISFO',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('JURUSAN')->value('id'), 
            'prodi_id'      => 10, 
        ]);
        $sisfo->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('ADMIN JURUSAN SISFO'),
            'username'          => strtolower('sisfo'),
            'email'             => 'sisfo@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $sisfo->user->activation()->create(['user_id' => $sisfo->user->id, 'kode' => Str::random(20)]);
        $sisfo->user->assignRole('admin-jurusan');

        $labsisfo = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'SISFO',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 10, 
        ]);
        $labsisfo->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN SISFO'),
            'username'          => strtolower('laboransisfo'),
            'email'             => 'laborangsisfo@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labsisfo->user->activation()->create(['user_id' => $labsisfo->user->id, 'kode' => Str::random(20)]);
        $labsisfo->user->assignRole('laboran');

        $labsiskom = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'SISKOM',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 9, 
        ]);
        $labsiskom->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN SISKOM'),
            'username'          => strtolower('laboransiskom'),
            'email'             => 'laboransiskom@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labsiskom->user->activation()->create(['user_id' => $labsiskom->user->id, 'kode' => Str::random(20)]);
        $labsiskom->user->assignRole('laboran');

        $labbiologi = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'BIOLOGI',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 1, 
        ]);
        $labbiologi->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN BIOLOGI'),
            'username'          => strtolower('laboranbiologi'),
            'email'             => 'laboranbiologi@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labbiologi->user->activation()->create(['user_id' => $labbiologi->user->id, 'kode' => Str::random(20)]);
        $labbiologi->user->assignRole('laboran');

        $labfisika = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'FISIKA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 2, 
        ]);
        $labfisika->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN FISIKA'),
            'username'          => strtolower('laboranfisika'),
            'email'             => 'laboranfisika@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labfisika->user->activation()->create(['user_id' => $labfisika->user->id, 'kode' => Str::random(20)]);
        $labfisika->user->assignRole('laboran');

        $labgeofisika = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'GEOFISIKA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 3, 
        ]);
        $labgeofisika->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN GEOFISIKA'),
            'username'          => strtolower('laborangeofisika'),
            'email'             => 'laborangeofisika@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labgeofisika->user->activation()->create(['user_id' => $labgeofisika->user->id, 'kode' => Str::random(20)]);
        $labgeofisika->user->assignRole('laboran');

        $labilkel = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'ILMU',
            'lastname'      => 'KELAUTAN',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 4, 
        ]);
        $labilkel->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN ILMU KELAUTAN'),
            'username'          => strtolower('laboranilkel'),
            'email'             => 'laboranilmukelautan@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labilkel->user->activation()->create(['user_id' => $labilkel->user->id, 'kode' => Str::random(20)]);
        $labilkel->user->assignRole('laboran');

        $labmtk = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'MATEMATIKA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 7, 
        ]);
        $labmtk->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN MATEMATIKA'),
            'username'          => strtolower('laboranmtk'),
            'email'             => 'laboranmatematika@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labmtk->user->activation()->create(['user_id' => $labmtk->user->id, 'kode' => Str::random(20)]);
        $labmtk->user->assignRole('laboran');

        $labstatistika = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'STATISTIKA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 8, 
        ]);
        $labstatistika->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN STATISTIKA'),
            'username'          => strtolower('laboranstatistika'),
            'email'             => 'laboranmastatistika@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labstatistika->user->activation()->create(['user_id' => $labstatistika->user->id, 'kode' => Str::random(20)]);
        $labstatistika->user->assignRole('laboran');

        $labkimia = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'KIMIA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 5, 
        ]);
        $labkimia->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN KIMIA'),
            'username'          => strtolower('laborankimia'),
            'email'             => 'laborankimia@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labkimia->user->activation()->create(['user_id' => $labkimia->user->id, 'kode' => Str::random(20)]);
        $labkimia->user->assignRole('laboran');

        $labs2kimia = Pegawai::firstOrCreate([
            'firstname'     => 'LABORAN',
            'middlename'    => 'S2',
            'lastname'      => 'KIMIA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('LABORATORIUM')->value('id'), 
            'prodi_id'      => 6, 
        ]);
        $labs2kimia->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('LABORAN S2 KIMIA'),
            'username'          => strtolower('laborans2kimia'),
            'email'             => 'laborans2kimia@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $labs2kimia->user->activation()->create(['user_id' => $labs2kimia->user->id, 'kode' => Str::random(20)]);
        $labs2kimia->user->assignRole('laboran');

        $ruangbaca = Pegawai::firstOrCreate([
            'firstname'     => 'RUANG',
            'middlename'    => 'BACA',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('RUANG BACA')->value('id'), 
        ]);
        $ruangbaca->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('RUANG BACA'),
            'username'          => strtolower('ruangbaca'),
            'email'             => 'ruangbaca@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $ruangbaca->user->activation()->create(['user_id' => $ruangbaca->user->id, 'kode' => Str::random(20)]);
        $ruangbaca->user->assignRole('ruang-baca');

        $tik = Pegawai::firstOrCreate([
            'firstname'     => 'TIM',
            'middlename'    => 'TIK',
            'team_id'       => 4, 
            'unit_kerja_id' => UnitKerja::whereNama('TIM TIK')->value('id'), 
        ]);
        $tik->user()->firstOrCreate([
            'operator'          => 1,
            'name'              => strtoupper('TIM TIK'),
            'username'          => strtolower('timtik'),
            'email'             => 'timtik@fmipa.untan.ac.id',
            'email_verified_at' => now(),
            'password'          => 'mipauntan',
            'remember_token'    => Str::random(20),
        ]);
        $tik->user->activation()->create(['user_id' => $tik->user->id, 'kode' => Str::random(20)]);
        $tik->user->assignRole('tik');
    }
}
