<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenGeofisikaseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $andi = Pegawai::firstOrCreate([
            'firstname'         => 'Andi', 
            'middlename'        => 'Ihwan',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('197310082002121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $andi->user()->firstOrCreate([
            'name' => strtoupper('Dr. Andi Ihwan, S.Si., M.Si.'),
            'username' => strtolower('197310082002121001'),
            'email' => strtolower('andiihwan@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $andi->user->activation()->create(['user_id' => $andi->user->id, 'kode' => Str::random(20)]);
        $andi->user->assignRole('dosen');

        $ishak = Pegawai::firstOrCreate([
            'firstname'         => 'Muhammad', 
            'middlename'        => 'Ishak', 
            'lastname'          => 'Jumarang', 
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('197409212003121004'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $ishak->user()->firstOrCreate([
            'name' => strtoupper('Dr. Muhammad Ishak Jumarang, S.Si., M.Si.'),
            'username' => strtolower('197409212003121004'),
            'email' => strtolower('muhammadishakjumarang@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $ishak->user->activation()->create(['user_id' => $ishak->user->id, 'kode' => Str::random(20)]);
        $ishak->user->assignRole('dosen');

        $boni = Pegawai::firstOrCreate([
            'firstname'         => 'Boni', 
            'firstname'         => 'Pahlanop', 
            'firstname'         => 'Lapanporo', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('198011102005011002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $boni->user()->firstOrCreate([
            'name' => strtoupper('Boni Pahlanop Lapanporo, S.Si., M.Sc.'),
            'username' => strtolower('198011102005011002'),
            'email' => strtolower('bonipahlanoplapanporo@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $boni->user->activation()->create(['user_id' => $boni->user->id, 'kode' => Str::random(20)]);
        $boni->user->assignRole('dosen');

        $muliadi = Pegawai::firstOrCreate([
            'firstname'         => 'Muliadi', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('197005101999031003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $muliadi->user()->firstOrCreate([
            'name' => strtoupper('Muliadi, S.Si., M.Si.'),
            'username' => strtolower('197005101999031003'),
            'email' => strtolower('muliadi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $muliadi->user->activation()->create(['user_id' => $muliadi->user->id, 'kode' => Str::random(20)]);
        $muliadi->user->assignRole('dosen');

        $yoga = Pegawai::firstOrCreate([
            'firstname'         => 'Yoga', 
            'middlename'        => 'Satria',
            'lastname'          => 'Putra',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('19791025200501100'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $yoga->user()->firstOrCreate([
            'name' => strtoupper('Dr. Yoga Satria Putra, S.Si., M.Si.'),
            'username' => strtolower('19791025200501100'),
            'email' => strtolower('yogasatriputra@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $yoga->user->activation()->create(['user_id' => $yoga->user->id, 'kode' => Str::random(20)]);
        $yoga->user->assignRole(['ketua-prodi', 'dosen']);

        $joko = Pegawai::firstOrCreate([
            'firstname'         => 'Joko', 
            'middlename'        => 'Sampurno',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('198408252008011004'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $joko->user()->firstOrCreate([
            'name' => strtoupper('Dr. Joko Sampurno, S.Si., M.Si.'),
            'username' => strtolower('198408252008011004'),
            'email' => strtolower('jokosampurno@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $joko->user->activation()->create(['user_id' => $joko->user->id, 'kode' => Str::random(20)]);
        $joko->user->assignRole('dosen');

        $muhardi = Pegawai::firstOrCreate([
            'firstname'         => 'Muhardi', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('198509192018031001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $muhardi->user()->firstOrCreate([
            'name' => strtoupper('Muhardi, S.Si., M.Sc.'),
            'username' => strtolower('198509192018031001'),
            'email' => strtolower('muhardi@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $muhardi->user->activation()->create(['user_id' => $muhardi->user->id, 'kode' => Str::random(20)]);
        $muhardi->user->assignRole('dosen');

        $riza = Pegawai::firstOrCreate([
            'firstname'         => 'Riza', 
            'middlename'        => 'Adriat',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('198905162019031013'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $riza->user()->firstOrCreate([
            'name' => strtoupper('Riza Adriat, S.Si., M.Si.'),
            'username' => strtolower('198905162019031013'),
            'email' => strtolower('rizaadriat@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $riza->user->activation()->create(['user_id' => $riza->user->id, 'kode' => Str::random(20)]);
        $riza->user->assignRole('dosen');

        $radhitya = Pegawai::firstOrCreate([
            'firstname'         => 'Radhitya', 
            'middlename'        => 'Perdhana',
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 3, 
            'nomor_unik'        => strtolower('198911142019031011'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $radhitya->user()->firstOrCreate([
            'name' => strtoupper('Radhitya Perdhana, S.Si., M.Sc.'),
            'username' => strtolower('198911142019031011'),
            'email' => strtolower('radhityaperdhana@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $radhitya->user->activation()->create(['user_id' => $radhitya->user->id, 'kode' => Str::random(20)]);
        $radhitya->user->assignRole('dosen');

    }
}
