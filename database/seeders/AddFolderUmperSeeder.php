<?php

namespace Database\Seeders;

use App\Models\Folder;
use App\Models\Prodi;
use App\Traits\Mutators\EncryptId;
use Illuminate\Database\Seeder;

class AddFolderUmperSeeder extends Seeder
{
    use EncryptId;
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $prodis         = Prodi::all();
        $operatorUmper  = 171;
        $unitKerjaId    = 9;
        $folderUtama    = ['Surat Masuk', 'Surat Keluar', 'Surat Kepututsan (SK)', 'Barang Milik Negara (BMN)', 'Persediaan'];

        foreach ($folderUtama as $key => $utama) {
            $PUtama = Folder::create(['nama' => $utama, 'urutan' => ($key+1), 'parent_id' => 0, 'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $utama)), 'created_by' => $operatorUmper]);

            switch ($utama) {
                case 'Surat Kepututsan (SK)':
                    $PFakSK = Folder::create(['nama' => 'Fakultas', 'urutan' => 1, 'parent_id' => $PUtama->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', 'Fakultas')), 'created_by' => $operatorUmper]);
                    for ($i=0; $i < 7; $i++) { 
                        $PTAFak = Folder::create(['nama' => 'Tahun '.(2018+$i), 'urutan' => ($i+1), 'parent_id' => $PFakSK->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => NULL, 'slug' => strtolower(str_replace(' ', '-', 'Tahun '.(2018+$i))), 'created_by' => $operatorUmper]);
                    }

                    foreach ($prodis as $keyProdi => $prodi) {
                        $namaProdi = 'Prodi '.$prodi->nama;
                        $PProdiSK = Folder::create(['nama' => $namaProdi, 'urutan' => ($keyProdi+2), 'parent_id' => $PUtama->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $namaProdi)), 'created_by' => $operatorUmper]);

                        for ($i=0; $i < 7; $i++) { 
                            $namaTASK = 'Tahun '.(2018+$i);
                            $PTAProdi = Folder::create(['nama' => $namaTASK, 'urutan' => ($i+1), 'parent_id' => $PProdiSK->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => NULL, 'slug' => strtolower(str_replace(' ', '-', $namaTASK)), 'created_by' => $operatorUmper]);
                        }
                    }
                    break;
                
                case 'Barang Milik Negara (BMN)':
                    $BMNS = ['DAST', 'DBR'];
                    foreach ($BMNS as $keyBMN => $BMN) {
                        $PBMN = Folder::create(['nama' => $BMN, 'urutan' => ($keyBMN+1), 'parent_id' => $PUtama->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $BMN)), 'created_by' => $operatorUmper]);

                        for ($i=0; $i < 7; $i++) { 
                            $TABMN = 'Tahun '.(2018+$i);
                            $PTAProdi = Folder::create(['nama' => $TABMN, 'urutan' => ($i+1), 'parent_id' => $PBMN->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => NULL, 'slug' => strtolower(str_replace(' ', '-', $TABMN)), 'created_by' => $operatorUmper]);
                        }
                    }
                    
                    break;

                case 'Persediaan':
                    $Persediaans = ['Pemakaian', 'Permohonan', 'Stock Opname'];
                    foreach ($Persediaans as $keyPersediaan => $persediaan) {
                        $Ppersediaan = Folder::create(['nama' => $persediaan, 'urutan' => ($keyPersediaan+1), 'parent_id' => $PUtama->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => null, 'slug' => strtolower(str_replace(' ', '-', $persediaan)), 'created_by' => $operatorUmper]);

                        for ($i=0; $i < 7; $i++) { 
                            $TApersediaan = 'Tahun '.(2018+$i);
                            $PTApersediaan = Folder::create(['nama' => $TApersediaan, 'urutan' => ($i+1), 'parent_id' => $Ppersediaan->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => NULL, 'slug' => strtolower(str_replace(' ', '-', $TApersediaan)), 'created_by' => $operatorUmper]);
                        }
                    }
                    
                    break;
                
                default:
                    for ($i=0; $i < 7; $i++) { 
                        $TASurat = 'Tahun '.(2018+$i);
                        $PTASurat = Folder::create(['nama' => $TASurat, 'urutan' => ($i+1), 'parent_id' => $PUtama->id, 'parent_utama' => $PUtama->id,  'unit_kerja_id' => $unitKerjaId, 'prodi_id' => NULL, 'slug' => strtolower(str_replace(' ', '-', $TASurat)), 'created_by' => $operatorUmper]);
                    }
                    break;
            }

        }
    }
}
