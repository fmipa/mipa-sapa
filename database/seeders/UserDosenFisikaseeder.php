<?php

namespace Database\Seeders;

use App\Models\Pegawai;
use App\Models\UnitKerja;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserDosenFisikaseeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $bintoro = Pegawai::firstOrCreate([
            'firstname'         => 'Bintoro', 
            'middlename'        => 'Siswo',
            'lastname'          => 'Nugroho',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('198102062006041003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $bintoro->user()->firstOrCreate([
            'name' => strtoupper('Dr. Bintoro Siswo Nugroho, S.Si., M.Si.'),
            'username' => strtolower('198102062006041003'),
            'email' => strtolower('bintorosiswonugroho@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $bintoro->user->activation()->create(['user_id' => $bintoro->user->id, 'kode' => Str::random(20)]);
        $bintoro->user->assignRole(['ketua-jurusan', 'dosen']);

        $yudha = Pegawai::firstOrCreate([
            'firstname'         => 'Yudha', 
            'middlename'        => 'Arman', 
            'gelar_belakang'    => 'S.Si., M.Si., D.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('197805132003121002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $yudha->user()->firstOrCreate([
            'name' => strtoupper('Yudha Arman, S.Si, M.Si., D.Sc.'),
            'username' => strtolower('197805132003121002'),
            'email' => strtolower('yudhaarman@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $yudha->user->activation()->create(['user_id' => $yudha->user->id, 'kode' => Str::random(20)]);
        $yudha->user->assignRole(['wakil-dekan', 'dosen']);

        $mariana = Pegawai::firstOrCreate([
            'firstname'         => 'Mariana', 
            'middlename'        => "Bara'allo", 
            'lastname'          => 'Malino', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('197603082002122001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $mariana->user()->firstOrCreate([
            'name' => strtoupper("Mariana Bara'allo Malino, S.Si., M.Sc."),
            'username' => strtolower('197603082002122001'),
            'email' => strtolower('marianabaraallomalino@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $mariana->user->activation()->create(['user_id' => $mariana->user->id, 'kode' => Str::random(20)]);
        $mariana->user->assignRole('dosen');

        $nurhasanah = Pegawai::firstOrCreate([
            'firstname'         => 'Nurhasanah', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('198011252006042002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $nurhasanah->user()->firstOrCreate([
            'name' => strtoupper('Nurhasanah, S.Si., M.Si.'),
            'username' => strtolower('198011252006042002'),
            'email' => strtolower('nurhasanah@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $nurhasanah->user->activation()->create(['user_id' => $nurhasanah->user->id, 'kode' => Str::random(20)]);
        $nurhasanah->user->assignRole('dosen');

        $azrul = Pegawai::firstOrCreate([
            'firstname'         => 'Azrul', 
            'middlename'        => 'Azwar',
            'gelar_depan'       => 'Dr.',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('198107302005011002'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $azrul->user()->firstOrCreate([
            'name' => strtoupper('Dr. Azrul Azwar, S.Si., M.Si.'),
            'username' => strtolower('198107302005011002'),
            'email' => strtolower('azrulazwar@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $azrul->user->activation()->create(['user_id' => $azrul->user->id, 'kode' => Str::random(20)]);
        $azrul->user->assignRole(['ketua-prodi', 'dosen']);

        $hasanuddin = Pegawai::firstOrCreate([
            'firstname'         => 'Hasanuddin', 
            'gelar_belakang'    => 'S.Si., M.Si., Ph.D.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('198412162008121003'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $hasanuddin->user()->firstOrCreate([
            'name' => strtoupper('Hasanuddin, S.Si., M.Si., Ph.D.'),
            'username' => strtolower('198412162008121003'),
            'email' => strtolower('hasanuddin@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $hasanuddin->user->activation()->create(['user_id' => $hasanuddin->user->id, 'kode' => Str::random(20)]);
        $hasanuddin->user->assignRole(['sekretaris-jurusan', 'dosen']);

        $dwiria = Pegawai::firstOrCreate([
            'firstname'         => 'Dwiria', 
            'middlename'        => 'Wahyuni', 
            'gelar_belakang'    => 'S.Si., M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('198206082008122001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $dwiria->user()->firstOrCreate([
            'name' => strtoupper('Dr.Dwiria Wahyuni, S.Si., M.Sc.'),
            'username' => strtolower('198206082008122001'),
            'email' => strtolower('dwiriawahyuni@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $dwiria->user->activation()->create(['user_id' => $dwiria->user->id, 'kode' => Str::random(20)]);
        $dwiria->user->assignRole('dosen');

        $abdul = Pegawai::firstOrCreate([
            'firstname'         => 'Abdul', 
            'middlename'        => 'Muid',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('198012172008121001'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $abdul->user()->firstOrCreate([
            'name' => strtoupper('Abdul Muid, S.Si., M.Si.'),
            'username' => strtolower('198012172008121001'),
            'email' => strtolower('abdulmuid@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $abdul->user->activation()->create(['user_id' => $abdul->user->id, 'kode' => Str::random(20)]);
        $abdul->user->assignRole('dosen');

        $irfana = Pegawai::firstOrCreate([
            'firstname'         => 'Irfana', 
            'middlename'        => 'Diah',
            'lastname'          => 'Faryuni',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('198510132008122004'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $irfana->user()->firstOrCreate([
            'name' => strtoupper('Irfana Diah Faryuni, S.Si., M.Si.'),
            'username' => strtolower('198510132008122004'),
            'email' => strtolower('irfanadiahfaryuni@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $irfana->user->activation()->create(['user_id' => $irfana->user->id, 'kode' => Str::random(20)]);
        $irfana->user->assignRole('dosen');

        $zulfian = Pegawai::firstOrCreate([
            'firstname'         => 'Zulfian', 
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('198812142020121005'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $zulfian->user()->firstOrCreate([
            'name' => strtoupper('Zulfian, S.Si., M.Si.'),
            'username' => strtolower('198812142020121005'),
            'email' => strtolower('zulfian@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $zulfian->user->activation()->create(['user_id' => $zulfian->user->id, 'kode' => Str::random(20)]);
        $zulfian->user->assignRole('dosen');

        $asifa = Pegawai::firstOrCreate([
            'firstname'         => 'Asifa', 
            'middlename'        => 'Asri',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('199006052022032010'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $asifa->user()->firstOrCreate([
            'name' => strtoupper('Asifa Asri, S.Si., M.Si.'),
            'username' => strtolower('199006052022032010'),
            'email' => strtolower('asifaasri@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $asifa->user->activation()->create(['user_id' => $asifa->user->id, 'kode' => Str::random(20)]);
        $asifa->user->assignRole('dosen');

        $yuris = Pegawai::firstOrCreate([
            'firstname'         => 'Yuris', 
            'middlename'        => 'Sutanto',
            'gelar_belakang'    => 'M.Sc.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'L', 
            'status'            => 'PNS', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('199008272022031008'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $yuris->user()->firstOrCreate([
            'name' => strtoupper('Yuris Sutanto, M.Sc.'),
            'username' => strtolower('199008272022031008'),
            'email' => strtolower('yurissutanto@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $yuris->user->activation()->create(['user_id' => $yuris->user->id, 'kode' => Str::random(20)]);
        $yuris->user->assignRole('dosen');

        $megaNurhanisa = Pegawai::firstOrCreate([
            'firstname'         => 'Mega', 
            'middlename'        => 'Nurhanisa',
            'gelar_belakang'    => 'S.Si., M.Si.',
            'team_id'           => 2,
            'jenis_kelamin'     => 'P', 
            'status'            => 'KONTRAK', 
            'prodi_id'          => 2, 
            'nomor_unik'        => strtolower('8853370018'),
            'unit_kerja_id'     => UnitKerja::whereNama('DOSEN')->value('id'),
        ]);
        $megaNurhanisa->user()->firstOrCreate([
            'name' => strtoupper('Mega Nurhanisa, S.Si, M.Si.'),
            'username' => strtolower('8853370018'),
            'email' => strtolower('meganurhanisa@fmipa.untan.ac.id'),
            'email_verified_at' => now(),
            'password' => 'mipauntan',
            'remember_token' => Str::random(20),
        ]);

        $megaNurhanisa->user->activation()->create(['user_id' => $megaNurhanisa->user->id, 'kode' => Str::random(20)]);
        $megaNurhanisa->user->assignRole('dosen');

    }
}
