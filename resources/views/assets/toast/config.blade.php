@if(Session::get('status'))
<script>
    "use strict";
    !function() {
        const t = document.querySelector(".toast-placement-ex");
        let o, s, c;
        $(document).ready(function () {
            // function() {
                var e;
                c && (e = c) && null !== e._element && (t && (t.classList.remove(o),
                DOMTokenList.prototype.remove.apply(t.classList, s)),
                e.dispose()),
                // bg-primary, bg-secondary, bg-success, bg-danger, bg-warning, bg-info, bg-dark
                o = "{{ Session::get('status') ? 'bg-success' : 'bg-danger' }}",
                // top-0 start-0, top-0 start-50 translate-middle-x, top-0 end-0, top-50 start-0 translate-middle-y, top-50 start-50 translate-middle, top-50 end-0 translate-middle-y, bottom-0 start-0, bottom-0 start-50 translate-middle-x, bottom-0 end-0
                s = ['top-0', 'end-0'],
                t.classList.add(o),
                DOMTokenList.prototype.add.apply(t.classList, s),
                (c = new bootstrap.Toast(t)).show();
                $(".toast-body").html("{{ Session::get('message') }}");
            // }
        });
    }();
</script>
@endif