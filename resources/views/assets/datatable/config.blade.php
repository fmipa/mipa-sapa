$("{{ $identityTable ?? '#table-data' }}").DataTable( {
            dom: 'Bfrtip',
            lengthMenu: [
                [ 10, 25, 50, -1 ],
                [ '10 rows', '25 rows', '50 rows', 'Show all' ]
            ],
            buttons: [
                'pageLength',
                {
                    extend: 'collection',
                    text: 'Export',
                    className: 'btn btn-info',
                    buttons: [
                        {
                            extend: 'copy',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'csv',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'excel',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'pdf',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                        {
                            extend: 'print',
                            exportOptions: {
                                columns: ':visible'
                            }
                        },
                    ]
                },
                {
                    extend: 'colvis',
                    
                    postfixButtons: [ 'colvisRestore' ],
                    text: 'Tampilkan Kolom',
                    collectionLayout: 'two-column',
                    columnText: function ( dt, idx, title ) {
                        return (idx+1)+': '+title;
                    },
                }
            ]
        } );
