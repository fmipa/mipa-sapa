<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Label {{ strtoupper($data['tipeLabel']) }} Nomor {{ $data['nomor'] }}</title>
    
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous"> --}}
    <style>
        .page-break {
            page-break-after: always;
        }
        * {
            text-transform: uppercase;
            font-weight: bold;
            font-size: 60%;
        }
        body {
            box-shadow: inset 0 0 0 9999px var(--bs-table-bg-state,var(--bs-table-bg-type,var(--bs-table-accent-bg)));
        }
        table {
            width: 380px !important;
            box-shadow: 2px 2px 2px 2px #888888;
            border-radius: 5px;
            --bs-table-color-type: initial;
            --bs-table-bg-type: initial;
            --bs-table-color-state: initial;
            --bs-table-bg-state: initial;
            --bs-table-color: var(--bs-emphasis-color);
            --bs-table-bg: var(--bs-body-bg);
            --bs-table-border-color: var(--bs-border-color);
            --bs-table-accent-bg: transparent;
            --bs-table-striped-color: var(--bs-emphasis-color);
            --bs-table-striped-bg: rgba(var(--bs-emphasis-color-rgb), 0.05);
            --bs-table-active-color: var(--bs-emphasis-color);
            --bs-table-active-bg: rgba(var(--bs-emphasis-color-rgb), 0.1);
            --bs-table-hover-color: var(--bs-emphasis-color);
            --bs-table-hover-bg: rgba(var(--bs-emphasis-color-rgb), 0.075);
            width: 100%;
            margin-bottom: 1rem;
            vertical-align: top;
            border-color: var(--bs-table-border-color);
            border: 2px solid black;

        }
        .table>:not(caption)>*>* {
            padding: 0.5rem 0.5rem;
            color: var(--bs-table-color-state,var(--bs-table-color-type,var(--bs-table-color)));
            background-color: var(--bs-table-bg);
            border-bottom-width: var(--bs-border-width);
            box-shadow: inset 0 0 0 9999px var(--bs-table-bg-state,var(--bs-table-bg-type,var(--bs-table-accent-bg)));
        }
        table tr > td{
            border: 1px solid black
        }
        #kepala, #kepala > td {
            text-align: center;
            border: unset;
            font-size: 20px;
        }
        #kepala img {
            padding-top: 20px;
            padding-bottom: 10px;
        }
        #arsip {
            padding-bottom: unset;
            margin-bottom: unset;
        }
        #fmipa, #untan {
            word-wrap: normal;
            margin-top: 0;
        }
        #fmipa {
            margin-top: unset;
            margin-bottom: unset;
            font-size: 10px!important;
        }
        #untan {
            margin-bottom: 10px;
        }
        #isi td:nth-child(2) {
            text-align: center;
            width: 70%;
        }
        #nomor  {
            font-size: 50px;
        }
        #isi td {
            padding: 10px;
        }
        #isi {
            font-size: 20px;
        }
    </style>
</head>
<body class="p-1">
    <table class="table">
        <tr id="kepala">
            <td colspan="2">
                <img src="{{ asset('images/logo/untan.png') }}" alt="Logo UNTAN" width="80px" >
            </td>
        </tr>
        <tr id="kepala">
            <td colspan="2">
                <h2 id="arsip">
                    ARSIP
                </h2>
                <div id="fmipa">
                    FAKULTAS MATEMATIKA DAN ILMU PENGETAHUAN ALAM
                </div>
                <div id="untan">
                    UNIVERSITAS TANJUNGPURA
                </div>
            </td>
        </tr>
        <tr id="isi">
            <td>Nomor Box</td>
            <td id="nomor">{{ $data['nomor'] }}</td>
        </tr>
        <tr id="isi">
            <td>Nama Box</td>
            <td>{{ $data['nama'] }}</td>
        </tr>
        <tr id="isi">
            <td>Unit Kerja</td>
            <td>{{ $data['bagian'] }}</td>
        </tr>
        <tr id="isi">
            <td>Lokasi Box</td>
            <td>{{ $data['lokasi'] }}</td>
        </tr>
        <tr id="isi">
            <td>Tahun</td>
            <td>{{ $data['tahun'] ?? '-' }}</td>
        </tr>
        <tr id="isi">
            <td>QRCode</td>
            <td id="qrcode">
                <img src="data:image/png;base64, {!! $data['qr_code'] !!}">
            </td>
        </tr>
    </table>


    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script> --}}
</body>
</html>