<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Label {{ strtoupper($data['tipeLabel']) }} Nomor {{ $data['nomor'] }}</title>
    
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous"> --}}
    <style>
        .page-break {
            page-break-after: always;
        }
        * {
            text-transform: uppercase;
            font-weight: bold;
        }
        body {
            width: 800px;
            height: 190px;
            box-shadow: inset 0 0 0 9999px var(--bs-table-bg-state,var(--bs-table-bg-type,var(--bs-table-accent-bg)));
            box-shadow: 2px 2px 2px 2px #888888;
            border-radius: 5px;
            margin-bottom: 1rem;
            vertical-align: top;
            border-color: var(--bs-table-border-color);
            border: 2px solid black;
            text-align: center;
        }
        .container {
            display: inline-block;
            background-image: url("{{ asset('bg-label1.jpg') }}");
            background-repeat: no-repeat;
            background-size: cover;
        }

        .kiri {
            float: left;
            width: 80px;
            margin: 15px 0 0 5px;
        }

        .kanan {
            float: right;
            width: 700px;
            margin-left: 10px;
        }
        
        .judul {
            font-size: 10px!important;
            margin: 5px;
            padding: 5px;
        }

        .kotak-nama {
            position:absolute; 
            height:120px; 
            width: 690px;
            display: table;
            border-width: 4px;
            border-style: solid;
            border-image: linear-gradient(to right, rgb(245, 229, 4), darkblue) 1;
        }

        .kotak-footer {
            position:absolute; 
            height:50px; 
            width: 700px;
            display: table;
            padding-top: 123px;
        }
        .nama {
            display: table-cell;
            vertical-align: middle;
            text-align:center;
            padding-top: 5px;
            margin-top: 0;
            /* border: 2px solid black; */
        }

        #tahun {
            font-size: 13px;
        }

        .footer {
            font-size: 10px!important;
            margin: 5px;
            padding: 5px;
            bottom: 0;
        }

        #nomor, #qrcode {
            transform: rotate(90deg);
            /* Legacy vendor prefixes that you probably don't need... */
            /* Safari */
            -webkit-transform: rotate(90deg);
            /* Firefox */
            -moz-transform: rotate(90deg);
            /* IE */
            -ms-transform: rotate(90deg);
            /* Opera */
            -o-transform: rotate(90deg);
            /* Internet Explorer */
            filter: progid:DXImageTransform.Microsoft.BasicImage(rotation=3);
            height: 70px;
            width: 70px;
            border: 2px solid black;
            margin: 5px;
            background-color: white
        }

        #nomor {
            font-size: 40px;
            height:80px; 
            width: 80px;
            display: table-cell;
            vertical-align: middle;
            text-align:center;
        }
        
    </style>
</head>
<body class="p-1">
    <div class="container">
        <div class="kiri">
            <h3 id="nomor">{{ $data['nomor'] }}</h3>
            {!! $data['qr_code'] !!}
            {{-- <img src="data:image/png;base64, {!! $data['qr_code'] !!}" alt="QRCODE" id="qrcode"> --}}
        </div>
        <div class="kanan">
            <p class="judul">
                <img src="{{ asset('images/logo/untan.png') }}" alt="Logo UNTAN" width="12px" > &nbsp
                ARSIP FAKULTAS MATEMATIKA DAN ILMU PENGETAHUAN ALAM UNIVERSITAS TANJUNGPURA
                <img src="{{ asset('images/logo/untan.png') }}" alt="Logo UNTAN" width="12px" >
            </p>
            <div class="kotak-nama">
                <h3 class="nama">
                    {{ $data['nama'] }}
                    <br>
                    <span id="tahun">TAHUN {{ $data['tahun'] }}</span>
                </h3>
            </div>
            <div class="kotak-footer">
                <p class="footer">
                    {{ $data['bagian'] }}
                </p>
            </div>
        </div>
    </div>


    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script> --}}
</body>
</html>