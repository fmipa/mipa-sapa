@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="SK" menu="SK" submenu="Tambah" formLabel="Tambah SK" :urlback="route('sk.index')">

    <x-forms.form :action="route('sk.store')" method="POST" id="form" enctype="multipart/form-data">
        
        <x-forms.text label="SK" name="nomor_sk" class="text-danger font-weight-bold" :value="old('nomor_sk') ?? 'XXXX/UN22.8/XX/'.date('Y')" readonly></x-forms.text>
        
        <x-forms.textarea label="Judul SK" class="text-uppercase" name="judul" :value="old('judul')" required></x-forms.textarea>
        
        <x-forms.number label="Nomor SK" name="nomor" :value="old('nomor')" max="100000" required></x-forms.number>
        
        <x-forms.selectbox label="Kode Naskah" name="kode_naskah_id" required :datas="$kodeNaskahs" :selected="old('kode_naskah_id')" required></x-forms.selectbox>
        
        <x-forms.selectbox label="Tingkatan SK" name="kode_unit_id" required :datas="$kodeUnits" :selected="old('kode_unit_id') ?? $selectKodeUnit" required></x-forms.selectbox>

        <x-forms.number label="Tahun Surat" name="tahun" :value="old('tahun') ?? date('Y')" min="2000" :max="date('Y')" required></x-forms.number>
        
        <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id')"></x-forms.selectbox>

        <x-forms.selectbox label="Jenis Akses File" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id')" ></x-forms.selectbox>

        <div id="tim">
            <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? $selected" hasMultiple="true"></x-forms.selectbox>
        </div>

        <div id="tag">
            <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id')" hasMultiple="true"></x-forms.selectbox>
        </div>

        <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id')" hasMultiple="true" smallText="Jika Tidak Ada, dikosongkan saja."></x-forms.selectbox>

        {{-- <x-forms.selectbox label="Pohon/Folder" name="box_id[]" :datas="$boxes" :selected="old('box_id')" hasMultiple="true" smallText="Jika Tidak Ada, dikosongkan saja."></x-forms.selectbox> --}}

        <x-forms.file label="File" name="attachment" required  :accept="$accepts"></x-forms.fi>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection

@push('js')
    <script>
        $( document ).ready(function() {
            $("#tim, #tag").hide();

            $("select[name='jenis_akses_id']").on('click change keyup keydown', function () { 
                var id = $(this).val();
                switch (id) {
                    case '3':
                        $("#tag").show();
                        $("#tim").hide();
                        break;
                        
                    case '4':
                        $("#tim").show();
                        $("#tag").hide();
                        break;
                
                    default:
                        $("#tim, #tag").hide();
                        break;
                }
            })

            function stringSplit(text, separator = ' ') { 
                const result = $.trim(text).split(separator);
                return result;
            }
            var nomorSK = $("input[name='nomor_sk']");
            $("input[name='tahun'], input[name='nomor'], select[name='kode_unit_id'], select[name='kode_naskah_id']").on('click change', function() {
                var nomor = $("input[name='nomor']").val();
                var kodeUnit = $.trim(stringSplit($("select[name='kode_unit_id']").find(":selected").text(), '-')[0]);
                var kategori = $.trim(stringSplit($("select[name='kode_naskah_id']").find(":selected").text(), '-')[0]);
                var tahun = $("input[name='tahun']").val();
                nomorSK.val($.trim(nomor+'/'+kodeUnit+'/'+kategori+'/'+tahun));
            })
            
        });
    </script>
@endpush
