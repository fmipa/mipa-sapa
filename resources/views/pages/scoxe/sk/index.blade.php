@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="SK" menu="SK" :route="route('sk.create')" buttonTambah="Tambah SK">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nomor</th>
                    <th width="20%">Judul</th>
                    <th>Tahun</th>
                    <th>Naskah</th>
                    <th>Tingkatan</th>
                    <th>Folder</th>
                    <th>Lihat</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($sks as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nomor_sk }}</td>
                        <td>{{ $item->judul }}</td>
                        <td>{{ $item->tahun }}</td>
                        <td>{{ $item->kodeNaskah->substansi }}</td>
                        <td>{{ $item->tingkatan }}</td>
                        <td>{{ $item->file->folder->nama ?? '' }}</td>
                        <td>
                            <x-ahref :link="route('lihat-file', ['id'=> $item->idSecret, 'tipeFile' => 'sk'])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                        </td>
                        <td>
                            <x-tables.button-action route="sk" :id="$item->idSecret" :labelDelete="$item->nomor"></x-tables.button-action>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection



