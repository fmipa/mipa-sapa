@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="SK" menu="SK" submenu="Ubah" formLabel="Ubah SK" :urlback="route('sk.index')">

    <x-forms.form :action="route('sk.update', $data->id_secret)" method="POST" id="form" :urlback="route('sk.index')" enctype="multipart/form-data">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
        
        <x-forms.text label="SK" name="nomor_sk" class="text-danger font-weight-bold" :value="old('nomor_sk') ?? $data->nomor_sk" readonly></x-forms.text>

        <x-forms.textarea label="Judul SK" class="text-uppercase" name="judul" :value="old('judul') ?? $data->judul" required></x-forms.textarea>
        
        <x-forms.number label="Nomor SK" name="nomor" :value="old('nomor') ?? $data->nomor" max="100000" required></x-forms.number>
        
        <x-forms.selectbox label="Tingkatan SK" name="kode_unit_id" required :datas="$kodeUnits" :selected="old('kode_unit_id') ?? $data->kode_unit_id_encrypt" required></x-forms.selectbox>

        <x-forms.selectbox label="Kode Naskah" name="kode_naskah_id" required :datas="$kodeNaskahs" :selected="old('kode_naskah_id') ?? $data->kode_naskah_id_encrypt" required></x-forms.selectbox>
        
        <x-forms.number label="Tahun Surat" name="tahun" :value="old('tahun') ?? $data->tahun" min="2000" :max="date('Y')" required></x-forms.number>

        <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id') ?? $selectFolder" ></x-forms.selectbox>

        <x-forms.selectbox label="Jenis Akses File" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id') ?? $data->file->jenis_akses_id" ></x-forms.selectbox>

        <div id="tim">
            <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? ($data->file->jenis_akses_id == 4 ? $selectJenisAkses : NULL)" hasMultiple="true"></x-forms.selectbox>
        </div>

        <div id="tag">
            <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id') ?? ($data->file->jenis_akses_id == 3 ? $selectJenisAkses : NULL)" hasMultiple="true"></x-forms.selectbox>
        </div>

        <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id') ?? $selectBox" hasMultiple="true"></x-forms.selectbox>

        <x-forms.file label="File" name="attachment" :accept="$accepts"></x-forms.file>


        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection

@push('js')
    <script>
        $( document ).ready(function() {
            $("#tim, #tag").hide();
            var jenisAksesId = "{{ (old('jenis_akses_id') ?? $data->file->jenis_akses_id) }}";
            showHide(jenisAksesId);
            $("select[name='jenis_akses_id']").on('click, change', function () { 
                var id = $(this).val();
                showHide(id);
            })

            function showHide(id)
            {
                switch (id) {
                    case '3':
                        $("#tag").show();
                        $("#tim").hide();
                        break;
                        
                    case '4':
                        $("#tim").show();
                        $("#tag").hide();
                        break;
                
                    default:
                        $("#tim, #tag").hide();
                        break;
                }
            }
            function stringSplit(text, separator = ' ') { 
                const result = $.trim(text).split(separator);
                return result;
            }
            var nomorSK = $("input[name='nomor_sk']");
            $("input[name='tahun'], input[name='nomor'], select[name='kode_unit_id'], select[name='kode_naskah']").on('click change', function() {
                var nomor = $("input[name='nomor']").val();
                var kodeUnit = $.trim(stringSplit($("select[name='kode_unit_id']").find(":selected").text(), '-')[0]);
                var kategori = $.trim(stringSplit($("select[name='kode_naskah']").find(":selected").text(), '-')[0]);
                var tahun = $("input[name='tahun']").val();
                nomorSK.val($.trim(nomor+'/'+kodeUnit+'/'+kategori+'/'+tahun));
            })
            
        });
    </script>
@endpush
