@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Hak Akses" menu="Hak Akses" submenu="Ubah" formLabel="Ubah Hak Akses" :urlback="route('hak-akses.index')">

    <x-forms.form :action="route('hak-akses.update', $data->id_secret)" method="POST" id="form" :urlback="route('hak-akses.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
                
        <x-forms.text label="Nama Hak Akses" name="name" :value="old('name') ?? $data->name" threshold="40" required autofocus></x-forms.text>
        
        <x-forms.text label="Guard Name" name="guard_name" :value="old('guard_name') ?? $data->guard_name" threshold="20" required></x-forms.text>

        <x-forms.selectbox label="Izin (Permission)" name="permission_id[]" :datas="$permissions" :selected="old('permission_id') ?? $data->permissions" hasMultiple="true" ></x-forms.selectbox>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
