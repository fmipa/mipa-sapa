@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Hak Akses" menu="Hak Akses" submenu="Tambah" formLabel="Tambah Hak Akses" :urlback="route('hak-akses.index')">

    <x-forms.form :action="route('hak-akses.store')" method="POST" id="form">

        <x-forms.text label="Nama Akses" name="name" :value="old('name')" threshold="40" required autofocus></x-forms.text>

        <x-forms.text label="Guard Name" name="guard_name" :value="old('guard_name') ?? config('auth.defaults.guard')" threshold="20" required></x-forms.text>

        <x-forms.selectbox label="Izin (Permission)" name="permission_id[]" :datas="$permissions" :selected="old('permission_id')" hasMultiple="true" ></x-forms.selectbox>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>

    </x-forms.form>

</x-layouts.page-form>

@endsection
