@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Hak Akses" menu="Hak Akses" :route="route('hak-akses.create')" buttonTambah="Tambah Hak Akses">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Hak Akses</th>
                    <th>Guard</th>
                    <th>Total Izin</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->guard_name }}</td>
                        <td>{{ $item->permissions->count() }}</td>
                        <td>
                            <x-tables.button-action route="hak-akses" :id="$item->idSecret" :labelDelete="$item->name"></x-tables.button-action>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

