@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Pegawai" menu="Pegawai" route="{{ route('pegawai.create') }}" buttonTambah="Tambah Pegawai">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Lengkap</th>
                    <th>Nomor Unik</th>
                    <th>Prodi</th>
                    <th>Status Pegawai</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pegawais as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->full_name_with_title }}</td>
                        <td>{{ $item->nomor_unik }}</td>
                        <td>{{ $item->prodi->nama }}</td>
                        <td>{{ $item->status }}</td>
                        <td>
                            <x-tables.button-action route="pegawai" :id="$item->idSecret" :labelDelete="$item->firstname"></x-tables.button-action>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

