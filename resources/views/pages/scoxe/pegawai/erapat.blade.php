@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Pegawai" menu="Pegawai" route="#" buttonTambah="false">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>nip</th>
                    <th>nidn</th>
                    <th>nama</th>
                    <th>username</th>
                    <th>password</th>
                    <th>tanggal_lahir</th>
                    <th>jabatan</th>
                    <th>bidang_studi</th>
                    <th>jenis_kelamin</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($pegawais as $item)
                    <tr>
                        <td>{{ $item->nip }}</td>
                        <td>{{ $item->nidk }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->username }}</td>
                        <td>{{ $item->password }}</td>
                        <td>{{ $item->tanggal_lahir }}</td>
                        <td>{{ $item->jabatan }}</td>
                        <td>{{ $item->bidang_studi }}</td>
                        <td>{{ $item->jenis_kelamin }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

