@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Pegawai" menu="Pegawai" submenu="Ubah" formLabel="Ubah Pegawai" :urlback="route('pegawai.index')">

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <x-forms.form :action="route('pegawai.update', $data->id_secret)" method="POST" id="form" :urlback="route('pegawai.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
    

        <div class="row">
            <div class="col-4">
                <x-forms.text label="Nama Depan" name="firstname" :value="old('firstname') ?? $data->firstname" threshold="100" required autofocus></x-forms.text>
            </div>
            <div class="col-4">
                <x-forms.text label="Nama Tengah" name="middlename" :value="old('middlename') ?? $data->middlename" threshold="100" autofocus></x-forms.text>
            </div>
            <div class="col-4">
                <x-forms.text label="Nama Belakang" name="lastname" :value="old('lastname') ?? $data->lastname" threshold="100"></x-forms.text>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <x-forms.text label="Gelar Depan" name="gelar_depan" :value="old('gelar_depan') ?? $data->gelar_depan"></x-forms.text>
            </div>
            <div class="col-6">
                <x-forms.text label="Gelar Belakang" name="gelar_belakang" :value="old('gelar_belakang') ?? $data->gelar_belakang"></x-forms.text>
            </div>
        </div>
        
        <div class="row">
            <div class="col-6">
                <x-forms.selectbox label="Unit/Divisi" name="role_id[]" required :datas="$roles" :selected="old('role_id') ?? $selectRoles" hasMultiple="true"></x-forms.selectbox>
            </div>
            <div class="col-6">
                <x-forms.number label="Nomor Pegawai" name="nomor_unik" :value="old('nomor_unik') ?? $data->nomor_unik" placeholder="Masukkan Nomor Pegawai (NIP/NIDK/NIK)"></x-forms.number>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <x-forms.text label="Tempat Lahir" name="tempat_lahir" :value="old('tempat_lahir') ?? $data->tempat_lahir"></x-forms.text>
            </div>
            <div class="col-6">
                <x-forms.date label="Tanggal Lahir" name="tanggal_lahir" :value="old('tanggal_lahir') ?? $data->tanggal_lahir ? date('d-M-Y', strtotime($data->tanggal_lahir)) : NULL"></x-forms.date>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <x-forms.selectbox label="Jenis Kelamin" name="jenis_kelamin" :datas="$jenisKelamin" :selected="old('jenis_kelamin') ?? $data->jenis_kelamin"></x-forms.selectbox>
            </div>
            <div class="col-6">
                <x-forms.selectbox label="Status" name="status" :datas="$status" :selected="old('status') ?? $data->status"></x-forms.selectbox>
            </div>
        </div>

        <div class="row">
            <div class="col-6">
                <x-forms.selectbox label="Agama" name="agama_id" :datas="$agamas" :selected="old('agama_id') ?? $selectAgama"></x-forms.selectbox>
            </div>
            <div class="col-6">
                <x-forms.selectbox label="Program Studi" name="prodi_id" :datas="$prodis" :selected="old('prodi_id') ?? $selectProdi" ></x-forms.selectbox>
            </div>
        </div>

        <hr>
        
        <div class="row">
            <div class="col-6">
                <x-forms.text label="Username" name="username" required :value="old('username') ?? ($data->user ? $data->user->username : NULL)" threshold="40" ></x-forms.text>
            </div>
            <div class="col-6">
                <x-forms.email label="Email" name="email" required :value="old('email') ?? ($data->user ? $data->user->email : NULL)" threshold="50"  ></x-forms.email>
            </div>
        </div>
        
        <div class="row">
            <div class="col-6">
                <x-forms.password label="Password" name="password" :value="old('password')" threshold="20" ></x-forms.password>
            </div>
            <div class="col-6">
                <x-forms.password label="Konfirmasi Password" name="password_confirmation" :value="old('password_confirmation')" threshold="20" ></x-forms.password>
            </div>
        </div>
        
        <x-forms.checkbox label="Aktivasi Akun" name="active" :checked="($data->user ? $data->user->is_active : NULL)" :value="old('active') ?? 1" text="Aktif" class="" id=""></x-forms.checkbox>
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
