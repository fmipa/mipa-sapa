@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')
<x-layouts.page-form title="Folder" menu="Folder" submenu="Tambah" formLabel="Tambah Folder" :urlback="route('folder-storage.index')">
    
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <x-forms.form :action="route('folder-storage.store')" method="POST" id="form">

        <x-forms.text label="Nama Folder" name="nama" :value="old('nama')" threshold="100" required autofocus></x-forms.text>
        
        <x-forms.selectbox label="Jenis Akses Folder" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id')" ></x-forms.selectbox>

        <div id="tim">
            <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? $selected" hasMultiple="true"></x-forms.selectbox>
        </div>

        <div id="tag">
            <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id')" hasMultiple="true"></x-forms.selectbox>
        </div>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection

@push('js')
    <script>
        $( document ).ready(function() {
            $("#tim, #tag").hide();

            $("select[name='jenis_akses_id']").on('click, change', function () { 
                var id = $(this).val();
                switch (id) {
                    case '3':
                        $("#tag").show();
                        $("#tim").hide();
                        break;
                        
                    case '4':
                        $("#tim").show();
                        $("#tag").hide();
                        break;
                
                    default:
                        $("#tim, #tag").hide();
                        break;
                }
            })
        });
    </script>
@endpush