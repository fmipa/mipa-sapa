@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Folder" menu="Folder" submenu="Ubah" formLabel="Ubah Folder" :urlback="route('folder-storage.index')">

    <x-forms.form :action="route('folder-storage.update', $data->id_secret)" method="POST" id="form" :urlback="route('folder-storage.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
        
        <x-forms.text label="Nama Folder" name="nama" :value="old('nama') ?? $data->nama" threshold="100" required autofocus></x-forms.text>
        
        <x-forms.selectbox label="Jenis Akses Folder" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id') ?? $data->jenis_akses_id" ></x-forms.selectbox>

        <div id="tim">
            <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? ($data->jenis_akses_id == 4 ? $selected : NULL)" hasMultiple="true"></x-forms.selectbox>
        </div>

        <div id="tag">
            <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id') ?? ($data->jenis_akses_id == 3 ? $selected : NULL)" hasMultiple="true"></x-forms.selectbox>
        </div>
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection

@push('js')
    <script>
        
        $( document ).ready(function() {
            $("#tim, #tag").hide();
            var jenisAksesId = "{{ (old('jenis_akses_id') ?? $data->jenis_akses_id) }}";
            showHide(jenisAksesId);
            $("select[name='jenis_akses_id']").on('click, change', function () { 
                var id = $(this).val();
                showHide(id);
            })

            function showHide(id)
            {
                switch (id) {
                    case '3':
                        $("#tag").show();
                        $("#tim").hide();
                        break;
                        
                    case '4':
                        $("#tim").show();
                        $("#tag").hide();
                        break;
                
                    default:
                        $("#tim, #tag").hide();
                        break;
                }
            }
        });
    </script>
@endpush
