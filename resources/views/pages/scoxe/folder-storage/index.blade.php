@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Folder Storage" menu="Folder Storage" route="{{ route('folder-storage.create') }}" buttonTambah="Tambah Folder Storage">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Akses</th>
                    <th>Dibuat</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($folderStorages as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->jenisAkses->nama }}</td>
                        <td>{{ $item->creator->name }}</td>
                        <td>
                            <x-tables.button-action route="folder-storage" :id="$item->idSecret" :labelDelete="$item->nama"></x-tables.button-action>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

