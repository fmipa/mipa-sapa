@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Kategori Berkas" menu="Kategori Berkas" submenu="Ubah" formLabel="Ubah Kategori" :urlback="route('kategori-berkas.index')">

    <x-forms.form :action="route('kategori-berkas.update', $data->id_secret)" method="POST" id="form" :urlback="route('kategori-berkas.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
        
        <x-forms.text label="Kategori Berkas" class="text-uppercase" name="kategori" :value="old('kategori') ?? $data->kategori" threshold="50" required autofocus></x-forms.text>
        
        <x-forms.textarea label="Deskripsi" name="deskripsi" :value="old('deskripsi') ?? $data->deskripsi" rows="5"></x-forms.textarea>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
