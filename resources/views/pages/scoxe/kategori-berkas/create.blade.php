@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Kategori Berkas" menu="Kategori Berkas" submenu="Tambah" formLabel="Tambah Kategori" :urlback="route('kategori-berkas.index')">

    <x-forms.form :action="route('kategori-berkas.store')" method="POST" id="form">
        <x-forms.text label="Kategori Berkas" class="text-uppercase" name="kategori" :value="old('kategori')" threshold="50" required autofocus></x-forms.text>

        <x-forms.textarea label="Deskripsi" name="deskripsi" :value="old('deskripsi')" rows="5"></x-forms.textarea>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection


