@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Master Kategori Berkas" menu="Kategori Berkas" :route="route('kategori-berkas.create')" buttonTambah="Tambah Kategori">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Kategori</th>
                    <th>Deskripsi</th>
                    <th>Dibuat</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($kberkass as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->kategori }}</td>
                        <td>{{ $item->deskripsi }}</td>
                        <td>{{ $item->creator->name }}</td>
                        <td>
                            <x-tables.button-action route="kategori-berkas" :id="$item->idSecret" :labelDelete="$item->kategori"></x-tables.button-action>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

