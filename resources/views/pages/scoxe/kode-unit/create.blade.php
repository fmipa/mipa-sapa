@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Kode Unit" menu="Kode Unit" submenu="Tambah" formLabel="Tambah Kategori" :urlback="route('kode-unit.index')">

    <x-forms.form :action="route('kode-unit.store')" method="POST" id="form">
        
        <x-forms.text label="Kode Unit" class="text-uppercase" name="kode" :value="old('kode')" threshold="50" required autofocus></x-forms.text>
        
        <x-forms.textarea label="Deskripsi" name="deskripsi" :value="old('deskripsi')" rows="5"></x-forms.textarea>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection


