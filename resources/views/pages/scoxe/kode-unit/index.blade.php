@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Master Kode Unit" menu="Kode Unit" :route="route('kode-unit.create')" buttonTambah="Tambah Kode">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Kode Unit</th>
                    <th>Deskripsi</th>
                    <th>Dibuat</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($kodeunits as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->kode }}</td>
                        <td>{{ $item->deskripsi }}</td>
                        <td>{{ $item->creator->name }}</td>
                        <td>
                            <x-tables.button-action route="kode-unit" :id="$item->idSecret" :labelDelete="$item->kode"></x-tables.button-action>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

