@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Kode Unit" menu="Kode Unit" submenu="Ubah" formLabel="Ubah Kategori" :urlback="route('kode-unit.index')">

    <x-forms.form :action="route('kode-unit.update', $data->id_secret)" method="POST" id="form" :urlback="route('kode-unit.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
        
        <x-forms.text label="Kode Unit" class="text-uppercase" name="kode" :value="old('kode') ?? $data->kode" threshold="50" required autofocus></x-forms.text>
                
        <x-forms.textarea label="Deskripsi" name="deskripsi" :value="old('deskripsi') ?? $data->deskripsi" rows="5"></x-forms.textarea>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
