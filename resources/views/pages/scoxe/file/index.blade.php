@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="File" menu="File" :route="route('file.create')">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th width="5%">No.</th>
                    <th width="20%">Nama/Nomor</th>
                    <th width="20%">Perihal/Judul</th>
                    <th>Kategori</th>
                    <th>Pembuat</th>
                    <th>Lihat</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($files as $item)
                    <tr>
                        <td>{{ $item['no'] }}</td>
                        <td>
                            <a href="#" class="text-info" onclick="detail('{{ $item['idSecret'] }}','{{ $item['tipeFile'] }}')">
                                {{ $item['nama'] }}
                            </a>
                        </td>
                        <td>{{ $item['perihal'] }}</td>
                        <td>{{ $item['kategori'] }}</td>
                        <td>{{ $item['creator'] }}</td>
                        <td>
                            <x-ahref :link="route('lihat-file', ['id'=> $item['idSecret'], 'tipeFile' => $item['tipeFile']])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

    <div class="modal fade" id="modal-detail" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detail-title">Detail </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" data-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="detail-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary w-100" data-bs-dismiss="modal" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

</x-layouts.page-index-table>

@endsection

<script>
    function detail(idSecret, tipeDokumen) 
    {
        $("#detail-title").html("Detail "+tipeDokumen.toUpperCase());
        var modalUrl = '{{ url("file/detail") }}'+'/'+idSecret+'/'+tipeDokumen;
        $("#detail-body").load(modalUrl);
        $("#modal-detail").modal('show');
    }
</script>



