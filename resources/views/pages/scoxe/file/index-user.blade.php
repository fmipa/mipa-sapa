<!DOCTYPE html>
<!-- =========================================================
* Sneat - Bootstrap 5 HTML Admin Template - Pro | v1.0.0
==============================================================

* Product Page: https://themeselection.com/item/sneat-bootstrap-html-admin-template/
* Created by: ThemeSelection
* License: You must have a valid license purchased in order to legally use the theme for your project.
* Copyright ThemeSelection (https://themeselection.com)

=========================================================
-->
<!-- beautify ignore:start -->


<html lang="en" class="light-style layout-navbar-fixed layout-wide " dir="ltr" data-theme="theme-default" data-assets-path="{{ asset('sneat/assets\/') }}" data-template="front-pages">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />

    <title>Sistem Kearsipan SAPA - {{ $title ?? ucwords(str_replace('-', ' ', request()->segment(1))) }}</title>

    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="Arsip Digital Satu Data Mipa(SAPA)" name="description" />
    <meta content="MyraStudio, TIM TIK FMIPA UNTAN" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('SAPA LOGO.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('SAPA LOGO.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('SAPA LOGO.png') }}">
    <link rel="icon" type="image/x-icon" sizes="32x32" href="{{ asset('SAPA LOGO 32.ico') }}">
    <link rel="icon" type="image/x-icon" sizes="16x16" href="{{ asset('SAPA LOGO 16.ico') }}">
    
    <!-- Canonical SEO -->
    <link rel="canonical" href="https://themeselection.com/item/sneat-bootstrap-html-admin-template/">
    
    <!-- ? PROD Only: Google Tag Manager (Default ThemeSelection: GTM-5DDHKGP, PixInvent: GTM-5J3LMKC) -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
        new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
        j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
        'https://www.googletagmanager.com/gtm5445.html?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5DDHKGP');</script>
    <!-- End Google Tag Manager -->
    
    <!-- Favicon -->
    <link rel="icon" type="image/x-icon" href="https://demos.themeselection.com/sneat-bootstrap-html-admin-template/assets/img/favicon/favicon.ico" />

    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com/">
    <link rel="preconnect" href="https://fonts.gstatic.com/" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&amp;display=swap" rel="stylesheet">

    <link rel="stylesheet" href="{{ asset('/sneat/assets/vendor/fonts/boxicons.css') }}" />
    
    <!-- Core CSS -->
    @stack('css-core')
    <link rel="stylesheet" href="{{ asset('/sneat/assets/css/demo.css') }}" />
    <link rel="stylesheet" href="{{ asset('/sneat/assets/vendor/css/pages/front-page.css') }}" />
    <!-- Vendors CSS -->
    <link rel="stylesheet" href="{{ asset('/sneat/running-text.css') }}" />
    @stack('css')
    

    <!-- Helpers -->
    <script src="{{ asset('/sneat/assets/vendor/js/helpers.js') }}"></script>
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    <script src="{{ asset('/sneat/assets/vendor/js/template-customizer.js') }}"></script>
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ asset('/sneat/assets/js/front-config.js') }}"></script>
    
</head>

<body>

    <!-- ?PROD Only: Google Tag Manager (noscript) (Default ThemeSelection: GTM-5DDHKGP, PixInvent: GTM-5J3LMKC) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DDHKGP" height="0" width="0" style="display: none; visibility: hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <script src="{{ asset('/sneat/assets/vendor/js/dropdown-hover.js') }}"></script>
    <script src="{{ asset('/sneat/assets/vendor/js/mega-dropdown.js') }}"></script>

    <!-- Navbar: Start -->
    @include('layouts.sneat.header')
    <!-- Navbar: End -->
    
    <!-- Sections:Start -->
    @yield('content')
    <!-- / Sections:End -->

    <!-- Footer: Start -->
    @include('layouts.sneat.footer')
    <!-- Footer: End -->

    <div class="buy-now">
        <a href="https://wa.me/6285386150568?text=Hallo,%20Saya%20ingin%20bertanya%20tentang%20aplikasi%20Arsip%20Digital%20SAPA" target="_blank" class="btn btn-danger btn-buy-now">WhatApps</a>
    </div>
    

    

    <!-- Core JS -->
    <!-- build:js assets/vendor/js/core.js -->
    <script src="{{ asset('/sneat/assets/vendor/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('/sneat/assets/vendor/js/bootstrap.js') }}"></script>
    
    <!-- endbuild -->

    <!-- Vendors JS -->
    @stack('js')
    

    <!-- Main JS -->
    <script src="{{ asset('/sneat/assets/js/front-main.js') }}"></script>
    
    <!-- Page JS -->
</body>
</html>

<!-- beautify ignore:end -->

