@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Pengguna" menu="Pengguna" submenu="Tambah" formLabel="Tambah Pengguna" :urlback="route('pengguna.index')">

    <x-forms.form :action="route('pengguna.store')" method="POST" id="form">

        <x-forms.text label="Nama Lengkap" name="name" :value="old('name')" threshold="100" required autofocus></x-forms.text>
        
        <x-forms.text label="Username" name="username" :value="old('username')" threshold="40" required></x-forms.text>
        
        <x-forms.password label="Password" name="password" :value="old('password')" threshold="20" required></x-forms.password>

        <x-forms.password label="Konfirmasi Password" name="password_confirmation" :value="old('password_confirmation')" threshold="20" required></x-forms.password>
        
        <x-forms.email label="Email" name="email" :value="old('email')" threshold="50" required ></x-forms.email>
        
        <x-forms.checkbox label="Aktivasi Akun" name="active" :value="old('active') ?? 1" text="Aktif" class="" id=""></x-forms.checkbox>

        <x-forms.selectbox label="Hak Akses" name="role_id[]" required :datas="$roles" :selected="old('role_id')" hasMultiple="true"></x-forms.selectbox>
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
