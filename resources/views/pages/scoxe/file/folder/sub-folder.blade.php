<div class="container">
    <h5 class="text-center mb-4">Total Data <b>{{ strtoupper($kataKunci) }}</b> = <b>{{ $total }} Data</h3>
    <div class="row">
        <div class="col-lg-10 mx-auto">
            <div class="row">
                @foreach ($folders as $item)
                    
                <div class="col-md-3 mb-md-0 mb-4 pb-4">
                    <div class="card border shadow-none">
                        <div class="card-body text-center">
                    
                            <h5 class="my-2">{{ $item['name'] }}</h5>
                            <p> Total File : {{ $item['total'] }} </p>
                            <button type="button" class="btn btn-sm btn-success" onclick="folder('{{ $item['idSecret'] }}')">Lihat</button>
                        </div>
                    </div>
                </div>
                    
                @endforeach
            </div>
        </div>
    </div>
    {{-- <div class="row">
        <table id="tabel-sub-folder" class="table table-bordered table-striped dt-responsive text-center">
            <thead>
                <tr>
                    <th>#</th>
                    <th width="30%">Nomor/Nama</th>
                    <th width="40%">Judul/Perihal</th>
                    <th>Jenis Dokumen</th>
                    <th>File</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($datas as $item)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $item->nomor }}</td>
                    <td>{{ $item->judul }}</td>
                    <td>{{ $item->kategori }}</td>
                    <td>
                        <button type="button" class="btn btn-sm btn-info waves-effect waves-light" onclick="detail('{{ $item->idSecret }}','{{ $item->tipeFile }}')">
                            Detail
                        </button>
                        <x-ahref :link="route('unduh-file', ['id'=> $item->idSecret, 'tipeFile' => $item->tipeFile])" target="_blank" class="btn btn-sm btn-success" text="Unduh"></x-ahref>
                    </td>
                </tr>        
                @endforeach
            </tbody>
        </table>
    </div> --}}
</div>

{{-- <div class="modal fade" id="modal-detail" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="detail-title">Detail </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body" id="detail-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary w-100" data-bs-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div> --}}

<script>
    // var table = $('#tabel-sub-folder').DataTable( {
    //     lengthChange: false,
    //     responsive: true,
    //     buttons: [ 'copy', 'excel', 'pdf', 'colvis' ]
    // } );
    
    // table.buttons().container()
    //     .appendTo( '#tabel-pencarian_wrapper .col-md-6:eq(0)' );

    // function detail(idSecret, tipeDokumen) 
    // {
    //     $("#detail-title").html("Detail "+tipeDokumen.toUpperCase());
    //     var modalUrl = '{{ url("file/detail") }}'+'/'+idSecret+'/'+tipeDokumen;
    //     $("#detail-body").load(modalUrl);
    //     $("#modal-detail").modal('show');
    // }
    
</script>