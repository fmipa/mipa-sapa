<div class="container" id="folder">
    <h3 class="text-center mb-4">FOLDER UNIT KERJA / BAGIAN</h3>
    <div class="row">
        <div class="col-lg-10 mx-auto">
            <div class="row">
                @foreach ($folders as $item)
                    
                <div class="col-md-3 mb-md-0 mb-4 pb-4">
                    <div class="card border shadow-none">
                        <div class="card-body text-center">
                    
                            <h5 class="my-2">{{ $item['name'] }}</h5>
                            <p> Total File : {{ $item['total'] }} </p>
                            <button type="button" class="btn btn-sm btn-success" onclick="folder('{{ $item['idSecret'] }}')">Lihat</button>
                        </div>
                    </div>
                </div>
                    
                @endforeach
            </div>
        </div>
    </div>
</div>

<script>
    function folder(idSecret) { 
        var url = '{{ route("file.sub-folder", ":idSecret") }}';
        url = url.replace(':idSecret', idSecret);
        console.log(url);
        $("#section-folder").load(url);
    }
</script>