@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Jurusan" menu="Jurusan" submenu="Ubah" formLabel="Ubah Jurusan" :urlback="route('jurusan.index')">

    <x-forms.form :action="route('jurusan.update', $data->id_secret)" method="POST" id="form" :urlback="route('jurusan.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
        
        <x-forms.text label="Nama" name="nama" :value="old('nama') $data->nama" threshold="100" required autofocus></x-forms.text>
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
