@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Jurusan" menu="Jurusan" submenu="Tambah" formLabel="Tambah Jurusan" :urlback="route('jurusan.index')">

    <x-forms.form :action="route('jurusan.store')" method="POST" id="form">

        <x-forms.text label="Nama" name="nama" :value="old('nama')" threshold="100" required autofocus></x-forms.text>
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
