@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Master Kode Naskah" menu="Kode Naskah" :route="route('kode-naskah.create')" buttonTambah="Tambah Kode Naskah">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive text-center">
            <thead class="thead-light">
                <tr>
                    <th width="5%">No.</th>
                    @canany(['update-kodenaskah', 'delete-kodenaskah'])
                    <th width="5%">#</th>
                    @endcan
                    <th width="10%">Kode</th>
                    <th width="50%">Substansi</th>
                    <th>Deskripsi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($kodeNaskahs as $key =>$item)
                    <tr>
                        <td width="5%">{{ ($key+1) }}</td>
                        @canany(['update-kodenaskah', 'delete-kodenaskah'])
                        <td width="5%">
                                <x-tables.button-action route="kode-naskah" :id="$item->idSecret" :labelDelete="$item->substansi"></x-tables.button-action>
                            </td>
                        @endcan
                        <td width="10%">{{ $item->kode }}</td>
                        <td width="50%">{{ $item->substansi }}</td>
                        <td>{{ $item->deskripsi }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

