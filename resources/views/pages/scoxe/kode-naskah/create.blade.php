@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Kode Naskah" menu="Kode Naskah" submenu="Tambah" formLabel="Tambah Kode Naskah" :urlback="route('kode-naskah.index')">

    <x-forms.form :action="route('kode-naskah.store')" method="POST" id="form">
        
        <x-forms.textarea label="Substansi" name="substansi" :value="old('substansi')" rows="5" required autofocus></x-forms.textarea>
        
        <x-forms.text label="Kode" class="text-uppercase" name="kode" :value="old('kode')" threshold="10" required></x-forms.text>

        <x-forms.textarea label="Deskripsi" name="deskripsi" :value="old('deskripsi')" rows="5"></x-forms.textarea>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection


