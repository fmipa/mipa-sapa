@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Pengguna" menu="Pengguna" submenu="Ubah" formLabel="Ubah Pengguna" :urlback="route('pengguna.index')">

    <x-forms.form :action="route('pengguna.update', $data->id_secret)" method="POST" id="form">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
        
        <x-forms.text label="Nama Lengkap" name="name" :value="$data->name ?? old('name')" threshold="100" required autofocus></x-forms.text>
        
        <x-forms.text label="Username" name="username" :value="$data->username ?? old('username')" threshold="40" required></x-forms.text>
        
        <x-forms.password label="Password Baru" name="password" :value="old('password')" threshold="20"></x-forms.password>

        <x-forms.password label="Konfirmasi Password Baru" name="password_confirmation" :value="old('password_confirmation')" threshold="20"></x-forms.password>
        
        <x-forms.email label="Email" name="email" :value="$data->email ?? old('email')" threshold="50" required ></x-forms.email>
        
        <x-forms.checkbox label="Aktivasi Akun" name="active" :checked="$data->is_active ?? NULL" :value="old('active') ?? 1" text="Aktif" class="" id=""></x-forms.checkbox>

        <x-forms.selectbox label="Hak Akses" name="role_id[]" required :datas="$roles" :selected="$selected" hasMultiple="true"></x-forms.selectbox>
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
