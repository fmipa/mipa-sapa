@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Pengguna" menu="Pengguna" route="{{ route('pengguna.create') }}" buttonTambah="Tambah Pengguna">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Hak Akses</th>
                    <th>Tanggal Dihapus</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($users as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->username }}</td>
                        <td>{{ $item->email }}</td>
                        <td>{{ $item->myRole }}</td>
                        <td>{{ $item->deleted_at }}</td>
                        <td>
                            <x-tables.button-action route="pengguna" :id="$item->idSecret" :labelDelete="$item->username"></x-tables.button-action>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

