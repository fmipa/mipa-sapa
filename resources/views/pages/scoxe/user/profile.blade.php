@extends('layouts.'.env('TEMPLATE').'.app')

@push('css')
    <link href="{{ asset(''.env('TEMPLATE').'/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(''.env('TEMPLATE').'/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" /> 
    <link href="{{ asset(''.env('TEMPLATE').'/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

@section('content')

<x-layouts.page-index title="Profil" menu="Profil">
    <div class="row">
        <div class="col-xl-3 col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="col-12 text-center mb-4">
                        <img src="{{ asset(''.env('TEMPLATE').'//vertical/images/users/avatar-1.jpg') }}" alt="Foto Default" class="img-fluid rounded-circle">
                    </div>
                    <form action="{{ route('update-profile') }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="simpleinput">Username <span class="text-danger">*</span></label>
                            <input type="text" id="simpleinput" class="form-control @error('username') is-invalid @enderror" name="username" placeholder="Masukkan Username" value="{{ $data->username ?? old('username') }}" required>
                            @error('username')
                                <span class="form-text invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="simpleinput">Email <span class="text-danger">*</span></label>
                            <input type="text" id="simpleinput" class="form-control @error('email') is-invalid @enderror" name="email" placeholder="Masukkan Email" value="{{ $data->email ?? old('email') }}" required>
                            @error('email')
                                <span class="form-text invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="example-password">Password</label>
                            <input type="password" id="example-password" class="form-control @error('password') is-invalid @enderror" name="password" value="">
                            <small class="text-info">Silahkan diisi jika ingin mengubah password.</small>
                            @error('password')
                                <span class="form-text invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="example-password">Konfirmasi Password</label>
                            <input type="password" id="example-password" class="form-control @error('password_confirmation') is-invalid @enderror" name="password_confirmation">
                            <small class="text-info">Silahkan diisi jika ingin mengubah password.</small>
                            @error('password_confirmation')
                                <span class="form-text invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <button type="submit" class="btn btn-success w-100">SIMPAN</button>
                        
                    </form>
                </div>
                <!-- end card-body-->
            </div>
            <!-- end card -->

        </div> <!-- end col -->
    
        <div class="col-xl-9 col-sm-12">
            <div class="card">
                <div class="card-body">
    
                    <h4 class="card-title">Informasi Pribadi</h4>
                    <p class="card-subtitle mb-4">Jika ingin mengubah Informasi Pribadi, silahkan melapor ke bagian <code>KEPEGAWAIAN</code> Fakultas.</p>
                    <hr>

                    <div class="row">
                        <div class="col col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="example-password">Nama Lengkap</label>
                                <input type="text" id="example-text" class="form-control" value="{{ $data->pegawai->full_name_with_title }}" readonly>
                                {{-- <small class="text-info">Jika ingin mengubah Nama Lengkap, silahkan melapor ke bagian Kepegawain Fakultas</small> --}}
                            </div>
        
                            <div class="form-group">
                                <label for="example-password">NIP/NIDK atau Nomor Pegawai</label>
                                <input type="text" id="example-text" class="form-control" value="{{ $data->pegawai->nomor_unik }}" readonly>
                                {{-- <small class="text-info">Jika ingin mengubah NIP/NIDK atau Nomor Pegawai, silahkan melapor ke bagian Kepegawain Fakultas</small> --}}
                            </div>
        
                            <div class="form-group">
                                <label for="example-password">Tempat Lahir</label>
                                <input type="text" id="example-text" class="form-control" value="{{ $data->pegawai->tempat_lahir }}" name="tempat_lahir" placeholder="Masukkan Tempat Lahir" readonly>
                            </div>
        
                            <div class="form-group">
                                <label for="example-password">Tanggal Lahir</label>
                                <input type="text" id="example-text" class="form-control @error('tanggal_lahir') is-invalid @enderror"
                                data-provide="datepicker" data-date-format="d-M-yyyy" value="{{ $data->pegawai->tanggal_lahir ? date('d-M-Y', strtotime($data->pegawai->tanggal_lahir)) : '' }}" name="tanggal_lahir" readonly>
                                @error('tanggal_lahir')
                                <span class="form-text invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="col col-sm-12 col-md-6">
                            <div class="form-group">
                                <label for="example-password">Agama</label>
                                <select name="master_agama_id" id="select-agama" class="form-control" disabled>
                                    <option value=""></option>
                                    @foreach ($masterAgama as $item)
                                        <option value="{{ $item['key'] }}">{{ $item['text'] }}</option>
                                    @endforeach
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="example-password">Jenis Kelamin</label>
                                <input type="text" id="example-text" class="form-control" value="{{ $data->pegawai->jenis_kelamin }}" name="tanggal_lahir" readonly>
                            </div>
        
                            <div class="form-group">
                                <label for="example-password">Program Studi</label>
                                <input type="text" id="example-text" class="form-control" value="{{ $data->prodiName }}" readonly>
                                {{-- <small class="text-info">Jika ingin mengubah Program Studi, silahkan melapor ke bagian Kepegawain Fakultas</small> --}}
                            </div>
        
                            <div class="form-group">
                                <label for="example-password">Status Pegawai</label>
                                <input type="text" id="example-text" class="form-control" value="{{ $data->pegawai->status ?? 'Tidak Ada' }}" readonly>
                                {{-- <small class="text-info">Jika ingin mengubah Status Pegawai, silahkan melapor ke bagian Kepegawain Fakultas</small> --}}
                            </div>

                        </div>
                    </div>
                    {{-- <button type="submit" class="btn btn-success w-100">SIMPAN</button> --}}

                </div> <!-- end card-body-->
            </div> <!-- end card-->

        </div> <!-- end col -->
    </div>
</x-layouts.page-index>

@endsection

@push('js')
<script src="{{ asset(''.env('TEMPLATE').'/plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset(''.env('TEMPLATE').'/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset(''.env('TEMPLATE').'/plugins/moment/moment.js') }}"></script>
<script src="{{ asset(''.env('TEMPLATE').'/plugins/daterangepicker/daterangepicker.js') }}"></script>

<script>
    $("#select-agama").select2({
        'placeholder': 'Pilih Agama'
    })
</script>
    
@endpush

