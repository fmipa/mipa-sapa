@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Prodi" menu="Prodi" submenu="Ubah" formLabel="Ubah Prodi" :urlback="route('prodi.index')">

    <x-forms.form :action="route('prodi.update', $data->id_secret)" method="POST" id="form" :urlback="route('prodi.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
        
        <x-forms.text label="Nama" name="nama" :value="old('nama') ?? $data->nama" threshold="100" required autofocus></x-forms.text>
        
        <x-forms.text label="Strata" name="strata" class="text-uppercase" :value="old('strata') ?? $data->strata" threshold="5" required></x-forms.text>

        <x-forms.selectbox label="Jurusan" name="jurusan_id" required :datas="$jurusans" :selected="old('jurusan_id') ?? $data->jurusan_secret"></x-forms.selectbox>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
