@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Prodi" menu="Prodi" submenu="Tambah" formLabel="Tambah Prodi" :urlback="route('prodi.index')">

    <x-forms.form :action="route('prodi.store')" method="POST" id="form">

        <x-forms.text label="Nama" name="nama" :value="old('nama')" threshold="100" required autofocus></x-forms.text>
        
        <x-forms.text label="Strata" name="strata" class="text-uppercase" :value="old('strata')" threshold="5" required></x-forms.text>

        <x-forms.selectbox label="Jurusan" name="jurusan_id" required :datas="$jurusans" :selected="old('jurusan_id')"></x-forms.selectbox>
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
