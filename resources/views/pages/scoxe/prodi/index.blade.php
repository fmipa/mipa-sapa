@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Prodi" menu="Prodi" route="{{ route('prodi.create') }}" buttonTambah="Tambah Prodi">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Strata</th>
                    <th>Jurusan</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($prodis as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->strata }}</td>
                        <td>{{ $item->nama_jurusan }}</td>
                        <td>
                            <x-tables.button-action route="prodi" :id="$item->idSecret" :labelDelete="$item->nama"></x-tables.button-action>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

