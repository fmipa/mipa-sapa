@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Izin" menu="Izin" submenu="Ubah" formLabel="Ubah Izin" :urlback="route('izin.index')">

    <x-forms.form :action="route('izin.update', $data->id_secret)" method="POST" id="form" :urlback="route('izin.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
                
        <x-forms.text label="Nama Izin" name="name" :value="old('name') ?? $data->name" threshold="50" required autofocus></x-forms.text>
        
        <x-forms.text label="Guard Name" name="guard_name" :value="old('guard_name') ?? $data->guard_name" threshold="20" required></x-forms.text>
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
