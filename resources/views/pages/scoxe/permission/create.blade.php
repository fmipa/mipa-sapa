@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Izin" menu="Izin" submenu="Tambah" formLabel="Tambah Izin" :urlback="route('izin.index')">

    <x-forms.form :action="route('izin.store')" method="POST" id="form">

        <x-forms.text label="Nama Izin" name="name" :value="old('name')" threshold="50" required autofocus></x-forms.text>
        
        <x-forms.text label="Guard Name" name="guard_name" :value="old('guard_name') ?? config('auth.defaults.guard')" threshold="20" required></x-forms.text>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
