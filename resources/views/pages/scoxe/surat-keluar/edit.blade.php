@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Surat Keluar" menu="Surat Keluar" submenu="Ubah" formLabel="Ubah Surat Keluar" :urlback="route('surat-keluar.index')">

    <x-forms.form :action="route('surat-keluar.update', $data->id_secret)" method="POST" id="form" :urlback="route('surat-keluar.index')" enctype="multipart/form-data">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
        
        <x-forms.text label="Surat Keluar" name="nomor_surat" class="text-danger font-weight-bold" :value="old('nomor_surat') ?? $data->nomor_surat" readonly></x-forms.text>
        
        <x-forms.text label="Nomor Surat" name="nomor" :value="old('nomor') ?? $data->nomor" threshold="50" required></x-forms.text>
        
        <x-forms.textarea label="Perihal" name="perihal" :value="old('perihal') ?? $data->perihal" required></x-forms.textarea>
        
        <x-forms.number label="Tahun Surat" name="tahun" :value="old('tahun') ?? $data->tahun ?? date('Y')" min="2000" :max="date('Y')" required></x-forms.number>
        
        <x-forms.textarea label="Tujuan Surat" name="tujuan" :value="old('tujuan') ?? $data->tujuan" required></x-forms.textarea>
        
        <x-forms.date label="Tanggal Diterbitkan/Dibuat" name="tanggal_diterbitkan" :value="old('tanggal_diterbitkan') ?? $data->tanggal_diterbitkan" required></x-forms.date>
        
        <x-forms.selectbox label="Kode Naskah" name="kode_naskah_id" :datas="$kodeNaskahs" :selected="old('kode_naskah_id') ?? $data->kode_naskah_id_encrypt" required></x-forms.selectbox>

        <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id') ?? $selectFolder" ></x-forms.selectbox>

        <x-forms.selectbox label="Jenis Akses File" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id') ?? $data->file->jenis_akses_id" ></x-forms.selectbox>

        <div id="tim">
            <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? ($data->file->jenis_akses_id == 4 ? $selectJenisAkses : NULL)" hasMultiple="true"></x-forms.selectbox>
        </div>

        <div id="tag">
            <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id') ?? ($data->file->jenis_akses_id == 3 ? $selectJenisAkses : NULL)" hasMultiple="true"></x-forms.selectbox>
        </div>

        <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id') ?? $selectBox" hasMultiple="true"></x-forms.selectbox>

        <x-forms.file label="File" name="attachment" :accept="$accepts"></x-forms.file>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
                
    </x-forms.form>

</x-layouts.page-form>

@endsection

@push('js')
    <script>
        
        $( document ).ready(function() {
            $("#tim, #tag").hide();
            var jenisAksesId = "{{ (old('jenis_akses_id') ?? $data->file->jenis_akses_id) }}";
            showHide(jenisAksesId);
            $("select[name='jenis_akses_id']").on('click, change', function () { 
                var id = $(this).val();
                showHide(id);
            })

            function showHide(id)
            {
                switch (id) {
                    case '3':
                        $("#tag").show();
                        $("#tim").hide();
                        break;
                        
                    case '4':
                        $("#tim").show();
                        $("#tag").hide();
                        break;
                
                    default:
                        $("#tim, #tag").hide();
                        break;
                }
            }

            function stringSplit(text, separator = ' ') { 
                const result = $.trim(text).split(separator);
                return result;
            }
            var nomorSK = $("input[name='nomor_surat']");
            $("input[name='tahun'], input[name='nomor'], select[name='kode_unit_id'], select[name='kode_naskah_id']").on('click change', function() {
                var nomor = $("input[name='nomor']").val();
                var kodeUnit = 'UN22.8';
                var kategori = $.trim(stringSplit($("select[name='kode_naskah_id']").find(":selected").text(), '-')[0]);
                var tahun = $("input[name='tahun']").val();
                nomorSK.val($.trim(nomor+'/'+kodeUnit+'/'+kategori+'/'+tahun));
            })
        });
    </script>
@endpush
