@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Surat Keluar" menu="Surat Keluar" :route="route('surat-keluar.create')" buttonTambah="Tambah Surat Keluar">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nomor</th>
                    <th>Perihal</th>
                    {{-- <th>Tahun</th> --}}
                    <th>Tujuan</th>
                    <th>Tanggal Diterima</th>
                    <th>Naskah</th>
                    <th>Folder</th>
                    <th>Lihat</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($suratKeluars as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nomorSurat }}</td>
                        <td>{{ $item->perihal }}</td>
                        {{-- <td>{{ $item->tahun }}</td> --}}
                        <td>{{ $item->tujuan }}</td>
                        <td>{{ $item->tanggal_diterbitkan }}</td>
                        <td>{{ $item->kodeNaskah ? $item->kodeNaskah->substansi : NULL }}</td>
                        <td>{{ $item->file->folder->nama ?? '' }}</td>
                        <td>
                            <x-ahref :link="route('lihat-file', ['id'=> $item->idSecret, 'tipeFile' => 'surat-keluar'])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                        </td>
                        <td>
                            <x-tables.button-action route="surat-keluar" :id="$item->idSecret" :labelDelete="$item->nomor"></x-tables.button-action>

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection



