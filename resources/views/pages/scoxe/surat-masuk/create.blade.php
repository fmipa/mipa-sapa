@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Surat Masuk" menu="Surat Masuk" submenu="Tambah" formLabel="Tambah Surat Masuk" :urlback="route('surat-masuk.index')">

    <x-forms.form :action="route('surat-masuk.store')" method="POST" id="form" enctype="multipart/form-data">
        
        <x-forms.text label="Nomor Surat" name="nomor" :value="old('nomor')" threshold="50" required></x-forms.text>
        
        <x-forms.textarea label="Perihal" name="perihal" :value="old('perihal')" required></x-forms.textarea>
        
        <x-forms.number label="Tahun Surat" name="tahun" :value="old('tahun') ?? date('Y')" min="2000" :max="date('Y')" required></x-forms.number>
        
        <x-forms.text label="Instansi Pengirim" name="instansi" :value="old('instansi')" threshold="40" required></x-forms.text>
        
        <x-forms.date label="Tanggal Diterbitkan/Dibuat" name="tanggal_diterbitkan" :value="old('tanggal_diterbitkan') ?? date('d-M-Y')" required></x-forms.date>
        
        <x-forms.date label="Tanggal Diterima" name="tanggal_diterima" :value="old('tanggal_diterima') ?? date('d-M-Y')" required></x-forms.date>

        <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id')"></x-forms.selectbox>

        <x-forms.selectbox label="Jenis Akses File" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id')" ></x-forms.selectbox>

        <div id="tim">
            <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? $selected" hasMultiple="true"></x-forms.selectbox>
        </div>

        <div id="tag">
            <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id')" hasMultiple="true"></x-forms.selectbox>
        </div>

        <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id')" hasMultiple="true"></x-forms.selectbox>

        <x-forms.file label="File" name="attachment" required  :accept="$accepts"></x-forms.fi>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection

@push('js')
    <script>
        $( document ).ready(function() {
            $("#tim, #tag").hide();

            $("select[name='jenis_akses_id']").on('click, change', function () { 
                var id = $(this).val();
                switch (id) {
                    case '3':
                        $("#tag").show();
                        $("#tim").hide();
                        break;
                        
                    case '4':
                        $("#tim").show();
                        $("#tag").hide();
                        break;
                
                    default:
                        $("#tim, #tag").hide();
                        break;
                }
            })
        });
    </script>
@endpush
