@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Surat Masuk" menu="Surat Masuk" :route="route('surat-masuk.create')" buttonTambah="Tambah Surat Masuk">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nomor</th>
                    <th>Perihal</th>
                    {{-- <th>Tahun</th> --}}
                    <th>Instansi</th>
                    <th>Tanggal Dibuat</th>
                    <th>Tanggal Diterima</th>
                    <th>Folder</th>
                    <th>Lihat</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($suratMasuks as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nomor }}</td>
                        <td>{{ $item->perihal }}</td>
                        {{-- <td>{{ $item->tahun }}</td> --}}
                        <td>{{ $item->instansi }}</td>
                        <td>{{ $item->tanggal_diterbitkan }}</td>
                        <td>{{ $item->tanggal_diterima }}</td>
                        <td>{{ $item->file->folder->nama ?? '' }}</td>
                        <td>
                            <x-ahref :link="route('lihat-file', ['id'=> $item->idSecret, 'tipeFile' => 'surat-masuk'])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                        </td>
                        <td>
                            <x-tables.button-action route="surat-masuk" :id="$item->idSecret" :labelDelete="$item->nomor"></x-tables.button-action>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection



