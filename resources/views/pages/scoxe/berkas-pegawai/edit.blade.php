@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Berkas Pegawai" menu="Berkas Pegawai" submenu="Ubah" formLabel="Ubah Berkas Pegawai" :urlback="route('berkas-pegawai.index')">

    <x-forms.form :action="route('berkas-pegawai.update', $data->id_secret)" method="POST" id="form" :urlback="route('berkas-pegawai.index')" enctype="multipart/form-data">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>

        <x-forms.text label="Nama" class="text-uppercase" name="nama" :value="old('nama') ?? $data->nama" threshold="200" required></x-forms.text>
        
        <x-forms.selectbox label="Kategori Berkas" name="kategori_id" required :datas="$kategoris" :selected="old('kategori_id') ?? $data->kategori_id_encrypt"></x-forms.selectbox>
        
        <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id') ?? $selectFolder" ></x-forms.selectbox>

        <x-forms.selectbox label="Jenis Akses File" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id') ?? $data->file->jenis_akses_id" ></x-forms.selectbox>

        <div id="tim">
            <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? ($data->file->jenis_akses_id == 4 ? $selectJenisAkses : NULL)" hasMultiple="true"></x-forms.selectbox>
        </div>

        <div id="tag">
            <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id') ?? ($data->file->jenis_akses_id == 3 ? $selectJenisAkses : NULL)" hasMultiple="true"></x-forms.selectbox>
        </div>

        <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id') ?? $selectBox" hasMultiple="true"></x-forms.selectbox>

        <x-forms.file label="File" name="attachment" :accept="$accepts"></x-forms.file>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection

@push('js')
    <script>
        $( document ).ready(function() {
            $("#tim, #tag").hide();
            var jenisAksesId = "{{ (old('jenis_akses_id') ?? $data->file->jenis_akses_id) }}";
            showHide(jenisAksesId);
            $("select[name='jenis_akses_id']").on('click, change', function () { 
                var id = $(this).val();
                showHide(id);
            })

            function showHide(id)
            {
                switch (id) {
                    case '3':
                        $("#tag").show();
                        $("#tim").hide();
                        break;
                        
                    case '4':
                        $("#tim").show();
                        $("#tag").hide();
                        break;
                
                    default:
                        $("#tim, #tag").hide();
                        break;
                }
            }
        });
    </script>
@endpush
