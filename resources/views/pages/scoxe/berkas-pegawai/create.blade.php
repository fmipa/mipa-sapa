@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Berkas Pegawai" menu="Berkas Pegawai" submenu="Tambah" formLabel="Tambah Berkas Pegawai" :urlback="route('berkas-pegawai.index')">

    <x-forms.form :action="route('berkas-pegawai.store')" method="POST" id="form" enctype="multipart/form-data">
                
        <x-forms.text label="Nama" class="text-uppercase" name="nama" :value="old('nama')" threshold="200" required></x-forms.text>
                
        <x-forms.selectbox label="Kategori Berkas" name="kategori_id" required :datas="$kategoris" :selected="old('kategori_id')" required></x-forms.selectbox>

        <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id')"></x-forms.selectbox>

        <x-forms.selectbox label="Jenis Akses File" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id')" ></x-forms.selectbox>

        <div id="tim">
            <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? $selected" hasMultiple="true"></x-forms.selectbox>
        </div>

        <div id="tag">
            <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id')" hasMultiple="true"></x-forms.selectbox>
        </div>

        <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id')" hasMultiple="true"></x-forms.selectbox>

        <x-forms.file label="File" name="attachment" required  :accept="$accepts"></x-forms.fi>

        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection

@push('js')
    <script>
        $( document ).ready(function() {
            $("#tim, #tag").hide();

            $("select[name='jenis_akses_id']").on('click change keyup keydown', function () { 
                var id = $(this).val();
                switch (id) {
                    case '3':
                        $("#tag").show();
                        $("#tim").hide();
                        break;
                        
                    case '4':
                        $("#tim").show();
                        $("#tag").hide();
                        break;
                
                    default:
                        $("#tim, #tag").hide();
                        break;
                }
            })
        });
    </script>
@endpush
