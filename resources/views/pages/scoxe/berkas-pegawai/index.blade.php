@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Berkas Pegawai" menu="Berkas Pegawai" :route="route('berkas-pegawai.create')" buttonTambah="Tambah Berkas Pegawai">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Nama Berkas</th>
                    <th>Kategori Berkas</th>
                    <th>Folder</th>
                    <th>Lihat</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($berkas as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nama }}</td>
                        <td>{{ $item->kategoriBerkas->kategori }}</td>
                        <td>{{ $item->file->folder->nama ?? '' }}</td>
                        <td>
                            <x-ahref :link="route('lihat-file', ['id'=> $item->idSecret, 'tipeFile' => 'berkas-pegawai'])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                        </td>
                        <td>
                            @if ($item->created_by == auth()->id() || auth()->id() == 1)
                            <x-tables.button-action route="berkas-pegawai" :id="$item->idSecret" :labelDelete="$item->nomor"></x-tables.button-action>
                            @else
                            <x-button type="button" class="btn-secondary btn-disabled btn-sm" label="Bukan Pemilik" id=""></x-button>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection



