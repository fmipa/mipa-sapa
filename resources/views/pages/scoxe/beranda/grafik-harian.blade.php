<div class="card">
    <div class="card-body">
        <h4 class="card-title">Grafik Arsip</h4>
        <p class="card-subtitle mb-4">grafik upload dalam 10 hari terakhir.</p>

        <div id="morris-bar-example" class="morris-chart" style="height: 304px;"></div>

    </div>
</div>

@push('js')
    <script>
        $(function() {
            'use strict';
                if ($("#morris-bar-example").length) {
                    Morris.Bar({
                        element: 'morris-bar-example',
                        barColors: ['#00c2b2'],
                        data: {!! $data['uploadPerHari'] !!},
                        xkey: 'tanggal',
                        ykeys: ['total'],
                        hideHover: 'auto',
                        // gridLineColor: '#eef0f2',
                        resize: true,
                        // barSizeRatio: 0.4,
                        labels: ['Total Upload']
                    });
                }
            });
    </script>
@endpush