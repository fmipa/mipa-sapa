<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase font-size-12 text-muted mb-3">Total SK</h6>
                        <span class="h3 mb-0"> {{ $data['totalSK'] }} </span>
                    </div>
                    <div class="col-auto">
                        <span class="badge badge-soft-success">+{{ $data['totalSKToday'] }}</span>
                    </div>
                </div> <!-- end row -->

                <div id="sparkline1" class="mt-3"></div>
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
    
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase font-size-12 text-muted mb-3">Total Berkas Pegawai</h6>
                        <span class="h3 mb-0"> {{ $data['totalBerkasPegawai'] }} </span>
                    </div>
                    <div class="col-auto">
                        <span class="badge badge-soft-success">+{{ $data['totalBerkasPegawaiToday'] }}</span>
                    </div>
                </div> <!-- end row -->

                <div id="sparkline2" class="mt-3"></div>
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase font-size-12 text-muted mb-3">Total Surat Masuk</h6>
                        <span class="h3 mb-0"> {{ $data['totalSuratMasuk'] }} </span>
                    </div>
                    <div class="col-auto">
                        <span class="badge badge-soft-success">+{{ $data['totalSuratMasukToday'] }}</span>
                    </div>
                </div> <!-- end row -->

                <div id="sparkline3" class="mt-3"></div>
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->

    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                <div class="row align-items-center">
                    <div class="col">
                        <h6 class="text-uppercase font-size-12 text-muted mb-3">Total Surat Keluar</h6>
                        <span class="h3 mb-0"> {{ $data['totalSuratKeluar'] }} </span>
                    </div>
                    <div class="col-auto">
                        <span class="badge badge-soft-success">+{{ $data['totalSuratKeluarToday'] }}</span>
                    </div>
                </div> <!-- end row -->

                <div id="sparkline4" class="mt-3"></div>
            </div> <!-- end card-body-->
        </div> <!-- end card-->
    </div> <!-- end col-->
</div>
<!-- end row-->

@push('js')
    <script>
        $(document).ready(function () {
            var DrawSparkline = function() {
                $('#sparkline1').sparkline([25, 23, 26, 24, 25, 32, 30, 24, 19], {
                    type: 'line',
                    width: "100%",
                    height: '80',
                    chartRangeMax: 35,
                    lineColor: '#3f87fd',
                    fillColor: 'rgba(46, 124, 228, 0.3)',
                    highlightLineColor: 'rgba(0,0,0,.1)',
                    highlightSpotColor: 'rgba(0,0,0,.2)',
                    maxSpotColor:false,
                    minSpotColor: false,
                    spotColor:false,
                    lineWidth: 1
                });

                $('#sparkline2').sparkline([24, 25, 32, 30, 24, 19, 32, 35, 23], {
                    type: 'line',
                    width: "100%",
                    height: '80',
                    chartRangeMax: 35,
                    lineColor: '#f1c31c',
                    fillColor: 'rgba(241, 195, 28, 0.3)',
                    highlightLineColor: 'rgba(0,0,0,.1)',
                    highlightSpotColor: 'rgba(0,0,0,.2)',
                    maxSpotColor:false,
                    minSpotColor: false,
                    spotColor:false,
                    lineWidth: 1
                });

                $('#sparkline3').sparkline([30, 24, 19, 32, 35, 23, 12, 25, 35], {
                    type: 'line',
                    width: "100%",
                    height: '80',
                    chartRangeMax: 35,
                    lineColor: '#ff5b5b',
                    fillColor: 'rgba(255, 91, 91, 0.3)',
                    highlightLineColor: 'rgba(0,0,0,.1)',
                    highlightSpotColor: 'rgba(0,0,0,.2)',
                    maxSpotColor:false,
                    minSpotColor: false,
                    spotColor:false,
                    lineWidth: 1
                });

                $('#sparkline4').sparkline([23, 12, 25, 35, 23, 12, 25, 35], {
                    type: 'line',
                    width: "100%",
                    height: '80',
                    chartRangeMax: 35,
                    lineColor: '#1a213b',
                    fillColor: 'rgba(67, 75, 107, 0.3)',
                    highlightLineColor: 'rgba(0,0,0,.1)',
                    highlightSpotColor: 'rgba(0,0,0,.2)',
                    maxSpotColor:false,
                    minSpotColor: false,
                    spotColor:false,
                    lineWidth: 1
                });
                
                };
            
            DrawSparkline();
            

            
            var resizeChart;
            
            $(window).resize(function(e) {
                clearTimeout(resizeChart);
                resizeChart = setTimeout(function() {
                    DrawSparkline();
                }, 300);
            });
        });
    </script>
@endpush