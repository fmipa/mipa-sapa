@extends('layouts.'.env('TEMPLATE').'.app')

@push('css')
    <link href="{{ asset('/'.env('TEMPLATE').'/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" /> 
    <style>
        .tree, .tree ul {
            margin:0;
            padding:0;
            list-style:none
        }
        .tree ul {
            margin-left:1em;
            position:relative
        }
        .tree ul ul {
            margin-left:1em
            padding:5px;
        }
        .tree ul:before {
            content:"";
            display:block;
            width:0;
            position:absolute;
            top:0;
            bottom:0;
            left:0;
            border-left:1px solid
        }
        .tree li {
            margin:0;
            padding:0 1em;
            line-height:2em;
            color:#369;
            font-weight:700;
            position:relative
        }
        .tree ul li:before {
            content:"";
            display:block;
            width:10px;
            height:0;
            border-top:1px solid;
            margin-top:-1px;
            position:absolute;
            top:1em;
            left:0
        }
        .tree ul li:last-child:before {
            background:#fff;
            height:auto;
            top:1em;
            bottom:0
        }
        .indicator {
            margin-right:5px;
        }
        .tree li a {
            text-decoration: none;
            color:#369;
        }
        .tree li button, .tree li button:active, .tree li button:focus {
            text-decoration: none;
            color:#369;
            border:none;
            background:transparent;
            margin:0px 0px 0px 0px;
            padding:0px 0px 0px 0px;
            outline: 0;
        }
    </style>
@endpush

@section('content')

<x-layouts.page-index title="Folder" menu="Pohon" >
    <div class="row">
        <div class="col col-md-7 col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center bg-dark">
                    <h4 class="text-white">Tambah Folder</h4>
                </div>
                <div class="card-body">
                    @if(count($errors) > 0)
                        <div class="alert alert-danger  alert-dismissible">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            @foreach($errors->all() as $error)
                                    {{ $error }}<br>
                            @endforeach
                        </div>
                    @endif
                    <x-forms.form :action="route('folder.store')" method="POST" id="form">

                        <x-forms.selectbox label="Kepala Folder" name="parent_id" :datas="$parentFolders" :selected="old('parent_id')" smallText="Jika ingin menjadi folder utama, dikosongkan saja"></x-forms.selectbox>
                        
                        <x-forms.text label="Nama Folder" name="nama" :value="old('nama')" threshold="100" required autofocus></x-forms.text>
                        
                        <x-forms.number label="Urutan" name="urutan" :value="old('urutan') ?? 1" min="1" required></x-forms.number>
                        
                        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
                        
                    </x-forms.form>
                </div>
            </div>
        </div>

        <div class="col col-md-5 col-sm-12">
            <div class="card">
                <div class="card-header d-flex justify-content-between align-items-center bg-dark">
                    <h4 class="text-white">Folder</h4>
                </div>
                <div class="card-body">
                    <ul id="tree1">
                        @foreach($folders as $folder)
                        <li idSecret="{{ $folder->idSecret }}" id="twig">
                            {{ $folder->nama }}
                            @if(count($folder->childs))
                                @include('pages.'.env('TEMPLATE').'.folder.subfolder',['subfolders' => $folder->childs])
                            @endif
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        
        </div>
    </div>
</x-layouts.page-index>

@endsection

@push('js')
    <script src="{{ asset('/'.env('TEMPLATE').'/plugins/select2/select2.min.js') }}"></script>
    <script>
        $.fn.extend({
            treed: function (o) {
            
            var openedClass = 'glyphicon-minus-sign';
            var closedClass = 'glyphicon-plus-sign';
            
            if (typeof o != 'undefined'){
                if (typeof o.openedClass != 'undefined'){
                    openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined'){
                    closedClass = o.closedClass;
                }
            };
            
                /* initialize each of the top levels */
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this);
                    branch.prepend("");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this == e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    })
                    branch.children().children().toggle();
                });
                /* fire event from the dynamically added icon */
                tree.find('.branch .indicator').each(function(){
                    $(this).on('click', function () {
                        $(this).closest('li').click();
                    });
                });
                /* fire event to open branch if the li contains an anchor instead of text */
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                /* fire event to open branch if the li contains a button instead of text */
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });
        /* Initialization of treeviews */
        $('#tree1').treed();

        $("#twig").on("click", function() {
            player = $(this).attr("value");
            console.log(player);
        });
        function selectRanting(idSecret)
        {
            $("select[name='parent_id']").val(idSecret).trigger('change');
            console.log(idSecret);
        }
    </script>
@endpush

