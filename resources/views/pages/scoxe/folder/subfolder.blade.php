<ul>
    @foreach($subfolders as $subfolder)
        <li idSecret="{{ $subfolder->idSecret }}" id="twig">
            {{ $subfolder->nama }}
            @if(count($subfolder->childs))
                @include('pages.'.env('TEMPLATE').'.folder.subfolder',['subfolders' => $subfolder->childs])
            @endif
        </li>
    @endforeach
</ul>
    