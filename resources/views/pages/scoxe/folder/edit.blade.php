@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Ekstensi" menu="Ekstensi" submenu="Ubah" formLabel="Ubah Ekstensi" :urlback="route('ekstensi.index')">

    <x-forms.form :action="route('ekstensi.update', $data->id_secret)" method="POST" id="form" :urlback="route('ekstensi.index')">

        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}" required>
                
        <x-forms.text label="Ekstensi" name="extension" :value="old('extension') ?? $data->extension" threshold="20" required autofocus></x-forms.text>
        
        <x-forms.text label="Deskripsi" name="deskripsi" :value="old('deskripsi') ?? $data->deskripsi" threshold="100"></x-forms.text>
                
        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
