@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Box / Gobi" menu="Box / Gobi" submenu="Tambah" formLabel="Tambah Box / Gobi" :urlback="route('box.index')">

    <x-forms.form :action="route('box.store')" method="POST" id="form">

        <x-forms.textarea label="Nama" name="nama" class="text-uppercase" :value="old('nama')" required autofocus></x-forms.textarea>
        
        <x-forms.number label="Nomor" name="nomor" :value="old('nomor') ?? $nomorUrut" required></x-forms.number>

        <x-forms.selectbox label="Tahun" name="tahuns[]" :datas="$tahuns" :selected="old('tahuns')" hasMultiple="true" smallText="Dapat dikosongkan dan dapat dipilih lebih dari 1."></x-forms.selectbox>

        <x-forms.selectbox label="Bagian / Unit" name="bagian" :datas="$bagians" :selected="old('bagian')" smallText="Jika dikosongkan akan terdaftar sebagai fakultas"></x-forms.selectbox>
        
        <x-forms.text label="Lokasi Penyimpanan Fisik" name="lokasi" :value="old('lokasi')" threshold="100" placeholder="Lokasi Penyimpanan contoh: Gedung Baru Ruang Arsiparis Lemari Berkas" required></x-forms.text>
        
        <x-forms.text label="Password Box" name="password" :value="old('password')" smallText="Jika dikosongkan file dalam box dapat dilihat dan diunduh oleh publik"></x-forms.text>
                        
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
