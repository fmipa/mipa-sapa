@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Box / Gobi" menu="Box / Gobi" route="{{ route('box.create') }}" buttonTambah="Tambah Box / Gobi">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>Nomor Box/Gobi</th>
                    <th width="5%">Label</th>
                    <th>Nama</th>
                    @if (auth()->id() == 1)
                    <th>Bagian</th>
                    @endif
                    <th>Lokasi</th>
                    <th>File</th>
                    {{-- <th>QRCode</th> --}}
                    <th>Dibuat</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($boxes as $item)
                    <tr>
                        <td>{{ $item->nomor }}</td>
                        <td>
                            {{-- <x-ahref :link="route('box.label', $item->unique_id)" target="_blank" class="text-danger" :text="$item->namaTahun"></x-ahref> --}}
                            <button type="button" class="btn btn-sm btn-primary waves-effect waves-light" onclick="label('{{ $item->unique_id }}')">
                                <i class="mdi mdi-printer"></i>
                            </button>
                        </td>

                        <td>
                            <x-ahref :link="route('cari-berkas.qrcode', $item->unique_id)" target="_blank" class="text-info" :text="$item->namaTahun"></x-ahref>
                        </td>
                        @if (auth()->id() == 1)
                        <td>{{ $item->bagian }}</td>
                        @endif
                        <td>{{ $item->lokasi }}</td>
                        <td>{{ $item->files()->count() }}</td>
                        {{-- <td> --}}
                            {{-- <x-modals.button class="info" target="qrcode-{{ $loop->iteration }}" text="QRCode"></x-modals.button>
                            <x-modals.modal modalId="qrcode-{{ $loop->iteration }}">
                                <x-modals.header judul="Lihat QRCode"></x-modals.header>
                                
                                <x-modals.body title="QRCode" class="text-center"> --}}
                                    {{-- {!! $item->qrcode !!} --}}
                                {{-- </x-modals.body>
                                
                                
                                <x-modals.footer closeText="TUTUP" saveButton="false"></x-modals.footer>
                            </x-modals.modal> --}}
                        {{-- </td> --}}
                        <td>{{ $item->creator->name }}</td>
                        <td>
                            @if ($item->created_by == auth()->id() || auth()->id() == 1)
                            <x-tables.button-action route="box" :id="$item->idSecret" :labelDelete="$item->nama"></x-tables.button-action>
                            @else
                            <x-button type="button" class="btn-secondary btn-disabled btn-sm" label="Bukan Pemilik" id=""></x-button>
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
        
        <x-modals.modal modalId="modal-label" modalSize="modal-dialog-centered">
            <x-modals.header judul="Cetak Label"></x-modals.header>
            
            <x-modals.body title="Tipe Label" class="text-center">
                <div class="row">
                    <div class="col">
                        <x-ahref target="_blank" class="btn btn-primary w-100" id="label-kotak" text="Kotak"></x-ahref>
                    </div>
                    <div class="col">
                        <x-ahref target="_blank" class="btn btn-info w-100" id="label-box-file" text="Box File"></x-ahref>
                    </div>
                    <div class="col">
                        <x-ahref target="_blank" class="btn btn-success w-100 disabled" id="label-gobi" text="Gobi"></x-ahref>
                    </div>
                    <div class="col">
                        <x-ahref target="_blank" class="btn btn-warning w-100" id="label-map" text="Map"></x-ahref>
                    </div>
                </div>
            </x-modals.body>

            <x-modals.footer closeText="TUTUP" saveButton="false"></x-modals.footer>
        </x-modals.modal>

</x-layouts.page-index-table>

@endsection

@push('js')
    <script>
        function label(unique)
        {
            let route = "{{ route('box.index') }}";
            $("#label-kotak").attr('href', route+"/label/"+unique+"/kotak");
            $("#label-box-file").attr('href', route+"/label/"+unique+"/box-file");
            $("#label-gobi").attr('href', route+"/label/"+unique+"/gobi");
            $("#label-map").attr('href', route+"/label/"+unique+"/map");
            $("#modal-label").modal('show');
        }
    </script>
@endpush
