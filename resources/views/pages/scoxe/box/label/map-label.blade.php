<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Label {{ strtoupper($data['tipeLabel']) }} Nomor {{ $data['nomor'] }}</title>
    
    {{-- <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous"> --}}
    <style>
        .page-break {
            page-break-after: always;
        }
        * {
            text-transform: uppercase;
            font-weight: bold;
        }
        body {
            width: 150px;
            height: 100px;
            max-height: 100px;
            box-shadow: inset 0 0 0 9999px var(--bs-table-bg-state,var(--bs-table-bg-type,var(--bs-table-accent-bg)));
            box-shadow: 2px 2px 2px 2px #888888;
            border-radius: 5px;
            margin-bottom: 1rem;
            /* vertical-align: top; */
            border-color: var(--bs-table-border-color);
            text-align: center;
        }

        .table>:not(caption)>*>* {
            padding: 0.5rem 0.5rem;
            color: var(--bs-table-color-state,var(--bs-table-color-type,var(--bs-table-color)));
            background-color: var(--bs-table-bg);
            border-bottom-width: var(--bs-border-width);
            box-shadow: inset 0 0 0 9999px var(--bs-table-bg-state,var(--bs-table-bg-type,var(--bs-table-accent-bg)));
        }
        table {
            height: 740px;
            border: 2px solid black;
        }

        table tr td{
            width: 50px;
            border: 2px solid black;
        }

        #logo, #logo > td {
            text-align: center;
            border: unset;
        }
        #logo img {
            padding-top: 10px;
            padding-bottom: 10px;
        }

        #fmipa {
            text-align: center;
            margin: unset;
            /* padding: 0 0 10px 0; */
            font-size: 15px;
        }

        #qrcode img, #nama {
            -webkit-transform:rotate(90deg);
            -moz-transform:rotate(90deg);
            -ms-transform:rotate(90deg);
            -o-transform:rotate(90deg);
            transform:rotate(90deg);
            transform-origin: 50%;
        }

        #qrcode {
            text-align: center;
            padding: 5px 0;
        }

        #kotak-nama {
            vertical-align: middle;
            text-align:center;
            width: 140px;
            height: 740px;
        }

        #nama {
            width: 700px;
            height: 200px;
            word-wrap: normal;
            vertical-align: middle;
            margin: -180px;
            padding-top: 0;
            left: -165px;
            top: 540px;
            position: absolute;
            font-size: 30px;
        }

        #nomor {
            font-size: 35px;
            height:100px; 
            word-wrap: normal;
            vertical-align: middle;
            text-align:center;
        }
        
    </style>
</head>
<body class="p-1">
    <table>
        <tr>
            <td id="logo">
                <img src="{{ asset('images/logo/untan.png') }}" alt="Logo UNTAN" width="50px"> 
                <p id="fmipa">FMIPA UNTAN</p>
            </td>
        </tr>
        <tr>
            <td id="kotak-nama">
                <p id="nama">
                    {{ $data['nama'] .' '. $data['tahun'] }}
                </p>
            </td>
        </tr>
        <tr>
            <td id="nomor">
                {{ $data['nomor'] }}
            </td>
        </tr>
        <tr>
            <td id="qrcode">
                {{-- {!! $data['qr_code'] !!} --}}
                <img src="data:image/png;base64, {!! $data['qr_code'] !!}" alt="QRCODE">
            </td>

        </tr>
    </table>


    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script> --}}
</body>
</html>