@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')
<x-layouts.page-form title="Master Agama" menu="Master Agama" submenu="Tambah" formLabel="Tambah Master Agama" :urlback="route('master-agama.index')">

    <x-forms.form :action="route('master-agama.update', $data->id_secret)" method="POST" id="form">
        
        @method('PUT')
        <input type="hidden" name="id" value="{{ $data->id_secret }}">
        
        <x-forms.text label="Agama" name="agama" :value="old('agama') ?? $data->agama" threshold="30" required autofocus></x-forms.text>
        
        <x-forms.text label="Kode" name="kode" class="text-uppercase" :value="old('kode') ?? $data->kode" threshold="10"></x-forms.text>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
