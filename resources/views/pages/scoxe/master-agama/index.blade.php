@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-index-table title="Master Agama" menu="Agama" :route="route('master-agama.create')" buttonTambah="Tambah Agama">

    <div class="table-responsive mt-4">
        <table id="datatable-buttons" class="table table-striped dt-responsive nowrap">
            <thead>
                <tr>
                    <th>No.</th>
                    <th>Agama</th>
                    <th>Kode/Singkatan</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($agamas as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->agama }}</td>
                        <td>{{ $item->kode }}</td>
                        <td>
                            <x-tables.button-action route="master-agama" :id="$item->idSecret" :labelDelete="$item->agama"></x-tables.button-action>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>

</x-layouts.page-index-table>

@endsection

