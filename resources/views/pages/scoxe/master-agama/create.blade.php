@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Master Agama" menu="Agama" submenu="Tambah" formLabel="Tambah Agama" :urlback="route('master-agama.index')">

    <x-forms.form :action="route('master-agama.store')" method="POST" id="form">
        
        <x-forms.text label="Agama" name="agama" :value="old('agama')" threshold="30" required autofocus></x-forms.text>
        
        <x-forms.text label="Kode" name="kode" class="text-uppercase" :value="old('kode')" threshold="10"></x-forms.text>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection


