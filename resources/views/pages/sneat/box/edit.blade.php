<x-pages.box.edit 
    :tahuns="$tahuns"
    :unitKerjas="$unitKerjas"
    :dataBox="$data"
    :selectTahun="$selectTahun"
    :selectUnit="$selectUnit"
></x-pages.box.edit>
