<x-pages.berkasPegawai.create
    :kategoris="$kategoris"
    :folders="$folders"
    :jenisAkses="$jenisAkses"
    :unitKerjas="$unitKerjas"
    :pegawais="$pegawais"
    :boxes="$boxes"
    :accepts="$accepts"
    :selected="$selected"
    :creators="$creators"
></x-pages.berkasPegawai.create>