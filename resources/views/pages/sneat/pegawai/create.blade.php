<x-pages.pegawai.create 
    :roles="$roles"
    :jenisKelamin="$jenisKelamin"
    :status="$status"
    :agamas="$agamas"
    :prodis="$prodis"
></x-pages.pegawai.create>