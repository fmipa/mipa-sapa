<x-pages.folderStorage.create
    :pegawais="$pegawais"
    :jenisAkses="$jenisAkses"
    :unitKerjas="$unitKerjas"
    :selected="$selected"
></x-pages.folderStorage.create>