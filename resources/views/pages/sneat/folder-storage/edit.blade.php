<x-pages.folderStorage.edit
    :dataFolder="$data"
    :jenisAkses="$jenisAkses"
    :unitKerjas="$unitKerjas"
    :pegawais="$pegawais"
    :selected="$selected"
></x-pages.folderStorage.edit>