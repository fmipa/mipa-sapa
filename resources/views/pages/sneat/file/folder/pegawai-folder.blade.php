<x-pages.file.folder.pegawaiFolder 
    :parentFolderPegawai="$folderId" 
    :folders="$folders" 
    :kembaliId="$kembaliId ?? NULL" 
    :namaFolder="$namaFolder ?? ''">
</x-pages.file.folder.pegawaiFolder>
