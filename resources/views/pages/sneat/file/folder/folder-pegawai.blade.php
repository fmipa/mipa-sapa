<x-pages.file.folder.folderPegawai 
    :folders="$folders" 
    :kembaliId="$kembaliId" 
    :namaPegawai="$namaPegawai ?? ''" 
    :pegawaiId="$pegawaiId">
</x-pages.file.folder.folderPegawai>
