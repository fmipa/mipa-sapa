<x-pages.file.folder.folder 
    :folders="$folders" 
    :kembaliId="$kembaliId" 
    :folderId="$folderId ?? NULL" 
    :namaFolder="$namaFolder ?? ''">
</x-pages.file.folder.folder>
