@extends('layouts.'.env('TEMPLATE').'.app')

@section('content')

<x-layouts.page-form title="Ekstensi" menu="Ekstensi" submenu="Tambah" formLabel="Tambah Ekstensi" :urlback="route('ekstensi.index')">

    <x-forms.form :action="route('ekstensi.store')" method="POST" id="form">

        <x-forms.text label="Ekstensi" name="extension" :value="old('extension')" threshold="20" required autofocus></x-forms.text>
        
        <x-forms.text label="Deskripsi" name="deskripsi" :value="old('deskripsi')" threshold="100"></x-forms.text>
                
        <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
        
    </x-forms.form>

</x-layouts.page-form>

@endsection
