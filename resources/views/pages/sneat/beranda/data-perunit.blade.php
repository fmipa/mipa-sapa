<div class="row">
    @foreach ($data['dataRoles'] as $item)
        <div class="col-xl-3">
            <div class="card m-b-20">
                <div class="card-body">
                    <h4 class="card-title">Urutan : {{ $item['ranking'] }}</h4>

                    <div class="text-center">
                        <input data-plugin="knob" data-width="120" data-height="120" data-linecap=round
                            data-fgColor="{{ $item['warna'] }}" value="{{ $item['hari_ini'] }}" data-skin="tron" data-angleOffset="100"
                            data-readOnly=true data-thickness=".1"/>

                        <div class="clearfix"></div>
                        <a href="#" class="btn btn-sm mt-2 text-white" style="background-color: {{ $item['warna'] }}">{{ $item['unit'] }}</a>
                        <ul class="list-inline row mt-4 mb-0">
                            <li class="col-6">
                                <h5>{{ $item['total'] }}</h5>
                                <p class="mb-0">Total</p>
                            </li>
                            <li class="col-6">
                                <h5>{{ $item['hari_ini'] }}</h5>
                                <p class="mb-0">Hari Ini</p>
                            </li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        
    @endforeach
    
</div>
