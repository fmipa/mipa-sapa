@extends('layouts.'.env('TEMPLATE').'.app-cari-qrcode')

@push('css')
    <link href="{{ asset(''.env('TEMPLATE').'/plugins/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(''.env('TEMPLATE').'/plugins/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(''.env('TEMPLATE').'/plugins/datatables/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(''.env('TEMPLATE').'/plugins/datatables/select.bootstrap4.css') }}" rel="stylesheet" type="text/css" />

<style>
    .container-fluid {
        margin-top: -30 !important;
    }

    .main-content {
        margin-left: unset !important;
    }

    .footer {
        left: 0 !important;
    }
</style>
@endpush
@section('content')

<div class="page-content">
    <div class="container-fluid">


        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center bg-dark">
                        <h4 class="text-white"><b>BOX / Gobi : {{ $box->nama }}</b></h4>
                        <h4 class="m-0 d-print-none text-white">Lokasi {{ $box->lokasi }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table id="datatable-buttons" class="table table-bordered table-striped dt-responsive text-center">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th width="30%">Nomor/Nama</th>
                                                <th width="40%">Judul/Perihal</th>
                                                <th>Kategori</th>
                                                <th>File</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($datas as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->nomor }}</td>
                                                <td>{{ $item->judul }}</td>
                                                <td>{{ $item->kategori }}</td>
                                                <td>
                                                    <button type="button" class="btn btn-sm btn-primary waves-effect waves-light" onclick="aksi('{{ $item->idSecret }}', '{{ $item->tipeFile }}')">
                                                        <i class="mdi mdi-folder-lock-open"></i> File
                                                    </button>
                        
                                                    {{-- <x-ahref :link="route('lihat-file', ['id'=> $item->idSecret, 'tipeFile' => $item->tipeFile])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                                                    <x-ahref :link="route('unduh-file', ['id'=> $item->idSecret, 'tipeFile' => $item->tipeFile])" target="_blank" class="btn btn-sm btn-secondary" text="Unduh"></x-ahref> --}}
                                                </td>
                                            </tr>        
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="clearfix pt-5">
                                    <h6 class="text-muted">Notes:</h6>

                                    <small>
                                        Total Berkas di dalam box/gobi {{ $datas->count() }}
                                    </small>
                                </div>

                            </div>
                        </div>
                    </div>
                </div> <!-- end card-->
            </div> <!-- end col -->
        </div>
        <!-- end row -->

    </div> <!-- container-fluid -->
</div>

<x-modals.modal modalId="modal-password" modalSize="modal-dialog-centered">
    <x-modals.header judul="Password Box/Gobi"></x-modals.header>
    
    <x-modals.body title="Password" class="text-center">
        <input type="hidden" id="id-secret" value="">
        <input type="hidden" id="tipe-file" value="">
        @if ($box->password != NULL)
        <div class="form-group">
            <label for="simpleinput">Masukkan Password :</label>
            <input type="text" id="password-box" class="form-control" placeholder="Masukkan Password Box/Gobi" required>
        </div>
        @endif
        <div class="row">
            <div class="col">
                <x-ahref target="_blank" id="lihat" class="btn btn-sm btn-primary w-100" text="Lihat"></x-ahref>
            </div>
            <div class="col">
                <x-ahref target="_blank" id="unduh" class="btn btn-sm btn-success w-100" text="Unduh"></x-ahref>
            </div>
        </div>
    </x-modals.body>
    
    <x-modals.footer closeText="TUTUP" saveButton="false"></x-modals.footer>
</x-modals.modal>

@endsection

@push('js')
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset(''.env('TEMPLATE').'/plugins/datatables/vfs_fonts.js') }}"></script>

    <!-- Datatables init -->
    <script>
        $(document).ready(function() {
            var a = $("#datatable-buttons").DataTable({
                keys: !0,
                stateSave: !0,
                lengthChange: !1,
                buttons: ["copy", "print", "pdf"],
                language: {
                    paginate: {
                        previous: "<i class='mdi mdi-chevron-left'>",
                        next: "<i class='mdi mdi-chevron-right'>"
                    }
                },
                drawCallback: function() {
                    $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                }
            });
            a.buttons().container().appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"), $("#complex-header-datatable").DataTable({
                language: {
                    paginate: {
                        previous: "<i class='mdi mdi-chevron-left'>",
                        next: "<i class='mdi mdi-chevron-right'>"
                    }
                },
                drawCallback: function() {
                    $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                },
                columnDefs: [{
                    visible: !1,
                    targets: -1
                }]
            })
        });
    </script>

    <script>
        function aksi(idSecret, tipeFile)
        {
            $("#id-secret").val(idSecret);
            $("#tipe-file").val(tipeFile);
            $("#modal-password").modal('show');
            proses();
        }
        function proses()
        {
            uniqueId = "{{ $box->unique_id }}";
            idSecret = $("#id-secret").val();
            tipeFile = $("#tipe-file").val();
            cekPass = "{{ $box->password ?? 'fmipa' }}";
            password = 'fmipa';
            switch (cekPass) {
                case 'fmipa':
                    password = cekPass;
                    break;
                    
                default:
                    inputPassword = $("#password-box").val();
                    password = inputPassword.length === 0 ? 'fmipa' : inputPassword;
            }
            console.log(password);
            $("#lihat").attr('href', '{{ url('/lihat-file/') }}'+'/'+uniqueId+'/'+idSecret+'/'+tipeFile+'/'+password);
            $("#unduh").attr('href', '{{ url('/download-file/') }}'+'/'+uniqueId+'/'+idSecret+'/'+tipeFile+'/'+password);
        }

        $("#lihat, #unduh").click(function(){
            cekPass = "{{ $box->password }}";
            inputPassword = $("#password-box").val();
            if (cekPass && inputPassword.length === 0) {
                alert('Box/Gobi memiliki password, Silahkan masukkan password.');
                return false;
            }
            
        });

        $("#password-box").on('click change keyup kedydown', function(e) {
            proses();
        })
    </script>
@endpush
