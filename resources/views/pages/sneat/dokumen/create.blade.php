<x-pages.dokumen.create
    :jenisAkses="$jenisAkses"
    :unitKerjas="$unitKerjas"
    :selected="$selected"
    :pegawais="$pegawais"
    :boxes="$boxes"
    :folders="$folders"
    :accepts="$accepts"
    :creators="$creators"
></x-pages.dokumen.create>