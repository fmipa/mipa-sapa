<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>{{ env('APP_NAMA') }} - {{ $title ?? 'Home' }}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta content="Arsip Digital Satu Data Mipa(SAPA)" name="description" />
    <meta content="MyraStudio, TIM TIK FMIPA UNTAN" name="author" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('SAPA LOGO.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('SAPA LOGO.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('SAPA LOGO.png') }}">
    <link rel="icon" type="image/x-icon" sizes="32x32" href="{{ asset('SAPA LOGO 32.ico') }}">
    <link rel="icon" type="image/x-icon" sizes="16x16" href="{{ asset('SAPA LOGO 16.ico') }}">

    <!-- App css -->
    <link href="{{ asset('/scoxe/vertical/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/scoxe/vertical/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/scoxe/vertical/css/theme.min.css') }}" rel="stylesheet" type="text/css" />
    
    <style>
        body {
            font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji !important;
        }
        .bg-login-custom {
            background: url("{{ asset('bg login 2.png') }}");
            background-position: center;
            /* background-size: cover; */
            background-repeat: no-repeat;
            background-size: 91%;
        }
    </style>
</head>
<body>
    
    <div>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="d-flex align-items-center min-vh-100">
                        <div class="w-100 d-block bg-white shadow-lg rounded my-5">

                            @yield('content')
                            
                        </div> <!-- end .w-100 -->
                    </div> <!-- end .d-flex -->
                </div> <!-- end col-->
            </div> <!-- end row -->
        </div>
        <!-- end container -->
    </div>
    <!-- end page -->

    <!-- jQuery  -->
    <script src="{{ asset('/scoxe/vertical/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/metismenu.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/waves.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/simplebar.min.js') }}"></script>

    <!-- App js -->
    <script src="{{ asset('/scoxe/vertical/js/theme.js') }}"></script>

    @stack('js')
</body>
</html>