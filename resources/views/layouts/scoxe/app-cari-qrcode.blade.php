<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{{ env('APP_NAMA') }} - {{ $title ?? ucwords(str_replace('-', ' ', request()->segment(1))) }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta content="Arsip Digital Satu Data Mipa(SAPA)" name="description" />
        <meta content="MyraStudio, TIM TIK FMIPA UNTAN" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('SAPA LOGO.png') }}">
        <link rel="icon" type="image/x-icon" href="{{ asset('SAPA LOGO.png') }}">
        <link rel="apple-touch-icon" href="{{ asset('SAPA LOGO.png') }}">
        <link rel="icon" type="image/x-icon" sizes="32x32" href="{{ asset('SAPA LOGO 32.ico') }}">
        <link rel="icon" type="image/x-icon" sizes="16x16" href="{{ asset('SAPA LOGO 16.ico') }}">
        
        {{-- Addition JS --}}
        <style>
            body {
                font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji !important;
            }
        </style>
        @stack('css')

        <!-- App css -->
        <link href="{{ asset('/scoxe/vertical/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/scoxe/vertical/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/scoxe/vertical/css/theme.min.css') }}" rel="stylesheet" type="text/css" />
    </head>
<body>
    
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!--- Topmenu -->
        <header id="page-topbar">
            <div class="navbar-header" style="background-color: #677ecd">
                <!-- LOGO -->
                <div class="navbar-brand-box d-flex align-items-left">
                    {{-- <a href="{{ route('beranda') }}" class="logo">
                        <i class="mdi mdi-album"></i>
                        <span>
                            {{ env('APP_NAMA') }}
                        </span>
                    </a> --}}
                    <img src="{{ asset('SAPA.png') }}" alt="Logo" class="logo" width="150px">/span>
                    </a>
                </div>
            </div>

            <!-- Alert Toast-->
            <div aria-live="polite" aria-atomic="true" style="position: relative; min-height: 200px; z-index:9999; right:20px;">
                <!-- Position it -->
                <div style="position: absolute; top: 10px; right: 10px;">

                    <!-- Then put toasts within -->
                    <div class="toast" role="alert" aria-live="assertive" aria-atomic="true"  data-delay="8000">
                        <div class="toast-header bg-danger text-white">
                            <img src="{{ asset('SAPA LOGO.png') }}" class="rounded mr-2" alt="..." width="40px">
                            <strong class="mr-auto">Notifikasi</strong>
                            <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="toast-body p-3">
                            {{-- Message --}}
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!--- Topmenu -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content" >

            @yield('content')

            @include('layouts.scoxe.include.footer')

        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    <!-- Overlay-->
    <div class="menu-overlay"></div>


    <!-- jQuery  -->
    <script src="{{ asset('/scoxe/vertical/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/metismenu.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/waves.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/simplebar.min.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    {{-- Addition JS --}}
    @stack('js')

    @if ($errors->any())
        <script>
            console.log("{{ $errors }}");
            $(".toast-body").html('{{ $errors->first() }}');
            $('.toast').toast('show')
        </script>
    @endif
    <!-- App js -->
    <script src="{{ asset('/scoxe/vertical/js/theme.js') }}"></script>

</body>
</html>