<header id="page-topbar">
    <div class="navbar-header" style="background-color: #677ecd">
        <!-- LOGO -->
        <div class="navbar-brand-box d-flex align-items-left">
            {{-- <a href="{{ route('beranda') }}" class="logo">
                <i class="mdi mdi-album"></i>
                <span>
                    {{ env('APP_NAMA') }}
                </span>
            </a> --}}
            <img src="{{ asset('SAPA.png') }}" alt="Logo" class="logo" width="150px">

            <button type="button" class="btn btn-sm mr-2 d-lg-none px-3 font-size-16 header-item waves-effect waves-light" id="vertical-menu-btn">
                <i class="fa fa-fw fa-bars"></i>
            </button>
        </div>

        <div class="d-flex align-items-center">

            {{-- <div class="dropdown d-none d-sm-inline-block ml-2">
                <button type="button" class="btn header-item noti-icon waves-effect waves-light" id="page-header-search-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="mdi mdi-magnify"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                    aria-labelledby="page-header-search-dropdown">
                    
                    <form class="p-3">
                        <div class="form-group m-0">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div> --}}
            <div class="d-sm-inline-block ml-2">
                <a href="{{ asset('Buku Manual SAPA.pdf') }}" class="btn btn-warning" target="_blank" >
                    <i class="mdi mdi-book"></i>
                    Buku Panduan 
                </a>
            </div>

            {{-- Language --}}
            <x-layouts.language></x-layouts.language>
            
            {{-- Notification --}}
            <x-layouts.language></x-layouts.language>

            <div class="dropdown d-inline-block ml-2">
                <button type="button" class="btn header-item waves-effect waves-light" id="page-header-user-dropdown"
                    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img class="rounded-circle header-profile-user" src="{{ asset('/scoxe/vertical/images/users/avatar-1.jpg') }}"
                        alt="Header Avatar">
                    <span class="d-none d-sm-inline-block ml-1">{{ Auth::user()->name ?? "-" }}</span>
                    <i class="mdi mdi-chevron-down d-none d-sm-inline-block"></i>
                </button>
                <div class="dropdown-menu dropdown-menu-right">
                    {{-- <a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)">
                        <span>Inbox</span>
                        <span>
                            <span class="badge badge-pill badge-info">3</span>
                        </span>
                    </a> --}}
                    <a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ route('profile') }}">
                        <span>Profile</span>
                    </a>
                    {{-- <a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)">
                        Settings
                    </a> --}}
                    {{-- <a class="dropdown-item d-flex align-items-center justify-content-between" href="javascript:void(0)">
                        <span>Lock Account</span>
                    </a> --}}
                    @guest
                        <a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ route('login') }}">
                            <span>Log In</span>
                        </a>
                        <a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ route('register') }}">
                            <span>Register</span>
                        </a>
                    @else
                        <a class="dropdown-item d-flex align-items-center justify-content-between" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();" >
                            <span>Log Out</span>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                            </form>
                        </a>
                    @endguest
                </div>
            </div>
            
        </div>
    </div>
</header>