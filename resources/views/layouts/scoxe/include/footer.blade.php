<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-4">
                2023 © TIK FMIPA UNTAN.
            </div>
            <div class="col-sm-4 text-warning text-center text-uppercase">
                <b>
                    --- Quality is Our Concern ---
                </b>
            </div>
            <div class="col-sm-4">
                <div class="text-sm-right d-none d-sm-block">
                    ARSIP DIGITAL V2.0
                </div>
            </div>
        </div>
    </div>
</footer>