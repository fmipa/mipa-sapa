<div id="sidebar-menu">
    <!-- Left Menu Start -->
    <ul class="metismenu list-unstyled" id="side-menu">
        <li class="menu-title">Modul</li>

        <x-menus.menu route="{{ route('beranda') }}" active="beranda" title="Beranda" icon="feather-airplay"></x-menus.menu>
        
        @can('file')
        <x-menus.menu route="{{ route('file.index') }}" active="file" title="Semua File" icon="mdi mdi-file-document-outline"></x-menus.menu>
        @endcan
        
        @if(auth()->user()->is_operator)
        @can('berkaspegawai')
        <x-menus.menu route="{{ route('berkas-pegawai.index') }}" active="berkas-pegawai" title="Berkas Pegawai" icon="mdi mdi-file-cabinet"></x-menus.menu>
        @endcan
        
        @can('suratmasuk')
        <x-menus.menu route="{{ route('surat-masuk.index') }}" active="surat-masuk" title="Surat Masuk" icon="mdi mdi-file-undo"></x-menus.menu>
        @endcan
        
        @can('suratkeluar')
        <x-menus.menu route="{{ route('surat-keluar.index') }}" active="surat-keluar" title="Surat Keluar" icon="mdi mdi-file-move"></x-menus.menu>
        @endcan
        
        @can('sk')
        <x-menus.menu route="{{ route('sk.index') }}" active="sk" title="SK" icon="mdi mdi-file-account"></x-menus.menu>
        @endcan
        @endif
        
        @can('document')
        <x-menus.menu route="{{ route('dokumen.index') }}" active="document" title="Dokumen" icon="mdi mdi-file-document-box-multiple-outline"></x-menus.menu>
        @endcan
        
        <li class="menu-title">Manajemen</li>
        
        @can('box')
        <x-menus.menu route="{{ route('box.index') }}" active="box" title="Box / Gobi" icon="mdi mdi-dropbox"></x-menus.menu>
        @endcan

        @if(auth()->user()->is_operator)
        @can('folder')
        <x-menus.menu route="{{ route('folder.index') }}" active="folder" title="Folder" icon="mdi mdi-file-tree"></x-menus.menu>
        @endcan

        @can('pegawai')
        <x-menus.menu route="{{ route('pegawai.index') }}" active="pegawai" title="Pegawai" icon="mdi mdi-account-card-details-outline"></x-menus.menu>
        @endcan

        @canany(['jurusan', 'prodi', 'agama', 'kberkas', 'kodeunit', 'kodenaskah'])
        <x-menus.dropdown-menu title="Master" icon="mdi mdi-view-list">
        @endcan

            @can('jurusan')
            <x-menus.level-menu route="{{ route('jurusan.index') }}" active="jurusan" titleMenu="Jurusan" classMenu="Master"></x-menus.level-menu>
            @endcan

            @can('prodi')
            <x-menus.level-menu route="{{ route('prodi.index') }}" active="prodi" titleMenu="Prodi" classMenu="Master"></x-menus.level-menu>
            @endcan

            @can('agama')
            <x-menus.level-menu route="{{ route('master-agama.index') }}" active="master-agama" titleMenu="Master Agama" classMenu="Master"></x-menus.level-menu>
            @endcan
            
            @can('kberkas')
            <x-menus.level-menu route="{{ route('kategori-berkas.index') }}" active="kategori-berkas" titleMenu="Kategori Berkas" classMenu="Master"></x-menus.level-menu>
            @endcan

            @can('kodeunit')
            <x-menus.level-menu route="{{ route('kode-unit.index') }}" active="kode-unit" titleMenu="Kode Unit" classMenu="Master"></x-menus.level-menu>
            @endcan
            
            @can('kodenaskah')
            <x-menus.level-menu route="{{ route('kode-naskah.index') }}" active="kode-naskah" titleMenu="Kode Naskah" classMenu="Master"></x-menus.level-menu>
            @endcan
            
        </x-menus.dropdown-menu>

        @can('extension')
        <x-menus.menu route="{{ route('ekstensi.index') }}" active="ekstensi" title="Ekstensi File" icon="mdi mdi-file-check-outline"></x-menus.menu>
        @endcan

        @can('folder-storage')
        <x-menus.menu route="{{ route('folder-storage.index') }}" active="folder-storage" title="Folder Storage" icon="mdi mdi-folder-outline"></x-menus.menu>
        @endcan

        @can('permission')
        <x-menus.menu route="{{ route('izin.index') }}" active="permission" title="Izin (Permission)" icon="mdi mdi-shield-lock-outline"></x-menus.menu>
        @endcan

        @can('hakakses')
        <x-menus.menu route="{{ route('hak-akses.index') }}" active="hakakses" title="Hak Akses" icon="mdi mdi-account-key-outline"></x-menus.menu>
        @endcan

        @can('user')
        <x-menus.menu route="{{ route('pengguna.index') }}" active="pengguna" title="Pengguna" icon="mdi mdi-account-group"></x-menus.menu>
        @endcan
        @endif

        <li>
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="waves-effect text-danger">
                <i class="mdi mdi-exit-to-app"></i>
                <span>Logout</span>
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                </form>
            </a>
        </li>
    </ul>

    <div class="help-box">
        <h5 class="text-muted font-size-15 mb-3">Butuh Bantuan</h5>
        <p class="font-size-13"><span class="font-weight-bold">Email:</span> <br> tik@fmipa.untan.ac.id</p>
        <p class="font-size-13"><span class="font-weight-bold">Telepon/WA:</span> <br> <a href="https://wa.me/085386150568" target="_blank" rel="noopener noreferrer">(+62) 853 8615 0568</a></p>
        <p class="mb-0 font-size-13"><span class="font-weight-bold">Alamat:</span> <br> <a href="https://maps.app.goo.gl/ZDPzbz23C5M2rfQN9" target="_blank" rel="noopener noreferrer">Fakultas Matematika dan Ilmu Pengetahuan Alam (FMIPA) Universitas Tanjunpura</a></p>
    </div>
</div>