<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{{ env('APP_NAMA') }} - {{ $title ?? ucwords(str_replace('-', ' ', request()->segment(1))) }}</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta content="Arsip Digital Satu Data Mipa(SAPA)" name="description" />
        <meta content="MyraStudio, TIM TIK FMIPA UNTAN" name="author" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
        <!-- App favicon -->
        <link rel="shortcut icon" href="{{ asset('SAPA LOGO.png') }}">
        <link rel="icon" type="image/x-icon" href="{{ asset('SAPA LOGO.png') }}">
        <link rel="apple-touch-icon" href="{{ asset('SAPA LOGO.png') }}">
        <link rel="icon" type="image/x-icon" sizes="32x32" href="{{ asset('SAPA LOGO 32.ico') }}">
        <link rel="icon" type="image/x-icon" sizes="16x16" href="{{ asset('SAPA LOGO 16.ico') }}">
        
        {{-- Addition JS --}}
        <style>
            body {
                font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Roboto,Helvetica Neue,Arial,Noto Sans,sans-serif,Apple Color Emoji,Segoe UI Emoji,Segoe UI Symbol,Noto Color Emoji !important;
            }
        </style>
        
        @stack('css')

        <!-- App css -->
        <link href="{{ asset('/scoxe/vertical/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/scoxe/vertical/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/scoxe/vertical/css/theme.min.css') }}" rel="stylesheet" type="text/css" />
    </head>
<body>
    
    <!-- Begin page -->
    <div id="layout-wrapper">

        <!--- Topmenu -->
        @include('layouts.scoxe.include.topmenu')
        <!--- Topmenu -->
        
        <!-- ========== Left Sidebar Start ========== -->
        <div class="vertical-menu">

            <div data-simplebar class="h-100">

                <!--- Sidemenu -->
                @include('layouts.scoxe.include.sidemenu')
                <!-- Sidebar -->
            </div>
        </div>
        <!-- Left Sidebar End -->

        <!-- ============================================================== -->
        <!-- Start right Content here -->
        <!-- ============================================================== -->
        <div class="main-content">

            @yield('content')

            @include('layouts.scoxe.include.footer')

        </div>
        <!-- end main content-->

    </div>
    <!-- END layout-wrapper -->

    <!-- Overlay-->
    <div class="menu-overlay"></div>


    <!-- jQuery  -->
    <script src="{{ asset('/scoxe/vertical/js/jquery.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/metismenu.min.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/waves.js') }}"></script>
    <script src="{{ asset('/scoxe/vertical/js/simplebar.min.js') }}"></script>

    {{-- Addition JS --}}
    @stack('js')

    <!-- App js -->
    <script src="{{ asset('/scoxe/vertical/js/theme.js') }}"></script>

</body>
</html>