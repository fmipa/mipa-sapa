<div {{ $attributes->merge(['class' => 'alert alert-danger '.$class]) }} role="alert">
    <strong>{{ $message }}</strong>
</div>