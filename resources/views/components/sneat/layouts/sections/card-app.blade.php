    <div class="bs-toast toast toast-placement-ex m-2" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="3000">
        <div class="toast-header">
            <i class="bx bx-bell me-2"></i>
            <div class="me-auto fw-medium">Notifikasi</div>
            {{-- <small>11 mins ago</small> --}}
            <button type="button" class="btn-close" data-bs-dismiss="toast" aria-label="Close"></button>
        </div>
        <div class="toast-body">
            
        </div>
    </div>

    <div class="card">
        @if ($card == true)
        <div class="py-3 card-header row pb-0">
            <div class="col col-lg-6 col-sm-12">
                <h4 class=" m-0"><span class="text-muted fw-light">{{ $pageName ? $pageName.' / ' : '' }} </span> {{ $subPageName }}</h4>
            </div>
            <div class="col col-lg-6 col-sm-12">
                @if ($create == true)
                <x-ahref :link="$createUrl" class="btn btn-primary float-end" id="tambah" text="Tambah"></x-ahref>
                @endif
                @if ($back == true)
                <x-ahref :link="$backUrl" class="btn btn-secondary float-end" id="kembali" text="Kembali"></x-ahref> 
                @endif
            </div>
        </div>
        @endif
        <hr class="my-2">
        <div class="card-body">
            <div class="row">
                <div class="col col-12 px-3 py-2">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    
                    {{ $slot }}
                </div>
            </div>
        </div>
    </div>