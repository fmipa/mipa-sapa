<footer class="landing-footer bg-body footer-text">
    {{-- <div class="footer-top">
        <div class="container">
            <div class="row gx-0 gy-4 g-md-5">
                <div class="col-lg-5">
                    <a href="landing-page.html" class="app-brand-link mb-4">
                    <span class="app-brand-logo demo">

                    </span>
                    <span class="app-brand-text demo footer-link fw-bold ms-2 ps-1">
                        <img src="{{ asset(config('variables.logoSAPA')) }}" alt="Logo" class="logo" width="250px"></span>
                    </a>
                    <p class="footer-text footer-logo-description mb-4">
                        Aplikasi SAPA (Satu Data FMIPA) merupakan aplikasi yang menghubungkan semua sistem aplikasi yang ada di fakultas MIPA UNTAN.
                    </p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <h6 class="footer-title mb-4">Link Terkait</h6>
                    <ul class="list-unstyled">
                    <li class="mb-3">
                        <a href="mipa.untan.ac.id" target="_blank" class="footer-link">FMIPA UNTAN</a>
                    </li>
                    <li class="mb-3">
                        <a href="untan.ac.id" target="_blank" class="footer-link">Universitas Tanjungpura</a>
                    </li>
                    <li class="mb-3">
                        <a href="#" target="_blank" class="footer-link">E-Meeting</a>
                    </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-4">
                    <h6 class="footer-title mb-4">Download our app</h6>
                    <a href="javascript:void(0);" class="d-block footer-link mb-3 pb-2"><img src="{{ asset('/sneat/img/front-pages/landing-page/apple-icon.png') }}" alt="apple icon" /></a>
                    <a href="javascript:void(0);" class="d-block footer-link"><img src="{{ asset('/sneat/img/front-pages/landing-page/google-play-icon.png') }}" alt="google play icon" /></a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom py-3"> --}}
    <div class="footer-top py-3">
        <div class="container d-flex flex-wrap justify-content-between flex-md-row flex-column text-center text-md-start">
            <div class="mb-2 mb-md-0">
                <span class="footer-text">© 2023</span>
                <a href="{{ config('variables.creatorUrl') }}" target="_blank" class="fw-medium text-white footer-link">{{ config('variables.creatorInstitute') }},</a>
                <span class="footer-text">Made with ❤️ {{ config('variables.creatorName') }}</span>
            </div>
            <div class="text-warning text-center text-uppercase">
                <b>
                    {{ config('variables.quotes') }}
                </b>
            </div>
        </div>
    </div>
</footer>