<!DOCTYPE html>

<!-- =========================================================
    * Sneat - Bootstrap 5 HTML Admin Template - Pro | v1.0.0
    ==============================================================

    * Product Page: https://themeselection.com/item/sneat-bootstrap-html-admin-template/
    * Created by: ThemeSelection
    * License: You must have a valid license purchased in order to legally use the theme for your project.
    * Copyright ThemeSelection (https://themeselection.com)

    =========================================================
-->

<html 
    clang="en"
    class="light-style layout-navbar-fixed layout-wide"
    dir="ltr"
    data-theme="theme-default" 
    data-assets-path="{{ asset('/'.config('variables.templateName')) . '/' }}" 
    data-base-url="{{url('/')}}" 
    data-framework="laravel" 
    data-template="front-pages"
>

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <title>{{ config('variables.appName') ?? 'Sistem Kearsipan SAPA' }} - {{ $title ?? ucwords(str_replace('-', ' ', request()->segment(1))) }} </title>

    <meta name="description" content="{{ config('variables.appDescription') ?? '' }}" />
    <meta name="keywords" content="{{ config('variables.appKeyword') ?? '' }}">
    <meta name="author" content="{{ config('variables.creatorName') ?? '' }}">
    <!-- laravel CRUD token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Canonical SEO -->
    <link rel="canonical" href="{{ config('variables.productPage') ?? '' }}">
    <!-- App favicon -->
    @include('components/'.config('variables.templateName').'/layouts/sections/asset/favicon')
    
    <!-- Include Styles -->
    @include('components/'.config('variables.templateName').'/layouts/sections/horizontal/asset/styles')

    <!-- Include Scripts for customizer, helper, analytics, config -->
    @include('components/'.config('variables.templateName').'/layouts/sections/horizontal/asset/scriptsIncludes')
</head>

<body>

    
    <!-- ?PROD Only: Google Tag Manager (noscript) (Default ThemeSelection: GTM-5DDHKGP, PixInvent: GTM-5J3LMKC) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DDHKGP" height="0" width="0" style="display: none; visibility: hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    
    <script src="{{ asset('/sneat/vendor/js/dropdown-hover.js') }}"></script>
    <script src="{{ asset('/sneat/vendor/js/mega-dropdown.js') }}"></script>
    {{-- Spinner: Start --}}
    <div class="bg-spin">
        <div class="sk-folding-cube center">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>
    {{-- Spinner: End --}}
    
    <!-- Include Navbar -->
    @include('components/'.config('variables.templateName').'/layouts/sections/horizontal/navbar/navbar')

    <!-- Layout Content -->
    <div data-bs-spy="scroll" class="scrollspy-example">
        @yield('layoutContent')
    </div>
    <!--/ Layout Content -->

    <!-- Footer: Start -->
    @include('components/'.config('variables.templateName').'/layouts/sections/horizontal/footer/footer')

    <!-- Footer: End -->

    {{-- remove while creating package --}}
    @include('components/'.config('variables.templateName').'/layouts/sections/helpdesk')
    {{-- remove while creating package end --}}

    <!-- Include Scripts -->
    @include('components/'.config('variables.templateName').'/layouts/sections/horizontal/asset/scripts')

</body>

</html>
