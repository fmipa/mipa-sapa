<!-- BEGIN: Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Public+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;1,300;1,400;1,500;1,600;1,700&display=swap" rel="stylesheet">
    <!-- END: Fonts -->

    <!-- BEGIN: Icons -->
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/fonts/boxicons.css') }}" />
    <!-- END: Icons -->
        
    <!-- BEGIN: Core Style -->
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/css/core.css') }}" class="template-customizer-core-css" />
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/css/theme-default.css') }}" class="template-customizer-theme-css" />
    <link rel="stylesheet" href="{{ asset('/sneat/css/demo.css') }}" />
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/css/pages/front-page.css') }}" />
@stack('style-core')
    <!-- END: Core Style -->

    <!-- BEGIN: Vendors Style -->
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/nouislider/nouislider.css') }}" />
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/swiper/swiper.css') }}" />
@stack('style-vendor')
    <!-- END: Vendors Style -->
    
    <!-- BEGIN: Page Styles -->
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/css/pages/front-page-landing.css') }}" />
    <style>
        .in-error {
            border: 1px solid #ff3e1d;
        }
        
        .sk-folding-cube {
            z-index: 999;
            margin: 20px auto;
            width: 100px;
            height: 100px;
            position: relative;
            -webkit-transform: rotateZ(45deg);
                transform: rotateZ(45deg);
        }

        .sk-folding-cube .sk-cube {
        float: left;
        width: 50%;
        height: 50%;
        position: relative;
        -webkit-transform: scale(1.1);
            -ms-transform: scale(1.1);
                transform: scale(1.1); 
        }
        .sk-folding-cube .sk-cube:before {
        content: '';
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: #696cff;
        -webkit-animation: sk-foldCubeAngle 2.4s infinite linear both;
                animation: sk-foldCubeAngle 2.4s infinite linear both;
        -webkit-transform-origin: 100% 100%;
            -ms-transform-origin: 100% 100%;
                transform-origin: 100% 100%;
        }
        .sk-folding-cube .sk-cube2 {
        -webkit-transform: scale(1.1) rotateZ(90deg);
                transform: scale(1.1) rotateZ(90deg);
        }
        .sk-folding-cube .sk-cube3 {
        -webkit-transform: scale(1.1) rotateZ(180deg);
                transform: scale(1.1) rotateZ(180deg);
        }
        .sk-folding-cube .sk-cube4 {
        -webkit-transform: scale(1.1) rotateZ(270deg);
                transform: scale(1.1) rotateZ(270deg);
        }
        .sk-folding-cube .sk-cube2:before {
        -webkit-animation-delay: 0.3s;
                animation-delay: 0.3s;
        }
        .sk-folding-cube .sk-cube3:before {
        -webkit-animation-delay: 0.6s;
                animation-delay: 0.6s; 
        }
        .sk-folding-cube .sk-cube4:before {
        -webkit-animation-delay: 0.9s;
                animation-delay: 0.9s;
        }

        .center {
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
        }

        .bg-spin {
            position:fixed;
            width:100%;
            left:0;right:0;top:0;bottom:0;
            background-color: #e7e7ff;
            z-index:999;
            /* display:none; */
        }
        
        @-webkit-keyframes sk-foldCubeAngle {
            0%, 10% {
                -webkit-transform: perspective(140px) rotateX(-180deg);
                        transform: perspective(140px) rotateX(-180deg);
                opacity: 0; 
            } 25%, 75% {
                -webkit-transform: perspective(140px) rotateX(0deg);
                        transform: perspective(140px) rotateX(0deg);
                opacity: 1; 
            } 90%, 100% {
                -webkit-transform: perspective(140px) rotateY(180deg);
                        transform: perspective(140px) rotateY(180deg);
                opacity: 0; 
        } 
        }

        @keyframes sk-foldCubeAngle {
            0%, 10% {
                -webkit-transform: perspective(140px) rotateX(-180deg);
                        transform: perspective(140px) rotateX(-180deg);
                opacity: 0; 
            } 25%, 75% {
                -webkit-transform: perspective(140px) rotateX(0deg);
                        transform: perspective(140px) rotateX(0deg);
                opacity: 1; 
            } 90%, 100% {
                -webkit-transform: perspective(140px) rotateY(180deg);
                        transform: perspective(140px) rotateY(180deg);
                opacity: 0; 
            }
        }

        
    </style>
@stack('style-page')
    <!-- END: Page Styles -->
