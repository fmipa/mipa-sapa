<!-- BEGIN: Core Script -->
    <script src="{{ asset('/sneat/vendor/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('/sneat/vendor/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('/sneat/vendor/js/bootstrap.js') }}"></script>
    @stack('script-core')
    <!-- End: Core Script -->

    <!-- BEGIN: Vendor Script -->
    <script src="{{ asset('/sneat/vendor/libs/nouislider/nouislider.js') }}"></script>
    <script src="{{ asset('/sneat/vendor/libs/swiper/swiper.js') }}"></script>
@stack('script-vendor')
    <!-- END: Vendor Script -->
    
    <!-- BEGIN: Main Script -->
    {{-- <script src="{{ asset('/sneat/js/front-main.js') }}"></script> --}}
    <script src="{{ asset('/sneat/js/main.js') }}"></script>
    <!-- END: Main Script -->

    <!-- BEGIN: Page Script -->
    {{-- <script src="{{ asset('/sneat/js/front-page-landing.js') }}"></script> --}}
    {{-- <script src="{{ asset('/sneat/js/page-auth.js') }}"></script> --}}
@stack('script-page')
    <script>
        function spinner(section, event) {
            if (event) {
                document.querySelector(section).style.visibility = "hidden";
                document.querySelector(".bg-spin").style.visibility = "visible";
                document.querySelector(".sk-folding-cube").style.visibility = "visible";
            } else {
                setTimeout(() => {  
                document.querySelector(".bg-spin").style.display = "none";
                    document.querySelector(".sk-folding-cube").style.display = "none";
                    document.querySelector(section).style.visibility = "visible";
                }, 2000);
            }
        }

        document.onreadystatechange = function () {
            if (document.readyState !== "complete") {
                spinner("body", false);
            } else {
                spinner("body", true);
            }
        };
    </script>
    <!-- END: Page Script -->