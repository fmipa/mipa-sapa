<nav class="layout-navbar shadow-none py-0">
    <div class="container">
        <div class="navbar navbar-expand-lg landing-navbar px-3 px-md-4">
            <!-- Menu logo wrapper: Start -->
            <div class="navbar-brand app-brand demo d-flex py-0 me-4">
                <!-- Mobile menu toggle: Start-->
                <button class="navbar-toggler border-0 px-0 me-2" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="tf-icons bx bx-menu bx-sm align-middle"></i>
                </button>
                <!-- Mobile menu toggle: End-->
                <a href="{{ url('/') }}" class="app-brand-link">
                    <span class="app-brand-logo demo">
                        <img src="{{ asset(config('variables.logoSAPA')) }}" alt="Logo" class="logo" width="100px">
                    </span>

                    {{-- <span class="app-brand-text demo menu-text fw-bold ms-2 ps-1">SAPA</span> --}}
                </a>
            </div>
            <!-- Menu logo wrapper: End -->
            <!-- Menu wrapper: Start -->
            <div class="collapse navbar-collapse landing-nav-menu" id="navbarSupportedContent">
                <button class="navbar-toggler border-0 text-heading position-absolute end-0 top-0 scaleX-n1-rtl" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="tf-icons bx bx-x bx-sm"></i>
                </button>
                <ul class="navbar-nav me-auto">
                    @if (auth()->check())
                    <li class="nav-item">
                        <a class="nav-link fw-medium" aria-current="page" href="{{ route('beranda') }}">Beranda</a>
                    </li>
                    @if(auth()->user()->hasRole('super-admin') || auth()->user()->pegawai->unit_kerja_id == 12)
                    <li class="nav-item active">
                        <a class="nav-link fw-medium" href="{{ route('file.folderView') }}">File Folder</a>
                    </li>
                    @endif
                    @can('file')
                    <li class="nav-item">
                        <a class="nav-link fw-medium" href="{{ route('file.index') }}">Semua File</a>
                    </li>
                    @endcan
                    @if(auth()->user()->is_operator)
                    @can('berkaspegawai')
                    <li class="nav-item">
                        <a class="nav-link fw-medium" href="{{ route('berkas-pegawai.index') }}">Berkas Pegawai</a>
                    </li>
                    @endcan
                    @can('suratmasuk')
                    <li class="nav-item">
                        <a class="nav-link fw-medium" href="{{ route('surat-masuk.index') }}">Surat Masuk</a>
                    </li>
                    @endcan
                    @can('suratkeluar')
                    <li class="nav-item">
                        <a class="nav-link fw-medium" href="{{ route('surat-keluar.index') }}">Surat Keluar</a>
                    </li>
                    @endcan
                    @can('sk')
                    <li class="nav-item">
                        <a class="nav-link fw-medium" href="{{ route('sk.index') }}">SK</a>
                    </li>
                    @endcan
                    @endif

                    @can('document')
                    <li class="nav-item">
                        <a class="nav-link fw-medium" href="{{ route('dokumen.index') }}">Dokumen</a>
                    </li>
                    @endcan
                    
                    @else
                    
                    <div id="scroll-container">
                        <div id="scroll-text"><img src="{{ asset(config('variables.logoUntan')) }}" alt="" width="30px"> <b class="ml-1 mr-1">{{ config('variables.appDescription') }}</b> <img src="{{  asset(config('variables.logoUntan')) }}" alt="" width="30px"><div>
                    </div>
                    @endif
                </ul>
            </div>
            <div class="landing-menu-overlay d-lg-none"></div>
            <!-- Menu wrapper: End -->
            <!-- Toolbar: Start -->
            <ul class="navbar-nav flex-row align-items-center ms-auto">
                <!-- navbar button: Start -->
                <li>
                    @guest
                        <a href="{{ route('login') }}" class="btn btn-info"><span class="tf-icons bx bx-user me-md-1"></span><span class="d-none d-md-block">Login</span></a>
                        @else
                        <a href="{{ route('logout') }}" class="btn btn-danger" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="waves-effect text-danger">
                            <span class="tf-icons bx bx-user me-md-1"></span>
                            <i class="mdi mdi-exit-to-app"></i>
                            <span>Logout</span>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST">
                                @csrf
                            </form>
                        </a>
                    @endguest
                </li>
                <!-- navbar button: End -->
            </ul>
            <!-- Toolbar: End -->
        </div>
    </div>
</nav>