<link rel="shortcut icon" href="{{ asset('/images/logo/SAPA LOGO.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('/images/logo/SAPA LOGO.png') }}">
    <link rel="apple-touch-icon" href="{{ asset('/images/logo/SAPA LOGO.png') }}">
    <link rel="icon" type="image/x-icon" sizes="32x32" href="{{ asset('/images/logo/SAPA LOGO 32.ico') }}">
    <link rel="icon" type="image/x-icon" sizes="16x16" href="{{ asset('/images/logo/SAPA LOGO 16.ico') }}">