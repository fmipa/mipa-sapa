@extends('layouts.sneat.vertical.mastervt-app')

@push('style-core')
@endpush

@push('style-vendor')   
@endpush

@push('style-page')
<style>
    .select2-search__field {
        width: 100%!important;
    }
    .light-style .select2-container--default .select2-selection--multiple .select2-selection__choice {
        color: #696cff;
        background-color: rgba(67,89,113,.08);
    }
</style>
@endpush

@section('layoutContent')
    <!-- Layout wrapper -->
    <div class="layout-wrapper layout-content-navbar">
        <div class="layout-container">
            <!-- Menu -->
    
            @include('components/'.config('variables.templateName').'/layouts/sections/vertical/menus/menus')
            <!-- / Menu -->
    
            <!-- Layout container -->
            <div class="layout-page">
                <!-- Navbar -->
                @include('components/'.config('variables.templateName').'/layouts/sections/vertical/navbar/navbar')
                <!-- / Navbar -->

                <!-- Content wrapper -->
                <div class="content-wrapper">
                    <div class="{{ config('variables.templateContainer') }} flex-grow-1 container-p-y">
                    
                        <!-- Content -->
                        @yield('content')
                        <!-- / Content -->
                        
                        <!-- Footer -->
                        @include('components/'.config('variables.templateName').'/layouts/sections/vertical/footer/footer')
                        <!-- / Footer -->

                        <div class="content-backdrop fade"></div>
                    </div>
                </div>
                <!-- Content wrapper -->

            </div>
            <!-- / Layout page -->
        </div>
        <!--/ Layout container -->

        <!-- Overlay -->
        <div class="layout-overlay layout-menu-toggle"></div>

        <!-- Drag Target Area To SlideIn Menu On Small Screens -->
        <div class="drag-target"></div>
    </div>
    <!-- / Layout wrapper -->

@endsection

@push('script-core')
@endpush

@push('script-vendor')
@endpush

@push('script-page')
@endpush