<!-- BEGIN: Laravel Style -->
    <script src="{{ asset('/sneat/vendor/js/helpers.js') }}"></script>
    <!-- END: Laravel Style -->
    <!--! Template customizer & Theme config files MUST be included after core stylesheets and helpers.js in the <head> section -->
    <!--? Template customizer: To hide customizer set displayCustomizer value false in config.js.  -->
    {{-- <script src="{{ asset('/sneat/vendor/js/template-customizer.js') }}"></script> --}}
    <!--? Config:  Mandatory theme config file contain global vars & default theme options, Set your preferred theme option in this file.  -->
    <script src="{{ asset('/sneat/js/config.js') }}"></script>

    <!-- ?PROD Only: Google Tag Manager (noscript) (Default ThemeSelection: GTM-5DDHKGP, PixInvent: GTM-5J3LMKC) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5DDHKGP" height="0" width="0" style="display: none; visibility: hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

    <!-- BEGIN: Dropdown Style -->
    {{-- <script src="{{ asset('/sneat/vendor/js/dropdown-hover.js') }}"></script>
    <script src="{{ asset('/sneat/vendor/js/mega-dropdown.js') }}"></script> --}}
    <!-- END: Dropdown Style -->
