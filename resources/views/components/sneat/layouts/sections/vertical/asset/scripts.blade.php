<!-- BEGIN: Core Script -->
    <script src="{{ asset('/sneat/vendor/libs/jquery/jquery.js') }}"></script>
    <script src="{{ asset('/sneat/vendor/libs/popper/popper.js') }}"></script>
    <script src="{{ asset('/sneat/vendor/js/bootstrap.js') }}"></script>
    <script src="{{ asset('/sneat/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
    <script src="{{ asset('/sneat/js/menu.js') }}"></script>
    {{-- <script src="{{ asset('/sneat/vendor/js/menu.js') }}"></script> --}}
@stack('script-core')
    <!-- End: Core Script -->

    <!-- BEGIN: Vendor Script -->
@stack('script-vendor')
    <!-- END: Vendor Script -->

    <!-- BEGIN: Page Script -->
@stack('script-page')
    <script>
        function spinner(section, event) {
            if (event) {
                document.querySelector(section).style.visibility = "hidden";
                document.querySelector(".bg-spin").style.visibility = "visible";
                document.querySelector(".sk-folding-cube").style.visibility = "visible";
            } else {
                setTimeout(() => {  
                document.querySelector(".bg-spin").style.display = "none";
                    document.querySelector(".sk-folding-cube").style.display = "none";
                    document.querySelector(section).style.visibility = "visible";
                }, 2000);
            }
        }

        document.onreadystatechange = function () {
            if (document.readyState !== "complete") {
                spinner("body", false);
            } else {
                spinner("body", true);
            }
        };
    </script>
    <!-- END: Page Script -->

    <!-- BEGIN: Main Script -->
    <script src="{{ asset('/sneat/js/main.js') }}"></script>
    <!-- END: Main Script -->

    <!-- Place this tag in your head or just before your close body tag. -->
    <script async defer src="https://buttons.github.io/buttons.js"></script>
