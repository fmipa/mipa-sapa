<aside id="layout-menu" class="layout-menu menu-vertical menu bg-menu-theme">
    <div class="app-brand demo">
        <a href="{{ url('/') }}" class="app-brand-link gap-2">
            <span class="app-brand-logo demo">
                <img src="{{ asset(config('variables.logoSAPA')) }}" alt="" width="150px">
            </span>
        </a>

        <a href="javascript:void(0);" class="layout-menu-toggle menu-link text-large ms-auto">
            <i class="bx bx-chevron-left bx-sm align-middle"></i>
        </a>
    </div>

    <div class="menu-divider mt-0  "></div>

    <ul class="menu-inner py-1">
        <x-layouts.vertical.menus.menu route="{{ route('beranda') }}" active="beranda" nameMenu="Beranda" iconMenu="menu-icon tf-icons bx bxs-dashboard"></x-layouts.vertical.menus.menu>

        @if(auth()->user()->hasRole('super-admin') || auth()->user()->pegawai->unit_kerja_id == 12)
        <x-layouts.vertical.menus.menu route="{{ route('file.folderView') }}" active="file-folder" nameMenu="File Folder" iconMenu="menu-icon tf-icons bx bx-folder-open"></x-layouts.vertical.menus.menu>
        @endif

        @can('file')
        <x-layouts.vertical.menus.menu route="{{ route('file.index') }}" active="file" nameMenu="Semua File" iconMenu="menu-icon tf-icons bx bx-search-alt-2"></x-layouts.vertical.menus.menu>
        @endcan                  
        
        <li class="menu-header small text-uppercase">
            <span class="menu-header-text">Module</span>
        </li>

        {{-- Begin : Operator Menus --}}
        @if(auth()->user()->is_operator)
        @can('berkaspegawai')
        <x-layouts.vertical.menus.menu route="{{ route('berkas-pegawai.index') }}" active="berkas-pegawai" nameMenu="Berkas Pegawai" iconMenu="menu-icon tf-icons bx bx-archive"></x-layouts.vertical.menus.menu>
        @endcan
        
        @can('suratmasuk')
        <x-layouts.vertical.menus.menu route="{{ route('surat-masuk.index') }}" active="surat-masuk" nameMenu="Surat Masuk" iconMenu="menu-icon tf-icons bx bx-envelope"></x-layouts.vertical.menus.menu>
        @endcan
        
        @can('suratkeluar')
        <x-layouts.vertical.menus.menu route="{{ route('surat-keluar.index') }}" active="surat-keluar" nameMenu="Surat Keluar" iconMenu="menu-icon tf-icons bx bx-envelope-open"></x-layouts.vertical.menus.menu>
        @endcan
        
        @can('sk')
        <x-layouts.vertical.menus.menu route="{{ route('sk.index') }}" active="sk" nameMenu="SK" iconMenu="menu-icon tf-icons bx bx-food-menu"></x-layouts.vertical.menus.menu>
        @endcan
        
        @endif
        {{-- End : Operator Menus --}}
        
        @can('document')
        <x-layouts.vertical.menus.menu route="{{ route('dokumen.index') }}" active="dokumen" nameMenu="Dokumen" iconMenu="menu-icon tf-icons bx bx-file"></x-layouts.vertical.menus.menu>
        @endcan

        {{-- Begin : Operator Menus --}}
        @if(auth()->user()->is_operator)
        <li class="menu-header small text-uppercase">
            <span class="menu-header-text">Manajemen</span>
        </li>

        @can('box')
        <x-layouts.vertical.menus.menu route="{{ route('box.index') }}" active="box" nameMenu="Box / Gobi" iconMenu="menu-icon tf-icons bx bx-box"></x-layouts.vertical.menus.menu>
        @endcan

        @can('folder')
        <x-layouts.vertical.menus.menu route="{{ route('folder.index') }}" active="folder" nameMenu="Folder" iconMenu="menu-icon tf-icons bx bx-folder"></x-layouts.vertical.menus.menu>
        @endcan

        @can('pegawai')
        <x-layouts.vertical.menus.menu route="{{ route('pegawai.index') }}" active="pegawai" nameMenu="Pegawai" iconMenu="menu-icon tf-icons bx bx-group"></x-layouts.vertical.menus.menu>
        @endcan

        @can('user')
        <x-layouts.vertical.menus.menu route="{{ route('pengguna.index') }}" active="pengguna" nameMenu="Pengguna" iconMenu="menu-icon tf-icons bx bx-user"></x-layouts.vertical.menus.menu>
        @endcan


        @canany(['jurusan', 'prodi', 'agama', 'kberkas', 'kodeunit', 'kodenaskah'])
        <x-layouts.vertical.menus.dropdown-menu parentMenu="Master Data" parentIcon="menu-icon tf-icons bx bx-layout">
        @endcan
            @can('jurusan')
            <x-layouts.vertical.menus.menu route="{{ route('jurusan.index') }}" active="jurusan" nameMenu="Jurusan" classMenu="Master Data"></x-layouts.vertical.menus.menu>
            @endcan

            @can('prodi')
            <x-layouts.vertical.menus.menu route="{{ route('prodi.index') }}" active="prodi" nameMenu="Prodi" classMenu="Master Data"></x-layouts.vertical.menus.menu>
            @endcan

            @can('agama')
            <x-layouts.vertical.menus.menu route="{{ route('master-agama.index') }}" active="master-agama" nameMenu="Master Agama" classMenu="Master Data"></x-layouts.vertical.menus.menu>
            @endcan
            
            @can('kberkas')
            <x-layouts.vertical.menus.menu route="{{ route('kategori-berkas.index') }}" active="kategori-berkas" nameMenu="Kategori Berkas" classMenu="Master Data"></x-layouts.vertical.menus.menu>
            @endcan

            @can('kodeunit')
            <x-layouts.vertical.menus.menu route="{{ route('kode-unit.index') }}" active="kode-unit" nameMenu="Kode Unit" classMenu="Master Data"></x-layouts.vertical.menus.menu>
            @endcan
            
            @can('kodenaskah')
            <x-layouts.vertical.menus.menu route="{{ route('kode-naskah.index') }}" active="kode-naskah" nameMenu="Kode Naskah" classMenu="Master Data"></x-layouts.vertical.menus.menu>
            @endcan
            
        @canany(['jurusan', 'prodi', 'agama', 'kberkas', 'kodeunit', 'kodenaskah'])
        </x-layouts.vertical.menus.dropdown-menu>
        @endcan

        @canany(['extension', 'folder-storage', 'permission', 'hakakses'])
        <x-layouts.vertical.menus.dropdown-menu parentMenu="Master Sistem" parentIcon="menu-icon tf-icons bx bx-cog">
        @endcan
            @can('extension')
            <x-layouts.vertical.menus.menu route="{{ route('ekstensi.index') }}" active="ekstensi" nameMenu="Ekstensi File" classMenu="Master Sistem"></x-layouts.vertical.menus.menu>
            @endcan

            @can('folder-storage')
            <x-layouts.vertical.menus.menu route="{{ route('folder-storage.index') }}" active="folder-storage" nameMenu="Folder Storage" classMenu="Master Sistem"></x-layouts.vertical.menus.menu>
            @endcan

            @can('permission')
            <x-layouts.vertical.menus.menu route="{{ route('izin.index') }}" active="izin" nameMenu="Izin (Permission)" classMenu="Master Sistem"></x-layouts.vertical.menus.menu>
            @endcan

            @can('hakakses')
            <x-layouts.vertical.menus.menu route="{{ route('hak-akses.index') }}" active="hak-akses" nameMenu="Hak Akses" classMenu="Master Sistem"></x-layouts.vertical.menus.menu>
            @endcan
        @canany(['extension', 'folder-storage', 'permission', 'hakakses'])
        </x-layouts.vertical.menus.dropdown-menu>
        @endcan

        @endif
        {{-- End : Operator Menus --}}

        <li class="menu-item ">
            <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" class="menu-link text-danger">
                <i class="menu-icon tf-icons bx bx-log-out"></i>
                <div>Log Out</div>
                <form id="logout-form" action="{{ route('logout') }}" method="POST">
                    @csrf
                </form>
            </a>
        </li>
    </ul>

</aside>