<li class="menu-item {{ (request()->segment(1) == $active) ? ($dropdownMenu ? 'active' : 'active open') : '' }}">
    <a href="{{ $route }}" class="menu-link">
        @isset($iconMenu)
        <i class="{{ $iconMenu }}"></i>
        @endisset
        <div>{{ $nameMenu }}</div>
        @isset($alertMenu)
        <div class="badge bg-danger rounded-pill ms-auto">{{ $alertMenu }}</div>
        @endisset
    </a>
</li>