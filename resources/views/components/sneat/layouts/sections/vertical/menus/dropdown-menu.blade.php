<li class="menu-item {{ strtolower($parentMenu) }}">
    <a href="javascript:void(0);" class="menu-link menu-toggle">
        @isset($parentIcon)
        <i class="{{ $parentIcon }}"></i>
        @endisset
        <div>{{ $parentMenu }}</div>
        @isset($parentAlert)
        <div class="badge bg-danger rounded-pill ms-auto">{{ $parentAlert }}</div>
        @endisset
    </a>
    <ul class="menu-sub">
        {{ $slot }}
    </ul>
</li>