<!-- Footer-->
<footer class="content-footer footer bg-footer-theme">
    <div class="{{ (!empty($containerNav) ? $containerNav : 'container-fluid') }} d-flex flex-wrap justify-content-between py-2 flex-md-row flex-column">
        <div class="col-sm-4">
            ©2023, made with ❤️ by <a href="{{ (!empty(config('variables.creatorUrl')) ? config('variables.creatorUrl') : '') }}" target="_blank" class="footer-link fw-bolder">{{ (!empty(config('variables.creatorName')) ? config('variables.creatorName') : '') }}</a>
        </div>
        <div class="col-sm-4 text-center">
            <b class="text-warning text-center text-uppercase">
                --- Quality is Our Concern ---
            </b>
        </div>
        <div class="col-sm-4">
            <div class="float-end d-none d-sm-block">
                ARSIP DIGITAL V3.1
            </div>
        </div>
    </div>
</footer>
<!--/ Footer-->
