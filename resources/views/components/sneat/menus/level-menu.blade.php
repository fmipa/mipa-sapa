<li class="{{ (request()->segment(1) == $active) ? 'mm-active' : '' }}"><a href="{{ $route ?? 'javascript: void(0);' }}">{{ $titleMenu }}</a></li>
@if ((request()->segment(1) == $active))
    @push('js')
        <script>
            $(".{{ strtolower($classMenu) }}").addClass('mm-active')
        </script>
    @endpush
@endif
