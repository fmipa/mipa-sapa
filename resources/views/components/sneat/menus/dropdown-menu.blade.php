<li class="{{ strtolower($title) }}">
    <a href="javascript: void(0);" class="has-arrow waves-effect">
        <i class="{{ $icon ?? '' }}"></i>
        <span>{{ $title }}</span>
    </a>
    <ul class="sub-menu" aria-expanded="true">
        {{ $slot }}
    </ul>
</li>