<div class="btn-group" role="group" aria-label="Basic example">
    <x-ahref :link="route($route.'.edit', $id)" title="Ubah Data">
        <x-button type="button" class="btn btn-info btn-sm" id="" label=" ">
            <i class="mdi mdi-square-edit-outline"></i>
        </x-button>
    </x-ahref>
    <x-forms.form :action="route($route.'.destroy', $id)" method="POST" id="form">
        @method('DELETE')
        <input type="hidden" name="{{ $id }}">
        <x-button type="submit" class="btn btn-danger btn-sm ml-1" id="" label=" " onClick="return confirm('{{  'Apakah anda yakin menghapus Data ' . $labelDelete . '?' }}')">
            <i class="mdi mdi-delete-circle-outline"></i>
        </x-button>
    </x-forms.form>
</div>