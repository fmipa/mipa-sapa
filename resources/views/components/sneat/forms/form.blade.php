<form 
    {{ $attributes->merge(
        [
        'class' => 'form-horizontal '.$class,
        'id'    => 'form-validate '.$id,
        ]
    ) }}
    action="{{ $action }}" 
    method="{{ $method }}" 
    {{ isset($enctype) ? "enctype='{$enctype}'" : NULL }}
    >
    @csrf
    
    {{ $slot }}
</form>