@error($name) 
    @php $classError=' in-error' @endphp
@enderror
<div {{ $attributes->merge(['class' => 'form-check mb-3 '.$classDiv]) }}>
    <input 
        type="checkbox" 
        {{ $attributes->merge(['class' => 'form-check-input '.$class . ($classError ?? '')]) }}
        {{ $attributes->merge(['id' => 'defaultCheck2 '.$id]) }}
        name="{{ $name ?? NULL }}"
        value="{{ $value ?? NULL }}"
        {{ old($name) ? (old($name) == $value ? 'checked' : '') : ($checked == $value ? 'checked' : '') }}
        @required($required) 
        @readonly($readonly)
        @disabled($disabled)
    >
    <label for="defaultCheck2" class="form-check-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>

    @if ($smallText)
    <div id="defaultFormControlHelp" class="form-text text-info">{{ $smallText }}</div>
    @endif
    @error($name)
    <div id="defaultFormControlHelp" class="form-text text-danger">{{ $message }}</div>
    @enderror
</div>
