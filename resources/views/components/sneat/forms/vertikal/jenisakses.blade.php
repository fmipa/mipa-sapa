@push('style-page')
<style>
    @media (min-width: 768px) {
        .col-md {
            flex: 1 0 0%;
        }
    }
    /* .row>* {
        flex-shrink: 0;
        width: 100%;
        max-width: 100%;
        padding-right: calc(var(--bs-gutter-x)*.5);
        padding-left: calc(var(--bs-gutter-x)*.5);
        margin-top: var(--bs-gutter-y);
    } */

    .custom-option.checked {
        border: 1px solid #696cff;
    }
    .custom-option {
        padding-left: 0;
        border: 1px solid #d9dee3;
        border-radius: 0.5rem;
    }
    .form-check {
        display: block;
        min-height: 1.434375rem;
        /* padding-left: 1.7em; */
        margin-bottom: 0.125rem;
    }

    .custom-option-icon .custom-option-content {
        text-align: center;
        padding: 1em;
    }

    .custom-option .custom-option-content {
        cursor: pointer;
        width: 100%;
    }

    .custom-option-icon .custom-option-body {
        display: block;
        margin-bottom: 0.5rem;
    }

    .custom-option-icon .form-check-input {
        float: none !important;
        margin: 0 !important;
    }

    .custom-option-icon .custom-option-body i {
        font-size: 2rem;
        margin-bottom: 0.25rem;
        display: block;
    }

    .custom-option-icon .custom-option-body .custom-option-title {
        display: block;
        font-size: .9375rem;
        font-weight: 500;
        color: #566a7f;
    }

    .form-check-input:checked {
        background-color: rgba(105,108,255,.08);
        border-color: rgba(105,108,255,.08);
    }
    .form-check-input[type=radio] {
        border-radius: 50%;
    }
    .form-check .form-check-input {
        float: left;
        margin-left: -1.7em;
    }

    .form-check-input:checked[type=radio] {
        --bs-form-check-bg-image: url(data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='-4 -4 8 8'%3e%3ccircle r='1.5' fill='%23fff'/%3e%3c/svg%3e);
    }

    .form-check-input:checked, .form-check-input[type=checkbox]:indeterminate {
        background-color: #696cff;
        border-color: #696cff;
        box-shadow: 0 2px 4px 0 rgba(105,108,255,.4);
    }

</style>
@endpush
@error('jenis_akses_id') 
@php 
    $classError=' error';
    $hasError = true 
@endphp
@enderror
    <div {{ $attributes->merge(['class' => 'mb-3 '.$classDiv]) }}>
        <label for="defaultFormControlInput" class="form-label custom-cursor-default-hover">
            Pilih Jenis Akses
        </label>
        <div class="row gy-3">
            <div class="col-md mb-md-0 mb-2">
                <div class="form-check custom-option custom-option-icon" id="jenis-akses-2">
                    <label class="form-check-label custom-option-content " for="customRadioIcon">
                    <span class="custom-option-body">
                        <i class='bx bx-key'></i>
                        <span class="custom-option-title"> Pribadi </span>
                        <small> Hanya dapat di lihat oleh akun yang mengupload. </small>
                    </span>
                    <input name="jenis_akses_id" class="form-check-input " type="radio" value="2" id="customRadioIcon" {{ (old('jenis_akses_id') ? (old('jenis_akses_id') == 2 ? 'checked' : '') : ($checked ? ($checked == 2 ? 'checked' : '') : ''))  }}>
                    </label>
                </div>
            </div>
            <div class="col-md mb-md-0 mb-2">
                <div class="form-check custom-option custom-option-icon" id="jenis-akses-5">
                    <label class="form-check-label custom-option-content " for="customRadioIcon">
                    <span class="custom-option-body">
                        <i class='bx bx-group'></i>                            
                        <span class="custom-option-title"> Tim </span>
                        <small class="">Dapat dilihat oleh seluruh anggota tim.</small>
                    </span>
                    <input name="jenis_akses_id" class="form-check-input " type="radio" value="5" id="customRadioIcon" {{ (old('jenis_akses_id') ? (old('jenis_akses_id') == 5 ? 'checked' : '') : ($checked ? ($checked == 5 ? 'checked' : '') : ''))  }}>
                    </label>
                </div>
            </div>
            <div class="col-md mb-md-0 mb-2">
                <div class="form-check custom-option custom-option-icon" id="jenis-akses-3">
                    <label class="form-check-label custom-option-content " for="customRadioIcon">
                    <span class="custom-option-body">
                        <i class='bx bx-user-plus'></i>
                        <span class="custom-option-title"> Perorang </span>
                        <small> Dapat dilihat oleh akun-akun yang ditag. </small>
                    </span>
                    <input name="jenis_akses_id" class="form-check-input " type="radio" value="3" id="customRadioIcon" {{ (old('jenis_akses_id') ? (old('jenis_akses_id') == 3 ? 'checked' : '') : ($checked ? ($checked == 3 ? 'checked' : '') : '' ))  }}>
                    </label>
                </div>
            </div>
            <div class="col-md mb-md-0 mb-2">
                <div class="form-check custom-option custom-option-icon" id="jenis-akses-4">
                    <label class="form-check-label custom-option-content " for="customRadioIcon">
                    <span class="custom-option-body">
                        <i class='bx bx-sitemap'></i>
                        <span class="custom-option-title"> Unit Kerja </span>
                        <small> Dapat dilihat oleh akun yang masuk ke dalam Unit Kerja yang ditag. </small>
                    </span>
                    <input name="jenis_akses_id" class="form-check-input " type="radio" value="4" id="customRadioIcon" {{ (old('jenis_akses_id') ? (old('jenis_akses_id') == 4 ? 'checked' : '') : ($checked ? ($checked == 4 ? 'checked' : '') : '' ))  }}>
                    </label>
                </div>
            </div>
        </div>
    </div>
    @error('jenis_akses_id')
    <div id="defaultFormControlHelp" class="form-text text-danger">{{ $message }}</div>
    @enderror

    <div id="tim">
        <x-forms.selectbox label="Unit Kerja" name="unit_kerja_id[]" :datas="$unitKerjas" :selected="old('unit_kerja_id') ?? ($jenisAksesId == 4 ? $selectJenisAkses : ($create ? $selectJenisAkses : NULL))" hasMultiple="true"></x-forms.selectbox>
    </div>

    <div id="tag">
        <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id') ?? ($jenisAksesId == 3 ? $selectJenisAkses : NULL)" hasMultiple="true"></x-forms.selectbox>
    </div>


@push('script-page')

    <!-- Custom Js -->
    <script>
        $( document ).ready(function() {
            $("#tim, #tag").hide();
    
            // $("select[name='jenis_akses_id']").on('click, change', function () { 
            //     var id = $(this).val();
            //     switch (id) {
            //         case '3':
            //             $("#tag").show();
            //             $("#tim").hide();
            //             break;
                        
            //         case '4':
            //             $("#tim").show();
            //             $("#tag").hide();
            //             break;
                
            //         default:
            //             $("#tim, #tag").hide();
            //             break;
            //     }
            // })
    
        @if ($checked)
            var jenisAksesId = "{{ $checked }}";
            element = $('#jenis-akses-'+jenisAksesId);
            checked(element);
            showHide(jenisAksesId);
        @endif
    
        $('.custom-option').click(function () {
            checked($(this));
            showHide(radio.val());
        });

        function checked(element)
        {
            $('.custom-option').removeClass('checked');
            $('.form-check-input').removeAttr('checked');
            element.addClass('checked');
            radio = element.find('#customRadioIcon');
            radio.addClass('checked');
            radio.attr('checked', 'checked');
            
        }

        function showHide(id)
        {
            switch (id) {
                case '3':
                    $("#tag").show();
                    $("#tim").hide();
                    break;
                    
                case '4':
                    $("#tim").show();
                    $("#tag").hide();
                    break;
            
                default:
                    $("#tim, #tag").hide();
                    break;
            }
        }
    });
    
    </script>
@endpush