@error($name) 
    @php $classError=' in-error' @endphp
@enderror
<div {{ $attributes->merge(['class' => 'mb-3 '.$classDiv]) }}>
    <label for="defaultFormControlInput" class="form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <input 
        type="email" 
        {{ $attributes->merge(['class' => 'form-control '.$class . ($classError ?? '')]) }}
        {{ $attributes->merge(['id' => 'defaultFormControlInput '.$id]) }}
        name="{{ $name ?? NULL }}"
        value="{{ $value ?? NULL }}"
        placeholder="{{ $placeholder ?? 'Masukkan '.$label }}"
        @required($required) 
        @readonly($readonly)
        @disabled($disabled)
        {{ old($name) == NULL ? ($autofocus ? "autofocus" : NULL) : NULL }}
        aria-describedby="defaultFormControlHelp"
    >
    @if ($smallText)
    <div id="defaultFormControlHelp" class="form-text text-info">{{ $smallText }}</div>
    @endif
    @error($name)
    <div id="defaultFormControlHelp" class="form-text text-danger">{{ $message }}</div>
    @enderror
</div>
