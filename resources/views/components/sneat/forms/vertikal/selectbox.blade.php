@push('style-page')
<style>
    .select2-selection--single {
        height: 100% !important;
    }
    .select2-selection__rendered{
        word-wrap: break-word !important;
        text-overflow: inherit !important;
        white-space: normal !important;
    }
</style>
@endpush
@error($name) 
@php 
    $classError=' error';
    $hasError = true 
@endphp
@enderror
<div {{ $attributes->merge(['class' => 'mb-3 '.$classDiv]) }}>
    <label for="defaultFormControlInput" class="form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <select 
        @class([
            'form-select', 
            'w-100',
            'select2-multiple'  => $hasMultiple,
            $class              => $class,
        ])
        @if ($hasMultiple)
        multiple="multiple"
        @endif
        data-toggle="select2"
        id="{{ $id }}"
        name="{{ $name }}"
        @required($required) 
        @readonly($readonly)
        @disabled($disabled)
        {{ ($autofocus && old($name) == NULL) ? "autofocus" : NULL }}
    >
        <option value="">- Pilih {{ $label }} -</option>
        @foreach ((array) $datas as $data)
            <option value="{{ $data['key'] }}">
                {{ $data['text'] }} 
            </option>
        @endforeach
    </select>
    @if ($smallText)
    <div id="defaultFormControlHelp" class="form-text text-info">{{ $smallText }}</div>
    @endif
    @error($name)
    <div id="defaultFormControlHelp" class="form-text text-danger">{{ $message }}</div>
    @enderror
</div>

@push('script-page')

    <!-- Custom Js -->
    <script>
        $( document ).ready(function() {
            @if ($selector)
                var selector = "{{ $selector }}";
            @else
                var selector = '[name="{{ $name }}"]';
            @endif
            $(selector).select2({
                placeholder: "{{ $placeholder ?? 'Pilih '.$label }}",
                allowClear: true,
                width: "resolve",
                @if($modal)
                dropdownParent: $("{{ $modal }}"),
                @endif
            });
            @if ($selected || old($name)) 
                @if ($hasMultiple)
                var datas = [];
                @foreach ($selected as $item)
                datas.push("{{ $item }}");
                @endforeach
                @else
                var datas = "{{ $selected ?? old($name) }}";
                @endif
                $('[name="{{ $name }}"]').val(datas).trigger('change');
            @endif
        });
    </script>
@endpush