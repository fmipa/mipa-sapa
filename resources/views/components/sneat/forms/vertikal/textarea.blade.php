@error($name) 
    @php $classError=' in-error' @endphp
@enderror
<div {{ $attributes->merge(['class' => 'mb-3 '.$classDiv]) }}>
    <label for="defaultFormControlInput" class="form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <textarea 
        {{ $attributes->merge(['class' => 'form-control '.$class . ($classError ?? '')]) }}
        {{ $attributes->merge(['id' => 'exampleFormControlTextarea1 '.$id]) }}
        rows="{{ $rows ?? 3 }}"
        name="{{ $name ?? NULL }}"
        placeholder="{{ $placeholder ?? 'Masukkan '.$label }}"
        @required($required) 
        @readonly($readonly)
        @disabled($disabled)
        {{ old($name) == NULL ? ($autofocus ? "autofocus" : NULL) : NULL }}
    ></textarea>
        
    @if ($smallText)
    <div id="defaultFormControlHelp" class="form-text text-info">{{ $smallText }}</div>
    @endif
    @error($name)
    <div id="defaultFormControlHelp" class="form-text text-danger">{{ $message }}</div>
    @enderror
</div>

@push('script-page')
<script>
    @if (old($name) ?? ($value ?? ''))
        $("textarea[name='{{ $name }}']").val("{{ old($name) ?? ($value ?? '') }}");
    @endif
</script>
@endpush
