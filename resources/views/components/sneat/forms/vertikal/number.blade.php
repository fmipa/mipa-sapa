@error($name) 
    @php $classError=' in-error' @endphp
@enderror
<div {{ $attributes->merge(['class' => 'mb-3 '.$classDiv]) }}>
    <label for="html5-number-input" class="form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <input
        type='number'
        {{ $attributes->merge(['class' => 'form-control '.$class . ($classError ?? '')]) }}
        {{ $attributes->merge(['id' => 'html5-number-input '.$id]) }}
        name="{{ $name ?? NULL }}"
        value="{{ $value ?? NULL }}"
        placeholder="{{ $placeholder ?? 'Masukkan '.$label }}"
        @if ($step) step="{{ $step }}" @endif
        @if ($max) max="{{ $max }}" @endif
        @if ($min) min="{{ $min }}" @endif
        @required($required) 
        @readonly($readonly)
        @disabled($disabled)
        {{ old($name) == NULL ? ($autofocus ? "autofocus" : NULL) : NULL }}
    >
    @if ($smallText)
    <div id="defaultFormControlHelp" class="form-text text-info">{{ $smallText }}</div>
    @endif
    @error($name)
    <div id="defaultFormControlHelp" class="form-text text-danger">{{ $message }}</div>
    @enderror
</div>
