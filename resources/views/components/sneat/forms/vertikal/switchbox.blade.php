@error($name) 
    @php $classError=' in-error' @endphp
@enderror
<div class="form-check form-switch mb-3">
    <input 
        type="checkbox" 
        {{ $attributes->merge(['class' => 'form-check-input '.$class . ($classError ?? '')]) }}
        {{ $attributes->merge(['id' => 'flexSwitchCheckDefault '.$id]) }}
        name="{{ $name ?? NULL }}"
        value="{{ $value ?? NULL }}"
        {{ old($name) ? (old($name) == $value ? 'checked' : '') : ($checked == $value ? 'checked' : '') }}
        @required($required) 
        @readonly($readonly)
        @disabled($disabled)
    >
    <label for="flexSwitchCheckDefault" class="form-check-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>

    @if ($smallText)
    <div id="defaultFormControlHelp" class="form-text text-info">{{ $smallText }}</div>
    @endif
    @error($name)
    <div id="defaultFormControlHelp" class="form-text text-danger">{{ $message }}</div>
    @enderror
</div>
