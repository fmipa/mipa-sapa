<div class="form-floating row mb-3">
        <input
            type='text'
            class="form-control {{ $class }} @error($name) is-invalid @enderror"
            @if ($threshold)
            maxlength="{{ $threshold }}"
            @else                    
            id="floatingInput {{ $id ?? NULL }}"
            @endif
            name="{{ $name ?? NULL }}"
            value="{{ $value ?? NULL }}"
            placeholder="{{ $placeholder ?? 'Masukkan '.$label }}"
            @required($required) 
            @readonly($readonly)
            @disabled($disabled)
            {{ old($name) == NULL ? ($autofocus ? "autofocus" : NULL) : NULL }}
            aria-describedby="floatingInputHelp"
        >
        <label for="floatingInput">
            {{ $label }}
            @if($required)
            <span class="text-danger">*</span>
            @endif
        </label>
    
        @if ($smallText)
        <span class="font-13 form-text text-info" id="floatingInputHelp">{{ $smallText }}</span>
        @endif
        @error($name)
        <span class="form-text invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
</div>

@if ($threshold)
@push('js')
    <!-- Custom Js -->
    <script>
        $( document ).ready(function() {
            $("input[name='{{ $name }}']").maxlength({threshold:30,warningClass:"badge badge-success",limitReachedClass:"badge badge-danger"})
        });
    </script>
@endpush
@endif
