<div class="row">
    <div class="col-lg-5 d-none d-lg-block bg-register rounded-left"></div>
    <div class="col-lg-7">
        <div class="p-5">
            <div class="text-center mb-5">
                <a href="index.html" class="text-dark font-size-22 font-family-secondary">
                    <i class="mdi mdi-album"></i> <b class="text-uppercase">Arsip Digital SAPA</b>
                </a>
            </div>
            <h1 class="h5 mb-1">Buat Akun!</h1>
            <p class="text-muted mb-4">Tidak mempunyai akun? Buat akunmu dalam beberapa menit</p>
            <form class="user">
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="text" class="form-control form-control-user" id="exampleFirstName" name="username" placeholder="Username">
                    </div>
                    <div class="col-sm-6">
                        <input type="email" class="form-control form-control-user" id="exampleLastName" name="email" placeholder="Email">
                    </div>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control form-control-user" id="exampleInputEmail" name="name" placeholder="Nama Lengkap">
                </div>
                <div class="form-group row">
                    <div class="col-sm-6 mb-3 mb-sm-0">
                        <input type="password" class="form-control form-control-user" id="exampleInputPassword" name="password" placeholder="Password">
                    </div>
                    <div class="col-sm-6">
                        <input type="password" class="form-control form-control-user" id="exampleRepeatPassword" name="confirm_password" placeholder="Repeat Password">
                    </div>
                </div>
                <a href="" class="btn btn-success btn-block waves-effect waves-light"> Daftar Akun </a>

                <div class="text-center mt-4">
                    <h5 class="text-muted font-size-16">Daftar Akun Menggunakan</h5>
                
                    <ul class="list-inline mt-3 mb-0">
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i class="mdi mdi-google"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-info text-info"><i class="mdi mdi-twitter"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i class="mdi mdi-github-circle"></i></a>
                        </li>
                    </ul>
                </div>
                
            </form>

            <div class="row mt-4">
                <div class="col-12 text-center">
                    <p class="text-muted mb-0">Sudah Memiliki Akun?  <a href="{{ route('login') }}" class="text-muted font-weight-medium ml-1"><b>Login</b></a></p>
                </div> <!-- end col -->
            </div>
            <!-- end row -->
        </div> <!-- end .padding-5 -->
    </div> <!-- end col -->
</div> <!-- end row -->