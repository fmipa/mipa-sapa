<div 
    class="modal-body {{ $class }}" 
    {{ isset($id) ? "id={$id}" : NULL }}
>
    {{ $slot }}
</div>