<div class="modal fade {{ $class }}" id="{{ $modalId }}" data-bs-backdrop="static" tabindex="-1">
    <div class="modal-dialog {{ $modalSize }}">
        <div class="modal-content">
            {{ $slot }}
        </div>
    </div>
</div>