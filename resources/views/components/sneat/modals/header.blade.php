<div class="modal-header {{ $class }}" {{ isset($id) ? "id={$id}" : NULL }}>
    <h5 class="modal-title text-uppercase {{ $classJudul }}" id="backDropModalTitle">{{ $judul }}</h5>
    {{ $slot }}
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
    </button>
</div>