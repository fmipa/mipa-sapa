<button 
    type="button" 
    class="btn btn-{{ $class }} {{ $size ? ('btn-'.$size) : '' }}" 
    {{ isset($id) ? "id={$id}" : NULL }}
    @if ($onclick)
        onClick="{{ $onclick }}"
    @endif
    @if ($target)
    data-toggle="modal" data-target="#{{ $target }}"
    @endif
>
    {{ $slot }}
    {{ $text }}
</button>