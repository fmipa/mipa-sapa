<div class="modal-footer {{ $class }}" {{ isset($id) ? "id={$id}" : NULL }} style="display: unset">
    <div class="row">
        <div class="col">
            <button type="button" class="btn btn-secondary w-100 {{ $saveButton != 'true' ? 'w-100' : NULL }}" data-bs-dismiss="modal">{{ $closeText }}</button>
        </div>
        @if ($saveButton == 'true')
        <div class="col">
            <button type="{{ $typeButton }}" class="btn btn-{{ $classButton }} w-100 tombol-simpan" {{ isset($onclick) ? "onclick={$onclick}" : NULL }}>{{ $saveText }}</button>
        </div>
        @endif
        
    </div>
</div>