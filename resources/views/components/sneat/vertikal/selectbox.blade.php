@push('css')
<style>
    .select2-search__field {
        width: 100%!important;
    }
</style>
@endpush
<div class="form-group row mb-3">
    <label for="inputText" class="col-md-3 col-sm-12 col-form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <div class="col-md-9 col-sm-12">
        @php $hasError = false @endphp
        @error($name) 
        @php $hasError = true @endphp
        @enderror
        <select 
            @class([
                'form-control', 
                'w-100',
                'select2-multiple'  => $hasMultiple,
                'is-invalid'        => $hasError,
                $class              => $class,
            ])
            @if ($hasMultiple)
            multiple="multiple"
            @endif
            data-toggle="select2"
            id="{{ $id }}"
            name="{{ $name }}"
            @required($required) 
            @readonly($readonly)
            @disabled($disabled)
            {{ ($autofocus && old($name) == NULL) ? "autofocus" : NULL }}
        >
            <option value="">- Pilih {{ $label }} -</option>
            @foreach ((array) $datas as $data)
                <option value="{{ $data['key'] }}">
                    {{ $data['text'] }} 
                </option>
            @endforeach
        </select>
        @if ($smallText)
        <span class="font-13 text-info">{{ $smallText }}</span>
        @endif
        @error($name)
        <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror

    </div>
</div>

@push('js')

    <!-- Custom Js -->
    <script>
        $( document ).ready(function() {
            $('[name="{{ $name }}"]').select2({
                placeholder: "{{ $placeholder ?? 'Pilih '.$label }}",
                allowClear: true,
                width: "resolve",
                theme: "classic",
                @if ($modal)
                dropdownParent: $('#{{ $modal }}'),
                @endif
            });
            @if ($selected || old($name)) 
                @if ($hasMultiple)
                var datas = [];
                @foreach ($selected as $item)
                datas.push("{{ $item }}");
                @endforeach
                @else
                var datas = "{{ $selected ?? old($name) }}";
                @endif
                $('[name="{{ $name }}"]').val(datas).trigger('change');
            @endif
        });
    </script>
@endpush