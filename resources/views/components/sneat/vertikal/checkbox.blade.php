<div class="form-group row mb-3">
    <label for="inputText" class="col-md-3 col-sm-12 col-form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <div class="col-md-9 col-sm-12">
        <div class="custom-control custom-checkbox">
            <input 
                type="checkbox" 
                class="custom-control-input {{ $class }} @error($name) is-invalid @enderror"
                id="customCheck1"
                {{ $name ? "name={$name}" : NULL }}
                value="{{ $value ?? 1 }}"
                @required($required) 
                @readonly($readonly)
                @disabled($disabled)
                {{ ($autofocus && old($name) == NULL) ? "autofocus" : NULL }}
                {{ old($name) ? (old($name) == $value ? 'checked' : '') : ($checked == $value ? 'checked' : '') }}
            >
            <label class="custom-control-label" for="customCheck1">
                {{ $text ?? $value }}
            </label>
        </div>

        @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
