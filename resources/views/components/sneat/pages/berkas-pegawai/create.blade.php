@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('berkas-pegawai.index')"
        pageName="Berkas Pegawai"
        subPageName="Tambah Berkas Pegawai"
    >

        <x-forms.form :action="route('berkas-pegawai.store')" method="POST" id="form" enctype="multipart/form-data">
                    
            <x-forms.text label="Nama" class="text-uppercase" name="nama" :value="old('nama')" threshold="200" required></x-forms.text>
                    
            <x-forms.selectbox label="Kategori Berkas" name="kategori_id" required :datas="$kategoris" :selected="old('kategori_id')" required></x-forms.selectbox>

            <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id')" smallText="keterangan : Folder -> Subfolder"></x-forms.selectbox>
            
            <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id')" hasMultiple="true" smallText="dapat dikosongkan & dapat dipilih lebih dari 1 Box"></x-forms.selectbox>

            @if(auth()->user()->hasRole('super-admin'))
                <x-forms.selectbox label="Akun Pembuat" name="created_by" :datas="$creators" :selected="old('created_by')" required></x-forms.selectbox>
            @endif

            <x-forms.jenisAkses :checked="old('jenis_akses_id')" :pegawais="$pegawais" :unitKerjas="$unitKerjas" :selectJenisAkses="$selected" create="true"></x-forms.jenisAkses>

            <x-forms.file label="File" name="attachment" required  :accept="$accepts" smallText="File tidak lebih dari 10MB dan harus berbentuk PDF"></x-forms.file>

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/berkas-pegawai/validate')
@include('assets/toast/config')
@endpush
