<script>
    $("#form-validate").validate(
        {
        rules: {
            nama: {
                required: true,
            },
            kategori_id: {
                required: true,
            },
            jenis_akses_id: {
                required: true,
            },
            attachment: {
                required: true,
            },
        },
        messages: {
            nama: {
                required: "Nama tidak boleh kosong",
            },
            kategori_id: {
                required: "Kategori Berkas tidak boleh kosong",
            },
            jenis_akses_id: {
                required: "Jenis Akses File tidak boleh kosong",
            },
            attachment: {
                required: "File tidak boleh kosong",
            },

        }
    }
    );
</script>
