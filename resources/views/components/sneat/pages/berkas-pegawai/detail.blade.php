<div class="row">
    <div class="card mb-3 col-12">
        <div class="card-header text-white bg-warning text-uppercase">Informasi Berkas Pegawai</div>
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table table-bordered mt-1">
                    <tr>
                        <td>Nama Berkas</td>
                        <td>{{ $dataBerkas->nama }}</td>
                    </tr>
                    <tr>
                        <td>Kategori</td>
                        <td>{{ $dataBerkas->kategoriBerkas ? $dataBerkas->kategoriBerkas->kategori : '' }}</td>
                    </tr>
                    @if (!empty($dataBerkas->file->boxes))
                        @php
                            $nama = $nomor = $bagian = $lokasi = ''; 
                            foreach ($dataBerkas->file->boxes as $key => $item) {
                                $nama .= ($key > 0 ? ',' : '' ) . $item->namaTahun;
                                $nomor .= ($key > 0 ? ',' : '' ) . $item->nomor;
                                $bagian .= ($key > 0 ? ',' : '' ) . $item->bagian;
                                $lokasi .= ($key > 0 ? ',' : '' ) . $item->lokasi;
                            }
                        @endphp
                    @endif
                    <tr>
                        <td>Nama Box/Kotak</td>
                        <td>
                            {{ $nama ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>Nomor</td>
                        <td>
                            {{ $nomor ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>Kepemilikan</td>
                        <td>
                            {{ $bagian ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <td>Lokasi</td>
                        <td>{{ $lokasi ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>Operator</td>
                        <td>{{ $dataBerkas->creator->name }}</td>
                    </tr>
                    <tr>
                        <td>Download</td>
                        <td>
                            <x-ahref :link="route('unduh-file', ['id'=> $dataBerkas->idSecret, 'tipeFile' => 'berkas-pegawai'])" target="_blank" class="btn btn-info" text="Download"></x-ahref>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>
