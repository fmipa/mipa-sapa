@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
<style>
    .tree, .tree ul {
        margin:0;
        padding:0;
        list-style:none
    }
    .tree ul {
        margin-left:1em;
        position:relative
    }
    .tree ul ul {
        margin-left:1em
        padding:5px;
    }
    .tree ul:before {
        content:"";
        display:block;
        width:0;
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        /* border-left:1px solid */
    }
    .tree li {
        margin:10px 0;
        padding:0 1em;
        line-height:2em;
        color:#369;
        font-weight:700;
        position:relative
    }

    .tree ul li:before {
        content:"";
        display:block;
        width:10px;
        height:0;
        border-top:1px solid;
        margin-top:-1px;
        position:absolute;
        top:1em;
        left:0
    }
    .tree ul li:last-child:before {
        background:#fff;
        height:auto;
        top:1em;
        bottom:0
    }
    .indicator {
        margin-right:5px;
        color: darkslateblue;
    }
    .tree li a {
        text-decoration: none;
        color:#369;
    }
    .tree li button, .tree li button:active, .tree li button:focus {
        text-decoration: none;
        color:#369;
        border:none;
        background:transparent;
        margin:0px 0px 0px 0px;
        padding:0px 0px 0px 0px;
        outline: 0;
    }
    .twig {
        border: 1px solid rgb(79, 139, 199);
        border-radius: 5px;
    }
    .margin-twig {
        margin-right:27px;
    }

    .tombol-tambah {
        position: absolute;
        top: 8px;
        right: 60px;
    }

    .tombol-ubah {
        position: absolute;
        top: 8px;
        right: 35px;
    }

    .tombol-hapus {
        position: absolute;
        top: 8px;
        right: 10px;
    }

    .form-tambah {
        position: absolute;
        top: 0;
    }
</style>
@endpush

@section('content') 
    @php
        $create = auth()->user()->hasRole('super-admin') ? true : false;
    @endphp   
    <x-layouts.card-app 
        :card="true"
        :create="$create"
        createUrl="#"
        pageName="Folder"
        subPageName="Data Folder"
    >
        <ul id="tree">
            @foreach($datas as $folder)
            <li id="{{ $folder->idSecret }}" class="twig " nama="{{ $folder->nama }}">
                {{ $folder->nama.' ('.$folder->unitKerja->nama.')' }} 
                @if (auth()->user()->hasRole('super-admin'))
                <i class='bx bx-plus-circle text-success cursor-pointer tombol-tambah' id="{{ $folder->idSecret }}"></i>
                <i class='bx bx-edit-alt text-warning cursor-pointer tombol-ubah' id="{{ $folder->idSecret.','.json_encode($folder->nama) }}"></i>
                <i class='bx bx-trash text-danger cursor-pointer tombol-hapus' id="{{ $folder->idSecret.','.json_encode($folder->nama) }}"></i>
                @endif
                @if(count($folder->childs))
                    @include('components.'.config('variables.templateName').'.pages.folder.subfolder',['subfolders' => $folder->childs])
                @endif
            </li>
            @endforeach
        </ul>
    </x-layouts.card-app>

    @if (auth()->user()->hasRole('super-admin'))

    <x-modals.modal modalId="modal-ubah" class="modalParent" modalSize="modal-dialog-centered">
        <x-modals.header judul="Ubah Folder" id="ubah-modal-header"></x-modals.header>
        
        <x-forms.form action="#" class="form-ubah" method="POST">
            <input type="hidden" name="_method" value="POST">
            <x-modals.body title="Ubah Folder">
                <input type="hidden" name="id" class="ubah-id">
                <x-forms.selectbox label="Kepala Folder" name="parent_id" class="ubah-parent" modal=".modalParent" selector=".ubah-parent" :datas="$parentFolders" smallText="Jika ingin menjadi folder utama, dikosongkan saja"></x-forms.selectbox>
            
                <x-forms.text label="Nama Folder" name="nama" class="ubah-nama" threshold="100" required autofocus></x-forms.text>
    
                @if(auth()->user()->hasRole('super-admin'))
                    <x-forms.selectbox label="Unit Kerja" name="unit_kerja_id" class="ubah-unit" :datas="$unitKerjas" modal="#modal-ubah" required></x-forms.selectbox>
                    
                    <x-forms.selectbox label="Prodi" name="prodi_id" class="ubah-prodi" :datas="$prodis" modal="#modal-ubah"></x-forms.selectbox>

                    <x-forms.selectbox label="Akun Pembuat" name="created_by" class="ubah-creator" :datas="$users" modal="#modal-ubah" required></x-forms.selectbox>
                @endif
            </x-modals.body>

            <x-modals.footer closeText="TUTUP" saveButton="true" saveText="Ubah" classButton="info" typeButton="submit"></x-modals.footer>
        </x-forms.form>
    </x-modals.modal>

    <x-modals.modal modalId="modal-hapus" modalSize="modal-dialog-centered">
        <x-modals.header judul="Hapus Folder"></x-modals.header>
        
        <x-forms.form action="#" class="form-hapus" method="POST">
            @method('DELETE')
            <x-modals.body title="Hapus Folder" class="text-center">
                <input type="hidden" name="id" id="hapus-id">

                Apakah yakin ingin menghapus Folder <b class="text-uppercase text-danger" id="data-nama"></b> dan <b class="text-danger">SUBFOLDER</b> didalamnya?
            </x-modals.body>

            <x-modals.footer closeText="TUTUP" saveButton="true" saveText="YA, Hapus data" classButton="danger" typeButton="submit"></x-modals.footer>
        </x-forms.form>
    </x-modals.modal>
    @endif
@endsection

@push('script-vendor')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@if (Session::has('status'))
@include('assets/toast/config')
@endif

{{-- Folder --}}
<script>
    $.fn.extend({
        treed: function (o) {
            // initialize variable
            var idSecret = '';
            var openedClass = 'bx bx-folder';
            var closedClass = 'bx bx-folder-open';
            
            if (typeof o != 'undefined'){
                if (typeof o.openedClass != 'undefined'){
                openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined'){
                closedClass = o.closedClass;
                }
            };
            
                //initialize each of the top levels
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul
                    branch.prepend("<i class='indicator " + closedClass + "'></i>");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this == e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    })
                    branch.children().children().toggle();
                });
                $("li:not('.branch,.menu-item,.menu-header,.dropdown,.dropdown-menu-user')").each(function () {
                    var branch = $(this);
                    branch.prepend("<span class='margin-twig'></span>");
                });

                //fire event from the dynamically added icon
            tree.find('.branch .indicator').each(function(){
                $(this).on('click', function () {
                    $(this).closest('li').click();
                });
            });
                //fire event to open branch if the li contains an anchor instead of text
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                //fire event to open branch if the li contains a button instead of text
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });
    /* Initialization of treeviews */
    $('#tree').treed({openedClass:'bx bx-folder-open', closedClass:'bx bx-folder'});

</script>

@if (auth()->user()->hasRole('super-admin'))
{{-- Aksi --}}
<script>
    // Get Id
    // $(".tombol-tambah").on("click", function(e) {
    //     idSecret = e.target.id;
    //     var url = "{{ route('folder.show', ':idSecret') }}".replace(':idSecret', idSecret);
    //     $.get(url, function (data) {
    //         // $(".tambah-nama").val(data.nama);
    //         $(".tambah-parent").val(data.parent_id_secret).trigger('change');
    //         @if(auth()->user()->hasRole('super-admin'))
    //         $(".tambah-unit").val(data.idSecret_unitKerja).trigger('change');
    //         $(".tambah-prodi").val(data.idSecret_prodi).trigger('change');
    //         $(".tambah-creator").val(data.idSecret_creator).trigger('change');
    //         @endif
    //     });
    //     $('html, body').animate({
    //         scrollTop: $(".form-tambah").offset().top
    //     }, 500);
    //     $(".tambah-nama").focus();

    // });
    function tambah()
    {
        $("#ubah-modal-header .modal-title").html("Tambah Folder");
        $("input[name='_method']").val("POST");
        $(".ubah-id").val("");
        $(".tombol-simpan").html("SIMPAN");
        $(".form-ubah").attr('action', "{{ route('folder.store') }}");
        $("#modal-ubah").modal('show');
    }

    function reset()
    {
        $(".ubah-nama").val('');
        $(".ubah-parent").val(0).trigger('change');
        @if(auth()->user()->hasRole('super-admin'))
        $(".ubah-unit").val(0).trigger('change');
        $(".ubah-prodi").val(0).trigger('change');
        $(".ubah-creator").val(0).trigger('change');
        @endif
    }

    $("#tambah").on("click", function(e) {
        reset();
        tambah();
    });
    $(".tombol-tambah").on("click", function(e) {
        value = e.target.id;
        data = value.split(",");
        ftambah(data[0]);
    });
    $(".tombol-ubah").on("click", function(e) {
        value = e.target.id;
        data = value.split(",");
        fubah(data[0], data[1]);
    });
    $(".tombol-hapus").on("click", function(e) {
        value = e.target.id;
        data = value.split(",");
        fhapus(data[0], data[1]);
    });

    function fhapus(id, nama)
    {
        nama = nama.replace('"', '').replace('"', '');
        url = "{{ route('folder.index') }}"+"/"+id;
        $("#data-nama").html(nama);
        $("#hapus-id").val(id);
        $(".form-hapus").attr('action', url);
        $("#modal-hapus").modal('show');
    }

    function ftambah(idSecret)
    {
        var url = "{{ route('folder.show', ':idSecret') }}".replace(':idSecret', idSecret);
        $.get(url, function (data) {
            $(".ubah-nama").val('');
            $(".ubah-parent").val(data.parent_id_secret).trigger('change');
            @if(auth()->user()->hasRole('super-admin'))
            $(".ubah-unit").val(data.idSecret_unitKerja).trigger('change');
            $(".ubah-prodi").val(data.idSecret_prodi).trigger('change');
            $(".ubah-creator").val(data.idSecret_creator).trigger('change');
            @endif
        });
        tambah();
    }

    function fubah(idSecret, nama)
    {
        nama = nama.replace('"', '').replace('"', '');
        $("#ubah-modal-header .modal-title").html("Ubah Folder "+nama);
        var url = "{{ route('folder.show', ':idSecret') }}".replace(':idSecret', idSecret);
        $(".ubah-id").val(idSecret);
        $.get(url, function (data) {
            $(".ubah-nama").val(data.nama);
            $(".ubah-parent").val(data.parent_id_secret).trigger('change');
            @if(auth()->user()->hasRole('super-admin'))
            $(".ubah-unit").val(data.idSecret_unitKerja).trigger('change');
            $(".ubah-prodi").val(data.idSecret_prodi).trigger('change');
            $(".ubah-creator").val(data.idSecret_creator).trigger('change');
            @endif
        });
        var urlUpdate = "{{ route('folder.update', ':idSecret') }}".replace(':idSecret', idSecret);
        $(".form-ubah").attr('action', urlUpdate);
        $("input[name='_method']").val("PUT");
        $(".tombol-simpan").html("UBAH");
        $("#modal-ubah").modal('show');
    }
</script>
@endif
@endpush