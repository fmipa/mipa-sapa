<ul>
    @foreach($subfolders as $subfolder)
        <li id="{{ $subfolder->idSecret }}" class="twig">
            {{ $subfolder->nama }}
            @if (auth()->user()->hasRole('super-admin'))
            <i class='bx bx-plus-circle text-success cursor-pointer tombol-tambah' id="{{ $subfolder->idSecret }}"></i>
            <i class='bx bx-edit-alt text-warning cursor-pointer tombol-ubah' id="{{ $subfolder->idSecret.','.json_encode($subfolder->nama) }}"></i>
            <i class='bx bx-trash text-danger cursor-pointer tombol-hapus' id="{{ $subfolder->idSecret.','.json_encode($subfolder->nama) }}"></i>
            @endif
            @if(count($subfolder->child))
                @include('components.'.config('variables.templateName').'.pages.folder.subfolder',['subfolders' => $subfolder->child])
            @endif
        </li>
    @endforeach
</ul>
    