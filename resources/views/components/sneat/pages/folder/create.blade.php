@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
    <link rel="stylesheet" href="//cdn.materialdesignicons.com/2.5.94/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro">
@endpush

@push('style-page')
<style>
    *{box-sizing:border-box;}

    body{
    font-family:source sans pro;
    }
    h3{
    font-weight:400;
    font-size:16px;
    }
    p{
    font-size:12px;
    color:#888;
    }

    .stage{
    max-width:80%;margin:60px 10%;
    position:relative;  
    }
    .folder-wrap{
    display: flex;
    flex-wrap:wrap;
    }
    .folder-wrap::before{
    content:'Folder name';
    display: block;
    position: absolute;
    top:-40px;
    }
    .folder-wrap:first-child::before{
    content:'Home (top of file structure)';
    display: block;
    position: absolute;
    top:-40px;
    }
    .tile{
        border-radius: 3px;
        width: calc(20% - 17px);
        margin-bottom: 23px;
        text-align: center;
        border: 1px solid #eeeeee;
        transition: 0.2s all cubic-bezier(0.4, 0.0, 0.2, 1);
        position: relative;
        padding: 35px 16px 25px;
        margin-right: 17px;
        cursor: pointer;
    }
    .tile:hover{
    box-shadow: 0px 7px 5px -6px rgba(0, 0, 0, 0.12);
    }
    .tile i{
        color: #00A8FF;
        height: 55px;
        margin-bottom: 20px;
        font-size: 55px;
        display: block;
        line-height: 54px;
        cursor: pointer;
    }
    .tile i.mdi-file-document{
    color:#8fd9ff;
    }

    .back{
    font-size: 26px;
    border-radius: 50px;
    background: #00a8ff;
    border: 0;
    color: white;
    width: 60px;
    height: 60px;
    margin: 20px 20px 0;
    outline:none;
    cursor:pointer;
    }

    /* Transitioning */
    .folder-wrap{
    position: absolute;
    width: 100%;
    transition: .365s all cubic-bezier(.4,0,.2,1);
    pointer-events: none;
    opacity: 0;
    top: 0;
    }
    .folder-wrap.level-up{
    transform: scale(1.2);
        
    }
    .folder-wrap.level-current{
    transform: scale(1);
    pointer-events:all;
    opacity:1;
    position:relative;
    height: auto;
    overflow: visible;
    }
    .folder-wrap.level-down{
    transform: scale(0.8);  
    }
</style>
@endpush

@section('content')
    
            <x-layouts.card-app 
                :card="true"
                :create="false"
                createUrl="#"
                pageName="Folder"
                subPageName="Lihat Folder"
            >
                <button class="back">
                    <i class="mdi mdi-arrow-left"></i>
                </button>
                
                
                <div class="stage">
                    {{-- ! UTAMA --}}
                    
                    @foreach ($datas as $key => $folder)
                        @if ($loop->first)
                            <div class="folder-wrap level-current scrolling">
                                <div class="tile folder">
                                    <i class="mdi mdi-folder"></i>
                                    {{-- <h3>{{ $folder['total'] }} Berkas</h3> --}}
                                    <p class="text-uppercase">{{ $folder['parent'] }}</p>
                                </div><!-- .tile.folder -->
                            </div><!-- .folder-wrap -->
                        @elseif ($key == 1)
                            <div class="folder-wrap level-down">
                                <div class="tile folder">
                                    <i class="mdi mdi-folder"></i>
                                    {{-- <h3>{{ $folder['total'] }} Berkas</h3> --}}
                                    <p class="text-uppercase">{{ $folder['parent'] }}</p>
                                </div><!-- .tile.folder -->
                            </div><!-- .folder-wrap -->
                        @else
                            <div class="folder-wrap">
                                <div class="tile folder">
                                    <i class="mdi mdi-folder"></i>
                                    {{-- <h3>{{ $folder['total'] }} Berkas</h3> --}}
                                    <p class="text-uppercase">{{ $folder['parent'] }}</p>
                                </div><!-- .tile.folder -->
                            </div><!-- .folder-wrap -->
                        @endif
                    @endforeach
                    
                </div><!-- .stage -->
            </x-layouts.card-app>
        </div>

    </div>

@endsection

@push('script-vendor')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@if (Session::has('status'))
@include('assets/toast/config')
@endif

<script>
    $( document ).ready(function() {
    
        // Folder on click
        $('.folder').on( "click", function() {
            
            console.log( "Drill down" );
            $('.level-up').removeClass('level-up');
            $('.level-current').addClass('level-up');
            $('.level-current').removeClass('level-current');       
            $('.level-down').addClass('level-current');
            $('.level-down').removeClass('level-down').next().addClass('level-down');
            
        });
            
        // Back on Click
        $('.back').on( "click", function() {
            if($('.level-current').is(':first-child')){
                console.log( "Current is top" );
            } else {
                console.log( "Drill back up" );
                $('.level-down').removeClass('level-down')
                $('.level-current').addClass('level-down');
                $('.level-current').removeClass('level-current');
                $('.level-up').addClass('level-current');
                $('.level-up').removeClass('level-up').prev().addClass('level-up');
            }
            
        });
    
    });
</script>
@endpush