@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
<style>
    .tree, .tree ul {
        margin:0;
        padding:0;
        list-style:none
    }
    .tree ul {
        margin-left:1em;
        position:relative
    }
    .tree ul ul {
        margin-left:1em
        padding:5px;
    }
    .tree ul:before {
        content:"";
        display:block;
        width:0;
        position:absolute;
        top:0;
        bottom:0;
        left:0;
        /* border-left:1px solid */
    }
    .tree li {
        margin:10px 0;
        padding:0 1em;
        line-height:2em;
        color:#369;
        font-weight:700;
        position:relative
    }

    .tree ul li:before {
        content:"";
        display:block;
        width:10px;
        height:0;
        border-top:1px solid;
        margin-top:-1px;
        position:absolute;
        top:1em;
        left:0
    }
    .tree ul li:last-child:before {
        background:#fff;
        height:auto;
        top:1em;
        bottom:0
    }
    .indicator {
        margin-right:5px;
        color: darkslateblue;
    }
    .tree li a {
        text-decoration: none;
        color:#369;
    }
    .tree li button, .tree li button:active, .tree li button:focus {
        text-decoration: none;
        color:#369;
        border:none;
        background:transparent;
        margin:0px 0px 0px 0px;
        padding:0px 0px 0px 0px;
        outline: 0;
    }
    .twig {
        border: 1px solid rgb(79, 139, 199);
        border-radius: 5px;
    }
    .margin-twig {
        margin-right:27px;
    }

    .tombol-tambah {
        position: absolute;
        top: 8px;
        right: 60px;
    }

    .tombol-ubah {
        position: absolute;
        top: 8px;
        right: 35px;
    }

    .tombol-hapus {
        position: absolute;
        top: 8px;
        right: 10px;
    }

    .form-tambah {
        position: absolute;
        top: 0;
    }
</style>
@endpush

@section('content')
    <x-layouts.card-app 
        :card="true"
        :create="false"
        createUrl="#"
        pageName="Folder"
        subPageName="Data Folder"
    >
        <ul id="tree">
            @foreach($datas as $folder)
            <li id="{{ $folder->idSecret }}" class="twig " nama="{{ $folder->nama }}">
                {{ $folder->nama }} 
                @if(count($folder->child))
                    @include('components.'.config('variables.templateName').'.pages.folder.subfolder',['subfolders' => $folder->child])
                @endif
            </li>
            @endforeach
        </ul>
    </x-layouts.card-app>
@endsection

@push('script-vendor')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@if (Session::has('status'))
@include('assets/toast/config')
@endif

{{-- Folder --}}
<script>
    $.fn.extend({
        treed: function (o) {
            // initialize variable
            var idSecret = '';
            var openedClass = 'bx bx-folder';
            var closedClass = 'bx bx-folder-open';
            
            if (typeof o != 'undefined'){
                if (typeof o.openedClass != 'undefined'){
                openedClass = o.openedClass;
                }
                if (typeof o.closedClass != 'undefined'){
                closedClass = o.closedClass;
                }
            };
            
                //initialize each of the top levels
                var tree = $(this);
                tree.addClass("tree");
                tree.find('li').has("ul").each(function () {
                    var branch = $(this); //li with children ul
                    branch.prepend("<i class='indicator " + closedClass + "'></i>");
                    branch.addClass('branch');
                    branch.on('click', function (e) {
                        if (this == e.target) {
                            var icon = $(this).children('i:first');
                            icon.toggleClass(openedClass + " " + closedClass);
                            $(this).children().children().toggle();
                        }
                    })
                    branch.children().children().toggle();
                });
                $("li:not('.branch,.menu-item,.menu-header,.dropdown,.dropdown-menu-user')").each(function () {
                    var branch = $(this);
                    branch.prepend("<span class='margin-twig'></span>");
                });

                //fire event from the dynamically added icon
            tree.find('.branch .indicator').each(function(){
                $(this).on('click', function () {
                    $(this).closest('li').click();
                });
            });
                //fire event to open branch if the li contains an anchor instead of text
                tree.find('.branch>a').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
                //fire event to open branch if the li contains a button instead of text
                tree.find('.branch>button').each(function () {
                    $(this).on('click', function (e) {
                        $(this).closest('li').click();
                        e.preventDefault();
                    });
                });
            }
        });
    /* Initialization of treeviews */
    $('#tree').treed({openedClass:'bx bx-folder-open', closedClass:'bx bx-folder'});

</script>
@endpush