<script>
    $("#form-validate").validate(
        {
        rules: {
            name: {
                required: true,
                maxlength: 100,
            },
            username: {
                required: true,
                maxlength: 40,
            },
            email: {
                required: true,
            },
            password: {
                required: true,
                maxlength: 20,
            },
            password_confirmation: {
                required: true,
                maxlength: 20,
            },
            // role_id: {
            //     required: true,
            // },
        },
        messages: {
            name: {
                required: "Nama tidak boleh kosong",
                maxlength: jQuery.validator.format("Nama tidak boleh lebih dari {0} karakter!")
            },
            username: {
                required: "Username tidak boleh kosong",
                maxlength: jQuery.validator.format("Username tidak boleh lebih dari {0} karakter!")
            },
            email: {
                required: "Email tidak boleh kosong",
            },
            password: {
                required: "Password tidak boleh kosong",
                maxlength: jQuery.validator.format("Password tidak boleh lebih dari {0} karakter!")
            },
            password: {
                required: "Konfirmasi Password tidak boleh kosong",
                maxlength: jQuery.validator.format("Konfirmasi Password tidak boleh lebih dari {0} karakter!")
            },
            // role_id: {
            //     required: "Hak Akses tidak boleh kosong",
            // },

        }
    }
    );
</script>
