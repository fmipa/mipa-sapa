@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('pengguna.index')"
        pageName="Pengguna"
        subPageName="Ubah Pengguna"
    >
        <x-forms.form :action="route('pengguna.update', $dataUser->id_secret)" method="POST" id="form">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataUser->id_secret }}" required>
            
            <x-forms.text label="Nama Lengkap" name="name" :value="$dataUser->name ?? old('name')" threshold="100" required autofocus></x-forms.text>
            
            <x-forms.text label="Username" name="username" :value="$dataUser->username ?? old('username')" threshold="40" required></x-forms.text>
            
            <x-forms.email label="Email" name="email" :value="$dataUser->email ?? old('email')" threshold="50" required ></x-forms.email>
            
            <x-forms.password label="Password Baru" name="password" :value="old('password')" threshold="20"></x-forms.password>

            <x-forms.password label="Konfirmasi Password Baru" name="password_confirmation" :value="old('password_confirmation')" threshold="20"></x-forms.password>
            
            <x-forms.selectbox label="Hak Akses" name="role_id[]" required :datas="$roles" :selected="$selectRole" hasMultiple="true"></x-forms.selectbox>
            
            <x-forms.switchbox label="Aktivasi Akun" name="active" :value="old('active') ?? ($dataUser->password ? 1 : 0)"></x-forms.switchbox>
            
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
{{-- @include('components/'.config('variables.templateName').'/pages/box/validate') --}}
@include('assets/toast/config')

@endpush
