@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('pengguna.index')"
        pageName="Pengguna"
        subPageName="Tambah Pengguna"
    >
    
        <x-forms.form :action="route('pengguna.store')" method="POST">
            
            <x-forms.text label="Nama Lengkap" name="name" :value="old('name')" threshold="100" required autofocus></x-forms.text>
        
            <x-forms.text label="Username" name="username" :value="old('username')" threshold="40" required></x-forms.text>
            
            <x-forms.email label="Email" name="email" :value="old('email')" threshold="50" required ></x-forms.email>
            
            <x-forms.password label="Password" name="password" :value="old('password')" threshold="20" required></x-forms.password>

            <x-forms.password label="Konfirmasi Password" name="password_confirmation" :value="old('password_confirmation')" threshold="20" required></x-forms.password>
            
            <x-forms.selectbox label="Hak Akses" name="role_id[]" required :datas="$roles" :selected="old('role_id')" hasMultiple="true"></x-forms.selectbox>
            
            <x-forms.switchbox label="Aktivasi Akun" name="active" :value="old('active') ?? 1"></x-forms.switchbox>
                            
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
{{-- @include('components/'.config('variables.templateName').'/pages/user/validate') --}}
@include('assets/toast/config')
@endpush
