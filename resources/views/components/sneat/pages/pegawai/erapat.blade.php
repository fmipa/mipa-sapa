@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    @include('assets/datatable/style')
@endpush

@push('style-page')
@endpush

@section('content')
    
    <x-layouts.card-app 
        :card="true"
        :create="false"
        pageName="Pegawai"
        subPageName="Export Pegawai"
    >
        <div class="table-responsive">
            <table class="table table-hover table-bordered" id="table-data">
                <thead>
                    <tr>
                        <th>user_id</th>
                        <th>pegawai_id</th>
                        <th>nip</th>
                        <th>nidn</th>
                        <th>nama</th>
                        <th>username</th>
                        <th>password</th>
                        <th>tanggal_lahir</th>
                        <th>jabatan</th>
                        <th>bidang_studi</th>
                        <th>jenis_kelamin</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $item)
                    <tr>
                        <td>{{ $item['user_id'] }}</td>
                        <td>{{ $item['pegawai_id'] }}</td>
                        <td>{{ $item['nip'] }}</td>
                        <td>{{ $item['nidk'] }}</td>
                        <td>{{ $item['nama'] }}</td>
                        <td>{{ $item['username'] }}</td>
                        <td>{{ $item['password'] }}</td>
                        <td>{{ $item['tanggal_lahir'] }}</td>
                        <td>{{ $item['jabatan'] }}</td>
                        <td>{{ $item['bidang_studi'] }}</td>
                        <td>{{ $item['jenis_kelamin'] }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </x-layouts.card-app>
@endsection

@push('script-vendor')
    @include('assets/datatable/script')
@endpush

@push('script-page')
<script>
    $(document).ready(function() {
        @include('assets/datatable/config', ['identitytable' => '#table-data'])
    });
</script>

@endpush