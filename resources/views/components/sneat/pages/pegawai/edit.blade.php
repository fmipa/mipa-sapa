@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('pegawai.index')"
        pageName="Pegawai"
        subPageName="Ubah Pegawai"
    >

        <x-forms.form :action="route('pegawai.update', $dataPegawai->id_secret)" method="POST" id="form" :urlback="route('pegawai.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataPegawai->id_secret }}" required>
        

            <div class="row">
                <div class="col-4">
                    <x-forms.text label="Nama Depan" name="firstname" :value="old('firstname') ?? $dataPegawai->firstname" threshold="100" required autofocus></x-forms.text>
                </div>
                <div class="col-4">
                    <x-forms.text label="Nama Tengah" name="middlename" :value="old('middlename') ?? $dataPegawai->middlename" threshold="100" autofocus></x-forms.text>
                </div>
                <div class="col-4">
                    <x-forms.text label="Nama Belakang" name="lastname" :value="old('lastname') ?? $dataPegawai->lastname" threshold="100"></x-forms.text>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <x-forms.text label="Gelar Depan" name="gelar_depan" :value="old('gelar_depan') ?? $dataPegawai->gelar_depan"></x-forms.text>
                </div>
                <div class="col-6">
                    <x-forms.text label="Gelar Belakang" name="gelar_belakang" :value="old('gelar_belakang') ?? $dataPegawai->gelar_belakang"></x-forms.text>
                </div>
            </div>
            
            <div class="row">
                <div class="col-6">
                    <x-forms.selectbox label="Unit/Divisi" name="role_id[]" required :datas="$roles" :selected="old('role_id') ?? $selectRoles" hasMultiple="true"></x-forms.selectbox>
                </div>
                <div class="col-6">
                    <x-forms.number label="Nomor Pegawai" name="nomor_unik" :value="old('nomor_unik') ?? $dataPegawai->nomor_unik" placeholder="Masukkan Nomor Pegawai (NIP/NIDK/NIK)"></x-forms.number>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <x-forms.text label="Tempat Lahir" name="tempat_lahir" :value="old('tempat_lahir') ?? $dataPegawai->tempat_lahir"></x-forms.text>
                </div>
                <div class="col-6">
                    <x-forms.date label="Tanggal Lahir" name="tanggal_lahir" :value="old('tanggal_lahir') ?? ($dataPegawai->tanggal_lahir ? date('d-M-Y', strtotime($dataPegawai->tanggal_lahir)) : '')"></x-forms.date>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <x-forms.selectbox label="Jenis Kelamin" name="jenis_kelamin" :datas="$jenisKelamin" :selected="old('jenis_kelamin') ?? $dataPegawai->jenis_kelamin"></x-forms.selectbox>
                </div>
                <div class="col-6">
                    <x-forms.selectbox label="Status" name="status" :datas="$status" :selected="old('status') ?? $dataPegawai->status"></x-forms.selectbox>
                </div>
            </div>

            <div class="row">
                <div class="col-6">
                    <x-forms.selectbox label="Agama" name="agama_id" :datas="$agamas" :selected="old('agama_id') ?? $selectAgama"></x-forms.selectbox>
                </div>
                <div class="col-6">
                    <x-forms.selectbox label="Program Studi" name="prodi_id" :datas="$prodis" :selected="old('prodi_id') ?? $selectProdi" ></x-forms.selectbox>
                </div>
            </div>

            <hr>
            
            <div class="row">
                <div class="col-6">
                    <x-forms.text label="Username" name="username" required :value="old('username') ?? ($dataPegawai->user ? $dataPegawai->user->username : NULL)" threshold="40" ></x-forms.text>
                </div>
                <div class="col-6">
                    <x-forms.email label="Email" name="email" required :value="old('email') ?? ($dataPegawai->user ? $dataPegawai->user->email : NULL)" threshold="50"  ></x-forms.email>
                </div>
            </div>
            
            <div class="row">
                <div class="col-6">
                    <x-forms.password label="Password Baru" name="password" :value="old('password')" threshold="20" ></x-forms.password>
                </div>
                <div class="col-6">
                    <x-forms.password label="Konfirmasi Password Baru" name="password_confirmation" :value="old('password_confirmation')" threshold="20" ></x-forms.password>
                </div>
            </div>
            
            <x-forms.switchbox label="Aktivasi Akun" name="active" :checked="($dataPegawai->user ? $dataPegawai->user->is_active : NULL)" :value="old('active') ?? 1"></x-forms.switchbox>
            
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
{{-- @include('components/'.config('variables.templateName').'/pages/box/validate') --}}
@include('assets/toast/config')
@endpush
