@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('folder-storage.index')"
        pageName="Folder Storage"
        subPageName="Ubah Folder Storage"
    >
        <x-forms.form :action="route('folder-storage.update', $dataFolder->id_secret)" method="POST" id="form" :urlback="route('folder-storage.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataFolder->id_secret }}" required>
            
            <x-forms.text label="Nama Folder" name="nama" :value="old('nama') ?? $dataFolder->nama" threshold="100" required autofocus></x-forms.text>
            
            <x-forms.selectbox label="Jenis Akses Folder" name="jenis_akses_id" required :datas="$jenisAkses" :selected="old('jenis_akses_id') ?? $dataFolder->jenis_akses_id" ></x-forms.selectbox>

            <div id="tim">
                <x-forms.selectbox label="Tim" name="role_id[]" :datas="$roles" :selected="old('role_id') ?? ($dataFolder->jenis_akses_id == 4 ? $selected : NULL)" hasMultiple="true"></x-forms.selectbox>
            </div>

            <div id="tag">
                <x-forms.selectbox label="Tag Perorang" name="pegawai_id[]" :datas="$pegawais" :selected="old('pegawai_id') ?? ($dataFolder->jenis_akses_id == 3 ? $selected : NULL)" hasMultiple="true"></x-forms.selectbox>
            </div>
            
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
        
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/folder-storage/validate')
@include('assets/toast/config')

<script>
        
    $( document ).ready(function() {
        $("#tim, #tag").hide();
        var jenisAksesId = "{{ (old('jenis_akses_id') ?? $dataFolder->jenis_akses_id) }}";
        showHide(jenisAksesId);
        $("select[name='jenis_akses_id']").on('click, change', function () { 
            var id = $(this).val();
            showHide(id);
        })

        function showHide(id)
        {
            switch (id) {
                case '3':
                    $("#tag").show();
                    $("#tim").hide();
                    break;
                    
                case '4':
                    $("#tim").show();
                    $("#tag").hide();
                    break;
            
                default:
                    $("#tim, #tag").hide();
                    break;
            }
        }
    });
</script>
@endpush
