<script>
    $("#form-validate").validate(
        {
        rules: {
            nama: {
                required: true,
                maxlength: 100,
            },
            jenis_akses_id: {
                required: true,
            },
        },
        messages: {
            nama: {
                required: "Nama Folder tidak boleh kosong",
                maxlength: jQuery.validator.format("Nama Folder tidak boleh lebih dari {0} karakter!")
            },
            jenis_akses_id: {
                required: "Jenis Akses Folder Folder tidak boleh kosong",
            },

        }
    }
    );
</script>
