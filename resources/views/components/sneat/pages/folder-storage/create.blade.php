@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('folder-storage.index')"
        pageName="Folder Storage"
        subPageName="Tambah Folder Storage"
    >
        <x-forms.form :action="route('folder-storage.store')" method="POST" id="form">

            <x-forms.text label="Nama Folder" name="nama" :value="old('nama')" threshold="100" required autofocus></x-forms.text>
            
            <x-forms.jenisAkses :checked="old('jenis_akses_id')" :pegawais="$pegawais" :unitKerjas="$unitKerjas" :selectJenisAkses="$selected" create="true"></x-forms.jenisAkses>

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/folder-storage/validate')
@include('assets/toast/config')
@endpush
