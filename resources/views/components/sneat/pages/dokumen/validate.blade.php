<script>
    $("#form-validate").validate(
        {
        rules: {
            nama: {
                required: true,
            },
            jenis_akses_id: {
                required: true,
            },
            attachment: {
                required: true,
            },
        },
        messages: {
            nama: {
                required: "Nama tidak boleh kosong",
            },
            jenis_akses_id: {
                required: "Jenis Akses File tidak boleh kosong",
            },
            attachment: {
                required: "File tidak boleh kosong",
            },

        }
    }
    );
</script>
