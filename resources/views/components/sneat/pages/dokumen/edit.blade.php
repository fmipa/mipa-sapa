@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('dokumen.index')"
        pageName="Dokumen"
        subPageName="Ubah Dokumen"
    >
        <x-forms.form :action="route('dokumen.update', $dataDokumen->id_secret)" method="POST" id="form" :urlback="route('dokumen.index')" enctype="multipart/form-data">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataDokumen->id_secret }}" required>

            <x-forms.textarea label="Nama" class="text-uppercase" name="nama" :value="old('nama') ?? $dataDokumen->nama" required></x-forms.textarea>
                    
            @if (auth()->user()->operator == 1 || auth()->user()->hasRole('super-admin'))
                <x-forms.selectbox label="Folder" name="folder_id" :datas="$folders" :selected="old('folder_id') ?? $selectFolder" smallText="keterangan : Folder -> Subfolder" ></x-forms.selectbox>
                
                <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id') ?? $selectBox" hasMultiple="true"></x-forms.selectbox>
            @endif

            @if(auth()->user()->hasRole('super-admin'))
            <x-forms.selectbox label="Akun Pembuat" name="created_by" :datas="$creators" :selected="old('created_by') ?? $selectCreator" required></x-forms.selectbox>
            @endif

            @if(auth()->user()->hasRole('super-admin') || auth()->user()->operator == 1)
            <x-forms.jenisAkses 
                :checked="old('jenis_akses_id') ?? $dataDokumen->file->jenis_akses_id" 
                :pegawais="$pegawais" :unitKerjas="$unitKerjas" 
                :jenisAksesId="old('jenis_akses_id') ?? $dataDokumen->file->jenis_akses_id" 
                :selectJenisAkses="$selectJenisAkses"
            >
            </x-forms.jenisAkses>  
            @else
            <input type="hidden" name="jenis_akses_id" value="2" readonly>
            @endif 

            @if (auth()->user()->operator == 1 || (auth()->user()->hasRole('super-admin') || auth()->user()->hasRole('kepegawaian')))
            <x-forms.file label="File" name="attachment" :accept="$accepts" smallText="File tidak lebih dari 10 MB dan harus berbentuk PDF"></x-forms.file>
            @else
            <x-forms.file label="File" name="attachment" :accept="$accepts" smallText="File tidak lebih dari 2 MB dan harus berbentuk PDF"></x-forms.file>
            @endif

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/dokumen/validate')
@include('assets/toast/config')
@endpush
