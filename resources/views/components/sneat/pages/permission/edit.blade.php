@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('izin.index')"
        pageName="Izin"
        subPageName="Ubah Izin"
    >
        <x-forms.form :action="route('izin.update', $dataPermission->id_secret)" method="POST" id="form" :urlback="route('izin.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataPermission->id_secret }}" required>
                    
            <x-forms.text label="Nama Izin" name="name" :value="old('name') ?? $dataPermission->name" threshold="50" required autofocus></x-forms.text>
            
            <x-forms.text label="Guard Name" name="guard_name" :value="old('guard_name') ?? $dataPermission->guard_name" threshold="20" required></x-forms.text>
            
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
        
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/permission/validate')
@include('assets/toast/config')

@endpush
