<script>
    $("#form-validate").validate(
        {
        rules: {
            agama: {
                required: true,
                maxlength: 30,
            },
            kode: {
                required: true,
                maxlength: 10,
            },
        },
        messages: {
            agama: {
                required: "Agama tidak boleh kosong",
                maxlength: jQuery.validator.format("Agama tidak boleh lebih dari {0} karakter!")
            },
            kode: {
                required: "Kode tidak boleh kosong",
                maxlength: jQuery.validator.format("Kode tidak boleh lebih dari {0} karakter!")
            },

        }
    }
    );
</script>
