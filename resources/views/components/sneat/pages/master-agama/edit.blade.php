@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('master-agama.index')"
        pageName="Agama"
        subPageName="Ubah Agama"
    >
        <x-forms.form :action="route('master-agama.update', $dataAgama->id_secret)" method="POST" id="form">
            
            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataAgama->id_secret }}">
            
            <x-forms.text label="Agama" name="agama" :value="old('agama') ?? $dataAgama->agama" threshold="30" required autofocus></x-forms.text>
            
            <x-forms.text label="Kode" name="kode" class="text-uppercase" :value="old('kode') ?? $dataAgama->kode" threshold="10"></x-forms.text>
                    
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/master-agama/validate')
@include('assets/toast/config')

@endpush
