@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    @include('assets/datatable/style')
@endpush

@push('style-page')
@endpush

@section('content')
    
    @can('store-kodenaskah')
        @php
            $create = true;
        @endphp
    @endcan

    <x-layouts.card-app 
        :card="true"
        :create="$create ?? false"
        :createUrl="route('kode-naskah.create')"
        pageName="Kode Naskah"
        subPageName="Data Kode Naskah"
    >
        <div class="table-responsive">
            <table class="table table-hover table-bordered" id="table-data">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        @canany(['update-kodenaskah', 'delete-kodenaskah'])
                        <th width="5%">#</th>
                        @endcan
                        <th width="10%">Kode</th>
                        <th width="50%">Substansi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $key => $item)
                    <tr>
                        <td width="5%">{{ ($key+1) }}</td>
                        @canany(['update-kodenaskah', 'delete-kodenaskah'])
                        <td width="5%">
                            <div class="dropdown">
                                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                                    <i class="bx bx-dots-vertical-rounded"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item text-warning" href="{{ route('kode-naskah.edit', $item->idSecret) }}">
                                        <i class="bx bx-edit-alt me-1"></i> Ubah</a
                                    >
                                    <button class="dropdown-item text-danger" onclick="fhapus('{{ $item->idSecret }}','{{ $item->kode }}')">
                                        <i class="bx bx-trash me-1"></i> Hapus</
                                    >
                                </div>
                            </div>
                        </td>
                        @endcan
                        <td width="10%">{{ $item->kode }}</td>
                        <td width="50%">{{ $item->substansi }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </x-layouts.card-app>
    
    @can('delete-kodenaskah')
    <x-modals.modal modalId="modal-hapus" modalSize="modal-dialog-centered">
        <x-modals.header judul="Hapus Kode Naskah"></x-modals.header>
        
        <x-forms.form action="#" class="form-hapus" method="POST">
            @method('DELETE')
            <x-modals.body title="Hapus Kode Naskah" class="text-center">
                <input type="hidden" name="id" id="hapus-id">

                Apakah yakin ingin menghapus Kode Naskah <b class="text-uppercase" id="data-nama"></b>?
            </x-modals.body>

            <x-modals.footer closeText="TUTUP" saveButton="true" saveText="YA, Hapus data" classButton="danger" typeButton="submit"></x-modals.footer>
        </x-forms.form>
    </x-modals.modal>
    @endcan
@endsection

@push('script-vendor')
    @include('assets/datatable/script')
@endpush

@push('script-page')
<script>
    $(document).ready(function() {
        @include('assets/datatable/config', ['identitytable' => '#table-data'])
    });
    function fhapus(id, nama)
    {
        url = "{{ route('kode-naskah.index') }}"+"/"+id;
        $("#data-nama").html(nama);
        $("#hapus-id").val(id);
        $(".form-hapus").attr('action', url);
        $("#modal-hapus").modal('show');
    }
</script>
@if (Session::has('status'))
@include('assets/toast/config')
@endif

@endpush