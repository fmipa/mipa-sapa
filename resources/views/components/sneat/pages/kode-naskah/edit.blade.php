@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('kode-naskah.index')"
        pageName="Kode Naskah"
        subPageName="Ubah Kode Naskah"
    >
        <x-forms.form :action="route('kode-naskah.update', $dataKodeNaskah->id_secret)" method="POST" id="form" :urlback="route('kode-naskah.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataKodeNaskah->id_secret }}" required>
            
            <x-forms.textarea label="Substansi" name="substansi" :value="old('substansi') ?? $dataKodeNaskah->substansi" rows="5" required autofocus></x-forms.textarea>
            
            <x-forms.text label="Kode" class="text-uppercase" name="kode" :value="old('kode') ?? $dataKodeNaskah->kode" threshold="10" required></x-forms.text>
            
            <x-forms.textarea label="Deskripsi" name="deskripsi" :value="old('deskripsi') ?? $dataKodeNaskah->deskripsi" rows="5"></x-forms.textarea>
                    
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
        
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/kode-naskah/validate')
@include('assets/toast/config')

@endpush
