<script>
    $("#form-validate").validate(
        {
        rules: {
            substansi: {
                required: true,
            },
            kode: {
                required: true,
                maxlength: 10,
            },
            deskripsi: {
                required: true,
            },
        },
        messages: {
            substansi: {
                required: "Substansi tidak boleh kosong",
            },
            kode: {
                required: "Kode Unit tidak boleh kosong",
                maxlength: jQuery.validator.format("Kode Unit tidak boleh lebih dari {0} karakter!")
            },
            deskripsi: {
                required: "Deskripsi tidak boleh kosong",
            },

        }
    }
    );
</script>
