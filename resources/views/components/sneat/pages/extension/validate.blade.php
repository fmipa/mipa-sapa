<script>
    $("#form-validate").validate(
        {
        rules: {
            extension: {
                required: true,
                maxlength: 20,
            },
            deskripsi: {
                maxlength: 100,
            },
        },
        messages: {
            extension: {
                required: "Extension File tidak boleh kosong",
                maxlength: jQuery.validator.format("Extension File tidak boleh lebih dari {0} karakter!")
            },
            deskripsi: {
                maxlength: jQuery.validator.format("Deskripsi tidak boleh lebih dari {0} karakter!")
            },

        }
    }
    );
</script>
