@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('ekstensi.index')"
        pageName="Extension File Upload"
        subPageName="Ubah Extension"
    >
        <x-forms.form :action="route('ekstensi.update', $dataExtension->id_secret)" method="POST" id="form" :urlback="route('ekstensi.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataExtension->id_secret }}" required>
                    
            <x-forms.text label="Ekstensi" name="extension" :value="old('extension') ?? $dataExtension->extension" threshold="20" required autofocus></x-forms.text>
            
            <x-forms.text label="Deskripsi" name="deskripsi" :value="old('deskripsi') ?? $dataExtension->deskripsi" threshold="100"></x-forms.text>
                    
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
        
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/extension/validate')
@include('assets/toast/config')

@endpush
