@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/dark.css">
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('surat-keluar.index')"
        pageName="Surat Keluar"
        subPageName="Ubah Surat Keluar"
    >
        <x-forms.form :action="route('surat-keluar.update', $dataSurat->id_secret)" method="POST" id="form" :urlback="route('surat-keluar.index')" enctype="multipart/form-data">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataSurat->id_secret }}" required>
            
            <x-forms.text label="Surat Keluar" name="nomor_surat" class="text-danger font-weight-bold" :value="old('nomor_surat') ?? $dataSurat->nomor_surat" readonly></x-forms.text>
            
            <div class="row">
                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <x-forms.text label="Nomor Surat" name="nomor" :value="old('nomor') ?? $dataSurat->nomor" threshold="50" required></x-forms.text>
                </div>

                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <x-forms.number label="Tahun Surat" name="tahun" :value="old('tahun') ?? $dataSurat->tahun ?? date('Y')" min="2000" :max="date('Y')" required></x-forms.number>
                </div>
            </div>
            
            <x-forms.textarea label="Perihal" name="perihal" :value="old('perihal') ?? $dataSurat->perihal" required></x-forms.textarea>
            
            <x-forms.textarea label="Tujuan Surat" name="tujuan" :value="old('tujuan') ?? $dataSurat->tujuan" required></x-forms.textarea>
            
            <x-forms.date label="Tanggal Diterbitkan/Dibuat" name="tanggal_diterbitkan" :value="old('tanggal_diterbitkan') ?? $dataSurat->tanggal_diterbitkan" required></x-forms.date>
            
            <x-forms.selectbox label="Kode Naskah" name="kode_naskah_id" :datas="$kodeNaskahs" :selected="old('kode_naskah_id') ?? $dataSurat->kode_naskah_id_encrypt" required></x-forms.selectbox>

            <x-forms.selectbox label="Folder" name="folder_id" :datas="$folders" :selected="old('folder_id') ?? $selectFolder" smallText="keterangan : Folder -> Subfolder" ></x-forms.selectbox>

            <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id') ?? $selectBox" hasMultiple="true"></x-forms.selectbox>

            @if(auth()->user()->hasRole('super-admin'))
                <x-forms.selectbox label="Akun Pembuat" name="created_by" :datas="$creators" :selected="old('created_by') ?? $selectCreator" required></x-forms.selectbox>
            @endif

            <x-forms.jenisAkses 
                :checked="old('jenis_akses_id') ?? $dataSurat->file->jenis_akses_id" 
                :pegawais="$pegawais" :unitKerjas="$unitKerjas" 
                :jenisAksesId="old('jenis_akses_id') ?? $dataSurat->file->jenis_akses_id" 
                :selectJenisAkses="$selectJenisAkses"
                >
            </x-forms.jenisAkses>   

            @if (auth()->user()->operator == 1 || (auth()->user()->hasRole('super-admin') || auth()->user()->hasRole('kepegawaian')))
            <x-forms.file label="File" name="attachment" :accept="$accepts" smallText="File tidak lebih dari 10MB dan harus berbentuk PDF"></x-forms.file>
            @else
            <x-forms.file label="File" name="attachment" :accept="$accepts" smallText="File tidak lebih dari 1MB dan harus berbentuk PDF"></x-forms.file>
            @endif

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
                    
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/surat-keluar/validate')
@include('assets/toast/config')

<script>
        
    $( document ).ready(function() {

        function stringSplit(text, separator = ' ') { 
            const result = $.trim(text).split(separator);
            return result;
        }
        var nomorSK = $("input[name='nomor_surat']");
        $("input[name='tahun'], input[name='nomor'], select[name='kode_unit_id'], select[name='kode_naskah_id']").on('click change keyup keydown', function() {
            var nomor = $("input[name='nomor']").val();
            var kodeUnit = 'UN22.8';
            var kategori = $.trim(stringSplit($("select[name='kode_naskah_id']").find(":selected").text(), '-')[0]);
            var tahun = $("input[name='tahun']").val();
            nomorSK.val($.trim(nomor+'/'+kodeUnit+'/'+kategori+'/'+tahun));
        })
        $("input[name='tanggal_diterbitkan']").flatpickr({
            dateFormat: "Y-m-d",
        });
    });
</script>
@endpush
