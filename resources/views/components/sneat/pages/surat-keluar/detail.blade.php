<div class="row">
    <div class="col">
        <div class="card mb-3">
            <div class="card-header text-white bg-warning text-uppercase">Informasi Surat Keluar</div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-bordered mt-1">
                        <tr>
                            <td>Nomor Surat</td>
                            <td>{{ $dataSurat->nomorSurat }}</td>
                        </tr>
                        <tr>
                            <td>Perihal</td>
                            <td>{{ $dataSurat->perihal }}</td>
                        </tr>
                        <tr>
                            <td>Tahun</td>
                            <td>{{ $dataSurat->tahun }}</td>
                        </tr>
                        <tr>
                            <td>Tanggal Diterbitkan</td>
                            <td>{{ $dataSurat->tanggal_diterbitkan }}</td>
                        </tr>
                        <tr>
                            <td>Tujuan</td>
                            <td>{{ $dataSurat->tujuan }}</td>
                        </tr>
                        <tr>
                            <td>Kode Naskah</td>
                            <td>{{ $dataSurat->kodeNaskah ? $dataSurat->kodeNaskah->substansi : '-' }}</td>
                        </tr>
                        <tr>
                            <td>Operator</td>
                            <td>{{ $dataSurat->creator->name }}</td>
                        </tr>
                        <tr>
                            <td>Download</td>
                            <td>
                                <x-ahref :link="route('unduh-file', ['id'=> $dataSurat->idSecret, 'tipeFile' => 'surat-keluar'])" target="_blank" class="btn btn-info" text="Download"></x-ahref>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card mb-3">
            <div class="card-header text-white bg-warning text-uppercase">Informasi Box/Kotak</div>
            <div class="card-body p-0">
                @if (!empty($dataSurat->file->boxes))
                    @php
                        $nama = $nomor = $bagian = $lokasi = ''; 
                        foreach ($dataSurat->file->boxes as $key => $item) {
                            $nama .= ($key > 0 ? ',' : '' ) . $item->namaTahun;
                            $nomor .= ($key > 0 ? ',' : '' ) . $item->nomor;
                            $bagian .= ($key > 0 ? ',' : '' ) . $item->bagian;
                            $lokasi .= ($key > 0 ? ',' : '' ) . $item->lokasi;
                        }
                    @endphp
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered mt-1">
                        <tr>
                            <td>Nama Box/Kotak</td>
                            <td>
                                {{ $nama ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor</td>
                            <td>
                                {{ $nomor ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Kepemilikan</td>
                            <td>
                                {{ $bagian ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Lokasi</td>
                            <td>{{ $lokasi ?? '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
