<script>
    $("#form-validate").validate(
        {
        rules: {
            nomor: {
                required: true,
            },
            perihal: {
                required: true,
            },
            tahun: {
                required: true,
            },
            tujuan: {
                required: true,
            },
            tanggal_diterbitkan: {
                required: true,
            },
            kode_naskah_id: {
                required: true,
            },
            jenis_akses_id: {
                required: true,
            },
            attachment: {
                required: true,
            },
        },
        messages: {
            nomor: {
                required: "Nomor tidak boleh kosong",
            },
            perihal: {
                required: "Perihal tidak boleh kosong",
            },
            tahun: {
                required: "Tahun tidak boleh kosong",
            },
            tujuan: {
                required: "Tujuan tidak boleh kosong",
            },
            tanggal_diterbitkan: {
                required: "Tanggal diterbitkan tidak boleh kosong",
            },
            kode_naskah_id: {
                required: "Kode Naskah tidak boleh kosong",
            },
            jenis_akses_id: {
                required: "Jenis Akses File tidak boleh kosong",
            },
            attachment: {
                required: "File tidak boleh kosong",
            },

        }
    }
    );
</script>
