@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/dark.css">
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('surat-keluar.index')"
        pageName="Surat Keluar"
        subPageName="Tambah Surat Keluar"
    >
        <x-forms.form :action="route('surat-keluar.store')" method="POST" id="form" enctype="multipart/form-data">
            
            <x-forms.text label="Surat Keluar" name="nomor_surat" class="text-danger font-weight-bold" :value="old('nomor_surat') ?? 'XXXX/UN22.8/XX/'.date('Y')" readonly></x-forms.text>
            
            <div class="row">
                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <x-forms.number label="Nomor Surat" name="nomor" :value="old('nomor')" max="100000" required></x-forms.number>
                </div>
                
                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <x-forms.number label="Tahun Surat" name="tahun" :value="old('tahun') ?? date('Y')" min="2000" :max="date('Y')" required></x-forms.number>
                </div>
            </div>
            
            <x-forms.selectbox label="Kode Naskah" name="kode_naskah_id" :datas="$kodeNaskahs" :selected="old('kode_naskah_id')" required></x-forms.selectbox>

            <x-forms.textarea label="Perihal" name="perihal" :value="old('perihal')" required></x-forms.textarea>
            
            <x-forms.textarea label="Tujuan Surat" name="tujuan" :value="old('tujuan')" required></x-forms.texta>
            
            <x-forms.date label="Tanggal Diterbitkan/Dibuat" name="tanggal_diterbitkan" :value="old('tanggal_diterbitkan') ?? date('Y-m-d')" required></x-forms.date>    
            
            <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id') ?? $selectFolder" smallText="keterangan : Folder -> Subfolder"></x-forms.selectbox>
            
            <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id')" hasMultiple="true" smallText="dapat dikosongkan & dapat dipilih lebih dari 1 Box"></x-forms.selectbox>

            @if(auth()->user()->hasRole('super-admin'))
                <x-forms.selectbox label="Akun Pembuat" name="created_by" :datas="$creators" :selected="old('created_by')" required></x-forms.selectbox>
            @endif

            <x-forms.jenisAkses :checked="old('jenis_akses_id')" :pegawais="$pegawais" :unitKerjas="$unitKerjas" :selectJenisAkses="$selected" create="true"></x-forms.jenisAkses>

            @if (auth()->user()->operator == 1 || (auth()->user()->hasRole('super-admin') || auth()->user()->hasRole('kepegawaian')))
            <x-forms.file label="File" name="attachment" required  :accept="$accepts" smallText="File tidak lebih dari 10MB dan harus berbentuk PDF"></x-forms.file>
            @else
            <x-forms.file label="File" name="attachment" required  :accept="$accepts" smallText="File tidak lebih dari 1MB dan harus berbentuk PDF"></x-forms.file>
            @endif

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/surat-keluar/validate')
@include('assets/toast/config')

<script>
    $( document ).ready(function() {

        function stringSplit(text, separator = ' ') { 
            const result = $.trim(text).split(separator);
            return result;
        }
        var nomorSK = $("input[name='nomor_surat']");
        $("input[name='tahun'], input[name='nomor'], select[name='kode_unit_id'], select[name='kode_naskah_id']").on('click change keyup keydown', function() {
            var nomor = $("input[name='nomor']").val();
            var kodeUnit = 'UN22.8';
            var kategori = $.trim(stringSplit($("select[name='kode_naskah_id']").find(":selected").text(), '-')[0]);
            var tahun = $("input[name='tahun']").val();
            nomorSK.val($.trim(nomor+'/'+kodeUnit+'/'+kategori+'/'+tahun));
        })

        $("input[name='tanggal_diterbitkan']").flatpickr();
    });
</script>
@endpush
