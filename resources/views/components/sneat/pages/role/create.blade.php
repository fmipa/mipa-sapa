@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('hak-akses.index')"
        pageName="Hak Akses"
        subPageName="Tambah Hak Akses"
    >
        <x-forms.form :action="route('hak-akses.store')" method="POST" id="form">

            <x-forms.text label="Nama Akses" name="name" :value="old('name')" threshold="40" required autofocus></x-forms.text>

            <x-forms.text label="Guard Name" name="guard_name" :value="old('guard_name') ?? config('auth.defaults.guard')" threshold="20" required></x-forms.text>

            <x-forms.selectbox label="Izin (Permission)" name="permission_id[]" :datas="$permissions" :selected="old('permission_id')" hasMultiple="true" ></x-forms.selectbox>

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>

        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/role/validate')
@include('assets/toast/config')
@endpush
