<script>
    $("#form-validate").validate(
        {
        rules: {
            name: {
                required: true,
                maxlength: 40,
            },
            guard_name: {
                required: true,
                maxlength: 20,
            },
        },
        messages: {
            name: {
                required: "Nama tidak boleh kosong",
                maxlength: jQuery.validator.format("Nama tidak boleh lebih dari {0} karakter!")
            },
            guard_name: {
                required: "Guard Nama tidak boleh kosong",
                maxlength: jQuery.validator.format("Guard Nama tidak boleh lebih dari {0} karakter!")
            },

        }
    }
    );
</script>
