@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('hak-akses.index')"
        pageName="Hak Akses"
        subPageName="Ubah Hak Akses"
    >
        <x-forms.form :action="route('hak-akses.update', $dataRole->id_secret)" method="POST" id="form" :urlback="route('hak-akses.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataRole->id_secret }}" required>
                    
            <x-forms.text label="Nama Hak Akses" name="name" :value="old('name') ?? $dataRole->name" threshold="40" required autofocus></x-forms.text>
            
            <x-forms.text label="Guard Name" name="guard_name" :value="old('guard_name') ?? $dataRole->guard_name" threshold="20" required></x-forms.text>

            <x-forms.selectbox label="Izin (Permission)" name="permission_id[]" :datas="$permissions" :selected="old('permission_id') ?? $dataRole->permissions" hasMultiple="true" ></x-forms.selectbox>

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
        
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/role/validate')
@include('assets/toast/config')

@endpush
