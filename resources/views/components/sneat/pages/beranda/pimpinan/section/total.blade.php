<div class="col-12 mb-4">
    <div class="card">
        <div class="card-body">
        <div class="d-flex justify-content-between flex-sm-row flex-column gap-3">
            <div class="d-flex flex-sm-column flex-row align-items-start justify-content-between">
            <div class="card-title">
                <h5 class="text-nowrap mb-2">Total Berkas</h5>
                <span class="badge bg-label-warning rounded-pill">Tahun {{ date('Y') }}</span>
            </div>
            <div class="mt-sm-auto">
                {{-- <small class="text-success text-nowrap fw-medium"
                ><i class="bx bx-chevron-up"></i> 68.2%</small
                > --}}
                <h3 class="mb-0">{{ $datas['totalFile'] }}</h3>
            </div>
            </div>
            <div id="profileReportChart"></div>
        </div>
        </div>
    </div>
</div>

@push('script-page')
    <script>
        const profileReportChartEl = document.querySelector("#profileReportChart"),
        profileReportChartConfig = {
            chart: {
                height: 80,
                // width: 175,
                type: "line",
                toolbar: {
                    show: false,
                },
                dropShadow: {
                    enabled: true,
                    top: 10,
                    left: 5,
                    blur: 3,
                    color: config.colors.warning,
                    opacity: 0.15,
                },
                sparkline: {
                    enabled: true,
                },
            },
            grid: {
                show: false,
                padding: {
                    right: 8,
                },
            },
            colors: [config.colors.warning],
            dataLabels: {
                enabled: false,
            },
            stroke: {
                width: 5,
                curve: "smooth",
            },
            series: [
                {
                    data: [110, 270, 145, 245, 205, 285],
                },
            ],
            xaxis: {
                show: false,
                lines: {
                    show: false,
                },
                labels: {
                    show: false,
                },
                axisBorder: {
                    show: false,
                },
            },
            yaxis: {
                show: false,
            },
        };
        if (
            typeof profileReportChartEl !== undefined &&
            profileReportChartEl !== null
        ) {
            const profileReportChart = new ApexCharts(
                profileReportChartEl,
                profileReportChartConfig
            );
            profileReportChart.render();
        }
    </script>
@endpush