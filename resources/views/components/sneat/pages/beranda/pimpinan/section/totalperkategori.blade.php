<div class="row">
    <div class="col-6 mb-4">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex align-items-start justify-content-between">
                    <div class="avatar flex-shrink-0">
                        <button type="button" class="btn btn-success btn-sm">
                            <i class="rounded bx bx-food-menu"></i>
                        </button>
                    </div>
                    <div class="dropdown">
                        <button
                            class="btn p-0"
                            type="button"
                            id="cardOpt3"
                            data-bs-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt3">
                            <a class="dropdown-item" href="javascript:void(0);">View More</a>
                            {{-- <a class="dropdown-item" href="javascript:void(0);">Delete</a> --}}
                        </div>
                    </div>
                </div>
                <span class="fw-medium d-block mb-1">Surat Keputusan (SK)</span>
                <h3 class="card-title mb-2">{{ $datas['totalSK'] }}</h3>
                <small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i> +{{ $datas['totalSKToday'] }}</small>
            </div>
        </div>
    </div>
    <div class="col-6 mb-4">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex align-items-start justify-content-between">
                    <div class="avatar flex-shrink-0">
                        <button type="button" class="btn btn-info btn-sm">
                            <i class="rounded bx bx-archive"></i>
                        </button>
                    </div>
                    <div class="dropdown">
                        <button
                            class="btn p-0"
                            type="button"
                            id="cardOpt6"
                            data-bs-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt6">
                            <a class="dropdown-item" href="javascript:void(0);">View More</a>
                            {{-- <a class="dropdown-item" href="javascript:void(0);">Delete</a> --}}
                        </div>
                    </div>
                </div>
                <span>Berkas Pegawai</span>
                <h3 class="card-title text-nowrap mb-1">{{ $datas['totalBerkasPegawai'] }}</h3>
                <small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i> +{{ $datas['totalBerkasPegawaiToday'] }}</small>
            </div>
        </div>
    </div>
    <div class="col-6 mb-4">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex align-items-start justify-content-between">
                    <div class="avatar flex-shrink-0">
                        <button type="button" class="btn btn-danger btn-sm">
                            <i class="rounded bx bx-envelope"></i>
                        </button>
                    </div>
                    <div class="dropdown">
                        <button
                            class="btn p-0"
                            type="button"
                            id="cardOpt4"
                            data-bs-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt4">
                            <a class="dropdown-item" href="javascript:void(0);">View More</a>
                            {{-- <a class="dropdown-item" href="javascript:void(0);">Delete</a> --}}
                        </div>
                    </div>
                </div>
                <span class="d-block mb-1">Surat Masuk</span>
                <h3 class="card-title text-nowrap mb-2">{{ $datas['totalSuratMasuk'] }}</h3>
                <small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i> +{{ $datas['totalSuratMasukToday'] }}</small>
            </div>
        </div>
    </div>
    <div class="col-6 mb-4">
        <div class="card">
            <div class="card-body">
                <div class="card-title d-flex align-items-start justify-content-between">
                    <div class="avatar flex-shrink-0">
                        <button type="button" class="btn btn-primary btn-sm">
                            <i class="rounded bx bx-envelope-open"></i>
                        </button>
                    </div>
                    <div class="dropdown">
                        <button
                            class="btn p-0"
                            type="button"
                            id="cardOpt1"
                            data-bs-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                        </button>
                        <div class="dropdown-menu" aria-labelledby="cardOpt1">
                            <a class="dropdown-item" href="javascript:void(0);">View More</a>
                            {{-- <a class="dropdown-item" href="javascript:void(0);">Delete</a> --}}
                        </div>
                    </div>
                </div>
                <span class="fw-medium d-block mb-1">Surat Keluar</span>
                <h3 class="card-title mb-2">{{ $datas['totalSuratKeluar'] }}</h3>
                <small class="text-success fw-medium"><i class="bx bx-up-arrow-alt"></i> +{{ $datas['totalSuratKeluarToday'] }}</small>
            </div>
        </div>
    </div>
</div>