@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/apex-charts/apex-charts.css') }}" />    
@endpush

@push('style-page')
<style>
    .card:hover {
        transform: scale(1.1);
    }
</style>
@endpush

@section('content')

    <div class="{{ config('variables.templateContainer') }} flex-grow-1 container-p-y">
        <div class="row">
            <div class="col-lg-8 mb-4 order-0">
                @include('components.sneat.pages.beranda.pimpinan.section.welcome')
            </div>
            <div class="col-lg-4 col-md-4 order-1">
                @include('components.sneat.pages.beranda.pimpinan.section.total')
            </div>
            <!-- Grafik -->
            <div class="col-12 col-lg-8 order-2 order-md-3 order-lg-2 mb-4">
                @include('components.sneat.pages.beranda.pimpinan.section.grafik')
            </div>
            <!--/ Total Revenue -->
            <div class="col-12 col-md-8 col-lg-4 order-3 order-md-2">
                @include('components.sneat.pages.beranda.pimpinan.section.totalperkategori')
            </div>
        </div>
        
    </div>
@endsection

@push('script-vendor')
    <script src="{{ asset('/sneat/vendor/libs/apex-charts/apexcharts.js') }}"></script>
@endpush

@push('script-page')
    {{-- <script src="{{ asset('/sneat/js/dashboards-analytics.js') }}"></script> --}}
    
@endpush