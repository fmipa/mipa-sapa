@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')
@endpush

@push('style-page')
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/css/rtl/theme-default.css') }}" />
    <style>
        /* #icons-container .icon-card {
            width: 128px;
        } */

        #icons-container .icon-card i {
            font-size: 2rem;
        }

        
    </style>
@endpush

@section('content')

    <div class="{{ config('variables.templateContainer') }} flex-grow-1 container-p-y">
        <!-- Welcome Text & Total File -->
            <div class="row">

                <div class="col-12 col-md-6 card-separator m-auto">
                    <h3>Selamat Datang Kembali, {{ auth()->user()->username }} 👋🏻 </h3>
                    <p>Your progress this week is Awesome. let's keep it up and get a lot of points reward !</p>
                </div>
                    
                <div class="col-sm-6 col-lg-3 mb-4">
                    <div class="card card-border-shadow-primary h-100">
                        <div class="card-body">
                            <div class="d-flex align-items-center mb-2 pb-1">
                                <div class="avatar me-2">
                                    <span class="avatar-initial rounded bg-label-danger"><i class="bx bxs-truck"></i></span>
                                </div>
                                <h4 class="ms-1 mb-0">42</h4>
                            </div>
                            <p class="mb-1">Berkas yang di tag</p>
                            <p class="mb-0">
                                <span class="fw-medium me-1">+18.2%</span>
                                <small class="text-muted">seminggu terakhir</small>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-lg-3 mb-4">
                    <div class="card card-border-shadow-warning h-100">
                        <div class="card-body">
                            <div class="d-flex align-items-center mb-2 pb-1">
                                <div class="avatar me-2">
                                <span class="avatar-initial rounded bg-label-info"><i class='bx bx-error'></i></span>
                                </div>
                                <h4 class="ms-1 mb-0">8</h4>
                            </div>
                            <p class="mb-1">Berkas yg di upload</p>
                            <p class="mb-0">
                                <span class="fw-medium me-1">-8.7%</span>
                                <small class="text-muted">seminggu terakhir</small>
                            </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--/ Welcome Text & Total File -->

            <div class="row">
                <!-- Statistik File yang ditag-->
                <div class="col-lg-6 col-xxl-6 mb-4 order-3 order-xxl-1">
                    <div class="card h-100">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <div class="card-title mb-0">
                                <h5 class="m-0 me-2">Statistik Berkas yang di Tag</h5>
                                <small class="text-muted">Total Berkas Tahun Ini 23.8k</small>
                            </div>
                            <div class="dropdown">
                                <button type="button" class="btn btn-label-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">2024</button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="javascript:void(0);">2023</a></li>
                                    <li><a class="dropdown-item" href="javascript:void(0);">2024</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="shipmentStatisticsChart"></div>
                        </div>
                    </div>
                </div>
                <!--/ Statistik File yang ditag -->

                <!-- Statistik File yang diupload-->
                <div class="col-lg-6 col-xxl-6 mb-4 order-3 order-xxl-1">
                    <div class="card h-100">
                        <div class="card-header d-flex align-items-center justify-content-between">
                            <div class="card-title mb-0">
                                <h5 class="m-0 me-2">Statistik Berkas yang di Upload</h5>
                                <small class="text-muted">Total Berkas Tahun Ini 23.8k</small>
                            </div>
                            <div class="dropdown">
                                <button type="button" class="btn btn-label-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false">2024</button>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="javascript:void(0);">2023</a></li>
                                    <li><a class="dropdown-item" href="javascript:void(0);">2024</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div id="shipmentStatisticsChart"></div>
                        </div>
                    </div>
                </div>
                <!--/ Statistik File yang diupload -->
            </div>
            
            <div class="row">
                <div class="col-12 order-5">
                    <div class="card">
                      <div class="card-header d-flex align-items-center justify-content-between">
                        <div class="card-title mb-0">
                          <h5 class="m-0 me-2">On route vehicles</h5>
                        </div>
                        <div class="dropdown">
                          <button class="btn p-0" type="button" id="routeVehicles" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="bx bx-dots-vertical-rounded"></i>
                          </button>
                          <div class="dropdown-menu dropdown-menu-end" aria-labelledby="routeVehicles">
                            <a class="dropdown-item" href="javascript:void(0);">Select All</a>
                            <a class="dropdown-item" href="javascript:void(0);">Refresh</a>
                            <a class="dropdown-item" href="javascript:void(0);">Share</a>
                          </div>
                        </div>
                      </div>
                      <div class="card-datatable table-responsive">
                        <table class="dt-route-vehicles table">
                          <thead class="border-top">
                            <tr>
                              <th></th>
                              <th></th>
                              <th>location</th>
                              <th>starting route</th>
                              <th>ending route</th>
                              <th>warnings</th>
                              <th class="w-20">progress</th>
                            </tr>
                          </thead>
                        </table>
                      </div>
                    </div>
                  </div>
            </div>
    </div>
@endsection

@push('script-vendor')
    <script src="{{ asset('/sneat/vendor/libs/apex-charts/apexcharts.js') }}"></script>
@endpush

@push('script-page')
    {{-- <script src="{{ asset('/sneat/js/dashboards-analytics.js') }}"></script> --}}
    
@endpush