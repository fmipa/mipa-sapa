@extends('layouts.sneat.horizontal.authhz-app')

@push('style-core')
@endpush

@push('style-page')
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/css/pages/page-auth.css') }}" />
    <style>
        @media only screen and (max-width: 800px) {
            .app-brand-logo img {
                width: 280px;
            }
        }

        @media only screen and (max-width: 600px) {
            .app-brand-logo img {
                width: 250px;
            }
        }
    </style>
@endpush

@section('layoutContent')
<div class="container-xxl">
    <div class="authentication-wrapper authentication-basic container-p-y">
        <div class="authentication-inner">
            <!-- Register -->
            <div class="card">
                <div class="card-body">
                    <!-- Logo -->
                    <div class="app-brand justify-content-center mb-0">
                        <a href="{{ url('/') }}" class="app-brand-link gap-2">
                            <span class="app-brand-logo demo"><img src="{{ asset(config('variables.logoSAPA')) }}" alt="" width="400px"></span>
                        </a>
                    </div>
                    <!-- /Logo -->
                    {{-- <h4 class="mb-2">
                        Selamat Datang di {{config('variables.appName')}}! 👋
                    </h4> --}}
                    <p class="mb-4">👋 Silahkan Sign In untuk memulai aplikasi!</p>
                    
                    @error('message')
                    <x-messages.error :message="$message"></x-messages.error>
                    @enderror
                    
                    <form id="formAuthentication" class="mb-3" action="{{ route('authenticate') }}" method="POST">
                        @csrf
                        <div class="mb-3">
                            <label for="email" class="form-label">Username</label>
                            <input type="text" class="form-control @error('username') is-invalid @enderror" id="email" name="username" value="{{ old('username') }}" placeholder="Masukkan Username" required autofocus>

                            @error('username')
                            <x-messages.error :message="af"></x-messages.error>
                            @enderror
                        </div>
                        <div class="mb-3 form-password-toggle">
                            <div class="d-flex justify-content-between">
                                <label class="form-label" for="password">Password</label>
                            </div>
                            <div class="input-group input-group-merge">
                                <input type="password" id="password" class="form-control @error('password') is-invalid @enderror" name="password" placeholder="******" value="{{ old('password') }}" aria-describedby="password" />
                                <span class="input-group-text cursor-pointer"><i class="bx bx-hide" id="togglePassword"></i></span>

                                @error('password')
                                <x-messages.error :message="$message"></x-messages.error>
                                @enderror
                            </div>
                        </div>
                        <div class="mb-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="remember-me">
                                <label class="form-check-label" for="remember-me">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="mb-3">
                            <button class="btn btn-info d-grid w-100" type="submit">Sign in</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- /Register -->
    </div>
</div>
@endsection

@push('script-page')
@endpush