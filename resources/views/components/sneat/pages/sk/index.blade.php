@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    @include('assets/datatable/style')
@endpush

@push('style-page')
@endpush

@section('content')
    
    <x-layouts.card-app 
        :card="true"
        :create="true"
        :createUrl="route('sk.create')"
        pageName="SK"
        subPageName="Data SK"
    >
        <div class="table-responsive">
            <table class="table table-hover table-bordered" id="table-data">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nomor</th>
                        <th width="20%">Judul</th>
                        <th>Tahun</th>
                        <th>Naskah</th>
                        <th>Tingkatan</th>
                        <th>Folder</th>
                        <th>Lihat</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $item)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $item->nomor_sk }}</td>
                        <td>{{ $item->judul }}</td>
                        <td>{{ $item->tahun }}</td>
                        <td>{{ $item->kodeNaskah->substansi }}</td>
                        <td>{{ $item->tingkatan }}</td>
                        <td>{{ $item->file->folder ? $item->file->folder->nama .' ('.$item->file->folder->namaParentUtama.')' : '' }}</td>
                        <td>
                            <x-ahref :link="route('lihat-file', ['id'=> $item->idSecret, 'tipeFile' => 'sk'])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                        </td>
                        <td>
                            <div class="dropdown">
                                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                                    <i class="bx bx-dots-vertical-rounded"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item text-warning" href="{{ route('sk.edit', $item->idSecret) }}">
                                        <i class="bx bx-edit-alt me-1"></i> Ubah</a
                                    >
                                    <button class="dropdown-item text-danger" onclick="fhapus('{{ $item->idSecret }}','{{ $item->nomor_sk }}')">
                                        <i class="bx bx-trash me-1"></i> Hapus</
                                    >
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </x-layouts.card-app>
    
    <x-modals.modal modalId="modal-hapus" modalSize="modal-dialog-centered">
        <x-modals.header judul="Hapus SK"></x-modals.header>
        
        <x-forms.form action="#" class="form-hapus" method="POST">
            @method('DELETE')
            <x-modals.body title="Hapus SK" class="text-center">
                <input type="hidden" name="id" id="hapus-id">

                Apakah yakin ingin menghapus SK dengan nomor <b class="text-uppercase" id="data-nama"></b>?
            </x-modals.body>

            <x-modals.footer closeText="TUTUP" saveButton="true" saveText="YA, Hapus data" classButton="danger" typeButton="submit"></x-modals.footer>
        </x-forms.form>
    </x-modals.modal>
@endsection

@push('script-vendor')
    @include('assets/datatable/script')
@endpush

@push('script-page')
<script>
    $(document).ready(function() {
        @include('assets/datatable/config', ['identitytable' => '#table-data'])
    });
    function fhapus(id, nama)
    {
        url = "{{ route('sk.index') }}"+"/"+id;
        $("#data-nama").html(nama);
        $("#hapus-id").val(id);
        $(".form-hapus").attr('action', url);
        $("#modal-hapus").modal('show');
    }
</script>
@if (Session::has('status'))
@include('assets/toast/config')
@endif

@endpush