<div class="row">
    <div class="col">
        <div class="card mb-3">
            <div class="card-header text-white bg-warning text-uppercase">Informasi SK</div>
            <div class="card-body p-0">
                <div class="table-responsive">
                    <table class="table table-bordered mt-1">
                        <tr>
                            <td>Nomor SK</td>
                            <td>{{ $dataSK->nomorSK }}</td>
                        </tr>
                        <tr>
                            <td>Judul SK</td>
                            <td>{{ $dataSK->judul }}</td>
                        </tr>
                        <tr>
                            <td>Kode Naskah</td>
                            <td>{{ $dataSK->kodeNaskah->substansi }}</td>
                        </tr>
                        <tr>
                            <td>Tingkatan SK</td>
                            <td>{{ $dataSK->tingkatan }}</td>
                        </tr>
                        <tr>
                            <td>Operator</td>
                            <td>{{ $dataSK->creator->name }}</td>
                        </tr>
                        <tr>
                            <td>Download</td>
                            <td>
                                <x-ahref :link="route('unduh-file', ['id'=> $dataSK->idSecret, 'tipeFile' => 'sk'])" target="_blank" class="btn btn-info" text="Download"></x-ahref>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card mb-3">
            <div class="card-header text-white bg-warning text-uppercase">Informasi Box/Kotak</div>
            <div class="card-body p-0">
                @if (!empty($dataSK->file->boxes))
                    @php
                        $nama = $nomor = $bagian = $lokasi = ''; 
                        foreach ($dataSK->file->boxes as $key => $item) {
                            $nama .= ($key > 0 ? ',' : '' ) . $item->namaTahun;
                            $nomor .= ($key > 0 ? ',' : '' ) . $item->nomor;
                            $bagian .= ($key > 0 ? ',' : '' ) . $item->bagian;
                            $lokasi .= ($key > 0 ? ',' : '' ) . $item->lokasi;
                        }
                    @endphp
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered mt-1">
                        <tr>
                            <td>Nama Box/Kotak</td>
                            <td>
                                {{ $nama ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor</td>
                            <td>
                                {{ $nomor ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Kepemilikan</td>
                            <td>
                                {{ $bagian ?? '' }}
                            </td>
                        </tr>
                        <tr>
                            <td>Lokasi</td>
                            <td>{{ $lokasi ?? '' }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
