@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('sk.index')"
        pageName="SK"
        subPageName="Ubah SK"
    >
        <x-forms.form :action="route('sk.update', $dataSK->id_secret)" method="POST" id="form" :urlback="route('sk.index')" enctype="multipart/form-data">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataSK->id_secret }}" required>
            
            <x-forms.text label="SK" name="nomor_sk" class="text-danger font-weight-bold" :value="old('nomor_sk') ?? $dataSK->nomor_sk" readonly></x-forms.text>

            <x-forms.textarea label="Judul SK" class="text-uppercase" name="judul" :value="old('judul') ?? $dataSK->judul" required></x-forms.textarea>

            <div class="row">
                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <x-forms.number label="Nomor SK" name="nomor" :value="old('nomor') ?? $dataSK->nomor" max="100000" required></x-forms.number>
                </div>

                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <x-forms.number label="Tahun Surat" name="tahun" :value="old('tahun') ?? $dataSK->tahun" min="2000" :max="date('Y')" required></x-forms.number>
                </div>
            </div>
            
            <x-forms.selectbox label="Tingkatan SK" name="kode_unit_id" required :datas="$kodeUnits" :selected="old('kode_unit_id') ?? $dataSK->kode_unit_id_encrypt" required></x-forms.selectbox>

            <x-forms.selectbox label="Kode Naskah" name="kode_naskah_id" required :datas="$kodeNaskahs" :selected="old('kode_naskah_id') ?? $dataSK->kode_naskah_id_encrypt" required></x-forms.selectbox>
            
            <x-forms.selectbox label="Folder" name="folder_id" :datas="$folders" :selected="old('folder_id') ?? $selectFolder" smallText="keterangan : Folder -> Subfolder" ></x-forms.selectbox>

            <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id') ?? $selectBox" hasMultiple="true"></x-forms.selectbox>

            @if(auth()->user()->hasRole('super-admin'))
                <x-forms.selectbox label="Akun Pembuat" name="created_by" :datas="$creators" :selected="old('created_by') ?? $selectCreator" required></x-forms.selectbox>
            @endif

            <x-forms.jenisAkses 
                :checked="old('jenis_akses_id') ?? $dataSK->file->jenis_akses_id" 
                :pegawais="$pegawais" :unitKerjas="$unitKerjas" 
                :jenisAksesId="old('jenis_akses_id') ?? $dataSK->file->jenis_akses_id" 
                :selectJenisAkses="$selectJenisAkses"
                >
            </x-forms.jenisAkses>   

            @if (auth()->user()->operator == 1 || (auth()->user()->hasRole('super-admin') || auth()->user()->hasRole('kepegawaian')))
            <x-forms.file label="File" name="attachment" :accept="$accepts" smallText="File tidak lebih dari 1MB dan harus berbentuk PDF"></x-forms.file>
            @else
            <x-forms.file label="File" name="attachment" :accept="$accepts" smallText="File tidak lebih dari 1MB dan harus berbentuk PDF"></x-forms.file>
            @endif

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/sk/validate')
@include('assets/toast/config')

<script>
    $( document ).ready(function() {
        function stringSplit(text, separator = ' ') { 
            const result = $.trim(text).split(separator);
            return result;
        }
        var nomorSK = $("input[name='nomor_sk']");
        $("input[name='tahun'], input[name='nomor'], select[name='kode_unit_id'], select[name='kode_naskah_id']").on('click change keyup keydown', function() {
            var nomor = $("input[name='nomor']").val();
            var kodeUnit = $.trim(stringSplit($("select[name='kode_unit_id']").find(":selected").text(), '-')[0]);
            var kategori = $.trim(stringSplit($("select[name='kode_naskah_id']").find(":selected").text(), '-')[0]);
            var tahun = $("input[name='tahun']").val();
            nomorSK.val($.trim(nomor+'/'+kodeUnit+'/'+kategori+'/'+tahun));
        })
        
    });
</script>
@endpush
