<script>
    $("#form-validate").validate(
        {
        rules: {
            nomor_sk: {
                required: true,
            },
            judul: {
                required: true,
            },
            nomor: {
                required: true,
            },
            kode_unit_id: {
                required: true,
            },
            kode_naskah_id: {
                required: true,
            },
            tahun: {
                required: true,
            },
            jenis_akses_id: {
                required: true,
            },
            attachment: {
                required: true,
            },
        },
        messages: {
            nomor_sk: {
                required: "Nomor tidak boleh kosong",
            },
            judul: {
                required: "Judul tidak boleh kosong",
            },
            nomor: {
                required: "Nomor tidak boleh kosong",
            },
            kode_unit_id: {
                required: "Kode Unit tidak boleh kosong",
            },
            kode_naskah_id: {
                required: "Kode Naskah tidak boleh kosong",
            },
            tahun: {
                required: "Tahun tidak boleh kosong",
            },
            jenis_akses_id: {
                required: "Jenis Akses File tidak boleh kosong",
            },
            attachment: {
                required: "File tidak boleh kosong",
            },

        }
    }
    );
</script>
