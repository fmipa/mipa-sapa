<script>
    $("#form-validate").validate(
        {
        rules: {
            kode: {
                required: true,
                maxlength: 50,
            },
            deskripsi: {
                required: true,
            },
        },
        messages: {
            kode: {
                required: "Kode Unit tidak boleh kosong",
                maxlength: jQuery.validator.format("Kode Unit tidak boleh lebih dari {0} karakter!")
            },
            deskripsi: {
                required: "Deskripsi tidak boleh kosong",
            },

        }
    }
    );
</script>
