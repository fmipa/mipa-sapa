@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('kode-unit.index')"
        pageName="Kode Unit"
        subPageName="Ubah Kode Unit"
    >
        <x-forms.form :action="route('kode-unit.update', $dataKodeUnit->id_secret)" method="POST" id="form" :urlback="route('kode-unit.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataKodeUnit->id_secret }}" required>
            
            <x-forms.text label="Kode Unit" class="text-uppercase" name="kode" :value="old('kode') ?? $dataKodeUnit->kode" threshold="50" required autofocus></x-forms.text>
                    
            <x-forms.textarea label="Deskripsi" name="deskripsi" :value="old('deskripsi') ?? $dataKodeUnit->deskripsi" rows="5"></x-forms.textarea>
                    
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
        
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/kode-unit/validate')
@include('assets/toast/config')

@endpush
