@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('jurusan.index')"
        pageName="Jurusan"
        subPageName="Ubah Jurusan"
    >
        <x-forms.form :action="route('jurusan.update', $dataJurusan->id_secret)" method="POST" :urlback="route('jurusan.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataJurusan->id_secret }}" required>
            
            <x-forms.text label="Nama" name="nama" :value="old('nama') ?? $dataJurusan->nama" threshold="100" required autofocus></x-forms.text>
            
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/jurusan/validate')
@include('assets/toast/config')

@endpush
