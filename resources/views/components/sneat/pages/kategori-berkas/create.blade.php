@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('kategori-berkas.index')"
        pageName="Kategori Berkas"
        subPageName="Tambah Kategori Berkas"
    >
        <x-forms.form :action="route('kategori-berkas.store')" method="POST" id="form">
            <x-forms.text label="Kategori Berkas" class="text-uppercase" name="kategori" :value="old('kategori')" threshold="50" required autofocus></x-forms.text>

            <x-forms.textarea label="Deskripsi" name="deskripsi" :value="old('deskripsi')" rows="5"></x-forms.textarea>
                    
            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/kategori-berkas/validate')
@include('assets/toast/config')
@endpush
