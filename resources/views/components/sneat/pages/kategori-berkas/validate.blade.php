<script>
    $("#form-validate").validate(
        {
        rules: {
            kategori: {
                required: true,
                maxlength: 50,
            },
            deskripsi: {
                required: true,
            },
        },
        messages: {
            kategori: {
                required: "Kategori Berkas tidak boleh kosong",
                maxlength: jQuery.validator.format("Kategori Berkas tidak boleh lebih dari {0} karakter!")
            },
            deskripsi: {
                required: "Deskripsi tidak boleh kosong",
            },

        }
    }
    );
</script>
