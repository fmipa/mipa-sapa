<script>
    $("#form-validate").validate(
        {
        rules: {
            nama: {
                required: true,
                maxlength: 100,
            },
            strata: {
                required: true,
                maxlength: 10,
            },
            jurusan_id: {
                required: true,
            },
        },
        messages: {
            nama: {
                required: "Nama tidak boleh kosong",
                maxlength: jQuery.validator.format("Nama tidak boleh lebih dari {0} karakter!")
            },
            strata: {
                required: "strata tidak boleh kosong",
                maxlength: jQuery.validator.format("Strata tidak boleh lebih dari {0} karakter!")
            },
            jurusan_id: {
                required: "Jurusan tidak boleh kosong",
            },

        }
    }
    );
</script>
