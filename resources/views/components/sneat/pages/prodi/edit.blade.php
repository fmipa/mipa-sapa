@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('prodi.index')"
        pageName="Prodi"
        subPageName="Ubah Prodi"
    >
        <x-forms.form :action="route('prodi.update', $dataProdi->id_secret)" method="POST" id="form" :urlback="route('prodi.index')">

            @method('PUT')
            <input type="hidden" name="id" value="{{ $dataProdi->id_secret }}" required>
            
            <x-forms.text label="Nama" name="nama" :value="old('nama') ?? $dataProdi->nama" threshold="100" required autofocus></x-forms.text>
            
            <x-forms.text label="Strata" name="strata" class="text-uppercase" :value="old('strata') ?? $dataProdi->strata" threshold="5" required></x-forms.text>

            <x-forms.selectbox label="Jurusan" name="jurusan_id" required :datas="$jurusans" :selected="old('jurusan_id') ?? $dataProdi->jurusan_secret"></x-forms.selectbox>

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>
        
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/prodi/validate')
@include('assets/toast/config')

@endpush
