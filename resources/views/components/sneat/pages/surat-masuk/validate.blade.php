<script>
    $("#form-validate").validate(
        {
        rules: {
            nomor: {
                required: true,
            },
            perihal: {
                required: true,
            },
            tahun: {
                required: true,
            },
            instansi: {
                required: true,
            },
            tanggal_diterbitkan: {
                required: true,
            },
            tanggal_diterima: {
                required: true,
            },
            jenis_akses_id: {
                required: true,
            },
            attachment: {
                required: true,
            },
        },
        messages: {
            nomor: {
                required: "Nomor tidak boleh kosong",
            },
            perihal: {
                required: "Perihal tidak boleh kosong",
            },
            tahun: {
                required: "Tahun tidak boleh kosong",
            },
            instansi: {
                required: "Instansi tidak boleh kosong",
            },
            tanggal_diterbitkan: {
                required: "Tanggal diterbitkan tidak boleh kosong",
            },
            tanggal_diterima: {
                required: "Tanggal diterima tidak boleh kosong",
            },
            jenis_akses_id: {
                required: "Jenis Akses File tidak boleh kosong",
            },
            attachment: {
                required: "File tidak boleh kosong",
            },

        }
    }
    );
</script>
