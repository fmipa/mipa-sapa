@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" type="text/css" href="https://npmcdn.com/flatpickr/dist/themes/dark.css">
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('surat-masuk.index')"
        pageName="Surat Masuk"
        subPageName="Tambah Surat Masuk"
    >
        <x-forms.form :action="route('surat-masuk.store')" method="POST" id="form" enctype="multipart/form-data">
            
            <x-forms.text label="Nomor Surat" name="nomor" :value="old('nomor')" threshold="50" required></x-forms.text>
            
            <x-forms.textarea label="Perihal" name="perihal" :value="old('perihal')" required></x-forms.textarea>
            
            <x-forms.number label="Tahun Surat" name="tahun" :value="old('tahun') ?? date('Y')" min="2000" :max="date('Y')" required></x-forms.number>
            
            <x-forms.text label="Instansi Pengirim" name="instansi" :value="old('instansi')" threshold="40" required></x-forms.text>
            
            <div class="row">
                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <x-forms.date label="Tanggal Diterbitkan/Dibuat" name="tanggal_diterbitkan" :value="old('tanggal_diterbitkan') ?? date('Y-m-d')" required></x-forms.date>
                </div>

                <div class="col col-lg-6 col-md-6 col-sm-12">
                    <x-forms.date label="Tanggal Diterima" name="tanggal_diterima" :value="old('tanggal_diterima') ?? date('Y-m-d')" required></x-forms.date>
                </div>
            </div>            

            <x-forms.selectbox label="Folder" name="folder_id"  :datas="$folders" :selected="old('folder_id')" smallText="keterangan : Folder -> Subfolder"></x-forms.selectbox>
            
            <x-forms.selectbox label="Box/Gobi" name="box_id[]" :datas="$boxes" :selected="old('box_id')" hasMultiple="true" smallText="dapat dikosongkan & dapat dipilih lebih dari 1 Box"></x-forms.selectbox>

            @if(auth()->user()->hasRole('super-admin'))
                <x-forms.selectbox label="Akun Pembuat" name="created_by" :datas="$creators" :selected="old('created_by')" required></x-forms.selectbox>
            @endif

            <x-forms.jenisAkses :checked="old('jenis_akses_id')" :pegawais="$pegawais" :unitKerjas="$unitKerjas" :selectJenisAkses="$selected" create="true"></x-forms.jenisAkses>

            <x-forms.file label="File" name="attachment" required  :accept="$accepts" smallText="File tidak lebih dari 1MB dan harus berbentuk PDF"></x-forms.file>

            <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
            
        </x-forms.form>

    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
@endpush

@push('script-page')
    @include('components/'.config('variables.templateName').'/pages/surat-masuk/validate')
    @include('assets/toast/config')

    <script>
        $( document ).ready(function() {
            $("input[name='tanggal_diterbitkan'], input[name='tanggal_diterima']").flatpickr();
        });
    </script>
@endpush
