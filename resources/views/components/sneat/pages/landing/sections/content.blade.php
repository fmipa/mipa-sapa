<div data-bs-spy="scroll" class="scrollspy-example">
    <!-- Hero: Start -->
    <section id="hero-animation">
        <div id="landingHero" class="section-py landing-hero position-relative">
            <div class="container">
            <div class="hero-text-box text-center" style="max-width: 57.375rem;">
                <h1 class="text-primary hero-title display-4 fw-bold">
                    <img src="{{ asset(config('variables.logoSAPA')) }}" alt="" width="50%">
                </h1>
                <h2 class="hero-sub-title h6 mb-4 pb-1">
                    Hello, Selamat Datang Di SAPA (SATU DATA FMIPA). 
                    <br class="d-none d-lg-block" />
                    Layanan Kearsipan Terintegrasi di Lingkungan FMIPA Universitas Tanjungpura yang Mempunyai <b>Fungsi, Manajemen, Pengelolaan, Penyimpanan dan Pencarian Arsip</b>.
                </h2>
                <div class="landing-hero-btn d-inline-block position-relative">
                <span class="hero-btn-item position-absolute d-none d-md-flex text-heading">Masuk kedalam 
                    <img src="{{ asset('/sneat/img/front-pages/icons/Join-community-arrow.png') }}" alt="Join community arrow" class="scaleX-n1-rtl" /></span>
                <a href="{{ route('beranda') }}" class="btn btn-sm btn-primary">SAPA (SATU DATA FMIPA)</a>
                </div>
            </div>
            {{-- <div id="heroDashboardAnimation" class="hero-animation-img">
                <a href="https:/demos.themeselection.com/sneat-bootstrap-html-admin-template/html/vertical-menu-template/app-ecommerce-dashboard.html" target="_blank">
                <div id="heroAnimationImg" class="position-relative hero-dashboard-img">
                    <img src="{{ asset('/sneat/img/front-pages/landing-page/hero-dashboard-light.png') }}" alt="hero dashboard" class="animation-img" data-app-light-img="front-pages/landing-page/hero-dashboard-light.png" data-app-dark-img="front-pages/landing-page/hero-dashboard-dark.html" />
                    <img src="{{ asset('/sneat/img/front-pages/landing-page/hero-elements-light.png') }}" alt="hero elements" class="position-absolute hero-elements-img animation-img top-0 start-0" data-app-light-img="front-pages/landing-page/hero-elements-light.png" data-app-dark-img="front-pages/landing-page/hero-elements-dark.html" />
                </div>
                </a>
            </div> --}}
            </div>
        </div>
        {{-- <div class="landing-hero-blank"></div> --}}
    </section>
    <!-- Hero: End -->

    <!-- Useful features: Start -->
    <section id="landingFeatures" class="section-py landing-features">
        <div class="container">
            <div class="text-center mb-3 pb-1">
            <span class="badge bg-label-primary">Keunggulan</span>
            </div>
            <h3 class="text-center mb-1">
            <span class="section-title">Kelebihan - Kelebihan</span> pada Aplikasi SAPA
            </h3>
            <p class="text-center mb-3 mb-md-5 pb-3">
            Aplikasi yang di dasari dari kalimat "Quality is Our Concern".
            </p>
            <div class="features-icon-wrapper row gx-0 gy-4 g-sm-5">
            <div class="col-lg-4 col-sm-6 text-center features-icon-box">
                <div class="text-center mb-3">
                <img src="{{ asset('/sneat/img/front-pages/icons/laptop.png') }}" alt="laptop charging" />
                </div>
                <h5 class="mb-3">Efisien</h5>
                <p class="features-icon-description">
                    Mempermudah dan Mempercepat Melakukan Pencarian Arsip.
                </p>
            </div>
            <div class="col-lg-4 col-sm-6 text-center features-icon-box">
                <div class="text-center mb-3">
                <img src="{{ asset('/sneat/img/front-pages/icons/rocket.png') }}" alt="transition up" />
                </div>
                <h5 class="mb-3">Efektif</h5>
                <p class="features-icon-description">
                    Menghemat Waktu dalam pengelolaan Dokumen Arsip
                </p>
            </div>
            <div class="col-lg-4 col-sm-6 text-center features-icon-box">
                <div class="text-center mb-3">
                <img src="{{ asset('/sneat/img/front-pages/icons/check.png') }}" alt="edit" />
                </div>
                <h5 class="mb-3">AMAN</h5>
                <p class="features-icon-description">
                    Meminimalisir Kerusakan dan Kehilangan Dokumen
                </p>
            </div>
            <div class="col-lg-4 col-sm-6 text-center features-icon-box">
                <div class="text-center mb-3">
                <img src="{{ asset('/sneat/img/front-pages/icons/paper.png') }}" alt="3d select solid" />
                </div>
                <h5 class="mb-3">MUDAH</h5>
                <p class="features-icon-description">
                    Transfer Dokumen Menjadi Lebih Mudah.
                </p>
            </div>
            <div class="col-lg-4 col-sm-6 text-center features-icon-box">
                <div class="text-center mb-3">
                <img src="{{ asset('/sneat/img/front-pages/icons/user.png') }}" alt="lifebelt" />
                </div>
                <h5 class="mb-3">HEMAT</h5>
                <p class="features-icon-description">Menghemat Tempat Penyimpanan</p>
            </div>
            <div class="col-lg-4 col-sm-6 text-center features-icon-box">
                <div class="text-center mb-3">
                <img src="{{ asset('/sneat/img/front-pages/icons/keyboard.png') }}" alt="google docs" />
                </div>
                <h5 class="mb-3">TERPUSAT</h5>
                <p class="features-icon-description">Dokumen Arsip menjadi Terpusat.</p>
            </div>
            </div>
        </div>
    </section>
    <!-- Useful features: End -->

</div>