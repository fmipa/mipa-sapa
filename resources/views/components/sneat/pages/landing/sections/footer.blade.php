<footer class="landing-footer bg-body footer-text">
    <div class="footer-top py-3">
        <div class="container d-flex flex-wrap justify-content-between flex-md-row flex-column text-center text-md-start">
        <div class="mb-2 mb-md-0">
            <span class="footer-text">© 2023
            </span>
            <a href="{{ config('variables.creatorUrl') }}" target="_blank" class="fw-medium text-white footer-link">{{ config('variables.creatorInstitute') }},</a>
            <span class="footer-text">Made with ❤️ {{ config('variables.creatorName') }}</span>
        </div>
        <div class="text-warning text-center text-uppercase">
            {{-- <a href="https://github.com/themeselection" class="footer-link me-3" target="_blank">
            <img src="{{ asset('/sneat/assets/img/front-pages/icons/github-light.png') }}" alt="github icon" data-app-light-img="front-pages/icons/github-light.png" data-app-dark-img="front-pages/icons/github-dark.png" />
            </a>
            <a href="https://www.facebook.com/ThemeSelections/" class="footer-link me-3" target="_blank">
            <img src="{{ asset('/sneat/assets/img/front-pages/icons/facebook-light.png') }}" alt="facebook icon" data-app-light-img="front-pages/icons/facebook-light.png" data-app-dark-img="front-pages/icons/facebook-dark.png" />
            </a>
            <a href="https://twitter.com/Theme_Selection" class="footer-link me-3" target="_blank">
            <img src="{{ asset('/sneat/assets/img/front-pages/icons/twitter-light.png') }}" alt="twitter icon" data-app-light-img="front-pages/icons/twitter-light.png" data-app-dark-img="front-pages/icons/twitter-dark.png" />
            </a>
            <a href="https://www.instagram.com/themeselection/" class="footer-link" target="_blank">
            <img src="{{ asset('/sneat/assets/img/front-pages/icons/instagram-light.png') }}" alt="google icon" data-app-light-img="front-pages/icons/instagram-light.png" data-app-dark-img="front-pages/icons/instagram-dark.png" />
            </a> --}}
            <b>
                {{ config('variables.quotes') }}
            </b>
        </div>
        </div>
    </div>
</footer>