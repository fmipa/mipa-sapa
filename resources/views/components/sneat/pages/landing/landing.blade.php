@extends('layouts.sneat.horizontal.masterhz-app')

@push('style-core')
<link rel="stylesheet" href="{{ asset('/sneat/vendor/css/pages/front-page.css') }}" />
@endpush

@push('style-page')
    <link rel="stylesheet" href="{{ asset('/sneat/css/running-text.css') }}" />
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/css/pages/front-page-landing.css') }}" />    
@endpush

@section('layoutContent')
    <!-- Sections:Start -->
    @include('components.sneat.pages.landing.sections.content')
    <!-- / Sections:End -->
@endsection

@push('script-page')
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            const element = document.getElementById('template-customizer');
            element?.remove();
        });
    </script>
@endpush