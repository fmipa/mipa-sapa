@extends('layouts.sneat.horizontal.masterhz-app')

@push('style-vendor')   
    @include('assets/datatable/style')
@endpush

@push('style-core')
@endpush

@push('style-page')
    <link rel="stylesheet" href="{{ asset('/sneat/css/running-text.css') }}" />   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/css/pages/front-page-help-center.css') }}" />   
    <style>
        /* html,body { height:100%; } */
        .first-section-pt {
            padding-top: 8.84rem;
        }

        .antar-sesi {
            padding: 2rem 0 !important;
        }

        .card-folder {
            margin-bottom: 20px !important;
        }

        .card-folder .card {
            height: 167px;
        }

        .landing-footer {
            position: absolute;
            width: 100%;
        }
        #kembali {
            margin-bottom: 10px;
        }

        .nama-folder, .total-data {
            font-size: 100%;
            word-wrap: normal;
        }

        /* Tablet:768px. */
        @media (min-width: 768px) and (max-width: 1200px) {
        .nama-folder, .total-data {
            font-size: 85%;
        }
        }

        /* Mobile :320px. */
        @media (max-width: 767px) {
        .nama-folder, .total-data {
            font-size: 100%;
        }
        }
    </style>

@endpush

@section('layoutContent')

    <!-- Sections:Start -->
    <!-- Header: Start -->
    <section class="section-py first-section-pt help-center-header">
        <h3 class="text-center"> Silahkan Masukkan Kata Kunci Pencarian Data. </h3>
        <div class="input-wrapper my-3 input-group input-group-lg input-group-merge position-relative mx-auto">
            <span class="input-group-text" id="basic-addon1"><i class="bx bx-search bx-sm"></i></span>
            <input type="text" class="form-control" placeholder="Masukkan kata kunci pencarian dan tekan ENTER" aria-label="Search" aria-describedby="basic-addon1" id="kata-kunci"/>
        </div>
        <p class="text-center px-3 mb-0">Kata kunci: Nomor SK, Judul SK, Nomor Surat, Perihal Surat, Nama Dokumen</p>
    </section>
    <!-- Header: End -->

    <!-- Body: Start -->
    <section class="section-py antar-sesi folders"></section>
    <section class="section-py bg-body antar-sesi tables"></section>
    <input type="hidden" id="section-status">
    <input type="hidden" id="section-id">
    <input type="hidden" id="section-folder">
    <input type="hidden" id="section-jenis">
    <input type="hidden" id="section-pegawai">

    <!-- Body: End -->
    <!-- / Sections:End -->

    <!-- Footer: Start -->
    @include('components.sneat.pages.landing.sections.footer')
    <!-- Footer: End -->
@endsection

@push('script-core')
    <script src="{{ asset('/sneat/vendor/libs/perfect-scrollbar/perfect-scrollbar.js') }}"></script>
@endpush

@push('script-vendor')
    @include('assets/datatable/script')
@endpush

@push('script-page')
    {{-- <script src="{{ asset('/sneat/js/front-page-landing.js') }}"></script> --}}
    <script>
        document.addEventListener("DOMContentLoaded", () => {
            const element = document.getElementById('template-customizer');
            element?.remove();
        });
    </script>

    <script>
        $(document).ready(function () {
            @include('assets/datatable/config', ['identitytable' => '#tabel-datas'])
            unitfolder();
            // dataTables();
        });
        
        function unitfolder() {
            var url = '{{ route("file.unitfolder") }}';
            load = $(".folders").load(url);
            $("#section-status").val('folder');
        }

        function dataTables(idUnitKerja = false, idFolder = false, idPegawai = false) {
            cari = $("#kata-kunci").val();
            kataKunci   = cari == '' ? 'false' : cari;
            idUnitKerja = idUnitKerja ?? $("#section-id").val();
            idFolder    = idFolder ?? null;
            if (idPegawai) {
                idPegawai   = idPegawai ?? $("#section-pegawai").val();
                var url = '{{ route("file.cariByPegawai") }}'+'/'+kataKunci+'/'+idFolder+'/'+idPegawai;
            } else {
                var url = '{{ route("file.cari") }}'+'/'+kataKunci+'/'+idUnitKerja+'/'+idFolder;
            }
            
            $(".tables").load(url);
        }

        $("#kata-kunci").on("keyup", function(event) {
            event.preventDefault();
            if (event.keyCode === 13) {
                // var kataKunci = this.value;
                // if (kataKunci == false || kataKunci == "") {
                //     unitfolder();
                // } else {
                //     var url = '{{ route("file.cari", "kataKunci") }}';
                //     url = url.replace('kataKunci', kataKunci);
                //     $("#section-folder").load(url);
                // }
                dataTables();
            }
        });

        function isEmpty(value) {
            return (value == '0' || value == 0 || value == null || (typeof value === "string" && value.trim().length === 0));
        }

        function kembali(idSecret) {
            var idUnitKerja = $("#section-id").val();
            var status = $("#section-status").val();
            if (status == 'unit') {
                var url = '{{ route("file.unitfolder") }}';
                $("#section-folder").val(0);
                $("#section-id").val(0);
                $(".folders").load(url, function( response, status, xhr ) {
                    if (status == "success" ) {
                    }
                });
                // dataTables(0, 0);
                $(".tables .container").remove();

            } else {
                var url = '{{ route("file.kembali", [
                    'idSecret'      => ':idSecret', 
                    'idUnitKerja'   => ':idUnitKerja', 
                ]) }}';
                url = url.replace(':idSecret', idSecret).replace(':idUnitKerja', idUnitKerja);
                $(".folders").load(url, function( response, status, xhr ) {
                    if (status == "success" ) {
                    }
                });
                $("#section-id").val(idUnitKerja);
                $("#section-status").val(isEmpty(idSecret) ? 'unit' : 'folder');
                dataTables(idUnitKerja, idSecret);
            }
        }

        function folderUnitKerja(idUnitKerja) {
            var url = '{{ route("file.folder", [
                'idSecret'      => ':idSecret', 
                'idUnitKerja'   => ':idUnitKerja', 
            ]) }}';
            url = url.replace(':idSecret', 0).replace(':idUnitKerja', idUnitKerja);
            $(".folders").load(url);
            $("#section-id").val(idUnitKerja);
            $("#section-status").val('unit');
            dataTables(idUnitKerja);
        }
        
        function folder(idSecret) { 
            $("#section-folder").val(idSecret);
            var idUnitKerja = $("#section-id").val();
            var url = '{{ route("file.folder", [
                'idSecret'      => ':idSecret',
                'idUnitKerja'   => ':idUnitKerja', 
            ]) }}';
            url = url.replace(':idSecret', idSecret).replace(':idUnitKerja', idUnitKerja);
            $(".folders").load(url);
            $("#section-status").val('folder');
            dataTables(idUnitKerja, idSecret);
        }

        function folderPegawai(parentFolderPegawai, idPegawai) {
            var idUnitKerja = $("#section-id").val();
            var url = '{{ route("file.folderPegawai", [
                'parentFolderPegawai'   => ':parentFolderPegawai',
                'idPegawai'             => ':idPegawai',
                'idUnitKerja'           => ':idUnitKerja', 
            ]) }}';
            url = url.replace(':parentFolderPegawai', parentFolderPegawai).replace(':idPegawai', idPegawai).replace(':idUnitKerja', idUnitKerja);
            $(".folders").load(url);
            $("#section-pegawai").val(idPegawai);
            dataTables(idUnitKerja, false, idPegawai);
        }

        function pegawai(idSecret, idPegawai) {
            var idUnitKerja = $("#section-id").val();
            var url = '{{ route("file.dataPegawai", [
                'idSecret'  => ':idSecret',
                'idPegawai' => ':idPegawai',
            ]) }}';
            url = url.replace(':idSecret', idSecret).replace(':idPegawai', idPegawai);
            $(".folders").load(url);
            dataTables(idUnitKerja, idSecret, idPegawai);
            $("#section-pegawai").val(idPegawai);
        }
    </script>
@endpush