@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    @include('assets/datatable/style')
@endpush

@push('style-page')
@endpush

@section('content')
    
    <x-layouts.card-app 
        :card="true"
        :create="false"
        pageName="Semua File"
        subPageName="Data Semua File"
    >
        <div class="table-responsive">
            <table class="table table-hover table-bordered" id="table-data">
                <thead>
                    <tr>
                        <th width="5%">No.</th>
                        <th width="20%">Nama/Nomor</th>
                        <th width="20%">Perihal/Judul</th>
                        <th>Kategori</th>
                        <th>Pembuat</th>
                        <th>Lihat</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $item)
                    <tr>
                        <td>{{ $item['no'] }}</td>
                        <td>
                            <a href="#" class="text-info" onclick="detail('{{ $item['idSecret'] }}','{{ $item['tipeFile'] }}')">
                                {{ $item['nama'] }}
                            </a>
                        </td>
                        <td>{{ $item['perihal'] }}</td>
                        <td>{{ $item['kategori'] }}</td>
                        <td>{{ $item['creator'] }}</td>
                        <td>
                            <x-ahref :link="route('lihat-file', ['id'=> $item['idSecret'], 'tipeFile' => $item['tipeFile']])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </x-layouts.card-app>
    
    <div class="modal fade" id="modal-detail" tabindex="-1" aria-hidden="true">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="detail-title">Detail </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body" id="detail-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary w-100" data-bs-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    
@endsection

@push('script-vendor')
    @include('assets/datatable/script')
@endpush

@push('script-page')
<script>
    $(document).ready(function() {
        @include('assets/datatable/config', ['identitytable' => '#table-data'])
    });
    function detail(idSecret, tipeDokumen) 
    {
        $("#detail-title").html("Detail "+tipeDokumen.toUpperCase());
        var modalUrl = '{{ url("file/detail") }}'+'/'+idSecret+'/'+tipeDokumen;
        $("#detail-body").load(modalUrl);
        $("#modal-detail").modal('show');
    }
</script>

@endpush