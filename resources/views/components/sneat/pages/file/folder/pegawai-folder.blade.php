@if (count($folders)>0)
    <div class="container" id="folder">
        <button type="button" class="btn btn-primary" id="kembali" onclick="kembali('{{ $kembaliId }}')">Kembali</button>
        <h3 class="text-center text-uppercase mb-4">FOLDER {{ $namaFolder }}</h3>
        <div class="row">
            <div class="col-lg-10 mx-auto">
                <div class="row justify-content-center">
                    @foreach ($folders as $item)
                        
                    <div class="col-lg-4 col-md-4 col-sm-6 mb-md-0 card-folder">
                        <div class="card border shadow-none">
                            <div class="card-body text-center">
                        
                                <h5 class="my-2 text-uppercase nama-folder">{{ $item['name'] }}</h5>
                                <p class="total-data"> Total File : {{ $item['total'] }} </p>
                                <button type="button" class="btn btn-sm btn-success" onclick="folderPegawai('{{ $parentFolderPegawai }}','{{ $item['idPegawai'] }}')">Lihat</button>
                            </div>
                        </div>
                    </div>
                        
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@else
    <div class="container" id="folder">
        <button type="button" class="btn btn-primary" id="kembali" onclick="kembali('{{ $kembaliId }}')">Kembali</button>
        <h3 class="text-center text-mute mb-4">TIDAK ADA FOLDER</h3>
    </div>
@endif