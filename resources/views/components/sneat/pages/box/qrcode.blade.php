@extends('layouts.sneat.horizontal.masterhz-app')

@push('style-vendor')   
    @include('assets/datatable/style')
@endpush

@push('style-page')
@endpush

@section('layoutContent')
    <section class="section-py bg-body">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center mb-4">
                    <h2 class="mb-2"><b>BOX / Gobi : {{ $box->namaTahun }}</b></h2>
                    <p class="mb-1">Lokasi {{ $box->lokasi }}</p>
                </div>
            </div>
            <div class="row mx-4">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="table-qrcode" class="table table-bordered table-striped dt-responsive text-center">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th width="30%">Nomor/Nama</th>
                                    <th width="40%">Judul/Perihal</th>
                                    <th>Kategori</th>
                                    <th>File</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($datas as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->nomor }}</td>
                                    <td>{{ $item->judul }}</td>
                                    <td>{{ $item->kategori }}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm btn-primary waves-effect waves-light" onclick="aksi('{{ $item->idSecret }}', '{{ $item->tipeFile }}')">
                                            <i class="mdi mdi-folder-lock-open"></i> File
                                        </button>
            
                                        {{-- <x-ahref :link="route('lihat-file', ['id'=> $item->idSecret, 'tipeFile' => $item->tipeFile])" target="_blank" class="btn btn-sm btn-primary" text="Lihat"></x-ahref>
                                        <x-ahref :link="route('unduh-file', ['id'=> $item->idSecret, 'tipeFile' => $item->tipeFile])" target="_blank" class="btn btn-sm btn-secondary" text="Unduh"></x-ahref> --}}
                                    </td>
                                </tr>        
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="clearfix pt-3">
                        <h6 class="text-muted">
                            Notes:
                            
                            <small class="text-black">
                                Total Berkas di dalam box/gobi {{ $datas->count() }}
                            </small>
                        </h6>
                    </div>

                </div>
            </div>

        </div> <!-- container-fluid -->
    </section>

    <x-modals.modal modalId="modal-password" modalSize="modal-dialog-centered">
        <x-modals.header judul="Password Box/Gobi"></x-modals.header>
        
        <x-modals.body title="Password" class="text-center">
            <input type="hidden" id="id-secret" value="">
            <input type="hidden" id="tipe-file" value="">
            @if ($box->password != NULL)
            <x-forms.text label="Masukkan Password" id="password-box" value="" placeholder="Masukkan Password Box/Gobi" required></x-forms.text>
            @endif

            <div class="row">
                <div class="col">
                    <x-ahref target="_blank" id="lihat" class="btn btn-primary w-100" text="Lihat"></x-ahref>
                </div>
                <div class="col">
                    <x-ahref target="_blank" id="unduh" class="btn btn-success w-100" text="Unduh"></x-ahref>
                </div>
            </div>
        </x-modals.body>
        
        <x-modals.footer closeText="TUTUP" saveButton="false"></x-modals.footer>
    </x-modals.modal>

@endsection

@push('script-vendor')
    @include('assets/datatable/script')
@endpush

@push('script-page')
    <!-- Datatables init -->
    <script>
        $(document).ready(function() {
            @include('assets/datatable/config', ['identitytable' => '#table-qrcode'])
        });
    </script>

    <script>
        function aksi(idSecret, tipeFile)
        {
            $("#id-secret").val(idSecret);
            $("#tipe-file").val(tipeFile);
            $("#modal-password").modal('show');
            proses();
        }
        function proses()
        {
            uniqueId = "{{ $box->unique_id }}";
            idSecret = $("#id-secret").val();
            tipeFile = $("#tipe-file").val();
            cekPass = "{{ $box->password ?? 'fmipa' }}";
            password = 'fmipa';
            switch (cekPass) {
                case 'fmipa':
                    password = cekPass;
                    break;
                    
                default:
                    inputPassword = $("#password-box").val();
                    password = inputPassword.length === 0 ? 'fmipa' : inputPassword;
            }
            console.log(password);
            $("#lihat").attr('href', '{{ url('/lihat-file/') }}'+'/'+uniqueId+'/'+idSecret+'/'+tipeFile+'/'+password);
            $("#unduh").attr('href', '{{ url('/download-file/') }}'+'/'+uniqueId+'/'+idSecret+'/'+tipeFile+'/'+password);
        }

        $("#lihat, #unduh").click(function(){
            cekPass = "{{ $box->password }}";
            inputPassword = $("#password-box").val();
            if (cekPass && inputPassword.length === 0) {
                alert('Box/Gobi memiliki password, Silahkan masukkan password.');
                return false;
            }
            
        });

        $("#password-box").on('click change keyup kedydown', function(e) {
            proses();
        })
    </script>
@endpush
