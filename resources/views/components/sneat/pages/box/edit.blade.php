@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    <link rel="stylesheet" href="{{ asset('/sneat/vendor/libs/select2/select2.css') }}" />
@endpush

@push('style-page')
@endpush

@section('content')
    <x-layouts.card-app 
        card="true"
        :back="true"
        :backUrl="route('box.index')"
        pageName="Box"
        subPageName="Ubah Box"
    >
                <x-forms.form :action="route('box.update', $dataBox->id_secret)" method="POST">
                    @method('PUT')
                    <input type="hidden" name="id" value="{{ $dataBox->id_secret }}" required>

                    <x-forms.textarea label="Nama Box/Gobi" name="nama" class="text-uppercase" :value="old('nama') ?? $dataBox->nama" required autofocus></x-forms.textarea>
                    
                    <div class="row">
                        <div class="col col-md-6 col-sm-12">
                            <x-forms.number label="Nomor Box/Gobi" name="nomor" :value="old('nomor') ?? $dataBox->nomor" required></x-forms.number>
                        </div>
                        <div class="col col-md-6 col-sm-12">
                            <x-forms.selectbox label="Tahun" name="tahuns[]" :datas="$tahuns" :selected="old('tahuns') ?? $selectTahun" hasMultiple="true" smallText="Dapat dikosongkan dan dapat dipilih lebih dari 1."></x-forms.selectbox>
                        </div>
                    </div>
    
                    @if (auth()->user()->hasRole('super-admin'))
                    <x-forms.selectbox label="Bagian dari Unit Kerja" name="bagian" :datas="$unitKerja" :selected="old('bagian') ?? strtolower($selectUnit)" smallText="Jika dikosongkan akan terdaftar sebagai fakultas"></x-forms.selectbox>
                    @endif
                    
                    <x-forms.text label="Lokasi Penyimpanan Fisik" name="lokasi" :value="old('lokasi') ?? $dataBox->lokasi" threshold="100" placeholder="Lokasi Penyimpanan contoh: Gedung Baru Ruang Arsiparis Lemari Berkas" required></x-forms.text>
                    
                    <x-forms.switchbox label="Password Box/Gobi" name="switchbox" :value="old('switchbox') ?? ($dataBox->password ? 1 : 0)" class="switch-password"></x-forms.switchbox>
                    
                    <x-forms.text label="Password" name="password" :value="old('password') ?? $dataBox->password" classDiv="input-password"></x-forms.text>
                                    
                    <x-button type="submit" class="btn-success w-100" id="" label="Simpan"/>
                    
                </x-forms.form>
    </x-layouts.card-app>

@endsection

@push('script-vendor')
    @include('assets/validate/script')
    <script src="{{ asset('/sneat/vendor/libs/select2/select2.js') }}"></script>
@endpush

@push('script-page')
@include('components/'.config('variables.templateName').'/pages/box/validate')
@include('assets/toast/config')

<script>
    (function($) {
        $.fn.clickToggle = function(func1, func2) {
            var funcs = [func1, func2];
            this.data('toggleclicked', 0);
            this.click(function() {
                var data = $(this).data();
                var tc = data.toggleclicked;
                $.proxy(funcs[tc], this)();
                data.toggleclicked = (tc + 1) % 2;
            });
            return this;
        };
    }(jQuery));
    $(document).ready(function() {
        @if ($dataBox->password == NULL || $dataBox->password == '' || !isset($dataBox->password))
        $(".input-password").hide();
        $('.switch-password').clickToggle(function() {   
            $(".input-password").slideDown("show");
        },
        function() {
            $(".input-password").slideUp("hide");
        });
        @else
        $('.switch-password').clickToggle(function() {   
            $(".input-password").slideUp("hide");
        },
        function() {
            $(".input-password").slideDown("show");
        });
        @endif
    })
</script>
@endpush
