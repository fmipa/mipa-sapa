<script>
    $("#form-validate").validate({
        rules: {
            nama: {
                required: true,
                maxlength: 100,
            },
            nomor: {
                required: true,
            },
            lokasi: {
                required: true,
                maxlength: 100,
            },
        },
        messages: {
            nama: {
                required: "Nama tidak boleh kosong",
                maxlength: jQuery.validator.format("Nama tidak boleh lebih dari {0} karakter!")
            },
            nomor: {
                required: "Nomor tidak boleh kosong",
            },
            lokasi: {
                required: "Lokasi tidak boleh kosong",
                maxlength: jQuery.validator.format("Lokasi tidak boleh lebih dari {0} karakter!")
            },
        }
    });
</script>
