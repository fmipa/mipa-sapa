@extends('layouts.sneat.vertical.menuvt-app')

@push('style-vendor')   
    @include('assets/datatable/style')
@endpush

@push('style-page')
@endpush

@section('content')
    
    <x-layouts.card-app 
        :card="true"
        :create="true"
        :createUrl="route('box.create')"
        pageName="Box"
        subPageName="Data Box"
    >
        <div class="table-responsive">
            <table class="table table-hover table-bordered" id="table-data">
                <thead>
                    <tr>
                        <th>Nomor Box/Gobi</th>
                        <th width="5%">Label</th>
                        <th>Nama</th>
                        @if (auth()->id() == 1)
                        <th>Bagian</th>
                        @endif
                        <th>Lokasi</th>
                        <th>File</th>
                        <th>Dibuat</th>
                        <th>#</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($datas as $item)
                    <tr>
                        <td>{{ $item->nomor }}</td>
                        <td>
                            <button type="button" class="btn btn-sm btn-primary" onclick="label('{{ $item->unique_id }}')">
                                <i class="bx bx-printer"></i>
                            </button>
                        </td>

                        <td>
                            <x-ahref :link="route('cari-berkas.qrcode', $item->unique_id)" target="_blank" class="text-info" :text="$item->namaTahun"></x-ahref>
                        </td>
                        @if (auth()->id() == 1)
                        <td>{{ $item->bagian }}</td>
                        @endif
                        <td>{{ $item->lokasi }}</td>
                        <td>{{ $item->files()->count() }}</td>
                        <td>{{ $item->creator->name }}</td>
                        <td>
                            <div class="dropdown">
                                <button type="button" class="btn p-0 dropdown-toggle hide-arrow" data-bs-toggle="dropdown">
                                    <i class="bx bx-dots-vertical-rounded"></i>
                                </button>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item text-info" href="{{ route('cari-berkas.qrcode', $item->unique_id) }}" target="_blank">
                                        <i class="bx bx-show me-1"></i> Lihat Box</a
                                    >
                                    @if ($item->created_by == auth()->id())
                                    <a class="dropdown-item text-warning" href="{{ route('box.edit', $item->idSecret) }}">
                                        <i class="bx bx-edit-alt me-1"></i> Ubah</a
                                    >
                                    <button class="dropdown-item text-danger" onclick="fhapus('{{ $item->idSecret }}','{{ $item->namaTahun }}')">
                                        <i class="bx bx-trash me-1"></i> Hapus</
                                    >
                                    @else
                                    <button class="dropdown-item" disabled>
                                        <i class='bx bx-shield-x' ></i> Tidak ada akses</
                                    >
                                    @endif
                                </div>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </x-layouts.card-app>
    
    <x-modals.modal modalId="modal-label" modalSize="modal-dialog-centered">
        <x-modals.header judul="Cetak Label"></x-modals.header>
        
        <x-modals.body title="Tipe Label" class="text-center">
            <div class="row">
                <div class="col">
                    <x-ahref target="_blank" class="btn btn-primary w-100" id="label-kotak" text="Kotak"></x-ahref>
                </div>
                <div class="col">
                    <x-ahref target="_blank" class="btn btn-info w-100" id="label-box-file" text="Box File"></x-ahref>
                </div>
                <div class="col">
                    <x-ahref target="_blank" class="btn btn-success w-100 disabled" id="label-gobi" text="Gobi"></x-ahref>
                </div>
                <div class="col">
                    <x-ahref target="_blank" class="btn btn-warning w-100" id="label-map" text="Map"></x-ahref>
                </div>
            </div>
        </x-modals.body>

        <x-modals.footer closeText="TUTUP" saveButton="false"></x-modals.footer>
    </x-modals.modal>

    <x-modals.modal modalId="modal-hapus" modalSize="modal-dialog-centered">
        <x-modals.header judul="Hapus Box"></x-modals.header>
        
        <x-forms.form action="#" class="form-hapus" method="POST">
            @method('DELETE')
            <x-modals.body title="Hapus Box" class="text-center">
                <input type="hidden" name="id" id="hapus-id">

                Apakah yakin ingin menghapus Data Box <b class="text-uppercase"  id="data-nama"></b>?
            </x-modals.body>

            <x-modals.footer closeText="TUTUP" saveButton="true" saveText="YA, Hapus data" classButton="danger" typeButton="submit"></x-modals.footer>
        </x-forms.form>
    </x-modals.modal>
@endsection

@push('script-vendor')
    @include('assets/datatable/script')
@endpush

@push('script-page')
<script>
    $(document).ready(function() {
        @include('assets/datatable/config', ['identitytable' => '#table-data'])
    });
    function fhapus(id, nama)
    {
        url = "{{ route('box.index') }}"+"/"+id;
        $("#data-nama").html(nama);
        $("#hapus-id").val(id);
        $(".form-hapus").attr('action', url);
        $("#modal-hapus").modal('show');
    }
</script>
<script>
    function label(unique)
    {
        let route = "{{ route('box.index') }}";
        $("#label-kotak").attr('href', route+"/label/"+unique+"/kotak");
        $("#label-box-file").attr('href', route+"/label/"+unique+"/box-file");
        $("#label-gobi").attr('href', route+"/label/"+unique+"/gobi");
        $("#label-map").attr('href', route+"/label/"+unique+"/map");
        $("#modal-label").modal('show');
    }
</script>
@if (Session::has('status'))
@include('assets/toast/config')
@endif

@endpush