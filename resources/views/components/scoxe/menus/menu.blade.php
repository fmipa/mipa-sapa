<li {{ (request()->segment(1) == $active) ? 'mm-active' : '' }}>
    <a href="{{ $route }}" class="waves-effect {{ (request()->segment(1) == $active) ? 'active' : '' }}">
        <i class="{{ $icon ?? '' }}"></i>
        <span>{{ $title }}</span>
    </a>
</li>