@push('css')
    <link href="{{ asset('/scoxe/plugins/daterangepicker/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/scoxe/plugins/select2/select2.min.css') }}" rel="stylesheet" type="text/css" /> 
    <link href="{{ asset('/scoxe/plugins/switchery/switchery.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/scoxe/plugins/bootstrap-colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/scoxe/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endpush

<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <x-layouts.content-header :title="$title" :menu="$menu" :submenu="$submenu"></x-layouts.content-header>   
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center bg-dark">
                        <h4 class="text-white">{{ $formLabel ?? $title }}</h4>
                        <x-ahref :link="$urlback" class="btn btn-secondary" text="kembali"></x-ahref>
                    </div>
                    <div class="card-body">

                        {{ $slot }}
                        
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->

@push('js')
    <script>
        let selectMenu = $("a[href='{{ $urlback }}']");
        selectMenu.parent().addClass('mm-active');
        selectMenu.first().addClass('active');
    </script>

    <script src="{{ asset('/scoxe/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/moment/moment.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/select2/select2.min.js') }}"></script>
@endpush