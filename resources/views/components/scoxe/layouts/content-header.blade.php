<div class="row">
    <div class="col-12">
        <div class="page-title-box d-flex align-items-center justify-content-between">
            <h4 class="mb-0 font-size-18">{{ $title }}</h4>

            <div class="page-title-right">
                <ol class="breadcrumb m-0">
                    <li class="breadcrumb-item"><a href="javascript: void(0);">Halaman</a></li>
                    <li class="breadcrumb-item {{ $submenu ? '' : 'active' }}">{{ $menu }}</li>
                    @if ($submenu)
                    <li class="breadcrumb-item active">{{ $submenu }}</li>
                    @endif
                </ol>
            </div>

        </div>
    </div>
</div>  