@push('css')
    <link href="{{ asset('/scoxe/plugins/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/scoxe/plugins/datatables/responsive.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/scoxe/plugins/datatables/buttons.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/scoxe/plugins/datatables/select.bootstrap4.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .text-wrap{
            white-space:normal;
        }
    </style>
@endpush

<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <x-layouts.content-header :title="$title" :menu="$menu" :submenu="$submenu"></x-layouts.content-header>   
        <!-- end page title -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center bg-dark">
                        <h4 class="text-white">Data {{ $title }}</h4>
                        @if($buttonTambah)
                        <x-ahref :link="$route" class="btn btn-success" :text="$buttonTambah"></x-ahref>
                        @endif
                    </div>
                    <div class="card-body">

                        {{ $slot }}

                    </div>
                </div>
            </div>
        </div>

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->


@push('js')
    <script src="{{ asset('/scoxe/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/dataTables.responsive.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/responsive.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/buttons.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/buttons.print.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/dataTables.keyTable.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/dataTables.select.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/pdfmake.min.js') }}"></script>
    <script src="{{ asset('/scoxe/plugins/datatables/vfs_fonts.js') }}"></script>

    <!-- Datatables init -->
    {{-- <script src="{{ asset('vertical/pages/datatables-demo.js') }}"></script> --}}

    <script>
        $(document).ready(function() {
            var a = $("#datatable-buttons").DataTable({
                keys: !0,
                stateSave: !0,
                lengthChange: !1,
                buttons: ["copy", "print", "pdf"],
                responsive: true,
                language: {
                    paginate: {
                        previous: "<i class='mdi mdi-chevron-left'>",
                        next: "<i class='mdi mdi-chevron-right'>"
                    }
                },
                drawCallback: function() {
                    $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                }
            });
            a.buttons().container().appendTo("#datatable-buttons_wrapper .col-md-6:eq(0)"), $("#complex-header-datatable").DataTable({
                language: {
                    paginate: {
                        previous: "<i class='mdi mdi-chevron-left'>",
                        next: "<i class='mdi mdi-chevron-right'>"
                    }
                },
                drawCallback: function() {
                    $(".dataTables_paginate > .pagination").addClass("pagination-rounded")
                },
                columnDefs: [{
                    visible: !1,
                    targets: -1,
                    render: function (data, type, full, meta) {
                        return "<div class='text-wrap'>" + data + "</div>";
                    },
                }]
            })
        });
    </script>
@endpush