<div class="page-content">
    <div class="container-fluid">

        <!-- start page title -->
        <x-layouts.content-header :title="$title" :menu="$menu" :submenu="$submenu"></x-layouts.content-header>   
        <!-- end page title -->

        {{ $slot }}

    </div> <!-- container-fluid -->
</div>
<!-- End Page-content -->