<form 
    {{ $attributes->merge(['class' => 'form-horizontal '.$class]) }}
    {{ $id ? "id={$id}" : NULL }}
    action="{{ $action }}" 
    method="{{ $method }}" 
    {{ isset($enctype) ? "enctype='{$enctype}'" : NULL }}
    >
    @csrf
    
    {{ $slot }}
</form>