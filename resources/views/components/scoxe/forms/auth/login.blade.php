<div class="row">
    <div class="col-lg-5 d-none d-lg-block rounded-left w-100">
        <script src="https://unpkg.com/@dotlottie/player-component@latest/dist/dotlottie-player.mjs" type="module"></script> 

        <dotlottie-player src="https://lottie.host/1582a535-3c5f-4063-b249-9b532bd476d1/OrItLlQFDc.json" background="transparent" speed="1" style="width: 100%; padding-left: 30px;" loop autoplay></dotlottie-player>
    </div>
    <div class="col-lg-7">
        <div class="p-5">
            <div class="text-center mb-3">
                <a href="{{ url('/') }}" class="text-dark font-size-22 font-family-secondary">
                    {{-- <i class="mdi mdi-album"></i> <b class="text-uppercase">Arsip Digital SAPA</b> --}}
                    <img src="{{ asset('SAPA.png') }}" alt="Logo" class="logo" width="70%">

                </a>
            </div>
            <h1 class="h5 mb-1">Selamat Datang!</h1>
            <p class="text-muted mb-4">Masukkan Username & Passowrd mu untuk masuk ke dalam sistem!</p>
            
            @error('message')
                <div class="alert alert-danger">
                    <strong>{{ $message }}</strong>
                </div>
            @enderror

            <form action="{{ route('authenticate') }}" method="POST" >
                @csrf

                <div class="form-group">
                    <input type="text" class="form-control form-control-user @error('username') is-invalid @enderror" id="exampleInputEmail" placeholder="Username" name="username" value="{{ old('username') }}" required autofocus>

                    @error('username')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
                <div class="form-group">
                    <input type="password" class="form-control form-control-user @error('password') is-invalid @enderror" id="exampleInputPassword" placeholder="Password" name="password" value="{{ old('password') ?? '123456' }}" >

                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror

                </div>
                <button type="submit" class="btn btn-success btn-block waves-effect waves-light"> Log In </button>

                {{-- <div class="text-center mt-4">
                    <h5 class="text-muted font-size-16">Masuk Menggunakan</h5>
                
                    <ul class="list-inline mt-3 mb-0">
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i class="mdi mdi-facebook"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i class="mdi mdi-google"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-info text-info"><i class="mdi mdi-twitter"></i></a>
                        </li>
                        <li class="list-inline-item">
                            <a href="javascript: void(0);" class="social-list-item border-secondary text-secondary"><i class="mdi mdi-github-circle"></i></a>
                        </li>
                    </ul>
                </div> --}}
                
            </form>

            {{-- <div class="row mt-4">
                <div class="col-12 text-center">
                    @if (Route::has('password.request'))
                    <p class="text-muted mb-2"><a href="{{ route('password.request') }}" class="text-muted font-weight-medium ml-1">Lupa Password?</a></p>
                    @endif
                    <p class="text-muted mb-0">Tidak Mempunyai Akun? <a href="{{ route('register') }}" class="text-muted font-weight-medium ml-1"><b>Daftar</b></a></p>
                </div> <!-- end col -->
            </div> --}}
            <!-- end row -->
        </div> <!-- end .padding-5 -->
    </div> <!-- end col -->
</div> <!-- end row -->