<div class="form-group row mb-3">
    <label for="inputText" class="col-3 col-form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <div class="col-9">
        <textarea 
            class="form-control {{ $class }} @error($name) is-invalid @enderror" 
            id="exampleFormControlTextarea1" 
            rows="{{ $rows ?? 3 }}"
            name="{{ $name ?? NULL }}"
            placeholder="{{ $placeholder ?? 'Masukkan '.$label }}"
            @required($required) 
            @readonly($readonly)
            @disabled($disabled)
            {{ old($name) == NULL ? ($autofocus ? "autofocus" : NULL) : NULL }}
        >
        </textarea>
        
        @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>

@push('js')
<script>
    @if (old($name) ?? ($value ?? ''))
        console.log();
    @endif
    $("textarea[name='{{ $name }}']").val("{{ old($name) ?? ($value ?? '') }}");
</script>
@endpush
