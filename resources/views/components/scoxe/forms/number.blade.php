<div class="form-group row mb-3">
    <label for="inputText" class="col-md-3 col-sm-12 col-form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <div class="col-md-9 col-sm-12">
        <input
            type='number'
            class="form-control {{ $class }} @error($name) is-invalid @enderror"
            id="{{ $id ?? NULL }}"
            name="{{ $name ?? NULL }}"
            value="{{ $value ?? NULL }}"
            placeholder="{{ $placeholder ?? 'Masukkan '.$label }}"
            @if ($step) step="{{ $step }}" @endif
            @if ($max) max="{{ $max }}" @endif
            @if ($min) min="{{ $min }}" @endif
            @required($required) 
            @readonly($readonly)
            @disabled($disabled)
            {{ old($name) == NULL ? ($autofocus ? "autofocus" : NULL) : NULL }}
        >

        @error($name)
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
