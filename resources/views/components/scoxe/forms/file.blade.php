<div class="form-group row mb-3">
    <label for="inputText" class="col-md-3 col-sm-12 col-form-label">
        {{ $label }}
        @if($required)
        <span class="text-danger">*</span>
        @endif
    </label>
    <div class="col-md-9 col-sm-12">
        <input
            type='file'
            class="form-control {{ $class }} @error($name) is-invalid @enderror"
            id="{{ $id ?? NULL }}"
            name="{{ $name ?? NULL }}"
            accept="{{ $accept ?? '*' }}"
            @required($required) 
            @readonly($readonly)
            @disabled($disabled)
        >
        
        @error($name)
        <span class="form-text invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
        @enderror
    </div>
</div>
