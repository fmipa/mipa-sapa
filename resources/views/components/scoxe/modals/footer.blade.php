<div class="modal-footer {{ $class }}" {{ isset($id) ? "id={$id}" : NULL }}>
    <button type="button" class="btn btn-secondary {{ $saveButton != 'true' ? 'w-100' : NULL }}" data-dismiss="modal">{{ $closeText }}</button>
    
    @if ($saveButton == 'true')
    <button type="{{ $typeButton }}" class="btn btn-{{ $classButton }}" {{ isset($onclick) ? "onclick={$onclick}" : NULL }}>{{ $saveText }} {{ $saveButton }}</button>
    @endif
</div>