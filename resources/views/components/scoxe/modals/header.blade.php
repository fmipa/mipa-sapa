<div class="modal-header {{ $class }}" {{ isset($id) ? "id={$id}" : NULL }}>
    <h5 class="modal-title {{ $classJudul }}">{{ $judul }}</h5>
    {{ $slot }}
    <button type="button" class="close waves-effect waves-light" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">×</span>
    </button>
</div>