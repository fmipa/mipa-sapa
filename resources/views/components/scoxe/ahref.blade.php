<a href="{{ $link }}" 
    {{ $target ? "target={$target}" : NULL }}
    {{ $attributes->merge(['class' => $class]) }}
    {{ $id ? "id={$id}" : NULL }}
>
    {{ $text ?? '' }}

    {{ $slot }}
</a>