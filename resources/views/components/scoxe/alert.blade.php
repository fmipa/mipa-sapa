<div class="alert alert-{{ $typeAlert }}" role="alert">
    {{ $messages }}
</div>