<button 
    type="{{ $type }}" 
    class="btn waves-effect waves-light {{ $class }}"
    id="{{ $id ?? NULL }}"
    @if ($onClick)
    onClick="{{ $onClick }}"
    @endif
>
    {{ $slot }}
    {{ $label }}
</button>
